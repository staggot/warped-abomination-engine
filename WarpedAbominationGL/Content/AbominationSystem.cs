﻿using System;
using System.IO;
using System.Reflection;

namespace WarpedAbominationEngine.Content
{
    public static partial class AbominationSystem
    {

        static void InitPlatform()
        {
            EngineStorage = new FSResourceStorage(Assembly.GetExecutingAssembly().Location);
            UserStorage = new FSResourceStorage(Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
                ApplicationName));
        }
    }
}
