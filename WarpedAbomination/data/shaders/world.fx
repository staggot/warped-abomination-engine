MGFX ?uw
ps_uniforms_vec4@       0 vs_uniforms_vec4�   @ �  �  #ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

uniform vec4 ps_uniforms_vec4[4];
vec4 ps_r0;
vec4 ps_r1;
vec4 ps_r2;
#define ps_c0 ps_uniforms_vec4[0]
#define ps_c1 ps_uniforms_vec4[1]
#define ps_c2 ps_uniforms_vec4[2]
#define ps_c3 ps_uniforms_vec4[3]
uniform sampler2D ps_s0;
varying vec4 vTexCoord0;
#define ps_v0 vTexCoord0
#define ps_oC0 gl_FragColor
varying vec4 vTexCoord1;
#define ps_v1 vTexCoord1
varying vec4 vTexCoord2;
#define ps_v2 vTexCoord2

void main()
{
	ps_r0.xyz = ps_c0.xyz + -ps_v2.xyz;
	ps_r0.x = dot(ps_r0.xyz, ps_r0.xyz);
	ps_r0.x = inversesqrt(ps_r0.x);
	ps_r0.x = 1.0 / ps_r0.x;
	ps_r0.x = ps_r0.x + -ps_c2.x;
	ps_r1.x = ps_c2.x;
	ps_r0.y = -ps_r1.x + ps_c3.x;
	ps_r0.y = 1.0 / ps_r0.y;
	ps_r0.x = clamp(ps_r0.y * ps_r0.x, 0.0, 1.0);
	ps_r1 = texture2D(ps_s0, ps_v1.xy);
	ps_r2 = ps_r1 * ps_v0;
	ps_r1 = (ps_r1 * -ps_v0) + ps_c1;
	ps_oC0 = (ps_r0.xxxx * ps_r1) + ps_r2;
}

    ps_s0    #ifdef GL_ES
precision highp float;
precision mediump int;
#endif

uniform vec4 vs_uniforms_vec4[12];
uniform vec4 posFixup;
vec4 vs_r0;
vec4 vs_r1;
#define vs_c0 vs_uniforms_vec4[0]
#define vs_c1 vs_uniforms_vec4[1]
#define vs_c2 vs_uniforms_vec4[2]
#define vs_c3 vs_uniforms_vec4[3]
#define vs_c4 vs_uniforms_vec4[4]
#define vs_c5 vs_uniforms_vec4[5]
#define vs_c6 vs_uniforms_vec4[6]
#define vs_c7 vs_uniforms_vec4[7]
#define vs_c8 vs_uniforms_vec4[8]
#define vs_c9 vs_uniforms_vec4[9]
#define vs_c10 vs_uniforms_vec4[10]
#define vs_c11 vs_uniforms_vec4[11]
attribute vec4 vs_v0;
#define vs_o0 gl_Position
attribute vec4 vs_v1;
varying vec4 vTexCoord0;
#define vs_o1 vTexCoord0
attribute vec4 vs_v2;
varying vec4 vTexCoord1;
#define vs_o2 vTexCoord1
attribute vec4 vs_v3;
varying vec4 vTexCoord2;
#define vs_o3 vTexCoord2
varying vec4 vTexCoord3;
#define vs_o4 vTexCoord3

void main()
{
	vs_r0.w = dot(vs_v0, vs_c3);
	vs_r0.x = dot(vs_v0, vs_c0);
	vs_r0.y = dot(vs_v0, vs_c1);
	vs_r0.z = dot(vs_v0, vs_c2);
	vs_r1.x = dot(vs_r0, vs_c4);
	vs_r1.y = dot(vs_r0, vs_c5);
	vs_r1.z = dot(vs_r0, vs_c6);
	vs_r1.w = dot(vs_r0, vs_c7);
	vs_o3.xyz = vs_r0.xyz;
	vs_o0.x = dot(vs_r1, vs_c8);
	vs_o0.y = dot(vs_r1, vs_c9);
	vs_o0.z = dot(vs_r1, vs_c10);
	vs_o0.w = dot(vs_r1, vs_c11);
	vs_o1 = vs_v1;
	vs_o2.xy = vs_v2.xy;
	vs_o4.xyz = vs_v3.xyz;
	gl_Position.y = gl_Position.y * posFixup.y;
	gl_Position.xy += posFixup.zw * gl_Position.ww;
	gl_Position.z = gl_Position.z * 2.0 - gl_Position.w;
}

 vs_v0    vs_v1   vs_v2   vs_v3    �   #ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

const vec4 ps_c0 = vec4(0.0, 0.0, 0.0, 0.0);
#define ps_oC0 gl_FragColor

void main()
{
	ps_oC0 = ps_c0.xxxx;
}

   �  #ifdef GL_ES
precision highp float;
precision mediump int;
#endif

uniform vec4 vs_uniforms_vec4[12];
uniform vec4 posFixup;
vec4 vs_r0;
vec4 vs_r1;
#define vs_c0 vs_uniforms_vec4[0]
#define vs_c1 vs_uniforms_vec4[1]
#define vs_c2 vs_uniforms_vec4[2]
#define vs_c3 vs_uniforms_vec4[3]
#define vs_c4 vs_uniforms_vec4[4]
#define vs_c5 vs_uniforms_vec4[5]
#define vs_c6 vs_uniforms_vec4[6]
#define vs_c7 vs_uniforms_vec4[7]
#define vs_c8 vs_uniforms_vec4[8]
#define vs_c9 vs_uniforms_vec4[9]
#define vs_c10 vs_uniforms_vec4[10]
#define vs_c11 vs_uniforms_vec4[11]
attribute vec4 vs_v0;
#define vs_o0 gl_Position

void main()
{
	vs_r0.x = dot(vs_v0, vs_c0);
	vs_r0.y = dot(vs_v0, vs_c1);
	vs_r0.z = dot(vs_v0, vs_c2);
	vs_r0.w = dot(vs_v0, vs_c3);
	vs_r1.x = dot(vs_r0, vs_c4);
	vs_r1.y = dot(vs_r0, vs_c5);
	vs_r1.z = dot(vs_r0, vs_c6);
	vs_r1.w = dot(vs_r0, vs_c7);
	vs_o0.x = dot(vs_r1, vs_c8);
	vs_o0.y = dot(vs_r1, vs_c9);
	vs_o0.z = dot(vs_r1, vs_c10);
	vs_o0.w = dot(vs_r1, vs_c11);
	gl_Position.y = gl_Position.y * posFixup.y;
	gl_Position.xy += posFixup.zw * gl_Position.ww;
	gl_Position.z = gl_Position.z * 2.0 - gl_Position.w;
}

 vs_v0     �  #ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

uniform vec4 ps_uniforms_vec4[4];
const vec4 ps_c4 = vec4(1.0, -1.0, 0.0, 0.0);
vec4 ps_r0;
vec4 ps_r1;
vec4 ps_r2;
vec4 ps_r3;
#define ps_c0 ps_uniforms_vec4[0]
#define ps_c1 ps_uniforms_vec4[1]
#define ps_c2 ps_uniforms_vec4[2]
#define ps_c3 ps_uniforms_vec4[3]
uniform sampler2D ps_s9;
varying vec4 vTexCoord0;
#define ps_v0 vTexCoord0
#define ps_oC0 gl_FragColor
varying vec4 vTexCoord1;
#define ps_v1 vTexCoord1
varying vec4 vTexCoord2;
#define ps_v2 vTexCoord2
varying vec4 vTexCoord3;
#define ps_v3 vTexCoord3
varying vec4 vTexCoord4;
#define ps_v4 vTexCoord4
varying vec4 vTexCoord5;
#define ps_v5 vTexCoord5
varying vec4 vTexCoord8;
#define ps_v6 vTexCoord8

void main()
{
	ps_r0.xyz = ps_c0.xyz + -ps_v6.xyz;
	ps_r0.x = dot(ps_r0.xyz, ps_r0.xyz);
	ps_r0.x = inversesqrt(ps_r0.x);
	ps_r0.x = 1.0 / ps_r0.x;
	ps_r0.x = ps_r0.x + -ps_c2.x;
	ps_r1.x = ps_c2.x;
	ps_r0.y = -ps_r1.x + ps_c3.x;
	ps_r0.y = 1.0 / ps_r0.y;
	ps_r0.x = clamp(ps_r0.y * ps_r0.x, 0.0, 1.0);
	ps_r1.xy = ps_v1.xy;
	ps_r0.y = -ps_r1.x + ps_v2.x;
	ps_r2.xy = ps_v4.xy;
	ps_r0.z = ps_r2.x + -ps_v3.x;
	ps_r1.x = ((ps_v0.x >= 0.0) ? ps_c4.x : ps_c4.y);
	ps_r1.z = ((ps_v0.y >= 0.0) ? ps_c4.x : ps_c4.y);
	ps_r2.xz = ps_r1.xz * ps_v0.xy;
	ps_r2.xz = fract(ps_r2.xz);
	ps_r1.xz = ps_r1.xz * ps_r2.xz;
	ps_r3.x = mix(ps_r0.y, ps_r0.z, ps_r1.z);
	ps_r0.y = -ps_r1.y + ps_v3.y;
	ps_r0.z = ps_r2.y + -ps_v2.y;
	ps_r3.y = mix(ps_r0.y, ps_r0.z, ps_r1.x);
	ps_r0.yz = (ps_r1.xz * ps_r3.xy) + ps_v1.xy;
	ps_r1 = texture2D(ps_s9, ps_r0.yz);
	ps_r2 = ps_r1 * ps_v5;
	ps_r1 = (ps_r1 * -ps_v5) + ps_c1;
	ps_oC0 = (ps_r0.xxxx * ps_r1) + ps_r2;
}

 		 ps_s9  �  #ifdef GL_ES
precision highp float;
precision mediump int;
#endif

uniform vec4 vs_uniforms_vec4[12];
uniform vec4 posFixup;
vec4 vs_r0;
vec4 vs_r1;
#define vs_c0 vs_uniforms_vec4[0]
#define vs_c1 vs_uniforms_vec4[1]
#define vs_c2 vs_uniforms_vec4[2]
#define vs_c3 vs_uniforms_vec4[3]
#define vs_c4 vs_uniforms_vec4[4]
#define vs_c5 vs_uniforms_vec4[5]
#define vs_c6 vs_uniforms_vec4[6]
#define vs_c7 vs_uniforms_vec4[7]
#define vs_c8 vs_uniforms_vec4[8]
#define vs_c9 vs_uniforms_vec4[9]
#define vs_c10 vs_uniforms_vec4[10]
#define vs_c11 vs_uniforms_vec4[11]
attribute vec4 vs_v0;
#define vs_o0 gl_Position
attribute vec4 vs_v1;
varying vec4 vTexCoord0;
#define vs_o1 vTexCoord0
attribute vec4 vs_v2;
varying vec4 vTexCoord1;
#define vs_o2 vTexCoord1
attribute vec4 vs_v3;
varying vec4 vTexCoord2;
#define vs_o3 vTexCoord2
attribute vec4 vs_v4;
varying vec4 vTexCoord3;
#define vs_o4 vTexCoord3
attribute vec4 vs_v5;
varying vec4 vTexCoord4;
#define vs_o5 vTexCoord4
attribute vec4 vs_v6;
varying vec4 vTexCoord5;
#define vs_o6 vTexCoord5
attribute vec4 vs_v7;
varying vec4 vTexCoord6;
#define vs_o7 vTexCoord6
attribute vec4 vs_v8;
varying vec4 vTexCoord7;
#define vs_o8 vTexCoord7
varying vec4 vTexCoord8;
#define vs_o9 vTexCoord8

void main()
{
	vs_r0.w = dot(vs_v0, vs_c3);
	vs_r0.x = dot(vs_v0, vs_c0);
	vs_r0.y = dot(vs_v0, vs_c1);
	vs_r0.z = dot(vs_v0, vs_c2);
	vs_r1.x = dot(vs_r0, vs_c4);
	vs_r1.y = dot(vs_r0, vs_c5);
	vs_r1.z = dot(vs_r0, vs_c6);
	vs_r1.w = dot(vs_r0, vs_c7);
	vs_o9.xyz = vs_r0.xyz;
	vs_o0.x = dot(vs_r1, vs_c8);
	vs_o0.y = dot(vs_r1, vs_c9);
	vs_o0.z = dot(vs_r1, vs_c10);
	vs_o0.w = dot(vs_r1, vs_c11);
	vs_o1.xy = vs_v2.xy;
	vs_o2.xy = vs_v3.xy;
	vs_o3.xy = vs_v4.xy;
	vs_o4.xy = vs_v5.xy;
	vs_o5.xy = vs_v6.xy;
	vs_o6 = vs_v1;
	vs_o7.xy = vs_v7.xy;
	vs_o8.xyz = vs_v8.xyz;
	gl_Position.y = gl_Position.y * posFixup.y;
	gl_Position.xy += posFixup.zw * gl_Position.ww;
	gl_Position.z = gl_Position.z * 2.0 - gl_Position.w;
}

 	vs_v0    vs_v1   vs_v2   vs_v3  vs_v4  vs_v5  vs_v6  vs_v7  vs_v8    �  #ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

uniform vec4 ps_uniforms_vec4[4];
vec4 ps_r0;
vec4 ps_r1;
vec4 ps_r2;
#define ps_c0 ps_uniforms_vec4[0]
#define ps_c1 ps_uniforms_vec4[1]
#define ps_c2 ps_uniforms_vec4[2]
#define ps_c3 ps_uniforms_vec4[3]
uniform sampler2D ps_s6;
varying vec4 vTexCoord0;
#define ps_v0 vTexCoord0
#define ps_oC0 gl_FragColor
varying vec4 vTexCoord1;
#define ps_v1 vTexCoord1
varying vec4 vTexCoord2;
#define ps_v2 vTexCoord2

void main()
{
	ps_r0.xyz = ps_c0.xyz + -ps_v2.xyz;
	ps_r0.x = dot(ps_r0.xyz, ps_r0.xyz);
	ps_r0.x = inversesqrt(ps_r0.x);
	ps_r0.x = 1.0 / ps_r0.x;
	ps_r0.x = ps_r0.x + -ps_c2.x;
	ps_r1.x = ps_c2.x;
	ps_r0.y = -ps_r1.x + ps_c3.x;
	ps_r0.y = 1.0 / ps_r0.y;
	ps_r0.x = clamp(ps_r0.y * ps_r0.x, 0.0, 1.0);
	ps_r1 = texture2D(ps_s6, ps_v1.xy);
	ps_r2 = ps_r1 * ps_v0;
	ps_r1 = (ps_r1 * -ps_v0) + ps_c1;
	ps_oC0 = (ps_r0.xxxx * ps_r1) + ps_r2;
}

  ps_s6	  
CameraPosition                FogColor                     FogStart         FogEnd        World                                                                    View                                                                    
Projection                                                                    TerrainDiffuse      PortalDiffuse      SpriteDiffuse      	WorldTech Terrain     StencilPortal    Portal    Sprite    