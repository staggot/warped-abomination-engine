﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace WarpedAbominationEngine
{
    public class GameWindow : Game
    {
        Screen Screen;
        Point2 WindowedSize;

        public GameWindow()
        {
            XnaSystem.Create(this);
        }

        protected override void Initialize()
        {
            base.Initialize();
            XnaSystem.Initialize();

            Window.AllowUserResizing = true;
            Window.ClientSizeChanged+= Window_ClientSizeChanged;
        }

        void Window_ClientSizeChanged(object sender, EventArgs e)
        {
            Point2 newSize = new Point2(Window.ClientBounds.Width, Window.ClientBounds.Height);
            Point2 oldSize = XnaSystem.ScreenSize;
            if (newSize != oldSize)
            {
                if (!XnaSystem.IsFullscreen)
                    WindowedSize = newSize;
                XnaSystem.SetScreenSize(newSize);
                if (Screen != null)
                    Screen.SetScreenSize(newSize.X, newSize.Y);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (Screen != null)
                Screen.Dispose();
            base.Dispose(disposing);
        }

        protected override void OnExiting(object sender, EventArgs args)
        {
            base.OnExiting(sender, args);
            if (Screen != null)
                Screen.Dispose();
            Screen = null;
        }

        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            float time = (float)gameTime.ElapsedGameTime.TotalSeconds;
            XnaSystem.Update(time);
            XnaControlsSystem.Update(time);
            Screen.ApplyControls(time);
            Screen.Update(time);
        }

        protected override void Draw(GameTime gameTime)
        {
            Screen.Render();
            Screen.Draw(null);
        }
    }
}
