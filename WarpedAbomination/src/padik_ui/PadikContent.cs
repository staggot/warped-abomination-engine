﻿using System;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using WarpedAbominationEngine.Content;

namespace WarpedAbominationEngine
{
    public partial class PadikUI
    {
        public static BlendState UIBlendState { get; private set; }
        public static DepthStencilState UIDepthStencil { get; private set; }
        public static RasterizerState UIRasterisationState { get; private set; }
        public static MegaTexture UITexture { get; private set; }
        public static float PixelsPerTexel { get; private set; }

        static Vector4 fontTexture;


        static bool loaded = false;

        public static void Load()
        {
            if (loaded)
                return;


            UIBlendState = BlendState.NonPremultiplied;
            UIDepthStencil = new DepthStencilState()
            {
                StencilEnable = false,
                StencilFunction = CompareFunction.Always,

                StencilPass = StencilOperation.Keep,
                DepthBufferFunction = CompareFunction.Always,
                DepthBufferWriteEnable = false,
            };
            UIRasterisationState = RasterizerState.CullNone;

            PixelsPerTexel = (float)UITexture.Resolution;
            loaded = true;
        }
    }
}
