﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace WarpedAbominationEngine
{
    public class BoxLayout : PadikLayout
    {
        List<PadikPanelElement> controls;
        LayoutAxis axis;


        public LayoutAxis Axis
        {
            get { return axis; }
        }

        public BoxLayout(LayoutAxis axis)
        {
            controls = new List<PadikPanelElement>();
            this.axis = axis;
        }

        public override IEnumerator<PadikPanelElement> GetEnumerator()
        {
            return controls.GetEnumerator();
        }

        protected override void ChildSizeChanged()
        {

        }

        public override void Add(PadikPanelElement element)
        {
            throw new NotImplementedException();
        }

        public override void Remove(PadikPanelElement element)
        {
            throw new NotImplementedException();
        }
    }
}
