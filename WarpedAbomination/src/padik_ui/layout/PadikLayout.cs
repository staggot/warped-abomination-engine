﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace WarpedAbominationEngine
{
    public abstract class PadikLayout : PadikPanelElement, IEnumerable<PadikPanelElement>
    {
        public abstract IEnumerator<PadikPanelElement> GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public abstract void Add(PadikPanelElement element);
        public abstract void Remove(PadikPanelElement element);
    }
}
