﻿public enum CursorType : byte { Normal = 0, Link = 1, Horisontal = 2, Vertical = 3, DiagonalBottomRight = 4, DiagonalBottomLeft = 5 }
public enum DragType : byte { Position, Left, Right, Bottom, Top, LeftBottom, RightBottom, LeftTop, RightTop }
public enum Align : byte { CenterLeft, TopLeft, BottomLeft, Center, TopCenter, BottomCenter, TopRight, CenterRight, BottomRight };
public enum LayoutType: byte { Box, Grid, Flow };
public enum LayoutAxis: byte { X, Y};
