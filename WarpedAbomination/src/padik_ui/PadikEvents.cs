﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace WarpedAbominationEngine
{
    public delegate void PadikMouseEvent(PadikMouseEventArgs args);
    public delegate void PadikKeyboardEvent(PadikKeyboardEventArgs args);
    public delegate void PadikEvent(PadikEventArgs args);
    public delegate void PadikControlEvent(PadikControlEventArgs args);
    public delegate void PadikTextEvent(PadikTextEventArgs args);
    public delegate void PadikItemEvent(PadikItemEventArgs args);
    public delegate void PadikScrollEvent(PadikScrollEventArgs args);


    public class PadikEventArgs
    {
        public PadikEventArgs(object sender)
        {
            this.Sender = sender;
        }
        public object Sender { get; set; }
    }

    public class PadikItemEventArgs : PadikEventArgs
    {
        public object Item { get; set; }
        public PadikItemEventArgs(object sender, object item) : base(sender)
        {
            this.Item = item;
        }
    }

    public class PadikMouseEventArgs : PadikEventArgs
    {
        public MouseKeys MouseButton { get; set; }
        public Vector2 MousePosition { get; set; }

        public PadikMouseEventArgs(object sender, Vector2 mousePosition, MouseKeys mouseButton) : base(sender)
        {
            this.MousePosition = mousePosition;
            this.MouseButton = mouseButton;
        }
    }

    public class PadikControlEventArgs : PadikEventArgs
    {
        public PadikControl Control { get; set; }

        public PadikControlEventArgs(object sender, PadikControl control) : base(sender)
        {
            this.Control = control;
        }

    }

    public class PadikScrollEventArgs : PadikEventArgs
    {
        public int ScrollDelta { get; set; }
        public Vector2 MousePosition { get; set; }

        public PadikScrollEventArgs(object sender, Vector2 mousePosition, int scrollDelta) : base(sender)
        {
            this.MousePosition = mousePosition;
            this.ScrollDelta = scrollDelta;
        }
    }

    public class PadikTextEventArgs : PadikEventArgs
    {

        public string Text { get; set; }
        public PadikTextEventArgs(object sender, string text) : base(sender)
        {
            this.Text = text;
        }
    }

    public class PadikKeyboardEventArgs : PadikEventArgs
    {

        public PadikKeyboardEventArgs(object sender, Keys key) : base(sender)
        {
            this.Key = key;
        }

        public Keys Key { get; set; }
    }

}
