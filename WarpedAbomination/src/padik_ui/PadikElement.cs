﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;


namespace WarpedAbominationEngine
{
    public abstract class PadikElement
    {
        public event PadikMouseEvent OnMouseMove;
        public event PadikMouseEvent OnMouseDown;
        public event PadikMouseEvent OnMouseUp;
        public event PadikScrollEvent OnScroll;
        public event PadikEvent OnPositionChanged;
        public event PadikEvent OnSizeChanged;
        public event PadikKeyboardEvent OnKeyDown;
        public event PadikKeyboardEvent OnKeyUp;
        public event PadikEvent OnSelect;
        public event PadikEvent OnUnselect;
        public event PadikEvent OnHover;
        public event PadikEvent OnUnhover;

        public bool IsEnabled
        {
            get { return isEnabled; }
        }


        protected PadikUI ui;
        Vector2 minimalSize = new Vector2(1, 1);
        Vector2 oldCursorPosition = new Vector2(-1, -1);
        protected Vector2 elementPosition;
        protected bool isEnabled = true;
        protected Vector2 elementSize;
        HashSet<Keys> pressedKeys = new HashSet<Keys>();
        HashSet<MouseKeys> pressedMouseKeys = new HashSet<MouseKeys>();



        public PadikElement Parent { get; internal set; }

        protected Vector2 CheckMinimalSize(Vector2 newSize)
        {
            if (newSize.X < minimalSize.X)
                newSize.X = minimalSize.X;
            if (newSize.Y < minimalSize.Y)
                newSize.Y = minimalSize.Y;
            return newSize;
        }

        public Vector2 MinimalSize
        {
            get { return minimalSize; }
            set
            {
                if (value.X < 0 || value.Y < 0)
                    return;
                minimalSize = value;
                Size = CheckMinimalSize(Size);
            }
        }

        public PadikUI ParentUi
        {
            get { if (ui != null) return ui; if (Parent == null) return null; return Parent.ParentUi; }
        }

        protected Vector2 parentSize
        {
            get
            {
                if (Parent != null)
                    return Parent.Size;
                return ParentUi.Size;
            }
        }

        protected Vector2 parentActualPosition
        {
            get
            {
                if (Parent != null)
                    return Parent.ActualPosition;
                return Vector2.Zero;
            }
        }

        protected Vector2 ActualPosition
        {
            get { return Position + parentActualPosition; }
        }

        public Vector2 Size
        {
            get { return elementSize; }
            set { if (value == elementSize) return; value = CheckMinimalSize(value); Vector2 delta = value - elementSize; elementSize = value; InvokeOnSizeChanged(delta); }
        }

        public Vector2 Position
        {
            get { return elementPosition; }
            set { if (value == elementPosition) return; elementPosition = value; InvokeOnPositionChanged(); }
        }

        protected abstract void ChildSizeChanged();


        protected abstract void ParentSizeChanged();

        public virtual void InvokeOnHover()
        {
            OnHover?.Invoke(new PadikEventArgs(this));
        }

        public virtual void InvokeOnUnhover()
        {
            OnUnhover?.Invoke(new PadikEventArgs(this));
        }

        public virtual void InvokeOnSelect()
        {
            OnSelect?.Invoke(new PadikEventArgs(this));
        }

        public virtual void InvokeOnUnselect()
        {

            OnUnselect?.Invoke(new PadikEventArgs(this));
        }

        public virtual void InvokeOnMouseScroll(Vector2 mousePosition, int ScrollDelta)
        {
            OnScroll?.Invoke(new PadikScrollEventArgs(this, mousePosition, ScrollDelta));
        }

        public virtual void InvokeOnMouseMove(Vector2 mousePosition)
        {
            OnMouseMove?.Invoke(new PadikMouseEventArgs(this, mousePosition, MouseKeys.Left));
        }

        public virtual void InvokeOnMouseDown(Vector2 mousePosition, MouseKeys mouseKey)
        {
            OnMouseDown?.Invoke(new PadikMouseEventArgs(this, mousePosition, mouseKey));
        }

        public virtual void InvokeOnMouseUp(Vector2 mousePosition, MouseKeys mouseKey)
        {
            OnMouseUp?.Invoke(new PadikMouseEventArgs(this, mousePosition, mouseKey));
        }

        public virtual void InvokeOnKeyDown(Keys key)
        {
            OnKeyDown?.Invoke(new PadikKeyboardEventArgs(this, key));
        }

        public virtual void InvokeOnKeyUp(Keys key)
        {
            OnKeyUp?.Invoke(new PadikKeyboardEventArgs(this, key));
        }

        public virtual void InvokeOnSizeChanged(Vector2 deltaSize)
        {
            Vector2 newPosition = Position;
            Position = newPosition;
            OnSizeChanged?.Invoke(new PadikEventArgs(this));
            if (Parent != null)
                Parent.ChildSizeChanged();
        }

        public virtual void InvokeOnPositionChanged()
        {
            OnPositionChanged?.Invoke(new PadikEventArgs(this));
        }

        public abstract bool IsHovered { get; }
        public abstract bool IsSelected { get; set; }

        public virtual void Update(Vector2 cursorPosition, bool readControls)
        {
            if (IsSelected && readControls)
            {
                for (int i = 0; i < XnaControlsSystem.TotalKeyboardKeys; i++)
                {
                    Keys key = (Keys)i;
                    if (XnaControlsSystem.IsKeyPressed(key))
                    {
                        pressedKeys.Add(key);
                        InvokeOnKeyDown(key);
                    }
                }
            }
            foreach (Keys key in pressedKeys)
            {
                if (XnaControlsSystem.IsKeyReleased(key))
                {
                    pressedKeys.Remove(key);
                    InvokeOnKeyUp(key);
                }
            }

            foreach (MouseKeys mouseKey in pressedKeys)
            {
                if (XnaControlsSystem.IsMouseButtonReleased(mouseKey))
                {
                    pressedMouseKeys.Remove(mouseKey);
                    InvokeOnMouseUp(cursorPosition, mouseKey);
                }
            }

            if (IsHovered && readControls)
            {
                if (XnaControlsSystem.IsMouseButtonPressed(MouseKeys.Left))
                {
                    pressedMouseKeys.Add(MouseKeys.Left);
                    InvokeOnMouseDown(cursorPosition, MouseKeys.Left);
                }
                if (XnaControlsSystem.IsMouseButtonPressed(MouseKeys.Right))
                {
                    pressedMouseKeys.Add(MouseKeys.Right);
                    InvokeOnMouseDown(cursorPosition, MouseKeys.Right);
                }
                if (XnaControlsSystem.IsMouseButtonPressed(MouseKeys.Middle))
                {
                    pressedMouseKeys.Add(MouseKeys.Middle);
                    InvokeOnMouseDown(cursorPosition, MouseKeys.Middle);
                }
                if (XnaControlsSystem.ScrollDelta() != 0)
                {
                    InvokeOnMouseScroll(cursorPosition, XnaControlsSystem.ScrollDelta());
                }
            }

            if (cursorPosition != oldCursorPosition && IsHovered)
            {
                oldCursorPosition = cursorPosition;
                InvokeOnMouseMove(cursorPosition);
            }
        }


        internal virtual bool IsCursorInside(Vector2 cursorPosition)
        {
            return cursorPosition.X >= 0 && cursorPosition.X <= elementSize.X && cursorPosition.Y >= 0 && cursorPosition.Y <= elementSize.Y;
        }

        internal abstract void Draw();

        public Vector2 GetStringSize(Vector2 scale, string str)
        {
            float width = 0;
            float height = scale.Y;
            float currentWidth = 0;
            for (int i = 0; i < str.Length; i++)
            {
                char sym = str[i];
                if (sym == '\r')
                {
                    currentWidth = 0;
                    continue;
                }
                if (sym == '\n')
                {
                    height += scale.Y;
                    currentWidth = 0;
                    continue;
                }
                currentWidth += scale.X;
                if (currentWidth > width)
                    width = currentWidth;
            }

            return new Vector2(width, height);
        }
        /*
                public void DrawFill(Vector4 location, Color color)
                {
                    DrawImage(location, UI.pixelTexture, color);
                }

                public void DrawGradient(Vector4 location, bool down, Color color)
                {
                    DrawImage(location, down ? UI.gradientDownTexture : UI.gradientUpTexture, color);
                }*/

        public void DrawImage(Texture2D texture, Vector4 location, Vector4 image)
        {
            DrawImage(texture, location, image, Color.White, ImageMod.None);
        }

        public void DrawImage(Texture2D texture, Vector4 location, Vector4 image, Color color, ImageMod mod)
        {
            PadikUI pUi = ParentUi;
            if (pUi == null)
                return;
            if (pUi != null)
                pUi.RenderStep();
            DrawImage(location, image, color, mod);
            if (pUi != null)
                pUi.RenderFlush(texture);

        }

        public void DrawString(Vector2 position, Vector2 scale, string str, Color color)
        {
            Vector2 textSize = GetStringSize(scale, str);

            float startX = position.X;
            for (int i = 0; i < str.Length; i++)
            {
                char sym = str[i];
                if (sym == '\r')
                {
                    position.X = startX;
                    continue;
                }
                if (sym == '\n')
                {
                    position.Y += scale.Y;
                    position.X = startX;
                    continue;
                }
                DrawImage(
                    new Vector4(position.X, position.Y, scale.X, scale.Y),
                    PadikUI.GetSymbolTexture(sym),
                    color);
                position.X += scale.X;
            }
        }

        protected void DrawImage(Vector4 location, Vector4 image)
        {
            DrawImage(location, image, Color.White);
        }

        internal static Vector2 CanculateAlignOrigin(Vector4 targetLocation, Align align, Vector2 size)
        {
            switch (align)
            {
                case Align.CenterLeft:
                    return new Vector2
                        (
                            targetLocation.X,
                            targetLocation.Y + targetLocation.W / 2 - size.Y / 2
                        );
                case Align.TopLeft:
                    return new Vector2
                        (
                            targetLocation.X,
                            targetLocation.Y
                        );
                case Align.BottomLeft:
                    return new Vector2
                        (
                            targetLocation.X,
                            targetLocation.Y + targetLocation.W - size.Y
                        );
                case Align.CenterRight:
                    return new Vector2
                        (
                            targetLocation.X + targetLocation.Z - size.X,
                            targetLocation.Y + targetLocation.W / 2 - size.Y / 2
                        );
                case Align.TopRight:
                    return new Vector2
                        (
                            targetLocation.X + targetLocation.Z - size.X,
                            targetLocation.Y
                        );
                case Align.BottomRight:
                    return new Vector2
                        (
                            targetLocation.X + targetLocation.Z - size.X,
                            targetLocation.Y + targetLocation.W - size.Y
                        );
                case Align.Center:
                    return new Vector2
                        (
                            targetLocation.X + targetLocation.Z / 2 - size.X / 2,
                            targetLocation.Y + targetLocation.W / 2 - size.Y / 2
                        );
                case Align.TopCenter:
                    return new Vector2
                        (
                            targetLocation.X + targetLocation.Z / 2 - size.X / 2,
                            targetLocation.Y
                        );
                case Align.BottomCenter:
                    return new Vector2
                        (
                            targetLocation.X + targetLocation.Z / 2 - size.X / 2,
                            targetLocation.Y + targetLocation.W - size.Y
                        );
            }
            return new Vector2(targetLocation.X, targetLocation.Y);
        }

        protected void DrawImage(Vector4 location, Vector4 image, Color color)
        {
            DrawImage(location, image, color, ImageMod.None);
        }

        protected void DrawImage(Vector4 location, Vector4 image, Color color, ImageMod mod)
        {
            switch (mod)
            {
                case ImageMod.None:
                    break;
            }

            Vector4 newTexture = image;
            Vector4 window = new Vector4(parentActualPosition, parentSize.X, parentSize.Y);
            Vector2 actualPos = ActualPosition;
            location.X += actualPos.X;
            location.Y += actualPos.Y;
            Vector4 newLoc = location;

            float right = location.X;
            float left = right + location.Z;
            float top = location.Y;
            float bottom = top + location.W;

            if (right < window.X)
            {
                float locDelta = window.X - right;
                newLoc.X = window.X;
                newLoc.Z = newLoc.Z - locDelta;

                float texDelta = image.Z * (locDelta / location.Z);
                newTexture.X = image.X + texDelta;
                newTexture.Z -= texDelta;
            }

            if (top < window.Y)
            {
                float locDelta = window.Y - top;
                newLoc.Y = window.Y;
                newLoc.W = newLoc.W - locDelta;

                float texDelta = image.W * (locDelta / location.W);
                newTexture.Y = image.Y + texDelta;
                newTexture.W -= texDelta;
            }

            if (left > window.X + window.Z)
            {
                float locDelta = left - (window.X + window.Z);
                newLoc.Z = newLoc.Z - locDelta;

                float texDelta = image.Z * (locDelta / location.Z);
                newTexture.Z -= texDelta;
            }

            if (bottom > window.Y + window.W)
            {
                float locDelta = bottom - (window.Y + window.W);
                newLoc.W = newLoc.W - locDelta;

                float texDelta = image.W * (locDelta / location.W);
                newTexture.W -= texDelta;
            }

            if (newLoc.Z <= 0 || newLoc.W <= 0)
                return;
            location = newLoc;
            image = newTexture;
            Vector2 leftTop, leftBottom, rightTop, rightBottom;

            switch (mod)
            {
                case ImageMod.RotateLeft:
                    leftTop = new Vector2(image.X + image.Z, image.Y);
                    leftBottom = new Vector2(image.X, image.Y);
                    rightTop = new Vector2(image.X + image.Z, image.Y + image.W);
                    rightBottom = new Vector2(image.X, image.Y + image.W);
                    break;
                case ImageMod.RotateRight:
                    leftTop = new Vector2(image.X, image.Y + image.W);
                    leftBottom = new Vector2(image.X + image.Z, image.Y + image.W);
                    rightTop = new Vector2(image.X, image.Y);
                    rightBottom = new Vector2(image.X + image.Z, image.Y);
                    break;
                case ImageMod.RotateBack:
                    leftTop = new Vector2(image.X + image.Z, image.Y + image.W);
                    leftBottom = new Vector2(image.X + image.Z, image.Y);
                    rightTop = new Vector2(image.X, image.Y + image.W);
                    rightBottom = new Vector2(image.X, image.Y);
                    break;
                case ImageMod.FlipHorisontal:
                    leftTop = new Vector2(image.X + image.Z, image.Y);
                    leftBottom = new Vector2(image.X + image.Z, image.Y + image.W);
                    rightTop = new Vector2(image.X, image.Y);
                    rightBottom = new Vector2(image.X, image.Y + image.W);
                    break;
                case ImageMod.FlipVertical:
                    leftTop = new Vector2(image.X, image.Y + image.W);
                    leftBottom = new Vector2(image.X, image.Y);
                    rightTop = new Vector2(image.X + image.Z, image.Y + image.W);
                    rightBottom = new Vector2(image.X + image.Z, image.Y);
                    break;
                default:
                    leftTop = new Vector2(image.X, image.Y);
                    leftBottom = new Vector2(image.X, image.Y + image.W);
                    rightTop = new Vector2(image.X + image.Z, image.Y);
                    rightBottom = new Vector2(image.X + image.Z, image.Y + image.W);
                    break;
            }


            if (ui.currentVertexIndex >= ui.vertexBuffer.Length)
                ParentUi.RenderStep();
            ui.vertexBuffer[ui.currentVertexIndex] = new VertexPositionColorTexture(
                        new Vector3(location.X + location.Z, location.Y, 0),
                        color,
                        rightTop
                    );
            ui.currentVertexIndex++;
            ui.vertexBuffer[ui.currentVertexIndex] = new VertexPositionColorTexture(
                        new Vector3(location.X, location.Y, 0),
                        color,
                        leftTop
                    );

            ui.currentVertexIndex++;
            ui.vertexBuffer[ui.currentVertexIndex] = new VertexPositionColorTexture(
                        new Vector3(location.X, location.Y + location.W, 0),
                        color,
                        leftBottom
                    );

            ui.currentVertexIndex++;
            ui.vertexBuffer[ui.currentVertexIndex] = new VertexPositionColorTexture(
                        new Vector3(location.X + location.Z, location.Y + location.W, 0),
                        color,
                        rightBottom
                    );

            ui.currentVertexIndex++;
            ui.vertexBuffer[ui.currentVertexIndex] = new VertexPositionColorTexture(
                        new Vector3(location.X + location.Z, location.Y, 0),
                        color,
                        rightTop
                    );

            ui.currentVertexIndex++;
            ui.vertexBuffer[ui.currentVertexIndex] = new VertexPositionColorTexture(
                        new Vector3(location.X, location.Y + location.W, 0),
                        color,
                        leftBottom
                    );
            ui.currentVertexIndex++;
        }

    }
}
