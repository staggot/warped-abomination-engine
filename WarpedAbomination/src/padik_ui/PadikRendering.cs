﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace WarpedAbominationEngine
{
    public partial class PadikUI
    {
        protected void DrawImage(Vector4 location, Vector4 image, Color color)
        {

            if (currentVertexIndex >= vertexBuffer.Length)
                return;
            vertexBuffer[currentVertexIndex] = new VertexPositionColorTexture(
                        new Vector3(location.X + location.Z, location.Y, 0),
                        color,
                        new Vector2(image.X + image.Z, image.Y)
                    );
            currentVertexIndex++;
            vertexBuffer[currentVertexIndex] = new VertexPositionColorTexture(
                        new Vector3(location.X, location.Y, 0),
                        color,
                        new Vector2(image.X, image.Y)
                    );

            currentVertexIndex++;
            vertexBuffer[currentVertexIndex] = new VertexPositionColorTexture(
                        new Vector3(location.X, location.Y + location.W, 0),
                        color,
                        new Vector2(image.X, image.Y + image.W)
                    );

            currentVertexIndex++;
            vertexBuffer[currentVertexIndex] = new VertexPositionColorTexture(
                        new Vector3(location.X + location.Z, location.Y + location.W, 0),
                        color,
                        new Vector2(image.X + image.Z, image.Y + image.W)
                    );

            currentVertexIndex++;
            vertexBuffer[currentVertexIndex] = new VertexPositionColorTexture(
                        new Vector3(location.X + location.Z, location.Y, 0),
                        color,
                        new Vector2(image.X + image.Z, image.Y)
                    );

            currentVertexIndex++;
            vertexBuffer[currentVertexIndex] = new VertexPositionColorTexture(
                        new Vector3(location.X, location.Y + location.W, 0),
                        color,
                        new Vector2(image.X, image.Y + image.W)
                    );
            currentVertexIndex++;
        }

        public void Render(RenderTarget2D target)
        {
            XnaSystem.GraphicsDevice.SetRenderTarget(target);
            XnaSystem.GraphicsDevice.Clear(Color.TransparentBlack);
            XnaSystem.GraphicsDevice.SamplerStates[0] = SamplerState.PointClamp;
            XnaSystem.GraphicsDevice.BlendState = UIBlendState;
            XnaSystem.GraphicsDevice.RasterizerState = UIRasterisationState;
            XnaSystem.GraphicsDevice.DepthStencilState = UIDepthStencil;
            for (int i = 0; i < panels.Count; i++)
            {
                PadikPanel panel = panels[i];
                if (panel.IsEnabled)
                    panel.Draw();
            }
            if (!XnaControlsSystem.MouseFixed && IsCursorVisible)
            {
                //Vector4 cursorTexture = cursors[(int)currentCursor];
                //DrawImage(new Vector4(cursorPosition, cursorTexture.Z * PixelsPerTexel, cursorTexture.W * PixelsPerTexel), cursorTexture, Color.White);
            }

            RenderStep();
        }


        internal void RenderStep()
        {
            RenderFlush(UITexture.Diffuse);
        }

        internal void RenderFlush(Texture2D texture)
        {
            if (currentVertexIndex > 0)
            {
                effect.Texture = texture;
                effect.CurrentTechnique.Passes[0].Apply();
                XnaSystem.GraphicsDevice.DrawUserPrimitives<VertexPositionColorTexture>(PrimitiveType.TriangleList, vertexBuffer, 0, currentVertexIndex / 3);
            }
            currentVertexIndex = 0;
        }

        public static Vector4 GetSymbolTexture(char symbol)
        {
            byte symbolB = (byte)symbol;
            int x = symbolB & 0x0f;
            int y = symbolB >> 4;
            float symWidth = fontTexture.Z / (float)16;
            float symHeight = fontTexture.W / (float)16;
            return new Vector4(fontTexture.X + symWidth * x, fontTexture.Y + symHeight * y, symWidth, symHeight);
        }
    }
}
