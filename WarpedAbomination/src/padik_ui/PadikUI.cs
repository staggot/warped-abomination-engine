﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Threading;

namespace WarpedAbominationEngine
{
    public partial class PadikUI
    {
        public const int VertexBufferSize = 512 * 6;
        public bool IsCursorVisible { get; set; } = true;

        internal VertexPositionColorTexture[] vertexBuffer = new VertexPositionColorTexture[VertexBufferSize];
        internal int currentVertexIndex = 0;

        int screenWidth, screenHeight;
        Vector2 size;
        Vector2 cursorPosition;
        RenderTarget2D renderTarget;
        BasicEffect effect;
        List<PadikPanel> panels = new List<PadikPanel>();

        public Vector2 Size
        {
            get { return size; }
        }

        public PadikUI()
        {
            effect = new BasicEffect(XnaSystem.GraphicsDevice);
            effect.Texture = UITexture.Diffuse;
            effect.TextureEnabled = true;
            effect.AmbientLightColor = new Vector3(1f, 1f, 1f);
            effect.VertexColorEnabled = true;
            SetResolution(640, 480);
        }

        public void SetResolution(int width, int height)
        {
            this.screenWidth = width;
            this.screenHeight = height;
            this.size = new Vector2(width, height);
            UpdateRenderTarget();
        }

        void UpdateRenderTarget()
        {
            if (renderTarget != null)
                renderTarget.Dispose();
            renderTarget = new RenderTarget2D(XnaSystem.GraphicsDevice, screenWidth, screenHeight);
            effect.World = Matrix.CreateWorld(Vector3.Zero, Vector3.Forward, Vector3.Up);
            effect.Projection = Matrix.CreateOrthographic(screenWidth, screenHeight, 0.005f, 10f);
            effect.View = Matrix.CreateLookAt(new Vector3(screenWidth / 2.0f, screenHeight / 2.0f, -0.5f), new Vector3(screenWidth / 2.0f, screenHeight / 2.0f, 0f), Vector3.Down);
        }

        public void Update(float time, Vector2 mousePosition, bool readControls, out bool continueReading)
        {
            cursorPosition = mousePosition;
            continueReading = true;
        }
    }
}
