﻿using System;
using System.Collections.Concurrent;
using Microsoft.Xna.Framework;
using System.Threading;

namespace WarpedAbominationEngine
{
    public class ScriptConsole
    {
        public static Color[] ColorMap =
        {
            Color.Black,
            new Color(0,0,128),
            new Color(0,128,0),
            new Color(0,128,128),
            new Color(128,0,0),
            new Color(128,0,128),
            new Color(128,128,0),
            new Color(128,128,128),
            new Color(64,64,64),
            new Color(0,0,255),
            new Color(0,255,0),
            new Color(0,255,255),
            new Color(255,0,0),
            new Color(255,0,255),
            new Color(255,255,0),
            new Color(255,255,255),

        };

        int bufferWidth;
        int bufferHeight;
        long writtenLines = 0;

        ConsoleColor foreColor;
        ConsoleColor backColor;
        Point2 cursor;

        char[,] buffer;
        byte[,] colors;

        ConcurrentQueue<char> readQueue = new ConcurrentQueue<char>();
        Semaphore readSemaphore;
        bool echo = true;

        public ScriptConsole(int bufferWidth = 80, int bufferHeight = 4096)
        {
            this.cursor = new Point2(0, 0);
            this.bufferWidth = bufferWidth;
            this.bufferHeight = bufferHeight;
            ResetColor();
            ResetBuffer();
        }

        public ConsoleColor ForegroundColor
        {
            get
            { return foreColor; }
            set
            { foreColor = value; }
        }

        public ConsoleColor BackgroundColor
        {
            get
            { return backColor; }
            set
            { backColor = value; }
        }

        public bool Echo
        {
            get { return echo; }
            set { echo = value; }
        }

        void AppendChar(char c)
        {
            switch (c)
            {
                case '\n':
                    NextLine();
                    cursor.X = 0;
                    break;
                case '\r':
                    cursor.X = 0;
                    break;
                case '\b':
                    if (cursor.X > 0)
                        cursor.X--;
                    buffer[cursor.Y, cursor.X] = ' ';
                    colors[cursor.Y, cursor.X] = colorValue;
                    break;
                default:
                    buffer[cursor.Y, cursor.X] = c;
                    colors[cursor.Y, cursor.X] = colorValue;
                    NextChar();
                    break;
            }
        }

        public void Write(string text)
        {
            for (int i = 0; i < text.Length; i++)
                AppendChar(text[i]);
        }

        public void WriteLine(string text)
        {
            Write(text);
            AppendChar('\n');
        }

        public char Read()
        {
            readSemaphore.WaitOne();
            char c;
            while (!readQueue.TryDequeue(out c)) { Thread.Yield(); }
            return c;
        }

        protected byte colorValue
        {
            get { return (byte)(((int)backColor << 4) | (int)foreColor); }
        }

        void NextChar()
        {
            cursor.X++;
            if (cursor.X >= bufferWidth)
            {
                cursor.X = 0;
                NextLine();
            }
        }

        void NextLine()
        {
            cursor.Y = (cursor.Y + 1) % bufferHeight;
            writtenLines++;
        }

        public void ResetColor()
        {
            foreColor = ConsoleColor.White;
            backColor = ConsoleColor.Black;
            colors = new byte[bufferHeight, bufferWidth];
        }

        public void ResetBuffer()
        {
            writtenLines = 0;
            buffer = new char[bufferHeight, bufferWidth];
            cursor = new Point2(0, 0);
        }

        internal void Update(bool readControls)
        {
            if (readControls)
            {
                if (XnaControlsSystem.PressedCharacter != '\0')
                {
                    readQueue.Enqueue(XnaControlsSystem.PressedCharacter);
                    readSemaphore.Release(1);
                }
            }
        }

        internal void Draw()
        {

        }
    }
}
