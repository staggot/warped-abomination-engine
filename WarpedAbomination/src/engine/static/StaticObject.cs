﻿using System;
using Microsoft.Xna.Framework;

namespace WarpedAbominationEngine
{
    public class StaticObject
    {
        Vector3 position;
        Sector sector;

        float yaw, pitch, roll;

        public StaticObject()
        {
        }

        public Vector3 Position
        {
            get
            { return position; }
            set
            {
                if (position == value)
                    return;
                position = value;
                OrientationUpdated();
            }
        }
        void OrientationUpdated()
        {
            
        }
    }
}
