﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
namespace WarpedAbominationEngine
{
    public enum MouseKeys
    {
        Left, Middle, Right
    };

    public static class XnaControlsSystem
    {

        public const int TotalKeyboardKeys = 256;
        static bool[] keyboardState0 = new bool[TotalKeyboardKeys];
        static bool[] keyboardState1 = new bool[TotalKeyboardKeys];
        static KeyBinding[] keyBindings = new KeyBinding[TotalKeyboardKeys];
        public static readonly HashSet<Keys> KeysDown = new HashSet<Keys>();
        public static readonly HashSet<Keys> KeysPressed = new HashSet<Keys>();

        static bool stateSelector = false;

        static MouseState oldMouseState;
        static MouseState mouseState;

        static bool wasActive = false;
        static char pressedCharacter = '\0';

        static bool fixMouse = false;
        static Point2 mouseOffCenter = new Point2(0, 0);
        static Vector2 mouseOffCenterRatio = new Vector2(0, 0);

        static XnaControlsSystem()
        {
            InitBindings(new KeyBinding[]
                {   new KeyBinding(Keys.OemPipe, '\\', '|', '\0', '\0'),
                    new KeyBinding(Keys.OemBackslash, '\\', '|', '\0', '\0'),
                    new KeyBinding(Keys.OemOpenBrackets, '[', '{', '\0', '\0'),
                    new KeyBinding(Keys.OemCloseBrackets, ']', '}', '\0', '\0'),
                    new KeyBinding(Keys.OemSemicolon, ';', ':', '\0', '\0'),
                    new KeyBinding(Keys.OemPlus, '=', '+', '\0', '\0'),
                    new KeyBinding(Keys.OemTilde, '`', '~', '\0', '\0'),
                    new KeyBinding(Keys.OemQuotes, '\'', '"', '\0', '\0'),
                    new KeyBinding(Keys.OemQuestion, '/', '?', '\0', '\0'),
                    new KeyBinding(Keys.OemComma, ',', '<', '\0', '\0'),
                    new KeyBinding(Keys.OemPeriod, '.', '>', '\0', '\0'),
                    new KeyBinding(Keys.Decimal, '.', '\0', '\0', '\0'),
                    new KeyBinding(Keys.OemMinus, '-', '_', '\0', '\0'),
                    new KeyBinding(Keys.Space, ' ', '\0', '\0', '\0'),
                    new KeyBinding(Keys.Tab, '\t', '\0', '\0', '\0'),
                    new KeyBinding(Keys.D1, '1', '!', '\0', '\0'),
                    new KeyBinding(Keys.D2, '2', '@', '\0', '\0'),
                    new KeyBinding(Keys.D3, '3', '#', '\0', '\0'),
                    new KeyBinding(Keys.D4, '4', '$', '\0', '\0'),
                    new KeyBinding(Keys.D5, '5', '%', '\0', '\0'),
                    new KeyBinding(Keys.D6, '6', '^', '\0', '\0'),
                    new KeyBinding(Keys.D7, '7', '&', '\0', '\0'),
                    new KeyBinding(Keys.D8, '8', '*', '\0', '\0'),
                    new KeyBinding(Keys.D9, '9', '(', '\0', '\0'),
                    new KeyBinding(Keys.D0, '0', ')', '\0', '\0'),
                    new KeyBinding(Keys.NumPad1, '1', '!', '\0', '\0'),
                    new KeyBinding(Keys.NumPad2, '2', '@', '\0', '\0'),
                    new KeyBinding(Keys.NumPad3, '3', '#', '\0', '\0'),
                    new KeyBinding(Keys.NumPad4, '4', '$', '\0', '\0'),
                    new KeyBinding(Keys.NumPad5, '5', '%', '\0', '\0'),
                    new KeyBinding(Keys.NumPad6, '6', '^', '\0', '\0'),
                    new KeyBinding(Keys.NumPad7, '7', '&', '\0', '\0'),
                    new KeyBinding(Keys.NumPad8, '8', '*', '\0', '\0'),
                    new KeyBinding(Keys.NumPad9, '9', '(', '\0', '\0'),
                    new KeyBinding(Keys.NumPad0, '0', ')', '\0', '\0'),
                    new KeyBinding(Keys.A, 'a', 'A', '\0', '\0'),
                    new KeyBinding(Keys.B, 'b', 'B', '\0', '\0'),
                    new KeyBinding(Keys.C, 'c', 'C', '\0', '\0'),
                    new KeyBinding(Keys.D, 'd', 'D', '\0', '\0'),
                    new KeyBinding(Keys.E, 'e', 'E', '\0', '\0'),
                    new KeyBinding(Keys.F, 'f', 'F', '\0', '\0'),
                    new KeyBinding(Keys.G, 'g', 'G', '\0', '\0'),
                    new KeyBinding(Keys.H, 'h', 'H', '\0', '\0'),
                    new KeyBinding(Keys.I, 'i', 'I', '\0', '\0'),
                    new KeyBinding(Keys.J, 'j', 'J', '\0', '\0'),
                    new KeyBinding(Keys.K, 'k', 'K', '\0', '\0'),
                    new KeyBinding(Keys.L, 'l', 'L', '\0', '\0'),
                    new KeyBinding(Keys.M, 'm', 'M', '\0', '\0'),
                    new KeyBinding(Keys.N, 'n', 'N', '\0', '\0'),
                    new KeyBinding(Keys.O, 'o', 'O', '\0', '\0'),
                    new KeyBinding(Keys.P, 'p', 'P', '\0', '\0'),
                    new KeyBinding(Keys.Q, 'q', 'Q', '\0', '\0'),
                    new KeyBinding(Keys.R, 'r', 'R', '\0', '\0'),
                    new KeyBinding(Keys.S, 's', 'S', '\0', '\0'),
                    new KeyBinding(Keys.T, 't', 'T', '\0', '\0'),
                    new KeyBinding(Keys.U, 'u', 'U', '\0', '\0'),
                    new KeyBinding(Keys.V, 'v', 'V', '\0', '\0'),
                    new KeyBinding(Keys.W, 'w', 'W', '\0', '\0'),
                    new KeyBinding(Keys.X, 'x', 'X', '\0', '\0'),
                    new KeyBinding(Keys.Y, 'y', 'Y', '\0', '\0'),
                    new KeyBinding(Keys.Z, 'z', 'Z', '\0', '\0'),
                    new KeyBinding(Keys.Back, '\b', '\b', '\b', '\b'),
                    new KeyBinding(Keys.Enter, '\b', '\b', '\b', '\b')
                });
        }

        public static Point2 MouseOffCenter
        {
            get { return mouseOffCenter; }
        }

        public static Vector2 MouseOffCenterRatio
        {
            get { return mouseOffCenterRatio; }
        }

        public static Point2 MousePosition
        {
            get { return new Point2(mouseState.Position.X, mouseState.Position.Y); }
            set { Mouse.SetPosition(value.X, value.Y); }
        }

        public static char KeyToChar(Keys key)
        {
            KeyBinding binding = keyBindings[(int)key];

            if (binding.Key == key)
            {
                if (Console.CapsLock || IsKeyDown(Keys.RightShift) || IsKeyDown(Keys.LeftShift))
                    return binding.Shift;
                else
                    return binding.Default;
            }
            return '\0';
        }

        public static bool MouseFixed
        {
            get
            {
                return fixMouse;
            }

            set
            {
                if (fixMouse == value)
                    return;
                fixMouse = value;
                if (value)
                {
                    ResetMouseToCenter();
                    mouseOffCenter = new Point2(0, 0);
                }
            }
        }

        static void ResetMouseToCenter()
        {
            MousePosition = XnaSystem.ScreenSize / 2;
        }

        public static void Update(float time)
        {
            oldMouseState = mouseState;
            mouseState = Mouse.GetState();
            KeyboardState keyState = Keyboard.GetState();
            bool[] currState;
            bool[] prevState;

            stateSelector = !stateSelector;
            if (stateSelector)
            {
                currState = keyboardState1;
                prevState = keyboardState0;
            }
            else
            {
                currState = keyboardState0;
                prevState = keyboardState1;
            }

            KeysDown.Clear();
            KeysPressed.Clear();
            if (XnaSystem.GameWindow.IsActive)
            {

                // Keyboard update
                foreach (Keys key in Keyboard.GetState().GetPressedKeys())
                    KeysDown.Add(key);


                for (int i = 0; i < TotalKeyboardKeys; i++)
                {
                    prevState[i] = currState[i];
                    currState[i] = KeysDown.Contains((Keys)i);
                    if (!prevState[i] && currState[i])
                    {
                        KeysPressed.Add((Keys)i);
                    }
                }

                pressedCharacter = '\0';
                bool isCharacterPressed = false;
                foreach (Keys key in KeysPressed)
                {
                    char c = KeyToChar(key);
                    if (c != '\0')
                    {
                        if (isCharacterPressed)
                            pressedCharacter = '\0';
                        else
                        {
                            pressedCharacter = c;
                            isCharacterPressed = true;
                        }
                    }
                }

                // Mouse update
                oldMouseState = mouseState;
                mouseState = Mouse.GetState();

                if (wasActive)
                {
                    Point2 screenSize = XnaSystem.ScreenSize;
                    mouseOffCenter = MousePosition - screenSize / 2;
                    float aspect = (float)screenSize.X / (float)screenSize.Y;
                    mouseOffCenterRatio = new Vector2(
                        (float)mouseOffCenter.X / (float)screenSize.Y * 2f,
                        (float)mouseOffCenter.Y / (float)screenSize.Y * 2f
                        );
                }
                else
                    wasActive = true;
                if (fixMouse)
                    ResetMouseToCenter();
            }
            else
            {
                wasActive = false;
                mouseOffCenter = new Point2(0, 0);
            }
        }

        static bool[] CurrentKeyState
        {
            get
            {
                if (stateSelector)
                    return keyboardState1;
                return keyboardState0;
            }
        }

        static bool[] PrevKeyState
        {
            get
            {
                if (stateSelector)
                    return keyboardState0;
                return keyboardState1;
            }
        }

        public static char PressedCharacter
        {
            get { return pressedCharacter; }
        }

        public static bool IsKeyPressed(Keys key)
        {
            return CurrentKeyState[(int)key] && !PrevKeyState[(int)key];
        }

        public static bool IsKeyReleased(Keys key)
        {
            return !CurrentKeyState[(int)key] && PrevKeyState[(int)key];
        }

        public static bool IsKeyDown(Keys key)
        {
            return CurrentKeyState[(int)key];
        }

        internal static void InitBindings(KeyBinding[] bindings)
        {
            for (int i = 0; i < bindings.Length; i++)
                keyBindings[(int)bindings[i].Key] = bindings[i];
        }

        public static bool IsMouseButtonDown(MouseKeys mouseKey)
        {
            switch (mouseKey)
            {
                case MouseKeys.Left:
                    return mouseState.LeftButton == ButtonState.Pressed;
                case MouseKeys.Right:
                    return mouseState.RightButton == ButtonState.Pressed;
                case MouseKeys.Middle:
                    return mouseState.MiddleButton == ButtonState.Pressed;
                default:
                    return false;
            }
        }

        public static bool IsMouseButtonPressed(MouseKeys mouseKey)
        {
            switch (mouseKey)
            {
                case MouseKeys.Left:
                    return mouseState.LeftButton == ButtonState.Pressed && oldMouseState.LeftButton == ButtonState.Released;
                case MouseKeys.Right:
                    return mouseState.RightButton == ButtonState.Pressed && oldMouseState.RightButton == ButtonState.Released;
                case MouseKeys.Middle:
                    return mouseState.MiddleButton == ButtonState.Pressed && oldMouseState.MiddleButton == ButtonState.Released;
                default:
                    return false;
            }
        }

        public static bool IsMouseButtonReleased(MouseKeys mouseKey)
        {
            switch (mouseKey)
            {
                case MouseKeys.Left:
                    return mouseState.LeftButton == ButtonState.Released && oldMouseState.LeftButton == ButtonState.Pressed;
                case MouseKeys.Right:
                    return mouseState.RightButton == ButtonState.Released && oldMouseState.RightButton == ButtonState.Pressed;
                case MouseKeys.Middle:
                    return mouseState.MiddleButton == ButtonState.Released && oldMouseState.MiddleButton == ButtonState.Pressed;
                default:
                    return false;
            }
        }

        public static int ScrollDelta()
        {
            return mouseState.ScrollWheelValue - oldMouseState.ScrollWheelValue;
        }
    }
}
