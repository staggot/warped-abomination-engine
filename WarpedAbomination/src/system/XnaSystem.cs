﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using qsldotnet.lib;
using System.IO;

namespace WarpedAbominationEngine
{

    public static class XnaSystem
    {
        static bool inited = false;
        static bool created = false;

        public static GameWindow GameWindow;
        public static GraphicsDevice GraphicsDevice;
        public static GraphicsDeviceManager GraphicsManager;
        public static SpriteBatch SpriteBatch;

        public static float Time = 0;

        public static void Create(GameWindow window)
        {
            if (created)
                return;
            created = true;
            GameWindow = window;
            GraphicsManager = new GraphicsDeviceManager(GameWindow);
        }

        public static void Initialize()
        {
            if (inited)
                return;
            inited = true;
            GraphicsDevice = GameWindow.GraphicsDevice;
            SpriteBatch = new SpriteBatch(GraphicsDevice);

        }

        public static void SaveTexture(Texture2D texture, string path, bool jpeg = false)
        {
            using (FileStream fs = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                if (jpeg)
                    texture.SaveAsJpeg(fs, texture.Width, texture.Height);
                else
                    texture.SaveAsPng(fs, texture.Width, texture.Height);
            }
        }

        public static Point2 ScreenSize
        {
            get 
            { 
                return new Point2(GraphicsManager.PreferredBackBufferWidth,
                                  GraphicsManager.PreferredBackBufferHeight
                                 ); }
        }

        public static bool IsFullscreen
        {
            get { return GraphicsManager.IsFullScreen; }
        }

        public static void SetScreenSize(Point2 size, bool fullscreen)
        {
            if (ScreenSize == size && (GraphicsManager.IsFullScreen == fullscreen))
                return;
            GraphicsManager.PreferredBackBufferWidth = size.X;
            GraphicsManager.PreferredBackBufferHeight = size.Y;
            GraphicsManager.IsFullScreen = fullscreen;
            GraphicsManager.ApplyChanges();
        }

        public static void SetScreenSize(Point2 size)
        {
            if (ScreenSize == size)
                return;
            GraphicsManager.PreferredBackBufferWidth = size.X;
            GraphicsManager.PreferredBackBufferHeight = size.Y;
            GraphicsManager.ApplyChanges();
        }



        public static RenderTarget2D ResizeRenderTarget(RenderTarget2D old, Point2 size, SurfaceFormat format, DepthFormat depthStencilFormat)
        {
            if (old != null)
            {
                if (old.Width != size.X ||
                old.Height != size.Y ||
                old.DepthStencilFormat != depthStencilFormat ||
                old.Format != format)
                    old.Dispose();
                else
                    return old;
            }
            return new RenderTarget2D(
                GraphicsDevice,
                size.X,
                size.Y,
                false,
                format,
                depthStencilFormat
            );
        }

        public static void Update(float time)
        {
            Time += time;
        }
    }
}