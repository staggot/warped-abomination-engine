﻿using System;
using Microsoft.Xna.Framework.Input;
namespace WarpedAbominationEngine
{
    public struct KeyBinding
    {
        public Keys Key;
        public char Default;
        public char Shift;
        public char Alt;
        public char ShiftAlt;

        public KeyBinding(Keys key, char def, char shift, char alt, char shiftAlt)
        {
            this.Key = key;
            this.Default = def;
            this.Shift = shift;
            this.Alt = alt;
            this.ShiftAlt = shiftAlt;
        }
    }
}
