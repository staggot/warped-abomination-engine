TerrainVertexOutput TerrainVS(in TerrainVertexInput input)
{
	TerrainVertexOutput output = (TerrainVertexOutput)0;
	ScreenWorld sw = TransformOnScreenWorld(input.Position);
  output.Position = sw.ScreenPosition;
  output.WorldPosition = sw.WorldPosition;
	output.TextureCoordinate = input.TextureCoordinate;
  output.Color = input.Color;
	output.Normal = input.Normal;
	return output;
}

float4 TerrainPS(TerrainVertexOutput input) : COLOR0
{
	float4 pixel = GetTerrainDiffuse(input.TextureCoordinate) * input.Color;
	return Fog(pixel, input.WorldPosition);
}
