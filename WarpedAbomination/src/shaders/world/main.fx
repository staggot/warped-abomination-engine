#if OPENGL
#define SV_POSITION POSITION
#define VS_SHADERMODEL vs_3_0
#define PS_SHADERMODEL ps_3_0
#else
#define VS_SHADERMODEL vs_4_0_level_9_1
#define PS_SHADERMODEL ps_4_0_level_9_1
#endif

#include "references.fx"
#include "vertices.fx"
#include "functions.fx"
#include "terrain.fx"
#include "portal.fx"
#include "sprite.fx"

technique WorldTech
{
  pass Terrain
	{
    VertexShader = compile VS_SHADERMODEL TerrainVS();
		PixelShader = compile PS_SHADERMODEL TerrainPS();
  }

  pass StencilPortal
	{
    VertexShader = compile VS_SHADERMODEL StencilPortalVS();
		PixelShader = compile PS_SHADERMODEL StencilPortalPS();
  }

  pass Portal
	{
    VertexShader = compile VS_SHADERMODEL PortalVS();
		PixelShader = compile PS_SHADERMODEL PortalPS();
  }

  pass Sprite
  {
    VertexShader = compile VS_SHADERMODEL SpriteVS();
    PixelShader = compile PS_SHADERMODEL SpritePS();
  }

}
