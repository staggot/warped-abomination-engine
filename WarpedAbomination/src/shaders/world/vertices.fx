struct TerrainVertexInput
{
	float4 Position : POSITION0;
	float4 Color : COLOR0;
	float2 TextureCoordinate : TEXCOORD0;
	float3 Normal : NORMAL0;
};

struct TerrainVertexOutput
{
	float4 Position : POSITION0;
	float4 Color : TEXCOORD0;
	float2 TextureCoordinate : TEXCOORD1;
	float3 WorldPosition : TEXCOORD2;
	float3 Normal : TEXCOORD3;
};

struct SpriteVertexInput
{
	float4 Position : POSITION0;
	float4 Color : COLOR0;
	float2 TextureCoordinate : TEXCOORD0;
	float3 Normal : NORMAL0;
};

struct SpriteVertexOutput
{
	float4 Position : POSITION0;
	float4 Color : TEXCOORD0;
	float2 TextureCoordinate : TEXCOORD1;
	float3 WorldPosition : TEXCOORD2;
	float3 Normal : TEXCOORD3;
};

struct StencilPortalVertex
{
	float4 Position : POSITION0;
};

struct PortalVertexInput
{
	float4 Position : POSITION0;
	float4 Color : COLOR0;
	float2 TextureCoordinate : TEXCOORD0;
	float2 TextureTL : TEXCOORD1;
	float2 TextureTR : TEXCOORD2;
	float2 TextureBL : TEXCOORD3;
	float2 TextureBR : TEXCOORD4;
	float2 Wobble : TEXCOORD5;
	float3 Normal : NORMAL0;
};

struct PortalVertexOutput
{
	float4 Position : POSITION0;
	float2 TextureCoordinate : TEXCOORD0;
	float2 TextureTL : TEXCOORD1;
	float2 TextureTR : TEXCOORD2;
	float2 TextureBL : TEXCOORD3;
	float2 TextureBR : TEXCOORD4;
	float4 Color : TEXCOORD5;
	float2 Wobble : TEXCOORD6;
	float3 Normal : TEXCOORD7;
	float3 WorldPosition : TEXCOORD8;
};
