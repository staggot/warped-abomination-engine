float4 StencilPortalPS(StencilPortalVertex input) : COLOR0
{
  return float4(0,0,0,0);
}

StencilPortalVertex StencilPortalVS(in StencilPortalVertex input)
{
	StencilPortalVertex output = (StencilPortalVertex)0;
	output.Position = TransformOnScreen(input.Position);
	return output;
}

PortalVertexOutput PortalVS(in PortalVertexInput input)
{
  PortalVertexOutput output = (PortalVertexOutput)0;
  ScreenWorld sw = TransformOnScreenWorld(input.Position);
  output.Position = sw.ScreenPosition;
  output.WorldPosition = sw.WorldPosition;
  output.TextureCoordinate = input.TextureCoordinate;
  output.Color = input.Color;
  output.TextureTL = input.TextureTL;
  output.TextureTR = input.TextureTR;
  output.TextureBL = input.TextureBL;
  output.TextureBR = input.TextureBR;
  output.Wobble = input.Wobble;
  output.Normal = input.Normal;
  return output;
}

float4 PortalPS(PortalVertexOutput input) : COLOR0
{
  float2 uv = input.TextureCoordinate;
  uv.x %= 1;
  uv.y %= 1;
  float topw = input.TextureTR.x - input.TextureTL.x;
  float bottomw = input.TextureBR.x - input.TextureBL.x;
  float lefth = input.TextureBL.y - input.TextureTL.y;
  float righth = input.TextureBR.y - input.TextureTR.y;
  float width = (topw) * (1 - uv.y) + (bottomw) * uv.y;
  float height = (lefth) * (1 - uv.x) + (righth) * uv.x;
  uv = input.TextureTL + uv * float2(width, height);
  float4 pixel = GetPortalDiffuse(uv) * input.Color;
  return Fog(pixel, input.WorldPosition);
}
