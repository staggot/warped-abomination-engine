#define pi 3.14159265359

float4x4 World;
float4x4 View;
float4x4 Projection;

float Time;
float3 CameraPosition;
float3 CameraDirection;
float3 SectorSize;
float4 FogColor;
float FogStart;
float FogEnd;


sampler TerrainDiffuseSampler : register(s0)
{
	Texture = <TerrainDiffuse>;
};
sampler TerrainNormalSampler : register(s1)
{
	Texture = <TerrainNormal>;
};
sampler TerrainMaterialSampler : register(s2)
{
	Texture = <TerrainMaterial>;
};

sampler StaticDiffuseSampler : register(s3)
{
	Texture = <StaticDiffuse>;
};
sampler StaticNormalSampler : register(s4)
{
	Texture = <StaticNormal>;
};
sampler StaticMaterialSampler : register(s5)
{
	Texture = <StaticMaterial>;
};

sampler SpriteDiffuseSampler : register(s6)
{
	Texture = <SpriteDiffuse>;
};
sampler SpriteNormalSampler : register(s7)
{
	Texture = <SpriteNormal>;
};
sampler SpriteMaterialSampler : register(s8)
{
	Texture = <SpriteMaterial>;
};

sampler PortalDiffuseSampler : register(s9)
{
	Texture = <PortalDiffuse>;
};
sampler PortalNormalSampler : register(s10)
{
	Texture = <PortalNormal>;
};
sampler PortalMaterialSampler : register(s11)
{
	Texture = <PortalMaterial>;
};


float4 GetTerrainDiffuse(float2 uv)
{
	return tex2D(TerrainDiffuseSampler,uv);
}
float4 GetTerrainNormal(float2 uv)
{
	return tex2D(TerrainNormalSampler,uv);
}
float4 GetTerrainMaterial(float2 uv)
{
	return tex2D(TerrainMaterialSampler,uv);
}

float4 GetPortalDiffuse(float2 uv)
{
	return tex2D(PortalDiffuseSampler, uv);
}
float4 GetPortalNormal(float2 uv)
{
	return tex2D(PortalNormalSampler, uv);
}
float4 GetPortalMaterial(float2 uv)
{
	return tex2D(PortalMaterialSampler, uv);
}

float4 GetSpriteDiffuse(float2 uv)
{
	return tex2D(SpriteDiffuseSampler, uv);
}
float4 GetSpriteNormal(float2 uv)
{
	return tex2D(SpriteNormalSampler, uv);
}
float4 GetSpriteMaterial(float2 uv)
{
	return tex2D(SpriteMaterialSampler, uv);
}
