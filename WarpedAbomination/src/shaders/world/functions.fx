struct ScreenWorld
{
	float4 ScreenPosition;
	float3 WorldPosition;
};

float4 TransformOnScreen(float4 position)
{
  float4 worldPosition = mul(position, World);
	float4 viewPosition = mul(worldPosition, View);
	return mul(viewPosition, Projection);
}

ScreenWorld TransformOnScreenWorld(float4 position)
{
	ScreenWorld result = (ScreenWorld)0;
  float4 worldPosition = mul(position, World);
	result.WorldPosition = worldPosition.xyz;
	float4 viewPosition = mul(worldPosition, View);
	result.ScreenPosition = mul(viewPosition, Projection);
	return result;
}

float3 ColorToNormal(float3 color)
{
	return color * 2 - float3(1,1,1);
}

float3 NormalToColor(float3 normal)
{
	return normal / 2 + float3(0.5,0.5,0.5);
}

float4 Fog(float4 color, float3 position)
{
	return lerp(color, FogColor, clamp((distance(CameraPosition, position) - FogStart) / (FogEnd - FogStart), 0, 1));
}

/*
float3 HSB2RGB(float3 c)
{
    float rgb = clamp(abs(fmod(c.x * 6.0 + float3(0.0, 4.0, 2.0), 6.0)-3.0)-1.0, 0.0, 1.0);
    rgb = rgb * rgb * (3.0 - 2.0 * rgb);
    return c.z * mix(float3(1.0, 1.0, 1.0), rgb, c.y);
}*/
