﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace WarpedAbominationEngine
{
    public class Perlin
    {
        const int GradientSizeTable = 256;
        const int GradientMask = GradientSizeTable - 1;

        byte[] permutationTable;

        Random rnd;
        Vector3[] gradients = new Vector3[GradientSizeTable];
        public float Frequency = 1;
        public int Octaves = 8;
        public float Persistance = 0.5f;
        public readonly int Seed;

        public Perlin() : this((int)DateTime.UtcNow.Ticks)
        {
        }

        public Perlin(int seed)
        {
            this.Seed = seed;
            rnd = new Random(seed);
            InitPermutationTable();
            InitGradients();
        }

        void InitPermutationTable()
        {
            List<byte> list = new List<byte>();
            permutationTable = new byte[GradientSizeTable];
            for (int i = 0; i < permutationTable.Length; i++)
                list.Add((byte)i);

            for (int i = 0; i < permutationTable.Length; i++)
            {
                int index = rnd.Next() % list.Count;
                permutationTable[i] = list[index];
                list.RemoveAt(index);
            }
        }

        void InitGradients()
        {
            for (int i = 0; i < GradientSizeTable; i++)
            {
                float z = 1f - 2f * (float)rnd.NextDouble();
                float r = (float)Math.Sqrt(1f - z * z);
                double theta = 2 * Math.PI * (float)rnd.NextDouble();
                gradients[i] = new Vector3
                    (
                        r * (float)Math.Cos(theta),
                        r * (float)Math.Sin(theta),
                        z
                    );
            }
        }

        public float CeiledNoise(Vector3 pos, Vector2 minMax)
        {
            return CeiledNoise(pos.X, pos.Y, pos.Z, minMax.X, minMax.Y);
        }

        public float CeiledNoise(Vector3 pos, float min, float max)
        {
            return CeiledNoise(pos.X, pos.Y, pos.Z, min, max);
        }

        public float CeiledNoize(float x, float y, float z, Vector2 minMax)
        {
            return CeiledNoise(x, y, z, minMax.X, minMax.Y);
        }

        public float CeiledNoise(float x, float y, float z, float min, float max)
        {
            float result = Noise(x, y, z) + 0.5f;
            return Lerp(result, min, max);
        }

        public float Noise(float x, float y, float z)
        {
            float total = 0;
            float currentFrequency = Frequency;
            float amplitude = 1;
            float maxValue = 0;
            for (int i = 0; i < Octaves; i++)
            {
                total += InterpolatedNoise(x * currentFrequency, y * currentFrequency, z * currentFrequency) * amplitude;
                maxValue += amplitude;
                amplitude *= Persistance;
                currentFrequency *= 2;
            }
            return total / maxValue;
        }

        public float InterpolatedNoise(float x, float y, float z)
        {
            int ix = (int)Math.Floor(x);

            float fx0 = x - ix;
            float fx1 = fx0 - 1;
            float wx = Smooth(fx0);

            int iy = (int)Math.Floor(y);
            float fy0 = y - iy;
            float fy1 = fy0 - 1;
            float wy = Smooth(fy0);

            int iz = (int)Math.Floor(z);
            float fz0 = z - iz;
            float fz1 = fz0 - 1;
            float wz = Smooth(fz0);

            float vx0 = Lattice(ix, iy, iz, fx0, fy0, fz0);
            float vx1 = Lattice(ix + 1, iy, iz, fx1, fy0, fz0);
            float vy0 = Lerp(wx, vx0, vx1);

            vx0 = Lattice(ix, iy + 1, iz, fx0, fy1, fz0);
            vx1 = Lattice(ix + 1, iy + 1, iz, fx1, fy1, fz0);
            float vy1 = Lerp(wx, vx0, vx1);

            float vz0 = Lerp(wy, vy0, vy1);

            vx0 = Lattice(ix, iy, iz + 1, fx0, fy0, fz1);
            vx1 = Lattice(ix + 1, iy, iz + 1, fx1, fy0, fz1);
            vy0 = Lerp(wx, vx0, vx1);

            vx0 = Lattice(ix, iy + 1, iz + 1, fx0, fy1, fz1);
            vx1 = Lattice(ix + 1, iy + 1, iz + 1, fx1, fy1, fz1);
            vy1 = Lerp(wx, vx0, vx1);

            float vz1 = Lerp(wy, vy0, vy1);
            return Lerp(wz, vz0, vz1);
        }

        int Permutate(int x)
        {
            return permutationTable[x & GradientMask];
        }

        int Index(int ix, int iy, int iz)
        {
            return Permutate(ix + Permutate(iy + Permutate(iz)));
        }

        float Lattice(int ix, int iy, int iz, float fx, float fy, float fz)
        {
            int index = Index(ix, iy, iz);
            return gradients[index].X * fx + gradients[index].Y * fy + gradients[index].Z * fz;
        }

        static float Smooth(float x)
        {
            return x;
        }

        static float Lerp(float t, float a, float b)
        {
            return a + (b - a) * t;
        }
    }
}
