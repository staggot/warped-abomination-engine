﻿using System;
using System.Collections;
using System.Collections.Generic;
using WarpedAbominationEngine.Serialization;

namespace WarpedAbominationEngine.Configuration 
{
    public class ConfigSection
    {
        public EngineConfig Config { get; private set; }
        public string Name { get; private set; }

        internal Dictionary<string, SerializationType> expectedTypes = new Dictionary<string, SerializationType>();
        internal Dictionary<string, object> values = new Dictionary<string, object>();
        internal Dictionary<string, object> defaultValues = new Dictionary<string, object>();

        public IEnumerable<string> ChangedVariables => values.Keys;
        public IEnumerable<string> AllVariables => values.Keys;


        internal ConfigSection(EngineConfig config, string name)
        {
            this.Config = config;
            this.Name = name;
        }

        public object this[string name]
        {
            get
            {
                if (values.ContainsKey(name))
                    return values[name];
                if (defaultValues.ContainsKey(name))
                    return defaultValues[name];
                return null;
            }
            set
            {
                if (!EngineConfig.IsValidName(name))
                    throw new InvalidNameException(name, "variable", EngineConfig.NamePattern);
                object newValue = value;
                if (expectedTypes.ContainsKey(name))
                {
                    newValue = AbominationSerialization.Convert(value, expectedTypes[name].Type);
                    if(defaultValues.ContainsKey(name) && newValue == defaultValues[name])
                        values.Remove(name);
                }
                SerializationType type = AbominationSerialization.DefineObjectType(newValue);
                if (type == null)
                    throw new UnsupportedSerializationTypeException(newValue.GetType());
                values[name] = value;
            }
        }

        public void AddVariable(string name, string type)
        {
            if (!EngineConfig.IsValidName(name))
                throw new InvalidNameException(name, "variable", EngineConfig.NamePattern);
            if (expectedTypes.ContainsKey(name))
                throw new DuplicateVariableNameException(this.Config, this.Name, name);
            SerializationType sType = AbominationSerialization.GetSerializationType(type);
            expectedTypes[name] = sType;
        }

        public void AddVariable(string name, string type, object defaultValue = null)
        {
            if (!EngineConfig.IsValidName(name))
                throw new InvalidNameException(name, "variable", EngineConfig.NamePattern);
            AddVariable(name, type);
            defaultValues[name] = AbominationSerialization.Convert(defaultValue, expectedTypes[name].Type);
        }
    }
}
