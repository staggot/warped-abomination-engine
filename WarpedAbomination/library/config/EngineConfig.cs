﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace WarpedAbominationEngine.Configuration
{
    public class EngineConfig
    {
        internal const string NamePattern = "^[a-zA-Z0-9_.]+$";
        internal static Regex NameRegex = new Regex(NamePattern, RegexOptions.Compiled);

        Dictionary<string, ConfigSection> sections = new Dictionary<string, ConfigSection>();
        public const string DefaultSection = ".";

        public IEnumerable<string> SectionsNames => sections.Keys;
        public IEnumerable<ConfigSection> Sections => sections.Values;

        public EngineConfig()
        {
            CreateSection(DefaultSection);
        }

        public ConfigSection CreateSection(string name)
        {
            if (!IsValidName(name))
                throw new InvalidNameException(name, "section", NamePattern);
            if (sections.ContainsKey(name))
                throw new DuplicateSectionNameException(this, name);

            ConfigSection section = new ConfigSection(this, name);
            return section;
        }

        public ConfigSection this[string name]
        {
            get { return sections[name]; }
        }

        public object this[string section, string variable]
        {
            get
            {
                return this[section][variable];
            }
            set
            {
                
                this[section][variable] = value;
            }
        }

        public static bool IsValidName(string name)
        {
            return NameRegex.IsMatch(name);
        }
    }
}
