﻿using System;
using System.IO;
using System.Text.RegularExpressions;

namespace WarpedAbominationEngine.Configuration
{
    public class ConfigReader
    {
        readonly static Regex SectionRegex = new Regex(@"^\s*\[([0-9a-zA-Z._]+)\]\s*(#.*)?$", RegexOptions.IgnoreCase |  RegexOptions.ECMAScript | RegexOptions.Compiled);

        readonly StreamReader reader;

        public ConfigReader(Stream stream)
        {
            reader = new StreamReader(stream);
        }

        public void ReadToConfig(EngineConfig config)
        {
            string currentSection = EngineConfig.DefaultSection;
            while(!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                Match sectionRegexMatch = SectionRegex.Match(line);
                if (sectionRegexMatch != null && sectionRegexMatch.Success)
                {
                    currentSection = sectionRegexMatch.Groups[1].Value;
                }
                else
                {

                }
             }
        }

        bool ParseLine(string line, out string name, out string value)
        { name = ""; value = null; return true; }
    }
}
