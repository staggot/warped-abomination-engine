﻿using System;
namespace WarpedAbominationEngine.Configuration
{
    public class DuplicateSectionNameException : Exception
    {

        public string Name { get; private set; }
        public EngineConfig Config { get; private set; }
        public DuplicateSectionNameException(EngineConfig config, string name) : base(string.Format(
            "Section '{0}' already exists in config", name))
        {
            this.Config = config;
            this.Name = name;
        }
    }

    public class InvalidNameException : Exception
    {
        public string Name { get; private set; }
        public string Target { get; private set; }
        public string Pattern { get; private set; }
        public InvalidNameException(string name, string target, string pattern) : base(string.Format(
            "Invalid value '{0}' for {1} name, use '{2}' pattern", name, target, pattern))
        {
            this.Pattern = pattern;
            this.Target = target;
            this.Name = name;
        }
    }


    public class DuplicateVariableNameException : Exception
    {
        public string Section { get; private set; }
        public string Name { get; private set; }
        public EngineConfig Config { get; private set; }
        public DuplicateVariableNameException(EngineConfig config, string section, string name) : base(string.Format(
            "Variable '{0}' already exists in section '{1}'", name, section))
        {
            this.Config = config;
            this.Section = section;
            this.Name = name;
        }
    }
}
