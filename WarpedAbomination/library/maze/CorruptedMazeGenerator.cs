﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WarpedAbominationEngine
{
    public class CorruptedMazeGenerator
    {
        public readonly Point2 Size;
        public readonly int Seed;
        public int WarpLevels = 1;
        public int RoomSizeMin = 4;
        public int RoomSizeMax = 10;
        public int MinRoomsIntersectFactor = 8;

        public float WarpRatio = 0.2f;
        public float RoomRatio = 0.8f;

        internal List<Point3> warps = new List<Point3>();

        internal Dictionary<int, Point3[]> warpMap = new Dictionary<int, Point3[]>();

        internal int RoomTries
        {
            get { return Size.X * Size.Y * (RoomSizeMax - RoomSizeMin) * (RoomSizeMax - RoomSizeMin); }
        }

        internal int MaxRoomArea
        {
            get { return (int)(Size.X * Size.Y * RoomRatio); }
        }

        internal int WarpTries
        {
            get { return Size.X * Size.Y; }
        }

        internal int MaxWarps
        {
            get { return (int)(((Size.X * Size.Y * (1 - RoomRatio)) / 4) * WarpRatio); }
        }

        internal List<MazeRoom> Rooms;
        internal ushort FIndex = 0;
        internal readonly Random Rnd;

        MazeLayer[] mazes;

        public CorruptedMazeGenerator(Point2 size, int warps, int seed)
        {
            Size = size;
            this.WarpLevels = warps;
            this.Size = size;
            this.Seed = seed;
            Clear(true);
            Rnd = new Random(seed);
        }

        public MazeNode GetNode(Point3 point)
        {
            return mazes[point.Z].Data[point.X, point.Y];
        }

        public void Generate()
        {
            for (int i = 0; i < WarpLevels; i++)
            {
                mazes[i].GenerateRooms(RoomTries, MaxRoomArea, RoomSizeMin, RoomSizeMax);
                mazes[i].GenerateCoridors();
                mazes[i].RoomsHoling();
                mazes[i].SetWarps(WarpTries, MaxWarps);
                mazes[i].Cleanup();
            }
            IndexWarps();
            DestructFloors();
            MakeBridges();

        }



        void IndexWarps()
        {
            for (int i = 0; i < (warps.Count / 2); i++)
            {
                Point3 one = warps[i * 2];
                Point3 two = warps[i * 2 + 1];
                mazes[one.Z].Data[one.X, one.Y].Data = i;
                mazes[two.Z].Data[two.X, two.Y].Data = i;
                warpMap[i] = new[] { one, two };
            }
            if (warps.Count % 2 == 1)
            {
                Point3 last = warps[warps.Count - 1];
                mazes[last.Z].Data[last.X, last.Y].Type = MazeNodeType.Block;
            }
        }


        void MakeBridges(MazeRoom room)
        {
            if (!room.PenetratedDowm)
                return;
            HashSet<Point2> doorsNeedBridge = new HashSet<Point2>();
            List<Point2> doorsFine = new List<Point2>();
            MazeLayer maze = mazes[room.Warp];
            for (int x = room.Position.X - 1; x < room.Position.X + room.Size.X + 1; x++)
                for (int y = room.Position.Y - 1; y < room.Position.Y + room.Size.Y + 1; y++)
                    if (maze.Data[x, y].IsDoor)
                    {
                        foreach (Point2 offset in MazeLayer.Directions)
                        {
                            Point2 cur = new Point2(x, y) + offset;
                            if (!room.IsInside(cur.X, cur.Y))
                                continue;
                            if (maze.Data[cur.X, cur.Y].Type == MazeNodeType.RoomDown
                               )
                                doorsNeedBridge.Add(cur);
                            else if (maze.Data[cur.X, cur.Y].Type == MazeNodeType.Room ||
                                      maze.Data[cur.X, cur.Y].Type == MazeNodeType.RoomUp)
                                doorsFine.Add(cur);
                        }

                    }
            foreach (Point2 pos in doorsNeedBridge)
            {
                int minDist = int.MaxValue;
                maze.Data[pos.X, pos.Y].Type = MazeNodeType.RoomBridge;
                Point2 nearest = pos;
                if (doorsFine.Count > 0)
                {
                    nearest = new Point2(-1, -1);
                    foreach (Point2 fineDoor in doorsFine)
                    {
                        int dist = Math.Min(Math.Abs(pos.X - fineDoor.X), Math.Abs(pos.Y - fineDoor.Y));
                        if (dist < minDist)
                        {
                            nearest = fineDoor;
                            minDist = dist;
                        }
                    }
                }
                else
                {
                    foreach (Point2 targetDoor in doorsNeedBridge)
                    {
                        if (targetDoor == pos)
                            continue;
                        int dist = Math.Min(Math.Abs(pos.X - targetDoor.X), Math.Abs(pos.Y - targetDoor.Y));
                        if (dist < minDist)
                        {
                            nearest = targetDoor;
                            minDist = dist;
                        }
                    }
                }
                int deltaX = Math.Sign(nearest.X - pos.X);
                int deltaY = Math.Sign(nearest.Y - pos.Y);

                if (Rnd.Next() % 2 == 0)
                {
                    for (int y = pos.Y;
                        (y <= nearest.Y && deltaY > 0 ||
                         y >= nearest.Y && deltaY < 0) &&
                         deltaY != 0;
                        y += deltaY)
                        if (maze.Data[pos.X, y].Type != MazeNodeType.Room && maze.Data[pos.X, y].Type != MazeNodeType.RoomUp)
                            maze.Data[pos.X, y].Type = MazeNodeType.RoomBridge;
                        else
                            break;
                    for (
                        int x = pos.X;
                        (x <= nearest.X && deltaX > 0 ||
                        x >= nearest.X && deltaX < 0) &&
                        deltaX != 0;
                        x += deltaX)
                        if (maze.Data[x, nearest.Y].Type != MazeNodeType.Room && maze.Data[x, nearest.Y].Type != MazeNodeType.RoomUp)
                            maze.Data[x, nearest.Y].Type = MazeNodeType.RoomBridge;
                        else
                            break;
                }
                else
                {
                    for (int x = pos.X;
                         (x <= nearest.X && deltaX > 0 ||
                          x >= nearest.X && deltaX < 0) &&
                         deltaX != 0;
                         x += deltaX)
                        if (maze.Data[x, pos.Y].Type != MazeNodeType.Room && maze.Data[x, pos.Y].Type != MazeNodeType.RoomUp)
                            maze.Data[x, pos.Y].Type = MazeNodeType.RoomBridge;
                        else
                            break;
                    for (int y = pos.Y;
                         (y <= nearest.Y && deltaY > 0 ||
                          y >= nearest.Y && deltaY < 0) &&
                         deltaY != 0;
                         y += deltaY)
                        if (maze.Data[nearest.X, y].Type != MazeNodeType.Room && maze.Data[nearest.X, y].Type != MazeNodeType.RoomUp)
                            maze.Data[nearest.X, y].Type = MazeNodeType.RoomBridge;
                        else
                            break;
                }
            }
        }

        // TODO: Optimize this fucked up shit
        void DestructFloors()
        {
            for (int i = 0; i < WarpLevels; i++)
            {
                MazeLayer maze = mazes[i];
                for (int r = 0; r < maze.Rooms.Count; r++)
                {
                    MazeRoom room = maze.Rooms[r];
                    if (room.Penetrated)
                        continue;
                    int offsetMax = (i == WarpLevels - 1 ? 0 : 1);
                    int offsetMin = (i == 0 ? 0 : -1);
                    if (offsetMax - offsetMin == 0)
                        continue;

                    int offset = Math.Sign(offsetMin * 10 + Rnd.Next() % ((offsetMax - offsetMin) * 10));
                    if (offset == 0)
                        continue;
                    MazeLayer testMaze = mazes[i + offset];
                    foreach (MazeRoom testRoom in testMaze.Rooms)
                    {
                        if (testRoom.Penetrated)
                            continue;
                        int intersectFactor = room.Intersects(testRoom);
                        if (intersectFactor > MinRoomsIntersectFactor && intersectFactor > 0)
                        {
                            testRoom.Penetrated = true;
                            room.Penetrated = true;
                            if (offset > 0)
                                testRoom.PenetratedDowm = true;
                            else
                                room.PenetratedDowm = true;
                            for (int x = room.Position.X; x < room.Position.X + room.Size.X; x++)
                                for (int y = room.Position.Y; y < room.Position.Y + room.Size.Y; y++)
                                {
                                    if (testRoom.IsInside(x, y))
                                    {
                                        mazes[room.Warp].Data[x, y].Type = offset > 0 ?
                                            MazeNodeType.RoomUp :
                                            MazeNodeType.RoomDown;
                                        mazes[testRoom.Warp].Data[x, y].Type = offset > 0 ?
                                            MazeNodeType.RoomDown :
                                            MazeNodeType.RoomUp;
                                    }
                                }
                            break;
                        }
                    }
                }
            }
        }

        void MakeBridges()
        {
            foreach (MazeRoom room in Rooms)
            {
                MakeBridges(room);
            }
        }

        void Clear(bool filler)
        {
            Rooms = new List<MazeRoom>();
            mazes = new MazeLayer[WarpLevels];
            for (int i = 0; i < WarpLevels; i++)
            {
                mazes[i] = new MazeLayer(this, i);
                mazes[i].Clear(filler);
            }
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < WarpLevels; i++)
            {
                stringBuilder.Append(mazes[i].ToString());
                stringBuilder.Append('\n');
            }
            return stringBuilder.ToString();
        }
    }
}
