﻿using System;
namespace WarpedAbominationEngine
{
    public struct MazeNode
    {
        public static MazeNode Empty = new MazeNode() { Type = MazeNodeType.Free, Data = 0, Neighbours = 0 };
        public static MazeNode Fill = new MazeNode() { Type = MazeNodeType.Block, Data = 0, Neighbours = 0 };

        internal int Data;
        public MazeNodeType Type;
        public NeighbourFlags Neighbours;
        internal ushort Index;

        public bool Solid
        {
            get { return (int)Type > 127; }
        }

        public bool IsDoor
        {
            get { return Type == MazeNodeType.DoorVertical || Type == MazeNodeType.DoorHorisontal; }
        }

    }
}
