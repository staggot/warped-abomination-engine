﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WarpedAbominationEngine
{
    public partial class MazeLayer
    {
        public readonly Point2 Size;
        public readonly CorruptedMazeGenerator Maze;
        public readonly int WarpLevel;

        internal readonly Point2 AbstractSize;
        internal MazeNode[,] Data;
        internal List<MazeRoom> Rooms;
        int roomArea = 0;


        public MazeLayer(CorruptedMazeGenerator maze, int warpLevel)
        {
            this.Maze = maze;
            this.WarpLevel = warpLevel;
            Point2 size = maze.Size;
            AbstractSize = (size - Point2.One) / 2;
            Size = size;
        }

        internal void Clear(bool filler)
        {
            Rooms = new List<MazeRoom>();
            Data = new MazeNode[Size.X, Size.Y];
            for (int y = 0; y < Size.Y; y++)
                for (int x = 0; x < Size.X; x++)
                {
                    Data[x, y] = filler ? MazeNode.Fill : MazeNode.Empty;
                }
        }

        void PlaceRoom(MazeRoom room)
        {
            room.Index = Maze.FIndex;
            Rooms.Add(room);
            Maze.Rooms.Add(room);
            for (int y = room.Position.Y; y < room.Position.Y + room.Size.Y; y++)
                for (int x = room.Position.X; x < room.Position.X + room.Size.X; x++)
                    Data[x, y] = new MazeNode() { Type = MazeNodeType.Room, Index = Maze.FIndex };
            Maze.FIndex++;
        }

        internal void RoomsHoling()
        {
            HashSet<Point2> connectors = new HashSet<Point2>();
            for (int i = 0; i < Rooms.Count; i++)
            {
                Dictionary<int, List<Point2>> map = new Dictionary<int, List<Point2>>();
                MazeRoom room = Rooms[i];
                for (int x = room.Position.X - 1; x <= room.Position.X + room.Size.X; x++)
                    for (int y = room.Position.Y - 1; y <= room.Position.Y + room.Size.Y; y++)
                    {
                        if (x % 2 == 0 && y % 2 == 0)
                            continue;
                        if (Data[x, y].Type != MazeNodeType.Block ||
                            x == 0 ||
                            y == 0 ||
                            x == Size.X - 1 ||
                            y == Size.Y - 1 ||
                            (x == room.Position.X - 1 && (y == room.Position.Y - 1 || y == room.Position.Y + room.Size.Y)) ||
                            (x == room.Position.X + room.Size.X && (y == room.Position.Y - 1 || y == room.Position.Y + room.Size.Y)))
                            continue;
                        foreach (Point2 offset in Directions)
                        {
                            Point2 checkPos = new Point2(x, y) + offset;
                            if ((Data[checkPos.X, checkPos.Y].Type == MazeNodeType.Corridor ||
                                 Data[checkPos.X, checkPos.Y].Type == MazeNodeType.Room) &&
                                Data[checkPos.X, checkPos.Y].Index != room.Index &&
                                !connectors.Contains(new Point2(room.Index, Data[checkPos.X, checkPos.Y].Index)))
                            {
                                int index = Data[checkPos.X, checkPos.Y].Index;
                                connectors.Add(new Point2(index, room.Index));
                                connectors.Add(new Point2(room.Index, index));
                                if (!map.ContainsKey(index))
                                    map[index] = new List<Point2>();
                                map[index].Add(new Point2(x, y));
                            }
                        }
                    }
                foreach (int key in map.Keys)
                {
                    List<Point2> selection = map[key];
                    Point2 place = selection[Maze.Rnd.Next() % selection.Count];
                    if (Data[place.X - 1, place.Y].Index == key ||
                        Data[place.X + 1, place.Y].Index == key)
                    {
                        Data[place.X, place.Y].Type = MazeNodeType.DoorHorisontal;
                    }
                    else
                    {
                        Data[place.X, place.Y].Type = MazeNodeType.DoorVertical;
                    }
                }
            }
        }

        internal void SetWarps(int tryes, int max)
        {
            int placed = 0;
            for (int i = 0; i < tryes && placed < max; i++)
            {
                Point2 randomPoint = new Point2(1 + (Maze.Rnd.Next() % (AbstractSize.X)) * 2,
                                                1 + (Maze.Rnd.Next() % (AbstractSize.Y)) * 2);
                if (Data[randomPoint.X, randomPoint.Y].Type != MazeNodeType.Corridor)
                    continue;
                List<Point2> options = new List<Point2>();
                foreach (Point2 offset in Directions)
                {
                    Point2 current = randomPoint + offset;
                    if (Data[current.X, current.Y].Type == MazeNodeType.Block)
                    {
                        options.Add(offset);
                    }
                }
                if (options.Count > 0)
                {
                    Point2 selectedOffset = options[Maze.Rnd.Next() % options.Count];
                    Point2 selected = randomPoint + selectedOffset;
                    if (selectedOffset.X < 0)
                        Data[selected.X, selected.Y].Type = MazeNodeType.WarpCorridorLeft;
                    if (selectedOffset.X > 0)
                        Data[selected.X, selected.Y].Type = MazeNodeType.WarpCorridorRight;
                    if (selectedOffset.Y < 0)
                        Data[selected.X, selected.Y].Type = MazeNodeType.WarpCorridorUp;
                    if (selectedOffset.Y > 0)
                        Data[selected.X, selected.Y].Type = MazeNodeType.WarpCorridorDown;
                    Maze.warps.Add(new Point3(selected.X, selected.Y, WarpLevel));
                    placed++;
                }
            }
        }

        internal void Cleanup()
        {
            for (int x = 1; x < Size.X - 1; x++)
                for (int y = 1; y < Size.Y - 1; y++)
                {
                    Point2 current = new Point2(x, y);
                    while (Data[current.X, current.Y].Type == MazeNodeType.Corridor && WallsIntact(current) == 3)
                    {
                        Data[current.X, current.Y].Type = MazeNodeType.Block;
                        foreach (Point2 offset in Directions)
                        {
                            Point2 check = current + offset;
                            if (Data[check.X, check.Y].Type == MazeNodeType.Corridor)
                            {
                                current = check;
                                break;
                            }
                        }
                    }
                }
        }

        internal void GenerateRooms(int tryes, int maxArea, int minAbstractSize, int maxAbstractSize)
        {

            for (int i = 0; i < tryes && roomArea < maxArea; i++)
            {
                Point2 position = new Point2(
                    1 + (Maze.Rnd.Next() % (AbstractSize.X - minAbstractSize)) * 2,
                    1 + (Maze.Rnd.Next() % (AbstractSize.Y - minAbstractSize)) * 2
                );

                Point2 size = new Point2(
                    minAbstractSize + Maze.Rnd.Next() % (maxAbstractSize - minAbstractSize),
                    minAbstractSize + Maze.Rnd.Next() % (maxAbstractSize - minAbstractSize)
                );
                size = size * 2 + Point2.One;
                if (CheckPlacement(position - Point2.One, size + Point2.One * 2))
                {
                    roomArea += size.X * size.Y;
                    PlaceRoom(
                        new MazeRoom() { Position = position, Size = size, Warp = WarpLevel }
                    );
                }
            }
        }

        internal void GenerateCoridors()
        {
            HashSet<Point2> free = new HashSet<Point2>();
            Stack<Point2> stack = new Stack<Point2>();
            Point2[] neighbours = new Point2[4];

            for (int x = 1; x < Size.X; x += 2)
                for (int y = 1; y < Size.Y; y += 2)
                {
                    if (Data[x, y].Solid)
                        free.Add(new Point2(x, y));
                }

            while (free.Count > 0)
            {
                Point2 current = Point2.One;
                foreach (Point2 a in free)
                {
                    stack.Push(a);
                    current = a;
                    break;
                }
                while (stack.Count > 0)
                {
                    Data[current.X, current.Y].Type = MazeNodeType.Corridor;
                    Data[current.X, current.Y].Index = Maze.FIndex;
                    if (free.Contains(current))
                        free.Remove(current);

                    int cnt = GetValidNeighbors(current, neighbours);
                    if (cnt > 0)
                    {
                        stack.Push(current);
                        int selected = Maze.Rnd.Next() % cnt;
                        current = neighbours[selected];
                    }
                    else
                    {
                        current = stack.Pop();
                    }
                }
                Maze.FIndex++;
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            int w = Size.X * 3;
            char[,] data = new char[Size.X * 6, Size.Y * 3];
            for (int y = 0; y < Size.Y; y += 1)
            {
                for (int x = 0; x < Size.X; x += 1)
                {

                    string currentData = stringifier[Data[x, y].Type];
                    if (Data[x, y].Type == MazeNodeType.WarpCorridorUp ||
                        Data[x, y].Type == MazeNodeType.WarpCorridorDown ||
                        Data[x, y].Type == MazeNodeType.WarpCorridorLeft ||
                        Data[x, y].Type == MazeNodeType.WarpCorridorRight)
                    {
                        currentData = string.Format(currentData, Data[x, y].Data);
                    }
                    for (int _x = 0; _x < 6; _x++)
                        for (int _y = 0; _y < 3; _y++)
                        {
                            char current = currentData[_y * 6 + _x];
                            data[_x + x * 6, _y + y * 3] = current;
                        }
                }
            }
            for (int y = 0; y < Size.Y * 3; y += 1)
            {
                for (int x = 0; x < Size.X * 6; x += 1)
                {

                    sb.Append(data[
                        x, y
                    ]);
                }
                sb.Append('\n');
            }
            return sb.ToString();
        }

    }
}
