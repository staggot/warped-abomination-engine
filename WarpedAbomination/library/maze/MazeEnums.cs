﻿using System;
namespace WarpedAbominationEngine
{
    public enum MazeNodeType : byte
    {
        Free = 0,
        Room = 1,
        Corridor = 2,
        DoorHorisontal = 3,
        DoorVertical = 4,
        WarpCorridorLeft = 5,
        WarpCorridorRight = 6,
        WarpCorridorUp = 7,
        WarpCorridorDown = 8,
        RoomDown = 10,
        RoomUp = 11,
        RoomBridge = 12,
        RoomStairsUp = 13,
        RoomStairsDown = 14,
        CorridorStairsUp = 15,
        CorridorStairsDown = 16,
        Block = 128,
    }

    [Flags]
    public enum NeighbourFlags : byte
    {
        None = 0,
        Left = 1,
        Right = 2,
        Up = 4,
        Down = 8,
        WarpDown = 16,
        WarpUp = 32
    }

}
