﻿using System;
using System.Collections.Generic;
namespace WarpedAbominationEngine
{
    
    public partial class MazeLayer
    {
        internal static Point2[] Directions = new Point2[] { new Point2(-1, 0), new Point2(1, 0), new Point2(0, -1), new Point2(0, 1) };

        static Dictionary<MazeNodeType, string> stringifier = new Dictionary<MazeNodeType, string>() {
            {MazeNodeType.Block, "000000000000000000" },
            {MazeNodeType.Corridor, "                  "},
            {MazeNodeType.WarpCorridorLeft, "00[]  00{0:X2}  00[]  "},
            {MazeNodeType.WarpCorridorRight, "  []00  {0:X2}00  []00"},
            {MazeNodeType.WarpCorridorDown, "      []{0:X2}[]000000"},
            {MazeNodeType.WarpCorridorUp, "000000[]{0:X2}[]      "},
            {MazeNodeType.Room, "                  "},
            {MazeNodeType.RoomUp, "        /\\        "},
            {MazeNodeType.RoomDown, "        \\/        "},
            {MazeNodeType.DoorHorisontal, "000000  |   000000"},
            {MazeNodeType.DoorVertical, "00  0000--0000  00"},
            {MazeNodeType.RoomBridge, "::::::::::::::::::"}
        };

        bool CheckPlacement( Point2 position, Point2 size)
        {
            if (position.X < 0 || position.Y < 0 ||
               position.X + size.X >= Size.X ||
               position.Y + size.Y >= Size.Y)
                return false;
            for (int y = position.Y; y < position.Y + size.Y; y++)
                for (int x = position.X; x < position.X + size.X; x++)
                    if (!Data[x, y].Solid)
                        return false;
            return true;
        }

        private bool IsInside(Point2 p)
        {
            return p.X >= 0 && p.Y >= 0 && p.X < Size.X && p.Y < Size.Y;
        }

        private int WallsIntact(Point2 pointToCheck)
        {
            int intactWallCounter = 0;
            foreach (var offset in Directions)
            {
                Point2 neighborToCheck = pointToCheck + offset;
                if (IsInside(neighborToCheck) && Data[neighborToCheck.X, neighborToCheck.Y].Solid)
                    intactWallCounter++;
            }
            return intactWallCounter;
        }

        Point2 RandomPoint()
        {
            return new Point2(
                1 + (Maze.Rnd.Next() % AbstractSize.X) * 2,
                1 + (Maze.Rnd.Next() % AbstractSize.Y) * 2
            );
        }

        private int  GetValidNeighbors(Point2 center, Point2[] array)
        {
            int count = 0;
            foreach (var offset in Directions)
            {
                Point2 toCheck = center + offset;
                if ((toCheck.X % 2) + (toCheck.Y % 2) >= 1)
                {
                    if (Data[toCheck.X, toCheck.Y].Solid && WallsIntact(toCheck) == 3)
                        array[count++] = toCheck;
                }
            }
            return count;
        }
    }
}
