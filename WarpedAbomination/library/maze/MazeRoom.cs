﻿using System;
namespace WarpedAbominationEngine
{
    public class MazeRoom
    {
        public Point2 Position;
        public int Warp;
        public Point2 Size;
        public int Index;
        public bool Penetrated = false;
        public bool PenetratedDowm = false;

        internal bool IsInside(int x, int y)
        {
            return x >= Position.X && x < Position.X + Size.X &&
                             y >= Position.Y && y < Position.Y + Size.Y;
        }

        internal int Intersects(MazeRoom room)
        {
            int result = 0;
            for (int x = Position.X; x < Position.X + Size.X; x++)
                for (int y = Position.Y; y < Position.Y + Size.Y; y++)
                {
                    if (room.IsInside(x, y))
                        result++;
                }
            return result;
                        
        }
    }
}
