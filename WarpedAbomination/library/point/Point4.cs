﻿using Microsoft.Xna.Framework;

namespace WarpedAbominationEngine
{
    public struct Point4
    {
        public int X, Y, Z, W;

        public Point4(Vector4 vector) : this((int)vector.X, (int)vector.Y, (int)vector.Z, (int)vector.W)
        {

        }

        public Point4(int x, int y, int z, int w)
        {
            this.X = x;
            this.Y = y;
            this.Z = z;
            this.W = w;
        }

        public int LengthSquared
        {
            get { return X * X + Y * Y + Z * Z + W * W; }
        }

        public static Point4 operator +(Point4 a, Point4 b)
        {
            return new Point4(a.X + b.X, a.Y + b.Y, a.Z + b.Z, a.W + b.W);
        }

        public static Point4 operator -(Point4 a, Point4 b)
        {
            return new Point4(a.X - b.X, a.Y - b.Y, +a.Z - b.Z, a.W - b.W);
        }

        public static Point4 operator -(Point4 a)
        {
            return new Point4(-a.X, -a.Y, -a.Z, a.W);
        }

        public static Point4 operator *(Point4 a, Point4 b)
        {
            return new Point4(a.X * b.X, a.Y * b.Y, a.Z * b.Z, a.W * b.W);
        }

        public static Point4 operator /(Point4 a, Point4 b)
        {
            return new Point4(a.X / b.X, a.Y / b.Y, a.Z / b.Z, a.W / b.W);
        }

        public static Point4 operator *(Point4 a, int b)
        {
            return new Point4(a.X * b, a.Y * b, a.Z * b, a.W * b);
        }

        public static Point4 operator /(Point4 a, int b)
        {
            return new Point4(a.X / b, a.Y / b, a.Z / b, a.W / b);
        }


    }
}
