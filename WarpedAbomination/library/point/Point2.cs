﻿using Microsoft.Xna.Framework;


namespace WarpedAbominationEngine
{
    public struct Point2
    {
        public readonly static Point2 Zero = new Point2(0, 0);
        public readonly static Point2 One = new Point2(1, 1);

        public bool Equals(Point2 other)
        {
            return X == other.X && Y == other.Y;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is Point2 && Equals((Point2) obj);
        }

        public int Length
        {
            get
            {
                return (int)(Vector.Length() + 0.5f);
            }
        }

        public int LeghtSquared
        {
            get
            {
                return X * X + Y * Y;
            }
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (X * 397) ^ Y;
            }
        }

        public int X, Y;

        public Point2(Vector2 vector) : this((int)vector.X, (int)vector.Y)
        {

        }

        public Point2(int x,int y)
        {
            this.X = x;
            this.Y = y;
        }

        public Vector2 Vector
        {
            get { return new Vector2(X, Y); }
        }

        public static Point2 operator +(Point2 a, Point2 b)
        {
            return new Point2(a.X + b.X, a.Y + b.Y);
        }

        public static Point2 operator -(Point2 a, Point2 b)
        {
            return new Point2(a.X - b.X, a.Y - b.Y);
        }

        public static Point2 operator -(Point2 a)
        {
            return new Point2(-a.X, -a.Y);
        }

        public static Point2 operator *(Point2 a, Point2 b)
        {
            return new Point2(a.X * b.X, a.Y * b.Y);
        }

        public static Point2 operator *(Point2 a, int b)
        {
            return new Point2(a.X * b, a.Y * b);
        }

        public static Point2 operator /(Point2 a, Point2 b)
        {
            return new Point2(a.X / b.X, a.Y / b.Y);
        }

        public static Point2 operator /(Point2 a, int b)
        {
            return new Point2(a.X / b, a.Y / b);
        }

        public static bool operator ==(Point2 a, Point2 b)
        {
            return a.X == b.X && a.Y == b.Y;
        }

        public static bool operator !=(Point2 a, Point2 b)
        {
            return a.X != b.X || a.Y != b.Y;
        }

        public override string ToString()
        {
	        return "{" + string.Format("{0}, {1}", X, Y) + "}";
        }
    }
}
