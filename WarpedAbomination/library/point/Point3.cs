﻿using Microsoft.Xna.Framework;
using System;

namespace WarpedAbominationEngine
{
    public struct Point3
    {
        public static Point3[] Directions =
        {
            new Point3(-1,0,0),
            new Point3(1,0,0),
            new Point3(0,-1,0),
            new Point3(0,1,0),
            new Point3(0,0,-1),
            new Point3(0,0,1)
        };

        public bool Equals(Point3 other)
        {
            return X == other.X && Y == other.Y && Z == other.Z;
        }

        public override bool Equals(object obj)
        {
            return obj is Point3 && Equals((Point3)obj);
        }

        public static Point3 Round(Vector3 value)
        {
            return new Point3(
                value.X < 0 ? (int)value.X - 1 : (int)value.X,
                value.Y < 0 ? (int)value.Y - 1 : (int)value.Y,
                value.Z < 0 ? (int)value.Z - 1 : (int)value.Z
            );
        }

#pragma warning disable RECS0025
        public override int GetHashCode()
        {
            unchecked {
                int hashCode = X;
                hashCode = (hashCode * 397) ^ Y;
                hashCode = (hashCode * 397) ^ Z;
                return hashCode;
            }
        }
#pragma warning restore RECS0025

        public int X, Y, Z;

        public int Length {
            get {
                return (int)(Vector.Length() + 0.5f);
            }
        }

        public int LengthSquared {
            get { return X * X + Y * Y + Z * Z; }
        }

        public Point3(Vector3 vector) : this((int)vector.X, (int)vector.Y, (int)vector.Z)
        {

        }

        public Point3(int x, int y, int z)
        {
            this.X = x;
            this.Y = y;
            this.Z = z;
        }

        public Vector3 Vector {
            get { return new Vector3(X, Y, Z); }
        }

        public static Point3 One = new Point3(1, 1, 1);
        public static Point3 Zero = new Point3(0, 0, 0);

        public static Point3 Max(Point3 a, Point3 b)
        {
            return new Point3(
                Math.Max(a.X, b.X),
                Math.Max(a.Y, b.Y),
                Math.Max(a.Z, b.Z)
            );
        }

        public static Point3 Min(Point3 a, Point3 b)
        {
            return new Point3(
                Math.Min(a.X, b.X),
                Math.Min(a.Y, b.Y),
                Math.Min(a.Z, b.Z)
            );
        }

        public static Point3 operator +(Point3 a, Point3 b)
        {
            return new Point3(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
        }

        public static Point3 operator -(Point3 a, Point3 b)
        {
            return new Point3(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
        }

        public static Point3 operator *(Point3 a, Point3 b)
        {
            return new Point3(a.X * b.X, a.Y * b.Y, a.Z * b.Z);
        }

        public static Point3 operator *(Point3 a, int b)
        {
            return new Point3(a.X * b, a.Y * b, a.Z * b);
        }

        public static Point3 operator /(Point3 a, Point3 b)
        {
            return new Point3(a.X / b.X, a.Y / b.Y, a.Z / b.Z);
        }

        public static Point3 operator /(Point3 a, int b)
        {
            return new Point3(a.X / b, a.Y / b, a.Z / b);
        }

        public static Point3 operator -(Point3 a)
        {
            return new Point3(-a.X, -a.Y, -a.Z);
        }

        public static bool operator ==(Point3 a, Point3 b)
        {
            return a.X == b.X && a.Y == b.Y && a.Z == b.Z;
        }

        public static bool operator !=(Point3 a, Point3 b)
        {
            return a.X != b.X || a.Y != b.Y || a.Z != b.Z;
        }

        public override string ToString()
        {
            return "{" + string.Format("{0}, {1}, {2}", X, Y, Z) + "}";
        }


    }
}
