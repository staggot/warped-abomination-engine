﻿using System;

namespace WarpedAbominationEngine
{
    public struct Bounds3
    {
        public Point3 Position, Size;

        public Bounds3(Point3 position, Point3 size)
        {
            this.Position = position;
            this.Size = size;
        }

        public bool IsIntersects(Bounds3 bounds)
        {
            return
                bounds.Position.X < Position.X + Size.X && Position.X < bounds.Position.X + bounds.Size.X &&
                      bounds.Position.Y < Position.Y + Size.Y && Position.Y < bounds.Position.Y + bounds.Size.Y &&
                      bounds.Position.Z < Position.Z + Size.Z && Position.Z < bounds.Position.X + bounds.Size.Z;
        }

        public bool IsInside(Point3 point)
        {
            return
                point.X >= Position.X && point.X < Position.X + Size.X &&
                     point.Y >= Position.Y && point.Y < Position.Y + Size.Y &&
                     point.Z >= Position.Z && point.Z < Position.Z + Size.Z;
        }

        public bool IsInside(Bounds3 bounds)
        {
            return
                bounds.Position.X >= Position.X && bounds.Position.X + bounds.Size.X < Position.X + Size.X &&
                      bounds.Position.Y >= Position.Y && bounds.Position.Y + bounds.Size.Y < Position.Y + Size.Y &&
                      bounds.Position.Z >= Position.Z && bounds.Position.Z + bounds.Size.Z < Position.Z + Size.Z;
        }


    }
}
