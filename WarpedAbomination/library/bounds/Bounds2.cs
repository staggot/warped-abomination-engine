﻿using System;

namespace WarpedAbominationEngine
{
    public struct Bounds2
    {
        public Point2 Position, Size;

        public Bounds2(Point2 position, Point2 size)
        {
            this.Position = position;
            this.Size = size;
        }

        public bool IsIntersects(Bounds3 bounds)
        {
            return
                bounds.Position.X < Position.X + Size.X && Position.X < bounds.Position.X + bounds.Size.X &&
                      bounds.Position.Y < Position.Y + Size.Y && Position.Y < bounds.Position.Y + bounds.Size.Y;
        }

        public bool IsInside(Point2 point)
        {
            return
                point.X >= Position.X && point.X < Position.X + Size.X &&
                     point.Y >= Position.Y && point.Y < Position.Y + Size.Y;
        }

        public bool IsInside(Bounds2 bounds)
        {
            return
                bounds.Position.X >= Position.X && bounds.Position.X + bounds.Size.X < Position.X + Size.X &&
                      bounds.Position.Y >= Position.Y && bounds.Position.Y + bounds.Size.Y < Position.Y + Size.Y;
        }
    }
}
