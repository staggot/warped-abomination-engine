﻿using System;
using System.Threading;
using System.Collections.Concurrent;
using System.Collections.Generic;
using qsldotnet.lib;

namespace WarpedAbominationEngine
{
    public class Cron : IDisposable
    {
        public Logger Logger;
        public readonly string Name;
        readonly ConcurrentQueue<CronJobInstance> jobs = new ConcurrentQueue<CronJobInstance>();
        readonly Dictionary<string, CronJob> jobRegistry = new Dictionary<string, CronJob>();
        readonly int TickPeriodMilliseconds;
        readonly Thread tickThread;
        volatile bool isAlive;
        readonly Semaphore stopSemaphore;


        public Cron(string name, int tickPeriodMilliseconds = 100, int executionThreads = 1)
        {
            Name = name;
            TickPeriodMilliseconds = tickPeriodMilliseconds;
            isAlive = true;
            tickThread = new Thread(Ticker);
            stopSemaphore = new Semaphore(1, 1);
        }

        public void RegisterJob(string name, Action action, int periodMilliseconds = 0)
        {
            if (jobRegistry.ContainsKey(name)) {
                throw new JobNameCollisionException(this, name);
            }
            jobRegistry.Add(name, new CronJob(this, name, action, periodMilliseconds));
        }

        void Enqueue(CronJobInstance job)
        {
            jobs.Enqueue(job);
        }

        public CronJobInstance Enqueue(string name, Delegate action, params object[] parameters)
        {
            CronJobInstance job = new CronJobInstance(name, action, parameters);
            Enqueue(job);
            return job;
        }

        void TickPeriodic(int time)
        {
            foreach (CronJob job in jobRegistry.Values) {
                job.Tick(time);
            }
        }

        void Ticker()
        {
            while (true) {
                TickPeriodic(TickPeriodMilliseconds);
                while (isAlive && jobs.TryDequeue(out CronJobInstance job)) {
                    if (job == null)
                        continue;
                    job.Execute();
                    if (job.Error != null)
                        Logger?.Error(string.Format("Cron [{0}] error while processing: {1}", Name, job.Name), job.Error);

                }
                if (!isAlive)
                    break;
                Thread.Sleep(TickPeriodMilliseconds);
            }
            stopSemaphore.Release();
        }

        public void Dispose()
        {
            isAlive = false;
            stopSemaphore.WaitOne();
        }
    }
}
