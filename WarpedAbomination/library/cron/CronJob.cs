﻿using System;
namespace WarpedAbominationEngine
{
    internal class CronJob
    {
        internal readonly int PeriodMilliseconds;
        internal int NextRun;
        internal readonly string Name;
        readonly Action Action;
        internal readonly Cron Cron;

        internal CronJob(Cron cron, string name, Action action, int periodMilliseconds)
        {
            Cron = cron;
            Name = name;
            PeriodMilliseconds = periodMilliseconds;
            NextRun = 0;
            Action = action;
        }


        internal void Tick(int time)
        {
            NextRun -= time;
            if(NextRun < 0) {
                NextRun = PeriodMilliseconds;
                Cron.Enqueue(Name, Action);
            }
        }


        internal void Run()
        {
            Action();
        }
    }
}
