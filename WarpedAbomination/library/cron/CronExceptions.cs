﻿using System;
namespace WarpedAbominationEngine
{
    public class JobNameCollisionException: Exception
    {
        public JobNameCollisionException(Cron cron, string name): base(
            string.Format("Cron '{0}' already contains job name '{1}'", cron, name)) { }
    }
}
