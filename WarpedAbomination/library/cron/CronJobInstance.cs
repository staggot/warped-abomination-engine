﻿using System;
using System.Threading;
using System.Diagnostics;

namespace WarpedAbominationEngine
{
    public class CronJobInstance
    {
        public Exception Error { get; private set; } = null;
        public float RunTime { get; private set; } = 0;
        public readonly string Name;
        readonly Semaphore semaphore;
        readonly Delegate action;
        readonly object[] parameters;

        internal CronJobInstance(string name, Delegate action, object[] parameters)
        {

            Name = name;
            semaphore = new Semaphore(1, 1);
            this.action = action;
            this.parameters = parameters;
        }

        internal void Execute()
        {
            Stopwatch timer = new Stopwatch();
            timer.Start();
            try {
                action.DynamicInvoke(parameters);
            } catch (Exception ex) {
                Error = ex;
            }
            timer.Stop();
            RunTime = (float)timer.Elapsed.TotalSeconds;
            semaphore.Release();
        }

        internal void Wait()
        {
            semaphore.WaitOne();
        }

    }
}
