﻿#pragma warning disable RECS0018
using System;
using Microsoft.Xna.Framework;
using WarpedAbominationEngine.Graphics;

namespace WarpedAbominationEngine
{
    public static class MathHelper
    {
        public const float SqrtTwo = 1.4142135623730950f;
        public const float SqrtTwoOverTwo = SqrtTwo / 2;
        public const float SqrtThree = 1.7320508075688772935274463415059f;
        public const float SqrtThreeOverThree = SqrtThree / 3;

        public const float PI = (float)Math.PI;
        public const float TwoPI = (float)Math.PI * 2.0f;
        public const float PIOver2 = (float)Math.PI / 2.0f;


        public static float SmoothStep(float source, float target, float speed, float time)
        {
            if (target == source)
                return source;
            if (target > source)
            {
                source += speed * time;
                if (target < source)
                    source = target;
            }
            else if (target < source)
            {
                source -= speed * time;
                if (target > source)
                    source = target;
            }
            return source;
        }

        public static Vector2 SmoothStep(Vector2 source, Vector2 target, float speed, float time)
        {
            if (target == source)
                return source;
            Vector2 delta = target - source;
            Vector2 deltaDir = MathHelper.Normalize(delta, out float factor);
            float needTime = factor / speed;
            if (needTime > time)
                return source + deltaDir * speed * time;
            return target;
        }

        public static Vector4 SmoothStep(Vector4 source, Vector4 target, float speed, float time)
        {
            if (target == source)
                return source;
            Vector4 delta = target - source;
            Vector4 deltaDir = MathHelper.Normalize(delta, out float factor);
            float needTime = factor / speed;
            if (needTime > time)
                return source + deltaDir * speed * time;
            return target;
        }

        public static Vector3 SmoothStep(Vector3 source, Vector3 target, float speed, float time)
        {
            if (target == source)
                return source;
            Vector3 delta = target - source;
            Vector3 deltaDir = MathHelper.Normalize(delta, out float factor);
            float needTime = factor / speed;
            if (needTime > time)
                return source + deltaDir * speed * time;
            return target;
        }

        public static Vector4 Abs(Vector4 vector)
        {
            return new Vector4(
                Math.Abs(vector.X),
                Math.Abs(vector.Y),
                Math.Abs(vector.Z),
                Math.Abs(vector.W));
        }

        public static Vector3 Abs(Vector3 vector)
        {
            return new Vector3(
                Math.Abs(vector.X),
                Math.Abs(vector.Y),
                Math.Abs(vector.Z));
        }

        public static Vector2 Abs(Vector2 vector)
        {
            return new Vector2(
                Math.Abs(vector.X),
                Math.Abs(vector.Y));
        }

        public static float Sqrt(float x)
        {
            return (float)Math.Sqrt(x);
        }

        public unsafe static float FastSqrt(float x)
        {
            const float threehalfs = 1.5F;

            float x2 = x * 0.5F;
            float y = x;
            int i = *(int*)&y;
            i = 0x5f3759df - (i >> 1);
            y = *(float*)&i;
            y *= threehalfs - (x2 * y * y);

            return 1.0f / y;
        }

        public unsafe static float FastInvSqrt(float x)
        {
            const float threehalfs = 1.5F;

            float x2 = x * 0.5F;
            float y = x;
            int i = *(int*)&y;
            i = 0x5f3759df - (i >> 1);
            y = *(float*)&i;
            y *= threehalfs - (x2 * y * y);

            return y;
        }

        public static int Floor(float a)
        {
            return (int)Math.Floor(a);
        }

        public static float Cos(float x)
        {
            return (float)Math.Cos(x);
        }

        public static float Sin(float x)
        {
            return (float)Math.Sin(x);
        }

        public static Point3 PositionToTerrain(Vector3 vector)
        {
            return new Point3(vector + new Vector3(0.5f, 0.5f, 0.5f));
        }

        public static float PlaneDistance(Vector3 planeNormal, Vector3 planePoint, Vector3 target)
        {
            return (planeNormal.X * target.X) + (planeNormal.Y * target.Y) + (planeNormal.Z * target.Z)
                - (planeNormal.X * planePoint.X) - (planeNormal.Y * planePoint.Y) - (planeNormal.Z * planePoint.Z);
        }

        public static Vector4 Normalize(Vector4 vector, out float factor)
        {
            factor = (float)Math.Sqrt((vector.X * vector.X) + (vector.Y * vector.Y) + (vector.Z * vector.Z) + (vector.W * vector.W));
            return vector / factor;
        }

        public static Vector3 Normalize(Vector3 vector, out float factor)
        {
            factor = (float)Math.Sqrt((vector.X * vector.X) + (vector.Y * vector.Y) + (vector.Z * vector.Z));
            return vector / factor;
        }

        public static Vector2 Normalize(Vector2 vector, out float factor)
        {
            factor = 1 / (float)Math.Sqrt((vector.X * vector.X) + (vector.Y * vector.Y));
            float div = 1f / factor;
            return vector * div;
        }

        public static Vector3 GetTriangleNormal(Vector3 a, Vector3 b, Vector3 c)
        {
            Vector3 v1 = a - c;
            Vector3 v2 = c - b;
            v1.Normalize();
            v2.Normalize();
            Vector3 normal = Vector3.Cross(v1, v2);
            normal.Normalize();
            return normal;
        }
    }
}
