﻿using System;
using System.Collections.Generic;
using System.IO;

namespace WarpedAbominationEngine.Serialization
{
    static class PrimitiveSerialization
    {
        static readonly HashSet<Type> Numbers = new HashSet<Type>
        {
            typeof(byte),
            typeof(sbyte),
            typeof(char),
            typeof(short),
            typeof(ushort),
            typeof(int),
            typeof(uint),
            typeof(long),
            typeof(ulong),
            typeof(float),
            typeof(double),
            typeof(decimal),
        };

        static PrimitiveSerialization()
        {
            AbominationSerialization.RegisterSerializer(
                WarpTypes.Byte,
                typeof(byte),
                (w, o) => { w.Write((byte)o); },
                (r) => r.ReadByte(),
                (o) => AbominationSerialization.Json.DumpString(o),
                (s) => AbominationSerialization.Json.LoadString(s)
            ) ;

            AbominationSerialization.RegisterSerializer(
                "sbyte",
                typeof(byte),
                (w, o) => { w.Write((sbyte)o); },
                (r) => r.ReadSByte(),
                (o) => AbominationSerialization.Json.DumpString(o),
                (s) => AbominationSerialization.Json.LoadString(s)
            );

            AbominationSerialization.RegisterSerializer(
                "bool",
                typeof(byte),
                (w, o) => { w.Write((bool)o); },
                (r) => r.ReadBoolean(),
                (o) => AbominationSerialization.Json.DumpString(o),
                (s) => AbominationSerialization.Json.LoadString(s)
            );

            AbominationSerialization.RegisterSerializer(
                "char",
                typeof(char),
                (w, o) => { w.Write((char)o); },
                (r) => r.ReadChar(),
                (o) => AbominationSerialization.Json.DumpString(o),
                (s) => AbominationSerialization.Json.LoadString(s)
            );

            AbominationSerialization.RegisterSerializer(
                "short",
                typeof(short),
                (w, o) => { w.Write((short)o); },
                (r) => r.ReadInt16(),
                (o) => AbominationSerialization.Json.DumpString(o),
                (s) => AbominationSerialization.Json.LoadString(s)
            );

            AbominationSerialization.RegisterSerializer(
                "ushort",
                typeof(ushort),
                (w, o) => { w.Write((ushort)o); },
                (r) => r.ReadUInt16(),
                (o) => AbominationSerialization.Json.DumpString(o),
                (s) => AbominationSerialization.Json.LoadString(s)
            );

            AbominationSerialization.RegisterSerializer(
                "int",
                typeof(int),
                (w, o) => { w.Write((int)o); },
                (r) => r.ReadInt32(),
                (o) => AbominationSerialization.Json.DumpString(o),
                (s) => AbominationSerialization.Json.LoadString(s)
            );

            AbominationSerialization.RegisterSerializer(
                "int",
                typeof(int),
                (w, o) => { w.Write((int)o); },
                (r) => r.ReadInt32(),
                (o) => AbominationSerialization.Json.DumpString(o),
                (s) => AbominationSerialization.Json.LoadString(s)
            );

            AbominationSerialization.RegisterSerializer(
                "uint",
                typeof(int),
                (w, o) => { w.Write((uint)o); },
                (r) => r.ReadUInt32(),
                (o) => AbominationSerialization.Json.DumpString(o),
                (s) => AbominationSerialization.Json.LoadString(s)
            );

            AbominationSerialization.RegisterSerializer(
                "long",
                typeof(long),
                (w, o) => { w.Write((long)o); },
                (r) => r.ReadInt64(),
                (o) => AbominationSerialization.Json.DumpString(o),
                (s) => AbominationSerialization.Json.LoadString(s)
            );

            AbominationSerialization.RegisterSerializer(
                "ulong",
                typeof(ulong),
                (w, o) => { w.Write((ulong)o); },
                (r) => r.ReadUInt64(),
                (o) => AbominationSerialization.Json.DumpString(o),
                (s) => AbominationSerialization.Json.LoadString(s)
            );

            AbominationSerialization.RegisterSerializer(
                "float",
                typeof(float),
                (w, o) => { w.Write((float)o); },
                (r) => r.ReadSingle(),
                (o) => AbominationSerialization.Json.DumpString(o),
                (s) => AbominationSerialization.Json.LoadString(s)
            );

            AbominationSerialization.RegisterSerializer(
                "double",
                typeof(double),
                (w, o) => { w.Write((double)o); },
                (r) => r.ReadDouble(),
                (o) => AbominationSerialization.Json.DumpString(o),
                (s) => AbominationSerialization.Json.LoadString(s)
            );

            AbominationSerialization.RegisterSerializer(
                "string",
                typeof(string),
                (w, o) => { w.Write((string)o); },
                (r) => r.ReadString(),
                (o) => AbominationSerialization.Json.DumpString(o),
                (s) => AbominationSerialization.Json.LoadString(s)
            );

            AbominationSerialization.RegisterSerializer(
                "string",
                typeof(string),
                (w, o) => { w.Write((string)o); },
                (r) => r.ReadString(),
                (o) => AbominationSerialization.Json.DumpString(o),
                (s) => AbominationSerialization.Json.LoadString(s)
            );

            AbominationSerialization.RegisterConverter(
                typeof(byte),
                (t) =>
                {
                    if (t == null)
                        return true;
                    if (Numbers.Contains(t))
                        return true;
                    if (typeof(string).IsAssignableFrom(t))
                        return true;
                    return false;
                },
                (o) =>
                {
                    if (o == null)
                        return (byte)0;
                    if (o is string)
                        return byte.Parse((string)o);
                    return (byte)o;
                });
            AbominationSerialization.RegisterConverter(
                typeof(sbyte),
                (t) =>
                {
                    if (t == null)
                        return true;
                    if (Numbers.Contains(t))
                        return true;
                    if (typeof(string).IsAssignableFrom(t))
                        return true;
                    return false;
                },
                (o) =>
                {
                    if (o == null)
                        return (sbyte)0;
                    if (o is string)
                        return sbyte.Parse((string)o);
                    return (sbyte)o;
                });
            AbominationSerialization.RegisterConverter(
                typeof(char),
                (t) =>
                {
                    if (t == null)
                        return true;
                    if (Numbers.Contains(t))
                        return true;
                    if (typeof(string).IsAssignableFrom(t))
                        return true;
                    return false;
                },
                (o) =>
                {
                    if (o == null)
                        return (char)0;
                    if (o is string)
                        return char.Parse((string)o);
                    return (char)o;
                });
            AbominationSerialization.RegisterConverter(
                typeof(short),
                (t) =>
                {
                    if (t == null)
                        return true;
                    if (Numbers.Contains(t))
                        return true;
                    if (typeof(string).IsAssignableFrom(t))
                        return true;
                    return false;
                },
                (o) =>
                {
                    if (o == null)
                        return (short)0;
                    if (o is string)
                        return short.Parse((string)o);
                    return (short)o;
                });

            AbominationSerialization.RegisterConverter(
                typeof(ushort),
                (t) =>
                {
                    if (t == null)
                        return true;
                    if (Numbers.Contains(t))
                        return true;
                    if (typeof(string).IsAssignableFrom(t))
                        return true;
                    return false;
                },
                (o) =>
                {
                    if (o == null)
                        return (ushort)0;
                    if (o is string)
                        return ushort.Parse((string)o);
                    return (ushort)o;
                });
            AbominationSerialization.RegisterConverter(
                typeof(int),
                (t) =>
                {
                    if (t == null)
                        return true;
                    if (Numbers.Contains(t))
                        return true;
                    if (typeof(string).IsAssignableFrom(t))
                        return true;
                    return false;
                },
                (o) =>
                {
                    if (o == null)
                        return (int)0;
                    if (o is string)
                        return int.Parse((string)o);
                    return (int)o;
                });
            AbominationSerialization.RegisterConverter(
                typeof(uint),
                (t) =>
                {
                    if (t == null)
                        return true;
                    if (Numbers.Contains(t))
                        return true;
                    if (typeof(string).IsAssignableFrom(t))
                        return true;
                    return false;
                },
                (o) =>
                {
                    if (o == null)
                        return (uint)0;
                    if (o is string)
                        return uint.Parse((string)o);
                    return (uint)o;
                });
            AbominationSerialization.RegisterConverter(
                typeof(long),
                (t) =>
                {
                    if (t == null)
                        return true;
                    if (Numbers.Contains(t))
                        return true;
                    if (typeof(string).IsAssignableFrom(t))
                        return true;
                    return false;
                },
                (o) =>
                {
                    if (o == null)
                        return (long)0;
                    if (o is string)
                        return long.Parse((string)o);
                    return (long)o;
                });
            AbominationSerialization.RegisterConverter(
                typeof(ulong),
                (t) =>
                {
                    if (t == null)
                        return true;
                    if (Numbers.Contains(t))
                        return true;
                    if (typeof(string).IsAssignableFrom(t))
                        return true;
                    return false;
                },
                (o) =>
                {
                    if (o == null)
                        return (ulong)0;
                    if (o is string)
                        return ulong.Parse((string)o);
                    return (ulong)o;
                });
            AbominationSerialization.RegisterConverter(
                typeof(float),
                (t) =>
                {
                    if (t == null)
                        return true;
                    if (Numbers.Contains(t))
                        return true;
                    if (typeof(string).IsAssignableFrom(t))
                        return true;
                    return false;
                },
                (o) =>
                {
                    if (o == null)
                        return (float)0;
                    if (o is string)
                        return float.Parse((string)o);
                    return (float)o;
                });
            AbominationSerialization.RegisterConverter(
                typeof(double),
                (t) =>
                {
                    if (t == null)
                        return true;
                    if (Numbers.Contains(t))
                        return true;
                    if (typeof(string).IsAssignableFrom(t))
                        return true;
                    return false;
                },
                (o) =>
                {
                    if (o == null)
                        return (double)0;
                    if (o is string)
                        return double.Parse((string)o);
                    return (double)o;
                });
            AbominationSerialization.RegisterConverter(
                typeof(decimal),
                (t) =>
                {
                    if (t == null)
                        return true;
                    if (Numbers.Contains(t))
                        return true;
                    if (typeof(string).IsAssignableFrom(t))
                        return true;
                    return false;
                },
                (o) =>
                {
                    if (o == null)
                        return (decimal)0;
                    if (o is string)
                        return decimal.Parse((string)o);
                    return (decimal)o;
                });


        }
    }
}
