﻿using System;
using System.Collections.Generic;

namespace WarpedAbominationEngine.Serialization
{
    static class PrimitiveStructuresSerialization
    {
        static PrimitiveStructuresSerialization()
        {
            AbominationSerialization.RegisterSerializer(
               WarpTypes.DICT,
               typeof(IDictionary<string, object>),
               (w, o) => {
                   IDictionary<string, object> dict = o as IDictionary<string, object>;
                   w.Write((int)dict.Count);
                   foreach (var kv in dict)
                   {
                       AbominationSerialization.WriteObject(w, kv.Key, true);
                       AbominationSerialization.WriteObject(w, kv.Key, true);
                   }
               },
               (r) =>
               {
                   Dictionary<string, object> result = new Dictionary<string, object>();
                   int count = r.ReadInt32();
                   for (int i = 0; i < count; i++)
                   {
                       string key = (string)AbominationSerialization.ReadObject(r);
                       object value = AbominationSerialization.ReadObject(r);
                       result[key] = value;
                   }
                   return result;
               }
            );
            AbominationSerialization.RegisterSerializer(
               WarpTypes.MAP,
               typeof(IDictionary<object, object>),
               (w, o) => {
                   IDictionary<object, object> dict = o as IDictionary<object, object>;
                   w.Write((int)dict.Count);
                   foreach (var kv in dict)
                   {
                       AbominationSerialization.WriteObject(w, kv.Key, true);
                       AbominationSerialization.WriteObject(w, kv.Key, true);
                   }
               },
               (r) =>
               {
                   Dictionary<object, object> result = new Dictionary<object, object>();
                   int count = r.ReadInt32();
                   for (int i = 0; i < count; i++)
                   {
                       object key = AbominationSerialization.ReadObject(r);
                       object value = AbominationSerialization.ReadObject(r);
                       result[key] = value;
                   }
                   return result;
               }
            );
        }
    }
}
