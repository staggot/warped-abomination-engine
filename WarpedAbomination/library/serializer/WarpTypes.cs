﻿using System;
namespace WarpedAbominationEngine
{
    public static class WarpTypes
    {
        public const string
            NULL = "null",
            String = "string",
            Bool = "bool",
            Byte = "byte",
            SByte = "sbyte",
            Char = "char",
            Short = "short",
            UShort = "ushort",
            Int = "int",
            UInt = "uint",
            Long = "long",
            ULong = "ulong",
            Float = "float",
            Double = "double",
            Decimal = "decimal",
            MAP = "map",
            DICT = "dict",
            Vector2 = "vector2",
            Vector3 = "vector3",
            Vector4 = "vector4",
            Point2 = "point2",
            Point3 = "point3",
            Point4 = "point4",
            Color = "color";
    }
}
