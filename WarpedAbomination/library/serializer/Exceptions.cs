﻿using System;
namespace WarpedAbominationEngine.Serialization
{
    public class DuplicateSerializerNameException : Exception
    {
        public DuplicateSerializerNameException(string name) : base(string.Format("Serializer with name '{0}' already registered.", name))
        {
        }
    }

    public class DuplicateSerializerTypeException : Exception
    {
        public DuplicateSerializerTypeException(Type type) : base(string.Format("Serializer for type '{0}' already registered.", type.Name))
        {
        }
    }

    public class DuplicateConvertableTypeException : Exception
    {
        public DuplicateConvertableTypeException(Type type) : base(string.Format("Converter for type '{0}' already registered.", type.Name))
        {
        }
    }

    public class UnsupportedSerializationTypeException : Exception
    {
        public UnsupportedSerializationTypeException(Type type) : base(string.Format("Serializer for type '{0}' is not registered.", type.Name))
        { 
        }
    }

    public class UnsupportedDeserializationTypeException : Exception
    {
        public UnsupportedDeserializationTypeException(string type) : base(string.Format("Deserializer for type '{0}' is not registered.", type))
        {
        }
    }

    public class CantConvertType : Exception
    {
        public CantConvertType(string from, string to) : base(string.Format("Can not convert type from '{0}' to '{1}'.", from, to))
        {

        }
    }
}
