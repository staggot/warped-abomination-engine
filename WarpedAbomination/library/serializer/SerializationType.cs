﻿using System;
using System.IO;

namespace WarpedAbominationEngine.Serialization
{
    public class SerializationType
    {
        public Action<BinaryWriter, object> SerializeMethod { get; set; }
        public Func<object, string> StringifyMethod { get; set; }
        public Func<BinaryReader, object> DeserializeMethod { get; set; }
        public Func<string, object> UnstringifyMethod { get; set; }
        public string Name { get; private set; }
		public Type Type { get; private set; }

        public SerializationType(string name, Type type)
        {
            this.Name = name;
            this.Type = type;
        }
    }
}
