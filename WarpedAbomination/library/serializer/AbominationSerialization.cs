﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using qsldotnet;

namespace WarpedAbominationEngine.Serialization
{
	public static class AbominationSerialization
	{
		static readonly Encoding DefaultEncoding = Encoding.UTF8;
		internal static QJson Json = new QJson();

		static Dictionary<string, SerializationType> typeMap = new Dictionary<string, SerializationType>();
		static Dictionary<Type, string> typeNameMap = new Dictionary<Type, string>();
		static Dictionary<Type, Func<Type, bool>> convertCheckMap = new Dictionary<Type, Func<Type, bool>>();
		static Dictionary<Type, Func<object, object>> convertersMap = new Dictionary<Type, Func<object, object>>();

		internal static SerializationType DefineObjectType(object obj)
        {
			if (obj == null)
				return typeMap[WarpTypes.NULL];
			Type t = obj.GetType();
			if (typeNameMap.ContainsKey(t))
				return typeMap[typeNameMap[t]];
			return null;
        }

		static AbominationSerialization()
        {
			typeMap[WarpTypes.NULL] = new SerializationType(WarpTypes.NULL, null)
			{
				DeserializeMethod = (s) => null,
				SerializeMethod = (s, o) => { },
				StringifyMethod = (o) => WarpTypes.NULL,
				UnstringifyMethod = (s) => null
			};
        }

        public static SerializationType GetSerializationType(string name)
        {
			return typeMap[name];
        }

        public static object Convert(object obj, Type targetType)
        {
			if (targetType == null)
				return obj;
			if (obj != null && targetType.IsAssignableFrom(obj.GetType()))
				return obj;
            if (obj == null)
            {
                if(convertCheckMap[targetType](null))
					return convertersMap[targetType](obj);
				throw new CantConvertType("null", targetType.Name);
            }
            if (convertCheckMap[targetType](obj.GetType()))
                return convertersMap[targetType](obj);
			throw new CantConvertType(obj.GetType().Name, targetType.Name);
        }

		static bool CanBeConverted(object obj, Type type)
        {
			if (type == null)
				return true;
			if (!convertCheckMap.ContainsKey(type))
				return false;
			if (obj != null && type == obj.GetType())
				return true;
			if (obj == null)
				return convertCheckMap[type](null);
			return convertCheckMap[type](obj.GetType());
		}

        static BinaryReader CreateReader(Stream stream)
        {
			return new BinaryReader(stream, DefaultEncoding, true);
        }

		static BinaryWriter CreateWriter(Stream stream)
		{
			return new BinaryWriter(stream, DefaultEncoding, true);
		}


		public static void WriteObject(BinaryWriter stream, object obj, bool writeMeta = false)
		{
			SerializationType type = DefineObjectType(obj);
			if (type == null)
				throw new UnsupportedSerializationTypeException(obj.GetType());
			if (writeMeta)
				stream.Write((string)type.Name);
			type.SerializeMethod(stream, obj);
		}

		public static object ReadObject(BinaryReader stream, string typeName)
        {
			if (!typeMap.ContainsKey(typeName))
				throw new UnsupportedDeserializationTypeException(typeName);
			return typeMap[typeName].DeserializeMethod(stream);
		}

		public static object ReadObject(BinaryReader stream)
		{
			string typeName = stream.ReadString();
			return ReadObject(stream, typeName);
		}


		public static void RegisterSerializer(string name, Type correspondType, Action<BinaryWriter, object> serializer, Func<BinaryReader, object> deserializer, Func<object, string> stringifier = null, Func<string, object> unstringifier = null)
		{
			if (name == null)
				throw new ArgumentNullException("name", "Serializer name cannot be null.");
			if (correspondType == null)
				throw new ArgumentNullException("correspondType", "Serializer correspond type cannot be null.");

			if (typeMap.ContainsKey(name))
				throw new DuplicateSerializerNameException(name);
			if (typeNameMap.ContainsKey(correspondType))
				throw new DuplicateSerializerTypeException(correspondType);

			typeMap[name] = new SerializationType(name, correspondType)
			{
				SerializeMethod = serializer,
				DeserializeMethod = deserializer,
                StringifyMethod = stringifier,
                UnstringifyMethod = unstringifier
                
			};
			typeNameMap[correspondType] = name;
		}

        public static void RegisterConverter(Type targetType, Func<Type, bool> checkFunction, Func<object, object> convertFunction)
        {
            if (targetType == null)
				if (targetType == null) throw new ArgumentNullException("targetType", "Target convertable type cannot be null.");
            if(checkFunction == null)
				if (targetType == null) throw new ArgumentNullException("checkFunction", "Check function for convertion cannot be null.");
			if(convertFunction == null)
				if (targetType == null) throw new ArgumentNullException("convertFunction", "Convert function for convertion cannot be null.");

			if (convertCheckMap.ContainsKey(targetType) || convertersMap.ContainsKey(targetType))
				throw new DuplicateConvertableTypeException(targetType);
			convertCheckMap[targetType] = checkFunction;
			convertersMap[targetType] = convertFunction;

		}
	}
}
