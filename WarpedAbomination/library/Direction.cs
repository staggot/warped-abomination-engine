﻿using System;
using Microsoft.Xna.Framework;
namespace WarpedAbominationEngine
{
    public enum Direction
    {
        Left = 0,
        Right = 1,
        Fore = 2,
        Back = 3,
        Down = 4,
        Up = 5
    }

    public static class DirecionOperations
    {
        static Vector3[] vectors = 
        {
            new Vector3(-1,0,0),
            new Vector3(1,0,0),
            new Vector3(0,0,-1),
            new Vector3(0,0,1),
            new Vector3(0,-1,0),
            new Vector3(0,1,0)
        };

        public static Vector3 ToVector(this Direction direction)
        {
            return vectors[(int)direction];
        }

        public static Direction ToDirection(this Vector3 vector)
        {
            int nearest = 0;
            float nearest_diff = float.PositiveInfinity;
            for (int i = 0; i < vectors.Length;i++)
            {
                float dist = (vectors[i] - vector).LengthSquared();
                if(dist < nearest_diff)
                {
                    nearest_diff = dist;
                    nearest = i;
                }
            }
            return (Direction)nearest;
        }
    }
}
