﻿using System;
using System.IO;

namespace WarpedAbominationEngine
{
	public interface IWarpedSerializable
	{
		void Serialize(Stream stream);
	}
}
