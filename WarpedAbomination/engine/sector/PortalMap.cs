﻿using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace WarpedAbominationEngine
{
    public class PortalMap : IEnumerable<Portal>
    {
        const byte MaxLinksTypes = 8;

        readonly Sector sector;
        readonly List<Portal> portals = new List<Portal>();
        Portal[] map;
        Point3 sectorSize;

        internal PortalLink[] PortalLinks = new PortalLink[MaxLinksTypes];
        internal object LinkLock = new object();

        public Scene Scene => sector.Scene;

        public PortalMap(Sector sector)
        {
            this.sector = sector;
            sectorSize = sector.Size;
            ResetMap();
        }

        public Portal Get(SectorSide side, Point2 positiom)
        {
            return map[MapIndex(side, positiom)];
        }

        private int MapIndex(SectorSide side, Point2 position)
        {
            switch (side) {
                case SectorSide.Left:
                    return position.X + position.Y * sectorSize.Z;
                case SectorSide.Right:
                    return sectorSize.Z * sectorSize.Y + position.X + position.Y * sectorSize.Z;
                case SectorSide.Fore:
                    return sectorSize.Z * sectorSize.Y * 2 + position.X + position.Y * sectorSize.X;
                case SectorSide.Back:
                    return sectorSize.Z * sectorSize.Y * 2 + sectorSize.X * sectorSize.Y + position.X + position.Y * sectorSize.X;
                case SectorSide.Bottom:
                    return 2 * sectorSize.X * sectorSize.Y + 2 * sectorSize.Z * sectorSize.Y + position.X + sectorSize.X * position.Y;
                case SectorSide.Top:
                    return 2 * sectorSize.X * sectorSize.Y + 2 * sectorSize.Z * sectorSize.Y + sectorSize.X * sectorSize.Z + position.X + sectorSize.X * position.Y;
                default:
                    return -1;
            }
        }

        public PortalLink this[PortalLinkType linkType] {
            get { return PortalLinks[(byte)linkType]; }
            internal set { PortalLinks[(byte)linkType] = value; }
        }


        public Portal this[SectorSide side, Point2 position] {
            get {
                if (position.X < 0 || position.Y < 0)
                    return null;
                switch (side) {
                    case SectorSide.Left:
                    case SectorSide.Right:
                        if (position.X >= sectorSize.Z || position.Y >= sectorSize.Y)
                            return null;
                        break;
                    case SectorSide.Fore:
                    case SectorSide.Back:
                        if (position.X >= sectorSize.X || position.Y >= sectorSize.Y)
                            return null;
                        break;
                    case SectorSide.Top:
                    case SectorSide.Bottom:
                        if (position.X >= sectorSize.X || position.Y >= sectorSize.Z)
                            return null;
                        break;

                }
                return portals[MapIndex(side, position)];
            }
        }

        public Portal this[SectorSide side, Vector2 planePosition] {
            get {
                return this[side, new Point2(planePosition)];
            }
        }

        public Portal this[SectorSide side, Vector3 position] {
            get {
                switch (side) {
                    case SectorSide.Left:
                    case SectorSide.Right:
                        return this[side, new Point2((int)position.Z, (int)position.Y)];
                    case SectorSide.Fore:
                    case SectorSide.Back:
                        return this[side, new Point2((int)position.X, (int)position.Y)];
                    case SectorSide.Top:
                    case SectorSide.Bottom:
                        return this[side, new Point2((int)position.X, (int)position.Z)];
                    default:
                        return null;
                }
            }
        }

        internal void Add(Portal portal)
        {
            portals.Add(portal);
            PlacePortal(portal);
            sector.PortalPlaced(portal);
        }

        public bool CheckPortalPlacement(Point2 position, Point2 size, SectorSide side, bool natural)
        {
            for (int y = position.Y; y < position.Y + size.Y; y++)
                for (int x = position.X; x < position.X + size.X; x++) {
                    int index = MapIndex(side, new Point2(x, y));
                    if (map[index] != null && (natural || !map[index].Natural))
                        return false;
                }
            return true;
        }

        void PlacePortal(Portal portal)
        {
            for (int y = portal.Position.Y; y < portal.Position.Y + portal.Size.Y; y++)
                for (int x = portal.Position.X; x < portal.Position.X + portal.Size.X; x++) {
                    int index = MapIndex(portal.Side, new Point2(x, y));
                    if (map[index] == null || (!portal.Natural && map[index].Natural))
                        map[index] = portal;
                }
        }

        internal void Remove(Portal portal)
        {
            portals.Remove(portal);
            ResetMap();
            foreach (Portal p in portals)
                PlacePortal(p);
            sector.PortalRemoved(portal);
        }


        internal void ResetMap()
        {
            map = new Portal[2 * (sector.Size.X * sector.Size.Y) + 2 * (sector.Size.Z * sector.Size.Y) + 2 * (sector.Size.X * sector.Size.Z)];
        }

        public void GetSurroundingPortals(Vector3 targetPosition, Vector2 targetSize, Portal[] output)
        {
            LocateBestSurroundingPortals(targetPosition, targetSize, output);
        }

        public Portal Portalize(Vector3 targetPosition, Vector2 targetSize)
        {
            if (sector.IsInside(targetPosition))
                return null;
            SectorSideMask outOfBounds = SectorSideMask.None;
            if (targetPosition.X < 0)
                outOfBounds |= SectorSideMask.Left;
            else if (targetPosition.X >= sector.Size.X)
                outOfBounds |= SectorSideMask.Right;
            if (targetPosition.Y < 0)
                outOfBounds |= SectorSideMask.Bottom;
            else if (targetPosition.Y >= sector.Size.Y)
                outOfBounds |= SectorSideMask.Top;
            if (targetPosition.Z < 0)
                outOfBounds |= SectorSideMask.Fore;
            else if (targetPosition.Z >= sector.Size.Z)
                outOfBounds |= SectorSideMask.Back;
            return LocateBestPortal(targetPosition, targetSize, outOfBounds);
        }

        void LocateBestSurroundingPortals(Vector3 targetPosition, Vector2 targetSize, Portal[] portals)
        {
            float[] bestSquares = new float[] { 0, 0, 0, 0, 0, 0 };
            SectorSideMask mask = SectorSideMask.None;
            if (targetPosition.X - targetSize.X / 2f <= 0)
                mask |= SectorSideMask.Left;
            if (targetPosition.X + targetSize.X / 2f >= sector.Size.X)
                mask |= SectorSideMask.Right;
            if (targetPosition.Y - targetSize.Y / 2f <= 0)
                mask |= SectorSideMask.Bottom;
            if (targetPosition.Y + targetSize.Y / 2f >= sector.Size.Y)
                mask |= SectorSideMask.Top;
            if (targetPosition.Z - targetSize.X / 2f <= 0)
                mask |= SectorSideMask.Fore;
            if (targetPosition.Z + targetSize.X / 2f >= sector.Size.Z)
                mask |= SectorSideMask.Back;
            for (int i = 0; i < portals.Length; i++)
                portals[i] = null;
            foreach (Portal portal in portals) {
                if (!mask.ContainsSide(portal.Side))
                    continue;
                float square = portal.IntersectSquare(targetPosition, targetSize);
                if (square > bestSquares[(int)portal.Side]) {
                    bestSquares[(int)portal.Side] = square;
                    portals[(int)portal.Side] = portal;
                }
            }
        }


        Portal LocateBestPortal(Vector3 targetPosition, Vector2 targetSize, SectorSideMask mask)
        {
            Portal best = null;
            float bestSquare = 0;
            foreach (Portal portal in portals) {
                if (!mask.ContainsSide(portal.Side))
                    continue;
                float square = portal.IntersectSquare(targetPosition, targetSize);
                if (square > bestSquare) {
                    bestSquare = square;
                    best = portal;
                }
            }
            return best;
        }

        public IEnumerator<Portal> GetEnumerator()
        {
            return portals.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
