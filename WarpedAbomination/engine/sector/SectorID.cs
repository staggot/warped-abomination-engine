﻿using System;
using System.Collections.Generic;

namespace WarpedAbominationEngine
{
    public struct SectorID
    {
        public Point2 RegionID;
        public int ID;

        public SectorID(Sector sector)
        {
            this.RegionID = sector.Region.ID;
            this.ID = sector.ID;
        }

        public SectorID(Point2 region, int id)
        {
            this.RegionID = region;
            this.ID = id;
        }

        public override bool Equals(object obj)
        {
            return obj is SectorID iD &&
                   EqualityComparer<Point2>.Default.Equals(RegionID, iD.RegionID) &&
                   ID == iD.ID;
        }
#pragma warning disable RECS0025
        public override int GetHashCode()
        {
            var hashCode = -1165533819;
            hashCode = hashCode * -1521134295 + EqualityComparer<Point2>.Default.GetHashCode(RegionID);

            hashCode = hashCode * -1521134295 + ID.GetHashCode();
            return hashCode;
        }
#pragma warning restore RECS0025

        public static bool operator ==(SectorID left, SectorID right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(SectorID left, SectorID right)
        {
            return !(left == right);
        }

        public override string ToString()
        {
            return string.Format("{{{0} {1}}}: {2}", RegionID.X, RegionID.Y, ID);
        }
    }
}
