﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
namespace WarpedAbominationEngine
{
    public partial class Sector
    { 
        internal int StorageId = -1;

        public Region Region;
        public int ID = -1;
        public string Name;
        public Point3 Size;
        public Point3 Position;

        public Bounds3 Bounds => new Bounds3(Position, Size);
        public Scene Scene => Region.Scene;
        public SectorID FullID => new SectorID(Region.ID, ID);

        public PortalMap Portals;
        public SectorTerrain Terrain;

        internal Sector(int id, Point3 position, Point3 size)
        {
            this.ID = id;
            this.Size = size;
            this.Position = position;
            Portals = new PortalMap(this);
            Terrain = new SectorTerrain(this);
        }

        internal void RegionEdge(out bool left, out bool right, out bool fore, out bool back)
        {
            left = Position.X == Region.ID.X * Region.RegionSize;
            right = Position.X + Size.X == Region.ID.X * Region.RegionSize + Region.RegionSize;
            fore = Position.Z == Region.ID.Y * Region.RegionSize;
            back = Position.Z + Size.Z == Region.ID.Y * Region.RegionSize + Region.RegionSize;
        }

        public bool Stick(Sector sec, SectorSideMask sideMask = SectorSideMask.ALL)
        {
            bool res = CheckStick(sec, out SectorSide side, out Point2 portalPos1, out Point2 portalPos2, out Point2 portalSize);
            if (res)
            {
                if (((int)sideMask & (1 << (int)side)) == 0)
                    return false;
                SectorSide outSide = side.GetOpposide();
                PortalPair pair = new PortalPair()
                {
                    Sector1 = new SectorID(this),
                    Sector2 = new SectorID(sec),
                    Position1 = portalPos1,
                    Position2 = portalPos2,
                    Side1 = side,
                    Side2 = outSide,
                    Size = portalSize,
                    TextureType = PortalTextureType.None,
                    TextureData = 0,
                    Wobble = new Vector2(0, 0),
                    Type = PortalType.Euclid
                };
                Region.Scene.Bridge.AddPortal(pair);
            }
            return res;
        }

        public bool IsInside(Vector3 position)
        {
            return
                position.X >= 0 && position.X < Size.X &&
                        position.Y >= 0 && position.Y < Size.Y &&
                        position.Z >= 0 && position.Z < Size.Z;
        }

        public bool IsInside(Point3 position)
        {
            return
                position.X >= 0 && position.X < Size.X &&
                        position.Y >= 0 && position.Y < Size.Y &&
                        position.Z >= 0 && position.Z < Size.Z;
        }

        internal void PortalPlaced(Portal portal)
        {
            Scene.CallPortalPlaced(portal);
        }

        internal void PortalRemoved(Portal portal)
        {
            for (byte linkTypeIndex = 0; linkTypeIndex < Portals.PortalLinks.Length; linkTypeIndex++) {
                if (Portals.PortalLinks[linkTypeIndex] == null)
                    continue;
                foreach (PortalLink link in Portals.PortalLinks[linkTypeIndex])
                    if (portal == link.Portal)
                        link.Remove();
            }
            Scene.CallPortalRemoved(portal);
        }

        public Point3 HowFarOutside(Point3 position)
        {
            Point3 result = Point3.Zero;
            if (position.X < 0)
                result.X = position.X;
            else if (position.X >= Size.X)
                result.X = Size.X - position.X - 1;
            if (position.Y < 0)
                result.Y = position.Y;
            else if (position.Y >= Size.Y)
                result.Y = Size.Y - position.Y - 1;
            if (position.Z < 0)
                result.Z = position.Z;
            else if (position.Z >= Size.Z)
                result.Z = Size.Z - position.Z - 1;
            return result;
        }

        public bool CheckStick(Sector sector, out SectorSide side, out Point2 thisPortalPos, out Point2 exitPortalPos, out Point2 portalSize)
        {
            side = SectorSide.Left;
            thisPortalPos = new Point2();
            exitPortalPos = new Point2();
            portalSize = new Point2();

            bool insideX = Position.X < sector.Position.X + sector.Size.X && Position.X + Size.X > sector.Position.X;
            bool insideY = Position.Y < sector.Position.Y + sector.Size.Y && Position.Y + Size.Y > sector.Position.Y;
            bool insideZ = Position.Z < sector.Position.Z + sector.Size.Z && Position.Z + Size.Z > sector.Position.Z;

            int left = Math.Max(Position.X, sector.Position.X);
            int right = Math.Min(Position.X + Size.X - 1, sector.Position.X + sector.Size.X - 1);

            int fore = Math.Max(Position.Z, sector.Position.Z);
            int back = Math.Min(Position.Z + Size.Z - 1, sector.Position.Z + sector.Size.Z - 1);

            int down = Math.Max(Position.Y, sector.Position.Y);
            int up = Math.Min(Position.Y + Size.Y - 1, sector.Position.Y + sector.Size.Y - 1);


            if (insideY && insideZ)
            {
                if (Position.X == sector.Position.X + sector.Size.X)
                {
                    thisPortalPos = new Point2(fore - Position.Z, down - Position.Y);
                    exitPortalPos = new Point2(fore - sector.Position.Z, down - sector.Position.Y);
                    portalSize = new Point2(back - fore + 1, up - down + 1);
                    side = SectorSide.Left;
                    return true;
                }
                if (Position.X + Size.X == sector.Position.X)
                {
                    thisPortalPos = new Point2(fore - Position.Z, down - Position.Y);
                    exitPortalPos = new Point2(fore - sector.Position.Z, down - sector.Position.Y);
                    portalSize = new Point2(back - fore + 1, up - down + 1);
                    side = SectorSide.Right;
                    return true;
                }
            }
            if (insideY && insideX)
            {
                if (Position.Z == sector.Position.Z + sector.Size.Z)
                {
                    thisPortalPos = new Point2(left - Position.X, down - Position.Y);
                    exitPortalPos = new Point2(left - sector.Position.X, down - sector.Position.Y);
                    portalSize = new Point2(right - left + 1, up - down + 1);
                    side = SectorSide.Fore;
                    return true;
                }

                if (Position.Z + Size.Z == sector.Position.Z)
                {
                    thisPortalPos = new Point2(left - Position.X, down - Position.Y);
                    exitPortalPos = new Point2(left - sector.Position.X, down - sector.Position.Y);
                    portalSize = new Point2(right - left + 1, up - down + 1);
                    side = SectorSide.Back;
                    return true;
                }
            }
            if (insideX && insideZ)
            {
                if (Position.Y == sector.Position.Y + sector.Size.Y)
                {
                    thisPortalPos = new Point2(left - Position.X, fore - Position.Z);
                    exitPortalPos = new Point2(left - sector.Position.X, fore - sector.Position.Z);
                    portalSize = new Point2(right - left + 1, back - fore + 1);
                    side = SectorSide.Bottom;
                    return true;
                }

                if (Position.Z + Size.Z == sector.Position.Z)
                {
                    thisPortalPos = new Point2(left - Position.X, fore - Position.Z);
                    exitPortalPos = new Point2(left - sector.Position.X, fore - sector.Position.Z);
                    portalSize = new Point2(right - left + 1, back - fore + 1);
                    side = SectorSide.Top;
                    return true;
                }
            }
            return false;
        }

        public Vector4 GetLightVector(Vector3 position)
        {
            return Vector4.One;
        }
    }
}
