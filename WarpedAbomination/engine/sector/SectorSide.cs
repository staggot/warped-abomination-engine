﻿using System;
using Microsoft.Xna.Framework;

namespace WarpedAbominationEngine
{
    [Flags]
    public enum SectorSideMask : byte
    {
        None = 0,
        Left = 1 << SectorSide.Left,
        Right = 1 << SectorSide.Right,
        Bottom = 1 << SectorSide.Bottom,
        Top = 1 << SectorSide.Top,
        Fore = 1 << SectorSide.Fore,
        Back = 1 << SectorSide.Back,
        ALL = 0x3f,
    }

    public enum SectorSide : byte
    {
        Left = 0,
        Right = 1,
        Bottom = 2,
        Top = 3,
        Fore = 4,
        Back = 5
    }

    public static class PortalSideExtender
    {
        public static bool ContainsSide(this SectorSideMask mask, SectorSide side)
        {
            return (sideToMask[(byte)side] & mask) > 0;
        }

        readonly static SectorSideMask[] sideToMask =
        {
            SectorSideMask.Left,
            SectorSideMask.Right,
            SectorSideMask.Bottom,
            SectorSideMask.Top,
            SectorSideMask.Fore,
            SectorSideMask.Back,
        };

        readonly static SectorSide[] opposideSides =
        {
            SectorSide.Right,
            SectorSide.Left,
            SectorSide.Top,
            SectorSide.Bottom,
            SectorSide.Back,
            SectorSide.Fore
        };

        readonly static Vector3[] normals =
        {
            new Vector3(1,0,0),
            new Vector3(-1,0,0),
            new Vector3(0,1,0),
            new Vector3(0,-1,0),
            new Vector3(1,0,0),
            new Vector3(-1,0,0)
        };

        public static Vector3 GetNormal (this SectorSide side)
        {
            return normals[(int)side];
        }

        public static SectorSide GetOpposide(this SectorSide side)
        {
            return opposideSides[(int)side];
        }
    }
}
