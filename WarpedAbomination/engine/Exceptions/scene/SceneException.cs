﻿using System;
namespace WarpedAbominationEngine
{
    public class SceneException : WarpedException
    {
        public SceneException(Scene scene, string message):base(message)
        {
        }
    }
}
