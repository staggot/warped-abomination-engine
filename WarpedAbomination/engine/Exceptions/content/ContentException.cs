﻿using System;
namespace WarpedAbominationEngine
{
    public class ContentException : Exception
    {
        public ContentException(string message) : base(message)
        {
        }
    }
}
