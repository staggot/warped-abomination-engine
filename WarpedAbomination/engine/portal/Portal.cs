﻿using System;
using Microsoft.Xna.Framework;
using WarpedAbominationEngine.Content;

namespace WarpedAbominationEngine
{
    public abstract class Portal

    {
        public abstract bool Natural { get; }

        public Portal OutPortal = null;
        public PortalPair Pair;
        public SectorSide Side;
        public SectorSide OutSide;
        public Point2 Position;
        public Point2 Size;
        public bool Solid;
        internal PortalTextureType Textured;
        internal ushort TextureData;
        public Sector Sector;

        public Direction DirectionToExit;
        public Sector OutSector = null;
        public string OutRegion = null;

        internal Vector3 PortalCenter;

        public Portal()
        {

        }


        internal abstract NodeShape ModifyNodeFrom(NodeShape node);
        internal abstract NodeShape ModifyNodeTo(NodeShape node);

        internal abstract Point3 TransformNodePositionTo(Point3 position);
        internal abstract Point3 TransformNodePositionFrom(Point3 position);

        internal abstract Point3 TransformTerrainPositionTo(Point3 position);
        internal abstract Point3 TransformTerrainPositionFrom(Point3 position);

        internal abstract Vector3 TransformPositionTo(Vector3 position);
        internal abstract Vector3 TransformPositionFrom(Vector3 position);

        internal abstract Vector3 TransformDirectionTo(Vector3 direction);
        internal abstract Vector3 TransformDirectionFrom(Vector3 direction);

        internal abstract float TransformYawTo(float angle);
        internal abstract float TransformPitchTo(float angle);
        internal abstract float TranformYawFrom(float angle);
        internal abstract float TransformPitchFrom(float angle);
        internal abstract float TransformRollTo(float angle);
        internal abstract float TransformRollFrom(float angle);

        internal abstract void Calculate();

        internal virtual ViewMod ViewMod
        {
            get { return new ViewMod() { Flags = ViewModFlag.None }; }
        }


        internal void Unlink()
        {
            Sector.Portals.Remove(this);
        }



        public static bool IsPlaceble(Sector sector, SectorSide side, Point2 position, Point2 size, bool euclid)
        {
            bool good = position.X >= 0 && position.Y >= 0 && size.X > 0 && size.Y > 0;
            switch (side)
            {
                case SectorSide.Left:
                case SectorSide.Right:
                    good = good &&
                        position.X + size.X < sector.Size.Z && position.Y + size.Y < sector.Size.Y;
                    break;
                case SectorSide.Fore:
                case SectorSide.Back:
                    good = good &&
                        position.X + size.X < sector.Size.X && position.Y + size.Y < sector.Size.Y;
                    break;
                case SectorSide.Bottom:
                case SectorSide.Top:
                    good = good &&
                        position.X + size.X < sector.Size.X && position.Y + size.Y < sector.Size.Z;
                    break;
            }
            if (!good)
                return false;
            for (int y = 0; y < size.Y; y++)
                for (int p = 0; p < size.X; p++)
                {
                    Portal val = sector.Portals[side, position + new Point2(p, y)];
                    if (val == null || !euclid && val.Natural) //Euclid Portals can be overrided
                        continue;
                    return false;
                }
            return true;
        }

        public bool IsTerrainOnPortal(int x, int y, int z)
        {
            switch (Side)
            {
                case SectorSide.Left:
                    if (x != 0)
                        return false;
                    return
                        y >= Position.Y && y <= Position.Y + Size.Y &&
                                     z >= Position.X && z <= Position.X + Size.X;
                case SectorSide.Right:
                    if (x != Sector.Size.X)
                        return false;
                    return
                        y >= Position.Y && y <= Position.Y + Size.Y &&
                                     z >= Position.X && z <= Position.X + Size.X;
                case SectorSide.Fore:
                    if (z != 0)
                        return false;
                    return
                        y >= Position.Y && y <= Position.Y + Size.Y &&
                                     x >= Position.X && x <= Position.X + Size.X;
                case SectorSide.Back:
                    if (z != Sector.Size.Z)
                        return false;
                    return
                        y >= Position.Y && y <= Position.Y + Size.Y &&
                        x >= Position.X && x <= Position.X + Size.X;
                default:
                    return false;
            }
        }

        internal Bounds3 PortalTerrainBounds
        {
            get
            {
                switch (Side)
                {
                    case SectorSide.Back:
                        return new Bounds3(new Point3(Position.X, Position.Y, Sector.Size.Z),
                                           new Point3(Size.X + 1, Size.Y + 1, 1));
                    case SectorSide.Fore:
                        return new Bounds3(new Point3(Position.X, Position.Y, 0),
                                           new Point3(Size.X + 1, Size.Y + 1, 1));
                    case SectorSide.Right:
                        return new Bounds3(new Point3(Sector.Size.X, Position.Y, Position.X),
                                           new Point3(1, Size.Y + 1, Size.X + 1));
                    case SectorSide.Left:
                        return new Bounds3(new Point3(0, Position.Y, Position.X),
                                           new Point3(1, Size.Y + 1, Size.X + 1));
                    case SectorSide.Bottom:
                        return new Bounds3(new Point3(Position.X, 0, Position.Y),
                                           new Point3(Size.X + 1, 1, Size.Y + 1));
                    case SectorSide.Top:
                        return new Bounds3(new Point3(Position.X, Sector.Size.Y, Position.Y),
                                           new Point3(Size.X + 1, 1, Size.Y + 1));
                    default:
                        return new Bounds3(Point3.Zero, Point3.Zero);
                }
            }
        }

        internal void GetDimentions(out Vector3 min, out Vector3 max)
        {
            switch (Side)
            {
                case SectorSide.Back:
                    min = new Vector3(Position.X, Position.Y, Sector.Size.Z);
                    max = new Vector3(Position.X + Size.X, Position.Y + Size.Y, Sector.Size.Z);
                    return;
                case SectorSide.Fore:
                    min = new Vector3(Position.X, Position.Y, 0);
                    max = new Vector3(Position.X + Size.X, Position.Y + Size.Y, 0);
                    return;
                case SectorSide.Right:
                    min = new Vector3(Sector.Size.X, Position.Y, Position.X);
                    max = new Vector3(Sector.Size.X, Position.Y + Size.Y, Position.X + Size.X);
                    return;
                case SectorSide.Left:
                    min = new Vector3(0, Position.Y, Position.X);
                    max = new Vector3(0, Position.Y + Size.Y, Position.X + Size.X);
                    return;
                case SectorSide.Bottom:
                    min = new Vector3(Position.X, 0, Position.Y);
                    max = new Vector3(Position.X + Size.X, 0, Position.Y + Size.Y);
                    return;
                case SectorSide.Top:
                    min = new Vector3(Position.X, Sector.Size.Y, Position.Y);
                    max = new Vector3(Position.X + Size.X, Sector.Size.Y, Position.Y + Size.Y);
                    return;
                default:
                    min = new Vector3(float.NaN);
                    max = new Vector3(float.NaN);
                    return;
            }
        }

        internal Bounds3 PortalNodeBounds
        {
            get
            {
                switch (Side)
                {
                    case SectorSide.Back:
                        return new Bounds3(new Point3(Position.X - 1, Position.Y - 1, Sector.Size.Z - 1),
                                           new Point3(Size.X + 2, Size.Y + 2, 1));
                    case SectorSide.Fore:
                        return new Bounds3(new Point3(Position.X - 1, Position.Y - 1, 0),
                                           new Point3(Size.X + 2, Size.Y + 2, 1));
                    case SectorSide.Right:
                        return new Bounds3(new Point3(Sector.Size.X - 1, Position.Y - 1, Position.X - 1),
                                           new Point3(1, Size.Y + 2, Size.X + 2));
                    case SectorSide.Left:
                        return new Bounds3(new Point3(0, Position.Y - 1, Position.X - 1),
                                           new Point3(1, Size.Y + 2, Size.X + 2));
                    case SectorSide.Bottom:
                        return new Bounds3(new Point3(Position.X - 1, 0, Position.Y - 1),
                                           new Point3(Size.X + 2, 1, Size.Y + 2));
                    case SectorSide.Top:
                        return new Bounds3(new Point3(Position.X - 1, Sector.Size.Y - 1, Position.Y - 1),
                                           new Point3(Size.X + 2, 1, Size.Y + 2));
                    default:
                        return new Bounds3(Point3.Zero, Point3.Zero);
                }
            }
        }

        internal Point3 GetPortalStart()
        {
            Point3 start = Point3.Zero;
            switch (Side)
            {
                case SectorSide.Left:
                    start = new Point3(0, Position.Y, Position.X);
                    break;
                case SectorSide.Right:
                    start = new Point3(Sector.Size.X, Position.Y, Position.X);
                    break;
                case SectorSide.Fore:
                    start = new Point3(Position.X, Position.Y, 0);
                    break;
                case SectorSide.Back:
                    start = new Point3(Position.X, Position.Y, Sector.Size.Z);
                    break;
                case SectorSide.Bottom:
                    start = new Point3(Position.X, 0, Position.Y);
                    break;
                case SectorSide.Top:
                    start = new Point3(Position.X, Sector.Size.Y, Position.Y);
                    break;
            }
            return start;
        }

        internal Vector3 GetPortalCenter()
        {
            Vector3 center = Vector3.Zero;
            switch (Side)
            {
                case SectorSide.Left:
                    center = new Vector3(0, (float)Position.Y + (float)Size.Y / 2f, (float)Position.X + (float)Size.X / 2f);
                    break;
                case SectorSide.Right:
                    center = new Vector3(Sector.Size.X, (float)Position.Y + (float)Size.Y / 2f, (float)Position.X + (float)Size.X / 2f);
                    break;
                case SectorSide.Back:
                    center = new Vector3((float)Position.X + (float)Size.X / 2f, (float)Position.Y + (float)Size.Y / 2f, Sector.Size.Z);
                    break;
                case SectorSide.Fore:
                    center = new Vector3((float)Position.X + (float)Size.X / 2f, (float)Position.Y + (float)Size.Y / 2f, 0);
                    break;
                case SectorSide.Bottom:
                    center = new Vector3((float)Position.X + (float)Size.X / 2f, 0, (float)Position.Y + (float)Size.Y / 2f);
                    break;
                case SectorSide.Top:
                    center = new Vector3((float)Position.X + (float)Size.X / 2f, Sector.Size.Y, (float)Position.Y + (float)Size.Y / 2f);
                    break;
            }
            return center;
        }

        internal Matrix GetVectorToExitMatrix()
        {
            Matrix result = Matrix.CreateTranslation(Vector3.Zero);
            result.Up = Vector3.Up;
            switch (DirectionToExit)
            {

                case Direction.Left:
                    result.Right = Vector3.Forward;
                    result.Forward = Vector3.Left;
                    break;
                case Direction.Right:
                    result.Right = Vector3.Backward;
                    result.Forward = Vector3.Right;
                    break;
                case Direction.Back:
                    result.Right = Vector3.Left;
                    result.Forward = Vector3.Backward;
                    break;
                case Direction.Fore:
                    result.Forward = Vector3.Forward;
                    result.Right = Vector3.Right;
                    break;
                case Direction.Down:
                    break;
                case Direction.Up:
                    break;
            }
            return result;
        }

        internal Matrix GetPositionToExitMatrix(Matrix vectorToExitMatrix)
        {
            Vector3 centerDelta = OutPortal.PortalCenter - PortalCenter;

            Matrix result = Matrix.CreateTranslation(centerDelta);
            result = Matrix.Multiply(vectorToExitMatrix, result);
            return result;
        }

        internal Direction GetDirectionToExit()
        {
            switch (Side)
            {
                case SectorSide.Left:
                    switch (OutSide)
                    {
                        case SectorSide.Left:
                            return Direction.Back;
                        case SectorSide.Right:
                            return Direction.Fore;
                        case SectorSide.Back:
                            return Direction.Right;
                        case SectorSide.Fore:
                            return Direction.Left;
                    }
                    break;
                case SectorSide.Right:
                    switch (OutSide)
                    {
                        case SectorSide.Left:
                            return Direction.Fore;
                        case SectorSide.Right:
                            return Direction.Back;
                        case SectorSide.Back:
                            return Direction.Left;
                        case SectorSide.Fore:
                            return Direction.Right;
                    }
                    break;
                case SectorSide.Fore:
                    switch (OutSide)
                    {
                        case SectorSide.Left:
                            return Direction.Right;
                        case SectorSide.Right:
                            return Direction.Left;
                        case SectorSide.Back:
                            return Direction.Fore;
                        case SectorSide.Fore:
                            return Direction.Back;
                    }
                    break;
                case SectorSide.Back:
                    switch (OutSide)
                    {
                        case SectorSide.Left:
                            return Direction.Left;
                        case SectorSide.Right:
                            return Direction.Right;
                        case SectorSide.Back:
                            return Direction.Back;
                        case SectorSide.Fore:
                            return Direction.Fore;
                    }
                    break;
                case SectorSide.Bottom:
                    switch (OutSide)
                    {
                        case SectorSide.Bottom:
                            return Direction.Up;
                        case SectorSide.Top:
                            return Direction.Fore;
                    }
                    break;
                case SectorSide.Top:
                    switch (OutSide)
                    {
                        case SectorSide.Bottom:
                            return Direction.Fore;
                        case SectorSide.Top:
                            return Direction.Down;
                    }
                    break;
            }
            return Direction.Fore;
        }

        internal void GetFullVertexPositions(out Vector3[] data, out Vector3 normal)
        {
            Vector3 start = Vector3.Zero;
            Vector3 deltaX = Vector3.Zero;
            Vector3 deltaY = Vector3.Zero;
            normal = Vector3.Zero;

            switch (Side)
            {
                case SectorSide.Left:
                    normal = new Vector3(1, 0, 0);
                    start = new Vector3(
                        0,
                        Position.Y + Size.Y,
                        Position.X + Size.X);
                    deltaX = new Vector3(0, 0, -1);
                    deltaY = new Vector3(0, -1, 0);
                    break;
                case SectorSide.Right:
                    normal = new Vector3(-1, 0, 0);
                    start = new Vector3(
                        Sector.Size.X,
                        Position.Y + Size.Y,
                        Position.X);
                    deltaX = new Vector3(0, 0, 1);
                    deltaY = new Vector3(0, -1, 0);
                    break;
                case SectorSide.Fore:
                    normal = new Vector3(0, 0, 1);
                    start = new Vector3(
                        Position.X,
                        Position.Y + Size.Y,
                        0);
                    deltaX = new Vector3(1, 0, 0);
                    deltaY = new Vector3(0, -1, 0);
                    break;
                case SectorSide.Back:
                    normal = new Vector3(0, 0, -1);
                    start = new Vector3(
                        Position.X + Size.X,
                        Position.Y + Size.Y,
                        0);
                    deltaX = new Vector3(-1, 0, 0);
                    deltaY = new Vector3(0, -1, 0);
                    break;
                case SectorSide.Bottom:
                    normal = new Vector3(0, 1, 0); ;
                    start = new Vector3(
                        Position.X,
                        0,
                        Position.Y);
                    deltaX = new Vector3(1, 0, 0);
                    deltaY = new Vector3(0, 0, 1);
                    break;
                case SectorSide.Top:
                    normal = new Vector3(0, -1, 0);
                    start = new Vector3(
                        Position.X,
                        Sector.Size.Y,
                        Position.Y + Size.Y);
                    deltaX = new Vector3(1, 0, 0);
                    deltaY = new Vector3(0, 0, -1);
                    break;
            }

            data = new Vector3[Size.X * Size.Y * 6];
            for (int y = 0; y < Size.Y; y++)
                for (int x = 0; x < Size.X; x++)
                {
                    int index = (y * Size.X + x) * 6;
                    Vector3 tl = start + (x + 0) * deltaX + (y + 0) * deltaY;
                    Vector3 tr = start + (x + 1) * deltaX + (y + 0) * deltaY;
                    Vector3 bl = start + (x + 0) * deltaX + (y + 1) * deltaY;
                    Vector3 br = start + (x + 1) * deltaX + (y + 1) * deltaY;
                    data[index + 0] = bl;
                    data[index + 1] = tl;
                    data[index + 2] = tr;
                    data[index + 3] = br;
                    data[index + 4] = bl;
                    data[index + 5] = tr;
                }
        }

        internal float IntersectSquare(Vector3 position, Vector2 size)
        {
            Vector2 min = Vector2.Zero;
            Vector2 max = Vector2.Zero;
            switch (Side)
            {
                case SectorSide.Left:
                case SectorSide.Right:
                    min = Vector2.Max(
                        Position.Vector,
                        new Vector2(position.Z - size.X / 2f, position.Y - size.Y / 2f)
                        );
                    max = Vector2.Min(
                        (Position + Size).Vector,
                        new Vector2(position.Z + size.X / 2f, position.Y + size.Y / 2f)
                        );

                    break;
                case SectorSide.Fore:
                case SectorSide.Back:
                    min = Vector2.Max(
                        Position.Vector,
                        new Vector2(position.X - size.X / 2f, position.Y - size.Y / 2f)
                        );
                    max = Vector2.Min(
                        (Position + Size).Vector,
                        new Vector2(position.X + size.X / 2f, position.Y + size.Y / 2f)
                        );
                    break;
                case SectorSide.Top:
                case SectorSide.Bottom:
                    min = Vector2.Max(
                       Position.Vector,
                       new Vector2(position.X - size.X / 2f, position.Z - size.X / 2f)
                       );
                    max = Vector2.Min(
                        (Position + Size).Vector,
                        new Vector2(position.X + size.X / 2f, position.Z + size.X / 2f)
                        );
                    break;
            }
            Vector2 diff = max - min;
            if (diff.X <= 0 || diff.Y <= 0)
                return 0;
            return diff.X * diff.Y;
        }

        internal void GetVertexPositions(out Vector3 topLeft, out Vector3 topRight, out Vector3 bottomRight, out Vector3 bottomLeft, out Vector3 normal)
        {
            topLeft = Vector3.Zero;
            topRight = Vector3.Zero;
            bottomLeft = Vector3.Zero;
            bottomRight = Vector3.Zero;
            normal = Vector3.Zero;
            switch (Side)
            {
                case SectorSide.Left:
                    normal = new Vector3(1, 0, 0);
                    topLeft = new Vector3(
                        0,
                        Position.Y + Size.Y,
                        Position.X + Size.X);
                    topRight = new Vector3(
                       0,
                       Position.Y + Size.Y,
                       Position.X);
                    bottomLeft = new Vector3(
                        0,
                        Position.Y,
                        Position.X + Size.X);
                    bottomRight = new Vector3(
                       0,
                       Position.Y,
                        Position.X);
                    break;
                case SectorSide.Right:
                    normal = new Vector3(-1, 0, 0);
                    topLeft = new Vector3(
                        Sector.Size.X,
                        Position.Y + Size.Y,
                        Position.X);
                    topRight = new Vector3(
                        Sector.Size.X,
                       Position.Y + Size.Y,
                        Position.X + Size.X);
                    bottomLeft = new Vector3(
                        Sector.Size.X,
                        Position.Y,
                        Position.X);
                    bottomRight = new Vector3(
                        Sector.Size.X,
                        Position.Y,
                        Position.X + Size.X);
                    break;

                case SectorSide.Fore:
                    normal = new Vector3(0, 0, 1);
                    topLeft = new Vector3(
                        Position.X,
                        Position.Y + Size.Y,
                        0);
                    topRight = new Vector3(
                        Position.X + Size.X,
                        Position.Y + Size.Y,
                        0);
                    bottomLeft = new Vector3(
                        Position.X,
                        Position.Y,
                        0);
                    bottomRight = new Vector3(
                        Position.X + Size.X,
                        Position.Y,
                        0);
                    break;
                case SectorSide.Back:
                    normal = new Vector3(0, 0, -1);
                    topLeft = new Vector3(
                        Position.X + Size.X,
                        Position.Y + Size.Y,
                        Sector.Size.Z);
                    topRight = new Vector3(
                        Position.X,
                        Position.Y + Size.Y,
                        Sector.Size.Z);
                    bottomLeft = new Vector3(
                        Position.X + Size.X,
                        Position.Y,
                        Sector.Size.Z);
                    bottomRight = new Vector3(
                        Position.X,
                        Position.Y,
                        Sector.Size.Z);
                    break;
                case SectorSide.Bottom:
                    normal = new Vector3(0, 1, 0);
                    topLeft = new Vector3(
                        Position.X,
                        0,
                        Position.Y);
                    topRight = new Vector3(
                        Position.X + Size.X,
                        0,
                        Position.Y);
                    bottomLeft = new Vector3(
                        Position.X,
                        0,
                        Position.Y + Size.Y);
                    bottomRight = new Vector3(
                        Position.X + Size.X,
                        0,
                        Position.Y + Size.Y);
                    break;
                case SectorSide.Top:
                    normal = new Vector3(0, -1, 0);
                    topLeft = new Vector3(
                        Position.X,
                        Sector.Size.Y,
                        Position.Y + Size.Y);
                    topRight = new Vector3(
                        Position.X + Size.X,
                        Sector.Size.Y,
                        Position.Y + Size.Y);
                    bottomLeft = new Vector3(
                        Position.X,
                        Sector.Size.Y,
                        Position.Y);
                    bottomRight = new Vector3(
                        Position.X + Size.X,
                        Sector.Size.Y,
                        Position.Y);

                    break;
            }
        }
    }
}
