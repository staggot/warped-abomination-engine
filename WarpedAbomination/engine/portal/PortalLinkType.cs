﻿using System;
namespace WarpedAbominationEngine
{
    public enum PortalLinkType : byte
    {
        None = 0,
        Thing = 1,
        Mesh = 2,
        PhysicsBody = 3,
        StaticBody = 4
    }
}
