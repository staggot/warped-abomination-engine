﻿using System;
using Microsoft.Xna.Framework;

namespace WarpedAbominationEngine
{
    public class WarpPortal : Portal
    {
        readonly static float[] horisontalAngleAdder = new float[]
        {
            -MathHelper.PI/2f,
            MathHelper.PI/2f,
            0,
            MathHelper.PI
        };

        internal Matrix VectorToExitMatrix;

        public override bool Natural => false;

        internal override void Calculate()
        {
            DirectionToExit = GetDirectionToExit();
            VectorToExitMatrix = GetVectorToExitMatrix();
        }

        internal override NodeShape ModifyNodeFrom(NodeShape node)
        {
            return node;
        }

        internal override NodeShape ModifyNodeTo(NodeShape node)
        {
            return node;
        }

        internal override Vector3 TransformDirectionFrom(Vector3 direction)
        {
            return OutPortal.TransformDirectionTo(direction);
        }

        internal override Vector3 TransformDirectionTo(Vector3 direction)
        {
            return Vector3.Transform(direction, VectorToExitMatrix);
        }

        internal override float TranformYawFrom(float angle)
        {
            return angle - horisontalAngleAdder[(int)DirectionToExit];
        }

        internal override float TransformYawTo(float angle)
        {
            return angle + horisontalAngleAdder[(int)DirectionToExit];
        }

        internal override float TransformPitchFrom(float angle)
        {
            return angle;
        }

        internal override float TransformPitchTo(float angle)
        {
            return angle;
        }

        internal override Point3 TransformNodePositionFrom(Point3 position)
        {
            return new Point3(TransformPositionFrom(position.Vector + new Vector3(0.5f)));
        }

        internal override Point3 TransformNodePositionTo(Point3 position)
        {
            return new Point3(TransformPositionTo(position.Vector + new Vector3(0.5f)));
        }

        internal override Vector3 TransformPositionFrom(Vector3 position)
        {
            return OutPortal.TransformPositionTo(position);
        }

        internal override Vector3 TransformPositionTo(Vector3 position)
        {
            return Vector3.Transform(position - PortalCenter, VectorToExitMatrix) + OutPortal.PortalCenter;
        }

        internal override Point3 TransformTerrainPositionFrom(Point3 position)
        {
            return OutPortal.TransformTerrainPositionTo(position);
        }

        internal override Point3 TransformTerrainPositionTo(Point3 position)
        {
            //Vector3 delta = Vector3.Transform(new Vector3(0.5f), VectorToExitMatrix); 
            //Vector3 pos = Vector3.Transform((position.Vector + new Vector3(0.5f)) - PortalCenter, VectorToExitMatrix) + OutPortal.PortalCenter;
            return new Point3(TransformPositionTo(position.Vector));
        }

        internal override float TransformRollTo(float angle)
        {
            return angle;
        }

        internal override float TransformRollFrom(float angle)
        {
            return angle;
        }
    }
}
