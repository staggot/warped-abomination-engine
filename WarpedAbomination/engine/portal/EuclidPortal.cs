﻿using System;
using Microsoft.Xna.Framework;

namespace WarpedAbominationEngine
{
    public class EuclidPortal : Portal

    {
        public Point3 DeltaInt;
        public Vector3 DeltaFloat;


        public override bool Natural => true;

        public EuclidPortal()
        {
            
        }

        internal override void Calculate()
        {
            DeltaInt = OutPortal.GetPortalStart() - GetPortalStart();
            DeltaFloat = DeltaInt.Vector;
        }

        internal override NodeShape ModifyNodeFrom(NodeShape node)
        {
            return node;
        }

        internal override NodeShape ModifyNodeTo(NodeShape node)
        {
            return node;
        }

        internal override Vector3 TransformDirectionFrom(Vector3 direction)
        {
            return direction;
        }

        internal override Vector3 TransformDirectionTo(Vector3 direction)
        {
            return direction;
        }

        internal override Point3 TransformNodePositionFrom(Point3 position)
        {
            return position - DeltaInt;   //this.OutSector.Position - this.Sector.Position + position;
        }

        internal override Point3 TransformNodePositionTo(Point3 position)
        {
            return position + DeltaInt;   //return this.Sector.Position - this.OutSector.Position + position;
        }

        internal override Vector3 TransformPositionFrom(Vector3 position)
        {
            return position - DeltaFloat; //return this.OutSector.Position.Vector - this.Sector.Position.Vector + position;
        }

        internal override Vector3 TransformPositionTo(Vector3 position)
        {
            return position + DeltaFloat; //return this.Sector.Position.Vector - this.OutSector.Position.Vector + position;
        }

        internal override Point3 TransformTerrainPositionFrom(Point3 position)
        {
            return position - DeltaInt;   //this.OutSector.Position - this.Sector.Position + position;
        }

        internal override Point3 TransformTerrainPositionTo(Point3 position)
        {
            return position + DeltaInt;   //this.Sector.Position - this.OutSector.Position + position;
        }

        internal override float TransformYawTo(float angle)
        {
            return angle;
        }

        internal override float TransformPitchTo(float angle)
        {
            return angle;
        }

        internal override float TranformYawFrom(float angle)
        {
            return angle;
        }

        internal override float TransformPitchFrom(float angle)
        {
            return angle;
        }

        internal override float TransformRollTo(float angle)
        {
            return angle;
        }

        internal override float TransformRollFrom(float angle)
        {
            return angle;
        }
    }
}
