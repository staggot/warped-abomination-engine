﻿using System;
namespace WarpedAbominationEngine
{
    public enum PortalType : byte
    {
        Euclid = 0,
        Warp = 1,
        EuclidWall = 2,
        WarpWall = 3,
        Mirror = 4
    };
}
