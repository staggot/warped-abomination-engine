﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace WarpedAbominationEngine
{
    public class PortalLinkEnumerator : IEnumerator<PortalLink>
    {
        public PortalLinkEnumerator(PortalLink first)
        {
            this.first = first;
            next = first;
            Current = null;

        }

        PortalLink first, next;

        public PortalLink Current { get; private set; }

        object IEnumerator.Current => Current;

        public bool MoveNext()
        {
            Current = next;
            if (Current != null)
            {
                next = Current.Next;
                return true;
            }
            return false;
        }

        public void Reset()
        {
            next = first;
            Current = null;
        }

        public void Dispose()
        {
            first = null;
            next = first;
            Current = null;
        }
    }


    public class PortalLink : IEnumerable<PortalLink>
    {
        public PortalLink Previous;
        public PortalLink Next;
        public Sector Sector;
        public PortalLinkable Target;
        public Portal Portal;
        public PortalLinkType Type => Target.LinkType;

        public PortalLink()
        {

        }

        public IEnumerator<PortalLink> GetEnumerator()
        {
            return new PortalLinkEnumerator(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        internal void Remove()
        {
            lock (Sector.Portals.LinkLock)
            {
                if (Previous == null)
                {
                    Sector.Portals[Type] = Next;
                    if (Next != null)
                        Next.Previous = null;
                }
                else
                {
                    Previous.Next = Next;
                    if (Next != null)
                        Next.Previous = Previous;
                }
                if (Portal == null)
                {
                    Target.Link = null;
                }
                else
                {
                    Target.ChildLinks[(int)Portal.OutPortal.Side] = null;
                }
            }
        }

        internal static PortalLink Add(Sector sector, PortalLinkable target, Portal portal)
        {
            lock (sector.Portals.LinkLock)
            {
                PortalLink next = new PortalLink()
                {
                    Sector = sector,
                    Target = target,
                    Portal = portal,
                    Next = sector.Portals[target.LinkType],
                    Previous = null
                };
                sector.Portals[target.LinkType] = next;
                if (next.Next != null)
                    next.Next.Previous = next;
                if (portal == null)
                    target.Link = next;
                else
                    target.ChildLinks[(int)portal.OutPortal.Side] = next;
                return next;
            }
        }

    }
}
