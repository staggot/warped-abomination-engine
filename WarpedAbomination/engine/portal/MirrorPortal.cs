﻿using System;
using Microsoft.Xna.Framework;

namespace WarpedAbominationEngine
{
    public class MirrorPortal : Portal

    {
        public override bool Natural => false;

        internal override void Calculate()
        {
            throw new NotImplementedException();
        }

        internal override NodeShape ModifyNodeFrom(NodeShape node)
        {
            throw new NotImplementedException();
        }

        internal override NodeShape ModifyNodeTo(NodeShape node)
        {
            throw new NotImplementedException();
        }

        internal override Vector3 TransformDirectionFrom(Vector3 direction)
        {
            throw new NotImplementedException();
        }

        internal override Vector3 TransformDirectionTo(Vector3 direction)
        {
            throw new NotImplementedException();
        }

        internal override float TranformYawFrom(float angle)
        {
            throw new NotImplementedException();
        }

        internal override float TransformYawTo(float angle)
        {
            throw new NotImplementedException();
        }

        internal override Point3 TransformNodePositionFrom(Point3 position)
        {
            throw new NotImplementedException();
        }

        internal override Point3 TransformNodePositionTo(Point3 position)
        {
            throw new NotImplementedException();
        }

        internal override Vector3 TransformPositionFrom(Vector3 position)
        {
            throw new NotImplementedException();
        }

        internal override Vector3 TransformPositionTo(Vector3 position)
        {
            throw new NotImplementedException();
        }

        internal override Point3 TransformTerrainPositionFrom(Point3 position)
        {
            throw new NotImplementedException();
        }

        internal override Point3 TransformTerrainPositionTo(Point3 position)
        {
            throw new NotImplementedException();
        }

        internal override float TransformPitchFrom(float angle)
        {
            throw new NotImplementedException();
        }

        internal override float TransformPitchTo(float angle)
        {
            throw new NotImplementedException();
        }

        internal override float TransformRollTo(float angle)
        {
            throw new NotImplementedException();
        }

        internal override float TransformRollFrom(float angle)
        {
            throw new NotImplementedException();
        }
    }
}
