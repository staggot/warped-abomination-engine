﻿using System;
using Microsoft.Xna.Framework;

#pragma warning disable RECS0025 //STFU! I know what I'm doing.

namespace WarpedAbominationEngine
{
    public partial class PortalPair
    {
        public Scene Scene;
        public PortalType Type;
        public SectorSide Side1, Side2;
        public SectorID Sector1, Sector2;
        public Point2 Position1, Position2;
        public Point2 Size;
        internal Portal Portal1, Portal2;
        public Vector2 Wobble;

        public ushort TextureData;
        public PortalTextureType TextureType;

        public bool IsReadyToLink
        {
            get
            {
                return
                    Scene[Sector1.RegionID] != null && (Scene[Sector2.RegionID] != null || Type == PortalType.Mirror) &&
                    (Scene[Sector1.RegionID].State == RegionState.Loaded) && (Scene[Sector2.RegionID].State == RegionState.Loaded || Type == PortalType.Mirror);
            }
        }


        public bool IsLinked
        {
            get { return Portal1 != null || Portal2 != null; }
        }

        void SetDefaultsMirror()
        {
            Portal2 = null;
            Portal1.Sector = Scene[Sector1];
            Portal1.OutPortal = Portal2;
            Portal1.Pair = this;
            Portal1.Textured = TextureType;
            Portal1.TextureData = TextureData;
            Portal1.Side = Side1;
            Portal1.Position = Position1;
            Portal1.Size = Size;
            Portal1.Calculate();
        }

        internal void SetDefaults()
        {
            Portal1.Sector = Scene[Sector1];
            Portal2.Sector = Scene[Sector2];
            Portal1.OutPortal = Portal2;
            Portal2.OutPortal = Portal1;
            Portal1.OutSector = Portal2.Sector;
            Portal2.OutSector = Portal1.Sector;
            Portal1.Pair = this;
            Portal2.Pair = this;
            Portal1.Textured = TextureType;
            Portal2.Textured = TextureType;
            Portal1.TextureData = TextureData;
            Portal2.TextureData = TextureData;
            Portal1.Side = Side1;
            Portal2.Side = Side2;
            Portal1.OutSide = Portal2.Side;
            Portal2.OutSide = Portal1.Side;
            Portal1.Position = Position1;
            Portal2.Position = Position2;
            Portal1.Size = Size;
            Portal2.Size = Size;
            Portal1.PortalCenter = Portal1.GetPortalCenter();
            Portal2.PortalCenter = Portal2.GetPortalCenter();
            Portal1.Calculate();
            Portal2.Calculate();
        }

        internal bool SanityCheck()
        {
            bool ok = true;
            switch (Type)
            {
                case PortalType.EuclidWall:
                case PortalType.Euclid:
                    ok = ok &&
                        ((Side1 == SectorSide.Left && Side2 == SectorSide.Right) ||
                         (Side1 == SectorSide.Right && Side2 == SectorSide.Left) ||
                         (Side1 == SectorSide.Fore && Side2 == SectorSide.Back) ||
                         (Side1 == SectorSide.Back && Side2 == SectorSide.Fore) ||
                         (Side1 == SectorSide.Bottom && Side2 == SectorSide.Top) ||
                         (Side1 == SectorSide.Top && Side2 == SectorSide.Bottom));
                    break;
                case PortalType.Mirror:
                    break;
                case PortalType.Warp:
                case PortalType.WarpWall:
                    ok = ok && (((
                                    Side1 == SectorSide.Left ||
                                    Side1 == SectorSide.Right ||
                                    Side1 == SectorSide.Fore ||
                                    Side1 == SectorSide.Back) && (
                                    Side2 == SectorSide.Left ||
                                    Side2 == SectorSide.Right ||
                                    Side2 == SectorSide.Fore ||
                                    Side2 == SectorSide.Back)) || ((
                                    Side1 == SectorSide.Bottom ||
                                    Side1 == SectorSide.Top) && (
                                    Side2 == SectorSide.Bottom ||
                                    Side2 == SectorSide.Top)));
                    break;
                default:
                    return false;
            }
            return ok;
        }

        public static bool operator ==(PortalPair pp1, PortalPair pp2)
        {
            if (pp1 == null && pp2 == null)
                return true;
            return pp1.Equals(pp2);
        }

        public static bool operator !=(PortalPair pp1, PortalPair pp2)
        {
            if (pp1 == null && pp2 == null)
                return false;
            return !pp1.Equals(pp2);
        }

        public override bool Equals(object obj)
        {

            if (!(obj is PortalPair))
                return false;
            PortalPair pp = obj as PortalPair;
            return
                Size == pp.Size && Type == pp.Type && (
                    (
                        Sector1 == pp.Sector2 &&
                        Sector2 == pp.Sector1 &&
                        Size == pp.Size &&
                        Position1 == pp.Position2 &&
                        Position2 == pp.Position1 &&
                        Side1 == pp.Side2 &&
                        Side2 == pp.Side1
                    ) || (
                        Sector2 == pp.Sector2 &&
                        Sector1 == pp.Sector1 &&
                        Position2 == pp.Position2 &&
                        Position1 == pp.Position1 &&
                        Side2 == pp.Side2 &&
                        Side1 == pp.Side1
                    ));
        }

        public override int GetHashCode()
        {
            int hashCode = 820215177;
            hashCode = hashCode * -1521134295 + (Side1.GetHashCode() ^ Side2.GetHashCode());
            hashCode = hashCode * -1521134295 + (Sector1.GetHashCode() ^ Sector2.GetHashCode());
            hashCode = hashCode * -1521134295 + (Position1.GetHashCode() ^ Position2.GetHashCode());
            hashCode = hashCode * -1521134295 + Size.GetHashCode();
            return hashCode;
        }
    }
}
