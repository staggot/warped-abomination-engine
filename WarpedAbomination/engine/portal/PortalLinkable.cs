﻿using System;
using Microsoft.Xna.Framework;

namespace WarpedAbominationEngine
{

    public abstract class PortalLinkable : IDisposable
    {
        public PortalLinkType LinkType = PortalLinkType.None;
        public PortalLinkable Slave, Master;

        public PortalLink Link;
        public PortalLink[] ChildLinks = new PortalLink[6];

        readonly Portal[] portals = new Portal[6];

        public Sector Sector { get; set; }
        public Vector3 Position { get; set; }
        public abstract Vector2 Size { get; set; }
        bool isChildLinksEnabled = true;

        protected bool IsChildLinksEnabled
        {
            get
            {
                return isChildLinksEnabled;
            }
            set
            {
                isChildLinksEnabled = false;
                if (isChildLinksEnabled && !value)
                    SacrificeChildren();
            }
        }

        public Scene Scene { get { return Sector?.Scene; } }

        protected abstract void Teleport(Portal portal);

        protected void InternalTeleport(Portal teleport)
        {
            RemoveLinks();
            Sector = teleport.OutSector;
            Position = teleport.TransformPositionTo(Position);
            Link = PortalLink.Add(Sector, this, null);
            Teleport(teleport);
            if (Slave != null)
                Slave.InternalTeleport(teleport);
        }

        public virtual void Update(float time)
        {
            Portal teleport;
            if(Master != null)
            {
                if (Sector != Master.Sector && Link != null && Link.Sector != Master.Sector)
                    RemoveLinks();
                Sector = Master.Sector;
                Position = Master.Position;
                
                if (Link == null)
                    PortalLink.Add(Sector, this, null);
            }
            while (Master == null && (teleport = Sector.Portals.Portalize(Position, Size)) != null)
                InternalTeleport(teleport);
            if (isChildLinksEnabled)
                CreateSurroundingLinks();
        }

        protected void CreateSurroundingLinks()
        {
            Sector.Portals.GetSurroundingPortals(Position, Size, portals);
            for (int i = 0; i < ChildLinks.Length; i++)
            {
                PortalLink link = ChildLinks[i];
                if (link == null && portals[i] == null)
                    continue;
                if (link != null && portals[i] != null && link.Portal.OutPortal == portals[i])
                    continue;
                if (link == null)
                {
                    PortalLink.Add(portals[i].OutSector, this, portals[i].OutPortal);
                    continue;
                }
                if (portals[i] != link.Portal.OutPortal)
                {
                    link.Remove();
                    PortalLink.Add(portals[i].OutSector, this, portals[i].OutPortal);
                    continue;
                }
            }
        }

        void RemoveLinks()
        {
            if (Link != null)
                Link.Remove();
            SacrificeChildren();
        }

        protected void SacrificeChildren()
        {
            for (int i = 0; i < ChildLinks.Length; i++)
            {
                PortalLink link = ChildLinks[i];
                if (link != null)
                    link.Remove();
            }
        }

        public virtual void Dispose()
        {
            RemoveLinks();
        }
    }
}
