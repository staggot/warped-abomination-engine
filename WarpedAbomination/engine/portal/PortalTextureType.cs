﻿using System;
namespace WarpedAbominationEngine
{
    public enum PortalTextureType : byte
    {
        None = 0,
        Fill = 1,
        TileHorisontal = 2,
        TileVertival = 3,
        Tile = 4
    }
}
