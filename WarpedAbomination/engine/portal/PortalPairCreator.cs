﻿using System;
using Microsoft.Xna.Framework;

namespace WarpedAbominationEngine
{
    public partial class PortalPair
    {

        public static PortalPair CreatEuclid(SectorID sector1, SectorID sector2, Point2 position1, Point2 position2, SectorSide sector1Side, Point2 size, PortalTextureType textured = PortalTextureType.None, ushort textureData = 0,float wobble1 = 0, float wobble2 = 0, bool wall = false)
        {

            SectorSide sector2Side = SectorSide.Back;

            switch(sector1Side)
            {
                case SectorSide.Left:
                    sector2Side = SectorSide.Right;
                    break;
                case SectorSide.Right:
                    sector2Side = SectorSide.Left;
                    break;
                case SectorSide.Fore:
                    sector2Side = SectorSide.Back;
                    break;
                case SectorSide.Back:
                    sector2Side = SectorSide.Fore;
                    break;
                case SectorSide.Top:
                    sector2Side = SectorSide.Bottom;
                    break;
                case SectorSide.Bottom:
                    sector2Side = SectorSide.Top;
                    break;
            }

            return new PortalPair()
            {
                Sector1 = sector1,
                Sector2 = sector2,
                Position1 = position1,
                Position2 = position2,
                Size = size,
                Side1 = sector1Side,
                Side2 = sector2Side,
                TextureType = textured,
                TextureData = textureData,
                Wobble = new Vector2(wobble1,wobble2),
                Type = wall ? PortalType.EuclidWall : PortalType.Euclid
            };
        }

        public static PortalPair CreatWarp(SectorID sector1, SectorID sector2, Point2 position1, Point2 position2, SectorSide sector1Side, SectorSide sector2Side, Point2 size, PortalTextureType textured = PortalTextureType.None, ushort textureData = 0, float wobble1 = 0, float wobble2 = 0, bool wall = false)

        {
            return new PortalPair()
            {
                Sector1 = sector1,
                Sector2 = sector2,
                Position1 = position1,
                Position2 = position2,
                Size = size,
                Side1 = sector1Side,
                Side2 = sector2Side,
                TextureType = textured,
                TextureData = textureData,
                Wobble = new Vector2(wobble1, wobble2),
                Type = wall ? PortalType.WarpWall : PortalType.Warp
            };
        }
    }
}