﻿using System;

namespace WarpedAbominationEngine
{
    public delegate bool RegionMaterializer(Scene scene, Region target);
    public delegate void RegionDisposer(Scene scene, Region target);
    public delegate bool RegionChecher(Scene scene, Region target);
}
