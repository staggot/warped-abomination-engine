﻿using System;
namespace WarpedAbominationEngine
{
    public enum RegionState : byte
    {
        Unloaded = 0,
        RequestedLoad,
        Loading,
        Loaded,
        RequestedUnload,
        Unloading
    }

    public static class RegionStateMethonds
    {
        public static bool IsActive(this RegionState state)
        {
            return state == RegionState.Loaded;
        }

        public static bool IsReadyToUnload(this RegionState state)
        {
            return (
                state == RegionState.RequestedLoad ||
                state == RegionState.Loaded
            );
                                       
        }
    }
}
