﻿using System;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using WarpedAbominationEngine.Physics;
using WarpedAbominationEngine.Content;

namespace WarpedAbominationEngine
{
    public partial class Region
    {
        public const int RegionSize = 512;
        const int InitialSectors = 16;

        public Scene Scene;
        public string Name;
        public Point2 ID;

        public RegionState State = RegionState.Unloaded;
        internal long tick = 0;

        internal Dictionary<int, Sector> sectors = new Dictionary<int, Sector>(InitialSectors);
        public readonly object ResourceLock = new object();

        readonly object bodiesLock = new object();
        readonly object thingsLock = new object();

        internal Thing[] Things;
        internal PhysicsBody[] PhysicsBodies;

        readonly Stack<ushort> physicsBodiesIndexStack = new Stack<ushort>();
        readonly Stack<ushort> thingsIndexStack = new Stack<ushort>();

        public Bounds2 Bounds => new Bounds2(ID * RegionSize, new Point2(RegionSize, RegionSize));

        internal Region(Scene scene, Point2 position)
        {
            Scene = scene;
            this.ID = position;
            Things = new Thing[AbominationSystem.RegionThings];
            for (ushort i = 0; i < AbominationSystem.RegionThings; i++)
                thingsIndexStack.Push(i);
            PhysicsBodies = new PhysicsBody[AbominationSystem.RegionBodies];
            for (ushort i = 0; i < AbominationSystem.RegionBodies; i++)
                physicsBodiesIndexStack.Push(i);
            State = RegionState.Unloaded;
        }

        public Sector this[int id]
        {
            get { if (sectors.ContainsKey(id)) return sectors[id]; return null; }
        }

        public bool IsInside(Point3 position, Point3 size)
        {
            return Bounds.IsInside(new Bounds2(
                new Point2(position.X, position.Z),
                new Point2(size.X, size.Z)
            ));
        }

        public bool IsLoaded
        {
            get { return State == RegionState.Loaded; }
        }

        int GenerateSectorId()
        {
            for (int i = 0; ; i++)
                if (!sectors.ContainsKey(i))
                    return i;
        }

        public bool IsIdExists(int id)
        {
            return sectors.ContainsKey(id);
        }

        public Point2 Min => new Point2(ID.X * RegionSize, ID.X * RegionSize);
        public Point2 Max => new Point2(ID.X * RegionSize + RegionSize, ID.X * RegionSize + RegionSize);

        internal Sector CreateSector(Point3 position, Point3 size, SectorSideMask sideFlags = SectorSideMask.ALL)
        {
            if (!CanAddSector(position, size))
                return null;
            int id = GenerateSectorId();

            Sector sector = AddSector(id, position, size);
            sector.RegionEdge(out bool left, out bool right, out bool fore, out bool back);
            foreach (int sectorId in sectors.Keys)
            {
                if (sectorId == sector.ID)
                    continue;
                sector.Stick(sectors[sectorId], sideFlags);
            }
            if (left)
            {
                Region leftRegion = Scene.Storage.ForceGetRegion(ID + new Point2(-1, 0));
                if (leftRegion != null)
                    foreach (int sectorId in leftRegion.sectors.Keys)
                        sector.Stick(leftRegion.sectors[sectorId], sideFlags);
            }
            if (right)
            {
                Region rightRegion = Scene.Storage.ForceGetRegion(ID + new Point2(1, 0));
                if (rightRegion != null)
                    foreach (int sectorId in rightRegion.sectors.Keys)
                        sector.Stick(rightRegion.sectors[sectorId], sideFlags);
            }
            if (fore)
            {
                Region foreRegion = Scene.Storage.ForceGetRegion(ID + new Point2(0, -1));
                if (foreRegion != null)
                    foreach (int sectorId in foreRegion.sectors.Keys)
                        sector.Stick(foreRegion.sectors[sectorId], sideFlags);
            }
            if (back)
            {
                Region backRegion = Scene.Storage.ForceGetRegion(ID + new Point2(0, 1));
                if (backRegion != null)
                    foreach (int sectorId in backRegion.sectors.Keys)
                        sector.Stick(backRegion.sectors[sectorId], sideFlags);
            }
            return sector;
        }


        public bool CanAddSector(Point3 position, Point3 size)
        {
            Bounds3 bounds = new Bounds3(position, size);
            if (!IsInside(position, size))
                return false;
            foreach (int sectorId in sectors.Keys)
            {
                if (sectors[sectorId].Bounds.IsIntersects(bounds))
                    return false;
            }
            return true;
        }

        internal Sector AddSector(int id, Point3 position, Point3 size)
        {
            Sector sector = new Sector(id, position, size);
            sectors[id] = sector;
            sector.ID = id;
            sector.Region = this;
            Scene.Storage.AddSector(sector);
            return sector;
        }

        internal void AddThing(Thing thing)
        {
            lock (thingsLock)
            {
                if(thingsIndexStack.Count == 0)
                    throw new Exception("Out of thing ids in region");
                ushort id = thingsIndexStack.Pop();
                Things[id] = thing;
                thing.ID = id;
            }
        }

        public void RemoveSector(Sector sector)
        {
            if (sector.Region != this || sectors[sector.ID] != sector)
                throw new Exception("Can't remove sector, not in this region or inconsistent state");
            if (IsLoaded)
                Scene.Storage.RemoveSector(sector);
            sectors.Remove(sector.ID);
        }

        public void Update(float time)
        {

        }

        public void Dispose()
        {

        }

        public bool IsExpired {
            get { return Scene.Tick - tick > AbominationSystem.RegionExpirationTicks; }
        }

        public void Tick() {
            tick = Scene.Tick;
        }
    }
}
