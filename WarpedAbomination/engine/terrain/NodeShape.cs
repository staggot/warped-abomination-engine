﻿using System;
namespace WarpedAbominationEngine
{
    public struct NodeShape
    {
        public static NodeShape Empty = new NodeShape()
        {
            TerrainShapeData = 0,
            BlockShape = TerrainBlockShapes.Empty
        };

        public static NodeShape Full = new NodeShape()
        {
            TerrainShapeData = 0,
            BlockShape = TerrainBlockShapes.Full
        };

        internal ushort TerrainShapeData;
        internal ushort BlockShapeData;

        internal byte MegatableIndex
        {
            get
            {
                ushort or = (ushort)(((TerrainShapeData & 0xAAAA) >> 1) | (TerrainShapeData & 0x5555));
                return (byte)(
                        ((or & 0x4000) >> 7) |
                        ((or & 0x1000) >> 6) |
                        ((or & 0x0400) >> 5) |
                        ((or & 0x0100) >> 4) |
                        ((or & 0x0040) >> 3) |
                        ((or & 0x0010) >> 2) |
                        ((or & 0x0004) >> 1) |
                        (or & 0x0001)
                    );
            }
        }

        internal bool IsEmptyBlock
        {
            get
            {
                return BlockShapeMap == 0 || BlockShape == TerrainBlockShapes.Empty;
            }
        }

        internal bool IsEmptyTerrain
        {
            get
            {
                byte megaIndex = MegatableIndex;
                return megaIndex == 0 || megaIndex == 0xff;
            }
        }

        public override string ToString()
        {
            return string.Format("Shape: {0}, Block {1}", TerrainShapeData, BlockShapeData);
        }

        public byte BlockShapeMap
        {
            get
            {
                return (byte)(BlockShapeData >> 8);
            }
            set
            {
                BlockShapeData = (ushort)((BlockShapeData & 0x00ff) | (value << 8));
            }
        }

        public byte BlockShape
        {
            get
            {
                return (byte)(BlockShapeData & 0xff);
            }
            set
            {
                BlockShapeData = (ushort)((BlockShapeData & 0xff00) | (value));
            }
        }
    }
}
