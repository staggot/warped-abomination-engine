﻿using System;
namespace WarpedAbominationEngine
{
    public enum TerrainTextureSelector : byte
    {
        YAlt = 0,
        XAlt = 1,
        ZAlt = 2,
        BAlt = 3
    }
}
