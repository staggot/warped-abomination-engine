﻿using System;
using WarpedAbominationEngine.Content;

namespace WarpedAbominationEngine
{
    public struct NodeTexture
    {
        const uint SubdataMask = 0x7fff;
        const int SubdataBits = 15;

        public static NodeTexture Empty = new NodeTexture() {
            Data = 0
        };

        public uint Data;

        public ushort MainData {
            get { return (ushort)(Data & SubdataMask); }
            set {
                Data =
                    (Data & 0xFFFF8000) |
                    ((value & SubdataMask));
            }
        }

        public ushort AltData {
            get { return (ushort)((Data >> SubdataBits) & SubdataMask); }
            set {
                Data =
                    (Data & 0xC0007FFF) |
                    ((value & SubdataMask) << SubdataBits);
            }
        }

        public TerrainTextureSelector Selector {
            get { return (TerrainTextureSelector)((Data >> (SubdataBits + SubdataBits)) & 3); }
            set {
                Data =
                    (Data & 0x3FFFFFFF) |
                    ((uint)value & 0x3) << (SubdataBits + SubdataBits);
            }
        }

        public ushort DataX {
            get {
                switch (Selector) {
                    case TerrainTextureSelector.XAlt:
                        return AltData;
                    case TerrainTextureSelector.ZAlt:
                    case TerrainTextureSelector.YAlt:
                        return MainData;
                    default:
                        return MainData;
                }
            }
        }

        public ushort DataY {
            get {
                switch (Selector) {
                    case TerrainTextureSelector.YAlt:
                        return AltData;
                    case TerrainTextureSelector.ZAlt:
                    case TerrainTextureSelector.XAlt:
                        return MainData;
                    default:
                        return MainData;
                }
            }
        }

        public ushort DataZ {
            get {
                switch (Selector) {
                    case TerrainTextureSelector.ZAlt:
                        return AltData;
                    case TerrainTextureSelector.YAlt:
                    case TerrainTextureSelector.XAlt:
                        return MainData;
                    default:
                        return MainData;
                }
            }
        }

        public MegatextureFragment MainTexture {
            get {
                return AbominationContent.Textures[(int)EngineTextureTarget.Terrain][MainData];
            }
        }

        public MegatextureFragment AltTexture {
            get {
                return AbominationContent.Textures[(int)EngineTextureTarget.Terrain][MainData];
            }
        }

        public MegatextureFragment XTerrain {
            get {
                switch (Selector) {
                    case TerrainTextureSelector.XAlt:
                        return AltTexture;
                    default:
                        return MainTexture;
                }
            }
        }

        public MegatextureFragment YTerrain {
            get {
                switch (Selector) {
                    case TerrainTextureSelector.YAlt:
                        return AltTexture;
                    default:
                        return MainTexture;
                }
            }
        }

        public MegatextureFragment ZTerrain {
            get {
                switch (Selector) {
                    case TerrainTextureSelector.ZAlt:
                        return AltTexture;
                    default:
                        return MainTexture;
                }
            }
        }


        public MegatextureFragment XBlock {
            get {
                switch (Selector) {
                    case TerrainTextureSelector.XAlt:
                    case TerrainTextureSelector.BAlt:
                        return AltTexture;
                    default:
                        return MainTexture;
                }
            }
        }

        public MegatextureFragment YBlock {
            get {
                switch (Selector) {
                    case TerrainTextureSelector.YAlt:
                    case TerrainTextureSelector.BAlt:
                        return AltTexture;
                    default:
                        return MainTexture;
                }
            }
        }

        public MegatextureFragment ZBlock {
            get {
                switch (Selector) {
                    case TerrainTextureSelector.ZAlt:
                    case TerrainTextureSelector.BAlt:
                        return AltTexture;
                    default:
                        return MainTexture;
                }
            }
        }

        public NodeTexture Change(TerrainTextureFlags flags, ushort textureData)
        {
            NodeTexture result = this;
            switch (flags) {
                case TerrainTextureFlags.All:
                    if (result.MainData != textureData || result.AltData != textureData) {
                        result.MainData = textureData;
                        result.AltData = textureData;
                        return result;
                    }
                    break;
                case TerrainTextureFlags.None:
                    return result;
                case TerrainTextureFlags.ExceptX:
                    if (result.MainData != textureData || result.Selector != TerrainTextureSelector.XAlt) {
                        result.AltData = result.DataX;
                        result.MainData = textureData;
                        result.Selector = TerrainTextureSelector.XAlt;
                        return result;
                    }
                    break;
                case TerrainTextureFlags.ExceptY:
                    if (result.MainData != textureData || result.Selector != TerrainTextureSelector.YAlt) {
                        result.AltData = result.DataY;
                        result.MainData = textureData;
                        result.Selector = TerrainTextureSelector.YAlt;
                        return result;
                    }
                    break;
                case TerrainTextureFlags.ExceptZ:
                    if (result.MainData != textureData || result.Selector != TerrainTextureSelector.ZAlt) {
                        result.AltData = result.DataZ;
                        result.MainData = textureData;
                        result.Selector = TerrainTextureSelector.ZAlt;
                        return result;
                    }
                    break;
                case TerrainTextureFlags.OnlyX:
                    if (result.AltData != textureData || result.Selector != TerrainTextureSelector.XAlt) {
                        result.AltData = textureData;
                        result.Selector = TerrainTextureSelector.XAlt;
                        return result;
                    }
                    break;
                case TerrainTextureFlags.OnlyY:
                    if (result.AltData != textureData || result.Selector != TerrainTextureSelector.YAlt) {
                        result.AltData = textureData;
                        result.Selector = TerrainTextureSelector.YAlt;
                        return result;
                    }
                    break;
                case TerrainTextureFlags.OnlyZ:
                    if (result.AltData != textureData || result.Selector != TerrainTextureSelector.ZAlt) {
                        result.AltData = textureData;
                        result.Selector = TerrainTextureSelector.ZAlt;
                        return result;
                    }
                    break;
                case TerrainTextureFlags.TerrainBlockBalt:
                    if (result.AltData != textureData || result.MainData != textureData || result.Selector != TerrainTextureSelector.BAlt) {
                        result.AltData = textureData;
                        result.MainData = textureData;
                        result.Selector = TerrainTextureSelector.BAlt;
                        return result;
                    }
                    break;
                case TerrainTextureFlags.OnlyBlockBAlt:
                    if (result.AltData != textureData || result.Selector != TerrainTextureSelector.BAlt) {
                        result.AltData = textureData;
                        result.Selector = TerrainTextureSelector.BAlt;
                        return result;
                    }
                    break;
                case TerrainTextureFlags.OnlyTerrainBAlt:
                    if (result.MainData != textureData || result.Selector != TerrainTextureSelector.BAlt) {
                        result.MainData = textureData;
                        result.Selector = TerrainTextureSelector.BAlt;
                        return result;
                    }
                    break;
            }
            return result;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is NodeTexture))
                return false;
            return Data == ((NodeTexture)obj).Data;
        }

#pragma warning disable RECS0025
        public override int GetHashCode()
        {
            return -301143667 + Data.GetHashCode();
        }
#pragma warning restore RECS0025

        public static bool operator ==(NodeTexture a, NodeTexture b)
        {
            return a.Data == b.Data;
        }

        public static bool operator !=(NodeTexture a, NodeTexture b)
        {
            return a.Data != b.Data;
        }
    }
}
