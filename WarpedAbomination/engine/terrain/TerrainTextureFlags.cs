﻿using System;
namespace WarpedAbominationEngine
{
    public enum TerrainTextureFlags : byte
    {
        None = 0,
        OnlyTerrainBAlt,
        OnlyBlockBAlt,
        TerrainBlockBalt,
        OnlyX,
        OnlyY,
        OnlyZ,
        ExceptX,
        ExceptY,
        ExceptZ,
        All

    }
}
