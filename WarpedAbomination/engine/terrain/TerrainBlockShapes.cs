﻿using System;
namespace WarpedAbominationEngine
{
    public static class TerrainBlockShapes
    {
        public const byte
        Empty = 0,
        Left0 = 1,
        Left1 = 2,
        Left2 = 3,
        Right0 = 4,
        Right1 = 5,
        Right2 = 6,
        Down0 = 7,
        Down1 = 8,
        Down2 = 9,
        Up0 = 10,
        Up1 = 11,
        Up2 = 12,
        Fore0 = 13,
        Fore1 = 14,
        Fore2 = 15,
        Back0 = 16,
        Back1 = 17,
        Back2 = 18,
        WallX = 19,
        WallY = 20,
        WallZ = 21,
        Full = 31;

        readonly static byte[] sideSurfaces = new byte[6 * 5];

        static TerrainBlockShapes()
        {
            sideSurfaces[(int)SectorSide.Left * 5 + 0] = Empty;
            sideSurfaces[(int)SectorSide.Left * 5 + 1] = Left0;
            sideSurfaces[(int)SectorSide.Left * 5 + 2] = Left1;
            sideSurfaces[(int)SectorSide.Left * 5 + 3] = Left2;
            sideSurfaces[(int)SectorSide.Left * 5 + 4] = Full;

            sideSurfaces[(int)SectorSide.Right * 5 + 0] = Empty;
            sideSurfaces[(int)SectorSide.Right * 5 + 1] = Right0;
            sideSurfaces[(int)SectorSide.Right * 5 + 2] = Right1;
            sideSurfaces[(int)SectorSide.Right * 5 + 3] = Right2;
            sideSurfaces[(int)SectorSide.Right * 5 + 4] = Full;

            sideSurfaces[(int)SectorSide.Bottom * 5 + 0] = Empty;
            sideSurfaces[(int)SectorSide.Bottom * 5 + 1] = Down0;
            sideSurfaces[(int)SectorSide.Bottom * 5 + 2] = Down1;
            sideSurfaces[(int)SectorSide.Bottom * 5 + 3] = Down2;
            sideSurfaces[(int)SectorSide.Bottom * 5 + 4] = Full;

            sideSurfaces[(int)SectorSide.Top * 5 + 0] = Empty;
            sideSurfaces[(int)SectorSide.Top * 5 + 1] = Up0;
            sideSurfaces[(int)SectorSide.Top * 5 + 2] = Up1;
            sideSurfaces[(int)SectorSide.Top * 5 + 3] = Up2;
            sideSurfaces[(int)SectorSide.Top * 5 + 4] = Full;

            sideSurfaces[(int)SectorSide.Fore * 5 + 0] = Empty;
            sideSurfaces[(int)SectorSide.Fore * 5 + 1] = Fore0;
            sideSurfaces[(int)SectorSide.Fore * 5 + 2] = Fore1;
            sideSurfaces[(int)SectorSide.Fore * 5 + 3] = Fore2;
            sideSurfaces[(int)SectorSide.Fore * 5 + 4] = Full;

            sideSurfaces[(int)SectorSide.Back * 5 + 0] = Empty;
            sideSurfaces[(int)SectorSide.Back * 5 + 1] = Back0;
            sideSurfaces[(int)SectorSide.Back * 5 + 2] = Back1;
            sideSurfaces[(int)SectorSide.Back * 5 + 3] = Back2;
            sideSurfaces[(int)SectorSide.Back * 5 + 4] = Full;
        }

        public static byte GetSurface(byte value, SectorSide side)
        {
            value = Math.Min(value, (byte)4);
            return sideSurfaces[(int)side * 5 + value];
        }

        internal static bool CoversLeft(byte center, byte target, byte terrain)
        {
            bool leftFull = (
                target == Right0 ||
                target == Right1 ||
                target == Right2 ||
                target == Full ||
                ((terrain & MarchingCubes.FullLeft) == MarchingCubes.FullLeft)
            );
            switch (center)
            {
                case Empty:
                    return true;
                case Left0:
                    return leftFull;
                case Left1:
                    return leftFull;
                case Left2:
                    return leftFull;
                case Right0:
                    return false;
                case Right1:
                    return false;
                case Right2:
                    return false;
                case Down0:
                    return (leftFull ||
                            target == Down0 ||
                            target == Down1 ||
                            target == Down2);
                case Down1:
                    return (leftFull ||
                            target == Down1 ||
                            target == Down2);
                case Down2:
                    return (leftFull ||
                            target == Down2);
                case Up0:
                    return (leftFull ||
                            target == Up0 ||
                            target == Up1 ||
                            target == Up2);
                case Up1:
                    return (leftFull ||
                            target == Up1 ||
                            target == Up2);
                case Up2:
                    return (leftFull ||
                            target == Up2);
                case Fore0:
                    return (leftFull ||
                            target == Fore0 ||
                            target == Fore1 ||
                            target == Fore2);
                case Fore1:
                    return (leftFull ||
                            target == Fore1 ||
                            target == Fore2);
                case Fore2:
                    return (leftFull ||
                            target == Fore2);
                case Back0:
                    return (leftFull ||
                            target == Back0 ||
                            target == Back1 ||
                            target == Back2);
                case Back1:
                    return (leftFull ||
                            target == Back1 ||
                            target == Back2);
                case Back2:
                    return (leftFull ||
                            target == Back2);
                case WallX:
                    return false;
                case WallY:
                    return (leftFull ||
                            target == WallY ||
                            target == Up2 ||
                            target == Down2);
                case WallZ:
                    return (leftFull ||
                            target == WallZ ||
                            target == Fore2 ||
                            target == Back2);
                case Full:
                    return leftFull;
                default:
                    return true;
            }
        }
        internal static bool CoversRight(byte center, byte target, byte terrain)
        {
            bool rightFull = (
                target == Left0 ||
                target == Left1 ||
                target == Left2 ||
                target == Full ||
                ((terrain & MarchingCubes.FullRight) == MarchingCubes.FullRight)
            );
            switch (center)
            {
                case Empty:
                    return true;
                case Left0:
                    return false;
                case Left1:
                    return false;
                case Left2:
                    return false;
                case Right0:
                    return rightFull;
                case Right1:
                    return rightFull;
                case Right2:
                    return rightFull;
                case Down0:
                    return (rightFull ||
                            target == Down0 ||
                            target == Down1 ||
                            target == Down2);
                case Down1:
                    return (rightFull ||
                            target == Down1 ||
                            target == Down2);
                case Down2:
                    return (rightFull ||
                            target == Down2);
                case Up0:
                    return (rightFull ||
                            target == Up0 ||
                            target == Up1 ||
                            target == Up2);
                case Up1:
                    return (rightFull ||
                            target == Up1 ||
                            target == Up2);
                case Up2:
                    return (rightFull ||
                            target == Up2);
                case Fore0:
                    return (rightFull ||
                            target == Fore0 ||
                            target == Fore1 ||
                            target == Fore2);
                case Fore1:
                    return (rightFull ||
                            target == Fore1 ||
                            target == Fore2);
                case Fore2:
                    return (rightFull ||
                            target == Fore2);
                case Back0:
                    return (rightFull ||
                            target == Back0 ||
                            target == Back1 ||
                            target == Back2);
                case Back1:
                    return (rightFull ||
                            target == Back1 ||
                            target == Back2);
                case Back2:
                    return (rightFull ||
                            target == Back2);
                case WallX:
                    return false;
                case WallY:
                    return (rightFull ||
                            target == WallY ||
                            target == Up2 ||
                            target == Down2);
                case WallZ:
                    return (rightFull ||
                            target == WallZ ||
                            target == Fore2 ||
                            target == Back2);
                case Full:
                    return rightFull;
                default:
                    return true;
            }
        }
        internal static bool CoversDown(byte center, byte target, byte terrain)
        {
            bool downFull = (
                target == Up0 ||
                target == Up1 ||
                target == Up2 ||
                target == Full ||
                ((terrain & MarchingCubes.FullDown) == MarchingCubes.FullDown)
            );
            switch (center)
            {
                case Empty:
                    return true;
                case Left0:
                    return (downFull ||
                            target == Left0 ||
                            target == Left1 ||
                            target == Left2);
                case Left1:
                    return (downFull ||
                            target == Left1 ||
                            target == Left2);
                case Left2:
                    return (downFull ||
                            target == Left2);
                case Right0:
                    return (downFull ||
                            target == Right0 ||
                            target == Right1 ||
                            target == Right2);
                case Right1:
                    return (downFull ||
                            target == Right1 ||
                            target == Right2);
                case Right2:
                    return (downFull ||
                            target == Right2);
                case Down0:
                    return downFull;
                case Down1:
                    return downFull;
                case Down2:
                    return downFull;
                case Up0:
                    return false;
                case Up1:
                    return false;
                case Up2:
                    return false;
                case Fore0:
                    return (downFull ||
                            target == Fore0 ||
                            target == Fore1 ||
                            target == Fore2);
                case Fore1:
                    return (downFull ||
                            target == Fore1 ||
                            target == Fore2);
                case Fore2:
                    return (downFull ||
                            target == Fore2);
                case Back0:
                    return (downFull ||
                            target == Back0 ||
                            target == Back1 ||
                            target == Back2);
                case Back1:
                    return (downFull ||
                            target == Back1 ||
                            target == Back2);
                case Back2:
                    return (downFull ||
                            target == Back2);
                case WallX:
                    return (downFull ||
                            target == Left2 ||
                            target == Right2
                           );
                case WallY:
                    return false;
                case WallZ:
                    return (downFull ||
                            target == WallZ ||
                            target == Fore2 ||
                            target == Back2);
                case Full:
                    return downFull;
                default:
                    return true;
            }
        }
        internal static bool CoversUp(byte center, byte target, byte terrain)
        {
            bool upFull = (
                target == Down0 ||
                target == Down1 ||
                target == Down2 ||
                target == Full ||
                ((terrain & MarchingCubes.FullUp) == MarchingCubes.FullUp)
            );
            switch (center)
            {
                case Empty:
                    return true;
                case Left0:
                    return (upFull ||
                            target == Left0 ||
                            target == Left1 ||
                            target == Left2);
                case Left1:
                    return (upFull ||
                            target == Left1 ||
                            target == Left2);
                case Left2:
                    return (upFull ||
                            target == Left2);
                case Right0:
                    return (upFull ||
                            target == Right0 ||
                            target == Right1 ||
                            target == Right2);
                case Right1:
                    return (upFull ||
                            target == Right1 ||
                            target == Right2);
                case Right2:
                    return (upFull ||
                            target == Right2);
                case Down0:
                    return false;
                case Down1:
                    return false;
                case Down2:
                    return false;
                case Up0:
                    return upFull;
                case Up1:
                    return upFull;
                case Up2:
                    return upFull;
                case Fore0:
                    return (upFull ||
                            target == Fore0 ||
                            target == Fore1 ||
                            target == Fore2);
                case Fore1:
                    return (upFull ||
                            target == Fore1 ||
                            target == Fore2);
                case Fore2:
                    return (upFull ||
                            target == Fore2);
                case Back0:
                    return (upFull ||
                            target == Back0 ||
                            target == Back1 ||
                            target == Back2);
                case Back1:
                    return (upFull ||
                            target == Back1 ||
                            target == Back2);
                case Back2:
                    return (upFull ||
                            target == Back2);
                case WallX:
                    return (upFull ||
                            target == Left2 ||
                            target == Right2
                           );
                case WallY:
                    return false;
                case WallZ:
                    return (upFull ||
                            target == WallZ ||
                            target == Fore2 ||
                            target == Back2);
                case Full:
                    return upFull;
                default:
                    return true;
            }
        }

        internal static bool CoversFore(byte center, byte target, byte terrain)
        {
            bool foreFull = (
                target == Back0 ||
                target == Back1 ||
                target == Back2 ||
                target == Full ||
                ((terrain & MarchingCubes.FullFore) == MarchingCubes.FullFore)
            );
            switch (center)
            {
                case Empty:
                    return true;
                case Left0:
                    return (foreFull ||
                            target == Left0 ||
                            target == Left1 ||
                            target == Left2);
                case Left1:
                    return (foreFull ||
                            target == Left1 ||
                            target == Left2);
                case Left2:
                    return (foreFull ||
                            target == Left2);
                case Right0:
                    return (foreFull ||
                            target == Right0 ||
                            target == Right1 ||
                            target == Right2);
                case Right1:
                    return (foreFull ||
                            target == Right1 ||
                            target == Right2);
                case Right2:
                    return (foreFull ||
                            target == Right2);
                case Down0:
                    return (foreFull ||
                            target == Down0 ||
                            target == Down1 ||
                            target == Down2);
                case Down1:
                    return (foreFull ||
                            target == Down1 ||
                            target == Down2);
                case Down2:
                    return (foreFull ||
                            target == Down2);
                case Up0:
                    return (foreFull ||
                            target == Up0 ||
                            target == Up1 ||
                            target == Up2);
                case Up1:
                    return (foreFull ||
                            target == Up1 ||
                            target == Up2);
                case Up2:
                    return (foreFull ||
                            target == Up2);
                case Fore0:
                    return foreFull;
                case Fore1:
                    return foreFull;
                case Fore2:
                    return foreFull;
                case Back0:
                    return false;
                case Back1:
                    return false;
                case Back2:
                    return false;
                case WallX:
                    return (foreFull ||
                            target == Left2 ||
                            target == Right2
                           );
                case WallY:
                    return (foreFull ||
                            target == WallY ||
                            target == Up2 ||
                            target == Down2);
                case WallZ:
                    return false;
                case Full:
                    return foreFull;
                default:
                    return true;
            }
        }
        internal static bool CoversBack(byte center, byte target, byte terrain)
        {
            bool backFull = (
                target == Fore0 ||
                target == Fore1 ||
                target == Fore2 ||
                target == Full ||
                ((terrain & MarchingCubes.FullBack) == MarchingCubes.FullBack)
            );
            switch (center)
            {
                case Empty:
                    return true;
                case Left0:
                    return (backFull ||
                            target == Left0 ||
                            target == Left1 ||
                            target == Left2);
                case Left1:
                    return (backFull ||
                            target == Left1 ||
                            target == Left2);
                case Left2:
                    return (backFull ||
                            target == Left2);
                case Right0:
                    return (backFull ||
                            target == Right0 ||
                            target == Right1 ||
                            target == Right2);
                case Right1:
                    return (backFull ||
                            target == Right1 ||
                            target == Right2);
                case Right2:
                    return (backFull ||
                            target == Right2);
                case Down0:
                    return (backFull ||
                            target == Down0 ||
                            target == Down1 ||
                            target == Down2);
                case Down1:
                    return (backFull ||
                            target == Down1 ||
                            target == Down2);
                case Down2:
                    return (backFull ||
                            target == Down2);
                case Up0:
                    return (backFull ||
                            target == Up0 ||
                            target == Up1 ||
                            target == Up2);
                case Up1:
                    return (backFull ||
                            target == Up1 ||
                            target == Up2);
                case Up2:
                    return (backFull ||
                            target == Up2);
                case Fore0:
                    return false;
                case Fore1:
                    return false;
                case Fore2:
                    return false;
                case Back0:
                    return backFull;
                case Back1:
                    return backFull;
                case Back2:
                    return backFull;
                case WallX:
                    return (backFull ||
                            target == Left2 ||
                            target == Right2
                           );
                case WallY:
                    return (backFull ||
                            target == WallY ||
                            target == Up2 ||
                            target == Down2);
                case WallZ:
                    return false;
                case Full:
                    return backFull;
                default:
                    return true;
            }
        }
    }
}
