﻿using System;
using System.Collections.Generic;

namespace WarpedAbominationEngine
{
    public class TerrainTransaction
    {
        readonly TerrainTransaction parent;
        readonly Dictionary<Sector, TerrainTransaction> children = new Dictionary<Sector, TerrainTransaction>();
        readonly Sector sector;
        readonly bool applied = false;

        Point3 ImpactMin = new Point3(-1, -1, -1);
        Point3 ImpactMax = new Point3(-1, -1, -1);



        public TerrainTransaction(Sector sector) : this(null, sector)
        {
            this.sector = sector;
        }

        private TerrainTransaction(TerrainTransaction parent, Sector sector)
        {
            this.parent = parent;
            this.sector = sector;
        }


        public void SetTerrainField(Point3 position, byte value)
        {
            //sector.Terrain
        }
    }
}
