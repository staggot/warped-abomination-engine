﻿using System;
namespace WarpedAbominationEngine
{
    public class SectorTerrain
    {
        readonly Sector sector;
        internal readonly NodeShape[] nodeShapes;
        internal readonly NodeTexture[] nodeTextures;
        readonly byte[] field;
        readonly Point3 size;

        public SectorTerrain(Sector sector)
        {
            this.sector = sector;
            size = sector.Size;
            field = new byte[(size.X + 1) * (size.Z + 1) * (size.Y + 1)];
            nodeShapes = new NodeShape[size.X * size.Z * size.Y];
            nodeTextures = new NodeTexture[size.X * size.Z * size.Y];
        }

        public int FieldIndex(Point3 position)
        {
            return position.X + position.Z * (size.X + 1) + position.Y * (size.X + 1) * (size.Z + 1);
        }

        public int NodeIndex(Point3 position)
        {
            return position.X + position.Z * size.X + position.Y * size.X * size.Z;
        }

        public bool IsFieldInside(Point3 position)
        {
            return position.X >= 0 && position.Y >= 0 && position.Z >= 0 &&
                position.X <= size.X && position.Y <= size.Y && position.Z <= size.Z;
        }

        public SectorSideMask GetFieldTouchingSides(Point3 position)
        {
            SectorSideMask result = SectorSideMask.None;
            if (position.X == 0)
                result |= SectorSideMask.Left;
            if (position.Y == 0)
                result |= SectorSideMask.Bottom;
            if (position.Z == 0)
                result |= SectorSideMask.Fore;
            if (position.X == size.X)
                result |= SectorSideMask.Right;
            if (position.Y == size.Y)
                result |= SectorSideMask.Top;
            if (position.Z == size.Z)
                result |= SectorSideMask.Back;
            return result;
        }

        internal bool CalculateNode(Point3 position)
        {
            int index = NodeIndex(position);
            byte specialShape = nodeShapes[index].BlockShape;

            bool changed = false;
            ushort terrainData = 0;
            byte terrainMap = 0;

            for (int i = 0; i < MarchingCubes.PointPositions.Length; i++)
            {
                byte value = (byte)((field[FieldIndex(MarchingCubes.PointPositions[i] + position)]) & 0x03);
                if (specialShape != (byte)TerrainBlockShapes.Full)
                    terrainData |= (ushort)(value << (i * 2));
                terrainMap |= (byte)(value << i);
            }

            byte leftShape = GetSimpleRelativeShape(position, new Point3(-1, 0, 0)).BlockShape;
            byte rightShape = GetSimpleRelativeShape(position, new Point3(1, 0, 0)).BlockShape;
            byte foreShape = GetSimpleRelativeShape(position, new Point3(0, 0, -1)).BlockShape;
            byte backShape = GetSimpleRelativeShape(position, new Point3(0, 0, 1)).BlockShape;
            byte downShape = GetSimpleRelativeShape(position, new Point3(0, -1, 0)).BlockShape;
            byte upShape = GetSimpleRelativeShape(position, new Point3(0, 1, 0)).BlockShape;

            byte shapeMask = MarchingCubes.GetBlockMap(specialShape, terrainMap, leftShape, rightShape, downShape, upShape, foreShape, backShape);

            if (nodeShapes[index].BlockShapeMap != shapeMask || nodeShapes[index].TerrainShapeData != terrainData)
            {
                nodeShapes[index].BlockShapeMap = shapeMask;
                nodeShapes[index].TerrainShapeData = terrainData;
                changed = true;
            }

            return changed;
        }

        internal NodeShape GetNodeShape(Point3 position)
        {
            return nodeShapes[NodeIndex(position)];
        }

        internal NodeTexture GetNodetexture(Point3 position)
        {
            return nodeTextures[NodeIndex(position)];
        }

        internal void GetNode(Point3 position, out NodeShape shape, out NodeTexture texture)
        {
            int index = NodeIndex(position);
            shape = nodeShapes[index];
            texture = nodeTextures[index];
        }

        public NodeShape GetSimpleRelativeShape(Point3 from, Point3 delta)
        {
            Point3 current = from + delta;
            bool xOk = current.X >= 0 && current.X < size.X;
            bool yOk = current.Y >= 0 && current.Y < size.Y;
            bool zOk = current.Z >= 0 && current.Z < size.Z;

            if (xOk && yOk && zOk)
            {
                return nodeShapes[NodeIndex(current)];
            }
            Portal portal = null;
            if (current.X < 0 && yOk && zOk)
            {
                portal = sector.Portals.Get(SectorSide.Left,new Point2(from.Z, from.Y));
            }
            else if (current.X >= size.X && yOk && zOk)
            {
                portal = sector.Portals.Get(SectorSide.Right, new Point2(from.Z, from.Y));
            }
            else if (current.Y < 0 && xOk && zOk)
            {
                portal = sector.Portals.Get(SectorSide.Bottom, new Point2(from.X, from.Z));
            }
            else if (current.Y >= size.Y && xOk && zOk)
            {
                portal = sector.Portals.Get(SectorSide.Top, new Point2(from.X, from.Z));
            }
            else if (current.Z < 0 && xOk && yOk)
            {
                portal = sector.Portals.Get(SectorSide.Fore, new Point2(from.X, from.Y));
            }
            else if (current.Z >= size.Z && xOk && yOk)
            {
                portal = sector.Portals.Get(SectorSide.Back, new Point2(from.X, from.Y));
            }

            if (portal is EuclidPortal)
                return portal.OutSector.Terrain.GetSimpleRelativeShape(portal.TransformNodePositionTo(from), delta);
            if (portal is WarpPortal)
                return NodeShape.Empty;
            return NodeShape.Full;
        }
    }
}
