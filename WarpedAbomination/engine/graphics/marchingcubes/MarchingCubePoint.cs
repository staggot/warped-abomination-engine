﻿using System;

namespace WarpedAbominationEngine
{
    public static class MarchingCubePoint
    {
        internal const byte
        LeftDownFore = 0,
        LeftDownBack = 4,
        LeftUpFore = 3,
        LeftUpBack = 7,
        RightDownFore = 1,
        RightDownBack = 5,
        RightUpFore = 2,
        RightUpBack = 6;
    }
}
