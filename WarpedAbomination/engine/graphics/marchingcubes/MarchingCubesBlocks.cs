﻿using System;
using Microsoft.Xna.Framework;

namespace WarpedAbominationEngine
{
    public abstract partial class MarchingCubes
    {
        internal static Vector3[][][] blockShapePoints;
        static void InitBlockShapes()
        {
            blockShapePoints = new Vector3[256][][];
            for (int i = 0; i < blockShapePoints.Length; i++)
            {
                blockShapePoints[i] = new Vector3[][]{
                    new Vector3[0],
                    new Vector3[0],
                    new Vector3[0],
                    new Vector3[0],
                    new Vector3[0],
                    new Vector3[0]
                };
            }

            blockShapePoints[TerrainBlockShapes.Full] = new Vector3[][]
            {
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 0.0f), new Vector3(0.0f, 1.0f, 0.0f),
                    new Vector3(0.0f, 1.0f, 1.0f), new Vector3(0.0f, 0.0f, 1.0f)
                },
                new Vector3[] {
                    new Vector3(1.0f, 0.0f, 1.0f), new Vector3(1.0f, 1.0f, 1.0f),
                    new Vector3(1.0f, 1.0f, 0.0f), new Vector3(1.0f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 0.0f), new Vector3(0.0f, 0.0f, 1.0f),
                    new Vector3(1.0f, 0.0f, 1.0f), new Vector3(1.0f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 1.0f, 1.0f), new Vector3(0.0f, 1.0f, 0.0f),
                    new Vector3(1.0f, 1.0f, 0.0f), new Vector3(1.0f, 1.0f, 1.0f)
                },
                new Vector3[] {
                    new Vector3(1.0f, 0.0f, 0.0f), new Vector3(1.0f, 1.0f, 0.0f),
                    new Vector3(0.0f, 1.0f, 0.0f), new Vector3(0.0f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 1.0f), new Vector3(0.0f, 1.0f, 1.0f),
                    new Vector3(1.0f, 1.0f, 1.0f), new Vector3(1.0f, 0.0f, 1.0f)
                }
            };

            blockShapePoints[TerrainBlockShapes.Left0] = new Vector3[][]
            {
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 0.0f), new Vector3(0.0f, 1.0f, 0.0f),
                    new Vector3(0.0f, 1.0f, 1.0f), new Vector3(0.0f, 0.0f, 1.0f)
                },
                new Vector3[] {
                    new Vector3(0.25f, 0.0f, 1.0f), new Vector3(0.25f, 1.0f, 1.0f),
                    new Vector3(0.25f, 1.0f, 0.0f), new Vector3(0.25f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 0.0f), new Vector3(0.0f, 0.0f, 1.0f),
                    new Vector3(0.25f, 0.0f, 1.0f), new Vector3(0.25f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 1.0f, 1.0f), new Vector3(0.0f, 1.0f, 0.0f),
                    new Vector3(0.25f, 1.0f, 0.0f), new Vector3(0.25f, 1.0f, 1.0f)
                },
                new Vector3[] {
                    new Vector3(0.25f, 0.0f, 0.0f), new Vector3(0.25f, 1.0f, 0.0f),
                    new Vector3(0.0f, 1.0f, 0.0f), new Vector3(0.0f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 1.0f), new Vector3(0.0f, 1.0f, 1.0f),
                    new Vector3(0.25f, 1.0f, 1.0f), new Vector3(0.25f, 0.0f, 1.0f)
                }
            };

            blockShapePoints[TerrainBlockShapes.Left1] = new Vector3[][]
            {
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 0.0f), new Vector3(0.0f, 1.0f, 0.0f),
                    new Vector3(0.0f, 1.0f, 1.0f), new Vector3(0.0f, 0.0f, 1.0f)
                },
                new Vector3[] {
                    new Vector3(0.5f, 0.0f, 1.0f), new Vector3(0.5f, 1.0f, 1.0f),
                    new Vector3(0.5f, 1.0f, 0.0f), new Vector3(0.5f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 0.0f), new Vector3(0.0f, 0.0f, 1.0f),
                    new Vector3(0.5f, 0.0f, 1.0f), new Vector3(0.5f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 1.0f, 1.0f), new Vector3(0.0f, 1.0f, 0.0f),
                    new Vector3(0.5f, 1.0f, 0.0f), new Vector3(0.5f, 1.0f, 1.0f)
                },
                new Vector3[] {
                    new Vector3(0.5f, 0.0f, 0.0f), new Vector3(0.5f, 1.0f, 0.0f),
                    new Vector3(0.0f, 1.0f, 0.0f), new Vector3(0.0f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 1.0f), new Vector3(0.0f, 1.0f, 1.0f),
                    new Vector3(0.5f, 1.0f, 1.0f), new Vector3(0.5f, 0.0f, 1.0f)
                }
            };

            blockShapePoints[TerrainBlockShapes.Left2] = new Vector3[][]
            {
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 0.0f), new Vector3(0.0f, 1.0f, 0.0f),
                    new Vector3(0.0f, 1.0f, 1.0f), new Vector3(0.0f, 0.0f, 1.0f)
                },
                new Vector3[] {
                    new Vector3(0.75f, 0.0f, 1.0f), new Vector3(0.75f, 1.0f, 1.0f),
                    new Vector3(0.75f, 1.0f, 0.0f), new Vector3(0.75f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 0.0f), new Vector3(0.0f, 0.0f, 1.0f),
                    new Vector3(0.75f, 0.0f, 1.0f), new Vector3(0.75f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 1.0f, 1.0f), new Vector3(0.0f, 1.0f, 0.0f),
                    new Vector3(0.75f, 1.0f, 0.0f), new Vector3(0.75f, 1.0f, 1.0f)
                },
                new Vector3[] {
                    new Vector3(0.75f, 0.0f, 0.0f), new Vector3(0.75f, 1.0f, 0.0f),
                    new Vector3(0.0f, 1.0f, 0.0f), new Vector3(0.0f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 1.0f), new Vector3(0.0f, 1.0f, 1.0f),
                    new Vector3(0.75f, 1.0f, 1.0f), new Vector3(0.75f, 0.0f, 1.0f)
                }
            };
            blockShapePoints[TerrainBlockShapes.Right0] = new Vector3[][]
            {
                new Vector3[] {
                    new Vector3(0.75f, 0.0f, 0.0f), new Vector3(0.75f, 1.0f, 0.0f),
                    new Vector3(0.75f, 1.0f, 1.0f), new Vector3(0.75f, 0.0f, 1.0f)
                },
                new Vector3[] {
                    new Vector3(1.0f, 0.0f, 1.0f), new Vector3(1.0f, 1.0f, 1.0f),
                    new Vector3(1.0f, 1.0f, 0.0f), new Vector3(1.0f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.75f, 0.0f, 0.0f), new Vector3(0.75f, 0.0f, 1.0f),
                    new Vector3(1.0f, 0.0f, 1.0f), new Vector3(1.0f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.75f, 1.0f, 1.0f), new Vector3(0.75f, 1.0f, 0.0f),
                    new Vector3(1.0f, 1.0f, 0.0f), new Vector3(1.0f, 1.0f, 1.0f)
                },
                new Vector3[] {
                    new Vector3(1.0f, 0.0f, 0.0f), new Vector3(1.0f, 1.0f, 0.0f),
                    new Vector3(0.75f, 1.0f, 0.0f), new Vector3(0.75f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.75f, 0.0f, 1.0f), new Vector3(0.75f, 1.0f, 1.0f),
                    new Vector3(1.0f, 1.0f, 1.0f), new Vector3(1.0f, 0.0f, 1.0f)
                }
            };
            blockShapePoints[TerrainBlockShapes.Right1] = new Vector3[][]
            {
                new Vector3[] {
                    new Vector3(0.5f, 0.0f, 0.0f), new Vector3(0.5f, 1.0f, 0.0f),
                    new Vector3(0.5f, 1.0f, 1.0f), new Vector3(0.5f, 0.0f, 1.0f)
                },
                new Vector3[] {
                    new Vector3(1.0f, 0.0f, 1.0f), new Vector3(1.0f, 1.0f, 1.0f),
                    new Vector3(1.0f, 1.0f, 0.0f), new Vector3(1.0f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.5f, 0.0f, 0.0f), new Vector3(0.5f, 0.0f, 1.0f),
                    new Vector3(1.0f, 0.0f, 1.0f), new Vector3(1.0f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.5f, 1.0f, 1.0f), new Vector3(0.5f, 1.0f, 0.0f),
                    new Vector3(1.0f, 1.0f, 0.0f), new Vector3(1.0f, 1.0f, 1.0f)
                },
                new Vector3[] {
                    new Vector3(1.0f, 0.0f, 0.0f), new Vector3(1.0f, 1.0f, 0.0f),
                    new Vector3(0.5f, 1.0f, 0.0f), new Vector3(0.5f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.5f, 0.0f, 1.0f), new Vector3(0.5f, 1.0f, 1.0f),
                    new Vector3(1.0f, 1.0f, 1.0f), new Vector3(1.0f, 0.0f, 1.0f)
                }
            };
            blockShapePoints[TerrainBlockShapes.Right2] = new Vector3[][]
            {
                new Vector3[] {
                    new Vector3(0.25f, 0.0f, 0.0f), new Vector3(0.25f, 1.0f, 0.0f),
                    new Vector3(0.25f, 1.0f, 1.0f), new Vector3(0.25f, 0.0f, 1.0f)
                },
                new Vector3[] {
                    new Vector3(1.0f, 0.0f, 1.0f), new Vector3(1.0f, 1.0f, 1.0f),
                    new Vector3(1.0f, 1.0f, 0.0f), new Vector3(1.0f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.25f, 0.0f, 0.0f), new Vector3(0.25f, 0.0f, 1.0f),
                    new Vector3(1.0f, 0.0f, 1.0f), new Vector3(1.0f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.25f, 1.0f, 1.0f), new Vector3(0.25f, 1.0f, 0.0f),
                    new Vector3(1.0f, 1.0f, 0.0f), new Vector3(1.0f, 1.0f, 1.0f)
                },
                new Vector3[] {
                    new Vector3(1.0f, 0.0f, 0.0f), new Vector3(1.0f, 1.0f, 0.0f),
                    new Vector3(0.25f, 1.0f, 0.0f), new Vector3(0.25f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.25f, 0.0f, 1.0f), new Vector3(0.25f, 1.0f, 1.0f),
                    new Vector3(1.0f, 1.0f, 1.0f), new Vector3(1.0f, 0.0f, 1.0f)
                }
            };
            blockShapePoints[TerrainBlockShapes.Down0] = new Vector3[][]
            {
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 0.0f), new Vector3(0.0f, 0.25f, 0.0f),
                    new Vector3(0.0f, 0.25f, 1.0f), new Vector3(0.0f, 0.0f, 1.0f)
                },
                new Vector3[] {
                    new Vector3(1.0f, 0.0f, 1.0f), new Vector3(1.0f, 0.25f, 1.0f),
                    new Vector3(1.0f, 0.25f, 0.0f), new Vector3(1.0f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 0.0f), new Vector3(0.0f, 0.0f, 1.0f),
                    new Vector3(1.0f, 0.0f, 1.0f), new Vector3(1.0f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.25f, 1.0f), new Vector3(0.0f, 0.25f, 0.0f),
                    new Vector3(1.0f, 0.25f, 0.0f), new Vector3(1.0f, 0.25f, 1.0f)
                },
                new Vector3[] {
                    new Vector3(1.0f, 0.0f, 0.0f), new Vector3(1.0f, 0.25f, 0.0f),
                    new Vector3(0.0f, 0.25f, 0.0f), new Vector3(0.0f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 1.0f), new Vector3(0.0f, 0.25f, 1.0f),
                    new Vector3(1.0f, 0.25f, 1.0f), new Vector3(1.0f, 0.0f, 1.0f)
                }
            };

            blockShapePoints[TerrainBlockShapes.Down1] = new Vector3[][]
            {
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 0.0f), new Vector3(0.0f, 0.5f, 0.0f),
                    new Vector3(0.0f, 0.5f, 1.0f), new Vector3(0.0f, 0.0f, 1.0f)
                },
                new Vector3[] {
                    new Vector3(1.0f, 0.0f, 1.0f), new Vector3(1.0f, 0.5f, 1.0f),
                    new Vector3(1.0f, 0.5f, 0.0f), new Vector3(1.0f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 0.0f), new Vector3(0.0f, 0.0f, 1.0f),
                    new Vector3(1.0f, 0.0f, 1.0f), new Vector3(1.0f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.5f, 1.0f), new Vector3(0.0f, 0.5f, 0.0f),
                    new Vector3(1.0f, 0.5f, 0.0f), new Vector3(1.0f, 0.5f, 1.0f)
                },
                new Vector3[] {
                    new Vector3(1.0f, 0.0f, 0.0f), new Vector3(1.0f, 0.5f, 0.0f),
                    new Vector3(0.0f, 0.5f, 0.0f), new Vector3(0.0f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 1.0f), new Vector3(0.0f, 0.5f, 1.0f),
                    new Vector3(1.0f, 0.5f, 1.0f), new Vector3(1.0f, 0.0f, 1.0f)
                }
            };
            blockShapePoints[TerrainBlockShapes.Down2] = new Vector3[][]
            {
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 0.0f), new Vector3(0.0f, 0.75f, 0.0f),
                    new Vector3(0.0f, 0.75f, 1.0f), new Vector3(0.0f, 0.0f, 1.0f)
                },
                new Vector3[] {
                    new Vector3(1.0f, 0.0f, 1.0f), new Vector3(1.0f, 0.75f, 1.0f),
                    new Vector3(1.0f, 0.75f, 0.0f), new Vector3(1.0f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 0.0f), new Vector3(0.0f, 0.0f, 1.0f),
                    new Vector3(1.0f, 0.0f, 1.0f), new Vector3(1.0f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.75f, 1.0f), new Vector3(0.0f, 0.75f, 0.0f),
                    new Vector3(1.0f, 0.75f, 0.0f), new Vector3(1.0f, 0.75f, 1.0f)
                },
                new Vector3[] {
                    new Vector3(1.0f, 0.0f, 0.0f), new Vector3(1.0f, 0.75f, 0.0f),
                    new Vector3(0.0f, 0.75f, 0.0f), new Vector3(0.0f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 1.0f), new Vector3(0.0f, 0.75f, 1.0f),
                    new Vector3(1.0f, 0.75f, 1.0f), new Vector3(1.0f, 0.0f, 1.0f)
                }
            };

            blockShapePoints[TerrainBlockShapes.Up0] = new Vector3[][]
            {
                new Vector3[] {
                    new Vector3(0.0f, 0.75f, 0.0f), new Vector3(0.0f, 1.0f, 0.0f),
                    new Vector3(0.0f, 1.0f, 1.0f), new Vector3(0.0f, 0.75f, 1.0f)
                },
                new Vector3[] {
                    new Vector3(1.0f, 0.75f, 1.0f), new Vector3(1.0f, 1.0f, 1.0f),
                    new Vector3(1.0f, 1.0f, 0.0f), new Vector3(1.0f, 0.75f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.75f, 0.0f), new Vector3(0.0f, 0.75f, 1.0f),
                    new Vector3(1.0f, 0.75f, 1.0f), new Vector3(1.0f, 0.75f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 1.0f, 1.0f), new Vector3(0.0f, 1.0f, 0.0f),
                    new Vector3(1.0f, 1.0f, 0.0f), new Vector3(1.0f, 1.0f, 1.0f)
                },
                new Vector3[] {
                    new Vector3(1.0f, 0.75f, 0.0f), new Vector3(1.0f, 1.0f, 0.0f),
                    new Vector3(0.0f, 1.0f, 0.0f), new Vector3(0.0f, 0.75f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.75f, 1.0f), new Vector3(0.0f, 1.0f, 1.0f),
                    new Vector3(1.0f, 1.0f, 1.0f), new Vector3(1.0f, 0.75f, 1.0f)
                }
            };
            blockShapePoints[TerrainBlockShapes.Up1] = new Vector3[][]
            {
                new Vector3[] {
                    new Vector3(0.0f, 0.5f, 0.0f), new Vector3(0.0f, 1.0f, 0.0f),
                    new Vector3(0.0f, 1.0f, 1.0f), new Vector3(0.0f, 0.5f, 1.0f)
                },
                new Vector3[] {
                    new Vector3(1.0f, 0.5f, 1.0f), new Vector3(1.0f, 1.0f, 1.0f),
                    new Vector3(1.0f, 1.0f, 0.0f), new Vector3(1.0f, 0.5f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.5f, 0.0f), new Vector3(0.0f, 0.5f, 1.0f),
                    new Vector3(1.0f, 0.5f, 1.0f), new Vector3(1.0f, 0.5f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 1.0f, 1.0f), new Vector3(0.0f, 1.0f, 0.0f),
                    new Vector3(1.0f, 1.0f, 0.0f), new Vector3(1.0f, 1.0f, 1.0f)
                },
                new Vector3[] {
                    new Vector3(1.0f, 0.5f, 0.0f), new Vector3(1.0f, 1.0f, 0.0f),
                    new Vector3(0.0f, 1.0f, 0.0f), new Vector3(0.0f, 0.5f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.5f, 1.0f), new Vector3(0.0f, 1.0f, 1.0f),
                    new Vector3(1.0f, 1.0f, 1.0f), new Vector3(1.0f, 0.5f, 1.0f)
                }
            };
            blockShapePoints[TerrainBlockShapes.Up2] = new Vector3[][]
            {
                new Vector3[] {
                    new Vector3(0.0f, 0.25f, 0.0f), new Vector3(0.0f, 1.0f, 0.0f),
                    new Vector3(0.0f, 1.0f, 1.0f), new Vector3(0.0f, 0.25f, 1.0f)
                },
                new Vector3[] {
                    new Vector3(1.0f, 0.25f, 1.0f), new Vector3(1.0f, 1.0f, 1.0f),
                    new Vector3(1.0f, 1.0f, 0.0f), new Vector3(1.0f, 0.25f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.25f, 0.0f), new Vector3(0.0f, 0.25f, 1.0f),
                    new Vector3(1.0f, 0.25f, 1.0f), new Vector3(1.0f, 0.25f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 1.0f, 1.0f), new Vector3(0.0f, 1.0f, 0.0f),
                    new Vector3(1.0f, 1.0f, 0.0f), new Vector3(1.0f, 1.0f, 1.0f)
                },
                new Vector3[] {
                    new Vector3(1.0f, 0.25f, 0.0f), new Vector3(1.0f, 1.0f, 0.0f),
                    new Vector3(0.0f, 1.0f, 0.0f), new Vector3(0.0f, 0.25f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.25f, 1.0f), new Vector3(0.0f, 1.0f, 1.0f),
                    new Vector3(1.0f, 1.0f, 1.0f), new Vector3(1.0f, 0.25f, 1.0f)
                }
            };
            blockShapePoints[TerrainBlockShapes.Fore0] = new Vector3[][]
            {
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 0.0f), new Vector3(0.0f, 1.0f, 0.0f),
                    new Vector3(0.0f, 1.0f, 0.25f), new Vector3(0.0f, 0.0f, 0.25f)
                },
                new Vector3[] {
                    new Vector3(1.0f, 0.0f, 0.25f), new Vector3(1.0f, 1.0f, 0.25f),
                    new Vector3(1.0f, 1.0f, 0.0f), new Vector3(1.0f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 0.0f), new Vector3(0.0f, 0.0f, 0.25f),
                    new Vector3(1.0f, 0.0f, 0.25f), new Vector3(1.0f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 1.0f, 0.25f), new Vector3(0.0f, 1.0f, 0.0f),
                    new Vector3(1.0f, 1.0f, 0.0f), new Vector3(1.0f, 1.0f, 0.25f)
                },
                new Vector3[] {
                    new Vector3(1.0f, 0.0f, 0.0f), new Vector3(1.0f, 1.0f, 0.0f),
                    new Vector3(0.0f, 1.0f, 0.0f), new Vector3(0.0f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 0.25f), new Vector3(0.0f, 1.0f, 0.25f),
                    new Vector3(1.0f, 1.0f, 0.25f), new Vector3(1.0f, 0.0f, 0.25f)
                }
            };
            blockShapePoints[TerrainBlockShapes.Fore1] = new Vector3[][]
            {
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 0.0f), new Vector3(0.0f, 1.0f, 0.0f),
                    new Vector3(0.0f, 1.0f, 0.5f), new Vector3(0.0f, 0.0f, 0.5f)
                },
                new Vector3[] {
                    new Vector3(1.0f, 0.0f, 0.5f), new Vector3(1.0f, 1.0f, 0.5f),
                    new Vector3(1.0f, 1.0f, 0.0f), new Vector3(1.0f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 0.0f), new Vector3(0.0f, 0.0f, 0.5f),
                    new Vector3(1.0f, 0.0f, 0.5f), new Vector3(1.0f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 1.0f, 0.5f), new Vector3(0.0f, 1.0f, 0.0f),
                    new Vector3(1.0f, 1.0f, 0.0f), new Vector3(1.0f, 1.0f, 0.5f)
                },
                new Vector3[] {
                    new Vector3(1.0f, 0.0f, 0.0f), new Vector3(1.0f, 1.0f, 0.0f),
                    new Vector3(0.0f, 1.0f, 0.0f), new Vector3(0.0f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 0.5f), new Vector3(0.0f, 1.0f, 0.5f),
                    new Vector3(1.0f, 1.0f, 0.5f), new Vector3(1.0f, 0.0f, 0.5f)
                }
            };
            blockShapePoints[TerrainBlockShapes.Fore2] = new Vector3[][]
            {
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 0.0f), new Vector3(0.0f, 1.0f, 0.0f),
                    new Vector3(0.0f, 1.0f, 0.75f), new Vector3(0.0f, 0.0f, 0.75f)
                },
                new Vector3[] {
                    new Vector3(1.0f, 0.0f, 0.75f), new Vector3(1.0f, 1.0f, 0.75f),
                    new Vector3(1.0f, 1.0f, 0.0f), new Vector3(1.0f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 0.0f), new Vector3(0.0f, 0.0f, 0.75f),
                    new Vector3(1.0f, 0.0f, 0.75f), new Vector3(1.0f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 1.0f, 0.75f), new Vector3(0.0f, 1.0f, 0.0f),
                    new Vector3(1.0f, 1.0f, 0.0f), new Vector3(1.0f, 1.0f, 0.75f)
                },
                new Vector3[] {
                    new Vector3(1.0f, 0.0f, 0.0f), new Vector3(1.0f, 1.0f, 0.0f),
                    new Vector3(0.0f, 1.0f, 0.0f), new Vector3(0.0f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 0.75f), new Vector3(0.0f, 1.0f, 0.75f),
                    new Vector3(1.0f, 1.0f, 0.75f), new Vector3(1.0f, 0.0f, 0.75f)
                }
            };
            blockShapePoints[TerrainBlockShapes.Back0] = new Vector3[][]
            {
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 0.75f), new Vector3(0.0f, 1.0f, 0.75f),
                    new Vector3(0.0f, 1.0f, 1.0f), new Vector3(0.0f, 0.0f, 1.0f)
                },
                new Vector3[] {
                    new Vector3(1.0f, 0.0f, 1.0f), new Vector3(1.0f, 1.0f, 1.0f),
                    new Vector3(1.0f, 1.0f, 0.75f), new Vector3(1.0f, 0.0f, 0.75f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 0.75f), new Vector3(0.0f, 0.0f, 1.0f),
                    new Vector3(1.0f, 0.0f, 1.0f), new Vector3(1.0f, 0.0f, 0.75f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 1.0f, 1.0f), new Vector3(0.0f, 1.0f, 0.75f),
                    new Vector3(1.0f, 1.0f, 0.75f), new Vector3(1.0f, 1.0f, 1.0f)
                },
                new Vector3[] {
                    new Vector3(1.0f, 0.0f, 0.75f), new Vector3(1.0f, 1.0f, 0.75f),
                    new Vector3(0.0f, 1.0f, 0.75f), new Vector3(0.0f, 0.0f, 0.75f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 1.0f), new Vector3(0.0f, 1.0f, 1.0f),
                    new Vector3(1.0f, 1.0f, 1.0f), new Vector3(1.0f, 0.0f, 1.0f)
                }
            };
            blockShapePoints[TerrainBlockShapes.Back1] = new Vector3[][]
            {
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 0.5f), new Vector3(0.0f, 1.0f, 0.5f),
                    new Vector3(0.0f, 1.0f, 1.0f), new Vector3(0.0f, 0.0f, 1.0f)
                },
                new Vector3[] {
                    new Vector3(1.0f, 0.0f, 1.0f), new Vector3(1.0f, 1.0f, 1.0f),
                    new Vector3(1.0f, 1.0f, 0.5f), new Vector3(1.0f, 0.0f, 0.5f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 0.5f), new Vector3(0.0f, 0.0f, 1.0f),
                    new Vector3(1.0f, 0.0f, 1.0f), new Vector3(1.0f, 0.0f, 0.5f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 1.0f, 1.0f), new Vector3(0.0f, 1.0f, 0.5f),
                    new Vector3(1.0f, 1.0f, 0.5f), new Vector3(1.0f, 1.0f, 1.0f)
                },
                new Vector3[] {
                    new Vector3(1.0f, 0.0f, 0.5f), new Vector3(1.0f, 1.0f, 0.5f),
                    new Vector3(0.0f, 1.0f, 0.5f), new Vector3(0.0f, 0.0f, 0.5f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 1.0f), new Vector3(0.0f, 1.0f, 1.0f),
                    new Vector3(1.0f, 1.0f, 1.0f), new Vector3(1.0f, 0.0f, 1.0f)
                }
            };
            blockShapePoints[TerrainBlockShapes.Back2] = new Vector3[][]
            {
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 0.25f), new Vector3(0.0f, 1.0f, 0.25f),
                    new Vector3(0.0f, 1.0f, 1.0f), new Vector3(0.0f, 0.0f, 1.0f)
                },
                new Vector3[] {
                    new Vector3(1.0f, 0.0f, 1.0f), new Vector3(1.0f, 1.0f, 1.0f),
                    new Vector3(1.0f, 1.0f, 0.25f), new Vector3(1.0f, 0.0f, 0.25f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 0.25f), new Vector3(0.0f, 0.0f, 1.0f),
                    new Vector3(1.0f, 0.0f, 1.0f), new Vector3(1.0f, 0.0f, 0.25f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 1.0f, 1.0f), new Vector3(0.0f, 1.0f, 0.25f),
                    new Vector3(1.0f, 1.0f, 0.25f), new Vector3(1.0f, 1.0f, 1.0f)
                },
                new Vector3[] {
                    new Vector3(1.0f, 0.0f, 0.25f), new Vector3(1.0f, 1.0f, 0.25f),
                    new Vector3(0.0f, 1.0f, 0.25f), new Vector3(0.0f, 0.0f, 0.25f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 1.0f), new Vector3(0.0f, 1.0f, 1.0f),
                    new Vector3(1.0f, 1.0f, 1.0f), new Vector3(1.0f, 0.0f, 1.0f)
                }
            };
            blockShapePoints[TerrainBlockShapes.WallX] = new Vector3[][]
            {
                new Vector3[] {
                    new Vector3(0.25f, 0.0f, 0.0f), new Vector3(0.25f, 1.0f, 0.0f),
                    new Vector3(0.25f, 1.0f, 1.0f), new Vector3(0.25f, 0.0f, 1.0f)
                },
                new Vector3[] {
                    new Vector3(0.75f, 0.0f, 1.0f), new Vector3(0.75f, 1.0f, 1.0f),
                    new Vector3(0.75f, 1.0f, 0.0f), new Vector3(0.75f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.25f, 0.0f, 0.0f), new Vector3(0.25f, 0.0f, 1.0f),
                    new Vector3(0.75f, 0.0f, 1.0f), new Vector3(0.75f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.25f, 1.0f, 1.0f), new Vector3(0.25f, 1.0f, 0.0f),
                    new Vector3(0.75f, 1.0f, 0.0f), new Vector3(0.75f, 1.0f, 1.0f)
                },
                new Vector3[] {
                    new Vector3(0.75f, 0.0f, 0.0f), new Vector3(0.75f, 1.0f, 0.0f),
                    new Vector3(0.25f, 1.0f, 0.0f), new Vector3(0.25f, 0.0f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.25f, 0.0f, 1.0f), new Vector3(0.25f, 1.0f, 1.0f),
                    new Vector3(0.75f, 1.0f, 1.0f), new Vector3(0.75f, 0.0f, 1.0f)
                }
            };
            blockShapePoints[TerrainBlockShapes.WallY] = new Vector3[][]
            {
                new Vector3[] {
                    new Vector3(0.0f, 0.25f, 0.0f), new Vector3(0.0f, 0.75f, 0.0f),
                    new Vector3(0.0f, 0.75f, 1.0f), new Vector3(0.0f, 0.25f, 1.0f)
                },
                new Vector3[] {
                    new Vector3(1.0f, 0.25f, 1.0f), new Vector3(1.0f, 0.75f, 1.0f),
                    new Vector3(1.0f, 0.75f, 0.0f), new Vector3(1.0f, 0.25f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.25f, 0.0f), new Vector3(0.0f, 0.25f, 1.0f),
                    new Vector3(1.0f, 0.25f, 1.0f), new Vector3(1.0f, 0.25f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.75f, 1.0f), new Vector3(0.0f, 0.75f, 0.0f),
                    new Vector3(1.0f, 0.75f, 0.0f), new Vector3(1.0f, 0.75f, 1.0f)
                },
                new Vector3[] {
                    new Vector3(1.0f, 0.25f, 0.0f), new Vector3(1.0f, 0.75f, 0.0f),
                    new Vector3(0.0f, 0.75f, 0.0f), new Vector3(0.0f, 0.25f, 0.0f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.25f, 1.0f), new Vector3(0.0f, 0.75f, 1.0f),
                    new Vector3(1.0f, 0.75f, 1.0f), new Vector3(1.0f, 0.25f, 1.0f)
                }
            };
            blockShapePoints[TerrainBlockShapes.WallZ] = new Vector3[][]
            {
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 0.25f), new Vector3(0.0f, 1.0f, 0.25f),
                    new Vector3(0.0f, 1.0f, 0.75f), new Vector3(0.0f, 0.0f, 0.75f)
                },
                new Vector3[] {
                    new Vector3(1.0f, 0.0f, 0.75f), new Vector3(1.0f, 1.0f, 0.75f),
                    new Vector3(1.0f, 1.0f, 0.25f), new Vector3(1.0f, 0.0f, 0.25f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 0.25f), new Vector3(0.0f, 0.0f, 0.75f),
                    new Vector3(1.0f, 0.0f, 0.75f), new Vector3(1.0f, 0.0f, 0.25f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 1.0f, 0.75f), new Vector3(0.0f, 1.0f, 0.25f),
                    new Vector3(1.0f, 1.0f, 0.25f), new Vector3(1.0f, 1.0f, 0.75f)
                },
                new Vector3[] {
                    new Vector3(1.0f, 0.0f, 0.25f), new Vector3(1.0f, 1.0f, 0.25f),
                    new Vector3(0.0f, 1.0f, 0.25f), new Vector3(0.0f, 0.0f, 0.25f)
                },
                new Vector3[] {
                    new Vector3(0.0f, 0.0f, 0.75f), new Vector3(0.0f, 1.0f, 0.75f),
                    new Vector3(1.0f, 1.0f, 0.75f), new Vector3(1.0f, 0.0f, 0.75f)
                }
            };

        }

        void MarchCubeShape(Point3 position, byte blockShapeMap, byte shape, NodeTexture node)
        {

            for (int i = 0; i < blockShapePoints[shape].Length; i++)
            {
                if ((blockShapeMap & (1 << i)) > 0)
                    for (int j = 0; j < blockShapePoints[shape][i].Length; j += 4)
                    {
                        AddSide(
                        position.Vector + blockShapePoints[shape][i][j + 0],
                        position.Vector + blockShapePoints[shape][i][j + 1],
                        position.Vector + blockShapePoints[shape][i][j + 2],
                        position.Vector + blockShapePoints[shape][i][j + 3],
                        position,
                        node);
                    }

            }
        }
    }
}
