﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace WarpedAbominationEngine
{
    public abstract partial class MarchingCubes
    {
        public abstract Point3 Size { get; }

        protected abstract void SampleNode(Point3 position, out NodeTexture node, out NodeShape gNode);

        protected abstract void AddTriangle(Vector3 a, Vector3 b, Vector3 c, Vector3 normal, Point3 nodePosition, NodeTexture node, byte edgeA, byte edgeB, byte edgeC);

        private void InternalAddTriangle(Vector3 a, Vector3 b, Vector3 c, Point3 nodePosition, NodeTexture node, byte edgeA, byte edgeB, byte edgeC)
        {
            AddTriangle(a, b, c, MathHelper.GetTriangleNormal(a, b, c), nodePosition, node, edgeA, edgeB, edgeC);
        }

        public virtual void March()
        {
            for (int y = 0; y < Size.Y; y++)
                for (int z = 0; z < Size.Z; z++)
                    for (int x = 0; x < Size.X; x++)
                        MarchCube(new Point3(x, y, z));
        }

        protected virtual void AddSide(Vector3 a, Vector3 b, Vector3 c, Vector3 d, Point3 nodePosition, NodeTexture node)
        {
            InternalAddTriangle(a, b, c, nodePosition, node, 255, 255, 255);
            InternalAddTriangle(d, a, c, nodePosition, node, 255, 255, 255);
        }

        protected virtual void MarchCube(Point3 position)
        {
            byte[] cubePoints = new byte[8];
            SampleNode(position, out NodeTexture node, out NodeShape gNode);
            byte shape = gNode.MegatableIndex;

            for (int i = 0; i < PointPositions.Length; i++)
                cubePoints[i] = (byte)((gNode.TerrainShapeData >> (i * 2)) & 0x03);

            byte[] current = megatable[shape];

            for (int i = 0; i < current.Length; i += 3) {
                byte edgeA = current[i];
                byte edgeB = current[i + 1];
                byte edgeC = current[i + 2];
                Vector3 a = position.Vector + GetVertexPos(edgeA, cubePoints[EdgePoints[edgeA].PointA], cubePoints[EdgePoints[edgeA].PointB]);
                Vector3 b = position.Vector + GetVertexPos(edgeB, cubePoints[EdgePoints[edgeB].PointA], cubePoints[EdgePoints[edgeB].PointB]);
                Vector3 c = position.Vector + GetVertexPos(edgeC, cubePoints[EdgePoints[edgeC].PointA], cubePoints[EdgePoints[edgeC].PointB]);
                InternalAddTriangle(a, b, c, position, node, edgeA, edgeB, edgeC);
            }

            MarchCubeShape(position, gNode.BlockShapeMap, gNode.BlockShape, node);
        }

        void Generate()
        {

        }
    }
}
