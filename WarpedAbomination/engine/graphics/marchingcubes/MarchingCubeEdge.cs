﻿using System;
namespace WarpedAbominationEngine
{
    internal struct MarchingCubeEdge
    {
        internal byte PointA, PointB;

        internal MarchingCubeEdge(byte a, byte b)
        {
            this.PointA = a;
            this.PointB = b;
        }

        internal const byte
        DownFore = 0,
        RightFore = 1,
        UpFore = 2,
        LeftFore = 3,
        DownBack = 4,
        RightBack = 5,
        UpBack = 6,
        LeftBack = 7,
        LeftDown = 8,
        RightDown = 9,
        RightUp = 10,
        LeftUp = 11;
    }
}