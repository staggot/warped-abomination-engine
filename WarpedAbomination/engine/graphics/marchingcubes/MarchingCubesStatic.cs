﻿using System;
using Microsoft.Xna.Framework;
namespace WarpedAbominationEngine
{
    public partial class MarchingCubes
    {
        public const byte
            ShapeIndexLeft = 0x00,
            ShapeIndexRight = 0x01,
            ShapeIndexDown = 0x02,
            ShapeIndexUp = 0x03,
            ShapeIndexFore = 0x04,
            ShapeIndexBack = 0x05;


        public const byte
            ShapeMaskLeft = 0x01,
            ShapeMaskRight = 0x02,
            ShapeMaskDown = 0x04,
            ShapeMaskUp = 0x08,
            ShapeMaskFore = 0x10,
            ShapeMaskBack = 0x20;


        internal const byte
        FullLeft =
            (1 << MarchingCubePoint.LeftUpBack) |
            (1 << MarchingCubePoint.LeftDownBack) |
            (1 << MarchingCubePoint.LeftUpFore) |
            (1 << MarchingCubePoint.LeftDownFore),
        FullRight =
            (1 << MarchingCubePoint.RightUpBack) |
            (1 << MarchingCubePoint.RightDownBack) |
            (1 << MarchingCubePoint.RightUpFore) |
            (1 << MarchingCubePoint.RightDownFore),
        FullUp =
            (1 << MarchingCubePoint.LeftUpBack) |
            (1 << MarchingCubePoint.LeftUpFore) |
            (1 << MarchingCubePoint.RightUpBack) |
            (1 << MarchingCubePoint.RightUpFore),
        FullDown =
            (1 << MarchingCubePoint.LeftDownBack) |
            (1 << MarchingCubePoint.LeftDownFore) |
            (1 << MarchingCubePoint.RightDownBack) |
            (1 << MarchingCubePoint.RightDownFore),
        FullFore =
            (1 << MarchingCubePoint.LeftUpFore) |
            (1 << MarchingCubePoint.LeftDownFore) |
            (1 << MarchingCubePoint.RightUpFore) |
            (1 << MarchingCubePoint.RightDownFore),
        FullBack =
            (1 << MarchingCubePoint.LeftUpBack) |
            (1 << MarchingCubePoint.LeftDownBack) |
            (1 << MarchingCubePoint.RightUpBack) |
            (1 << MarchingCubePoint.RightDownBack);

        internal static Point3[] PointPositions = {
            new Point3(0,0,0),
            new Point3(1,0,0),
            new Point3(1,1,0),
            new Point3(0,1,0),
            new Point3(0,0,1),
            new Point3(1,0,1),
            new Point3(1,1,1),
            new Point3(0,1,1)
        };

        internal static MarchingCubeEdge[] EdgePoints =
        {
            new MarchingCubeEdge(MarchingCubePoint.LeftDownFore, MarchingCubePoint.RightDownFore), //BottomFore
            new MarchingCubeEdge(MarchingCubePoint.RightDownFore, MarchingCubePoint.RightUpFore),  //RightFore
            new MarchingCubeEdge(MarchingCubePoint.RightUpFore, MarchingCubePoint.LeftUpFore),     //TopFore
            new MarchingCubeEdge(MarchingCubePoint.LeftUpFore,MarchingCubePoint.LeftDownFore),    //LeftFore
            new MarchingCubeEdge(MarchingCubePoint.LeftDownBack, MarchingCubePoint.RightDownBack), //BottomBack
            new MarchingCubeEdge(MarchingCubePoint.RightDownBack, MarchingCubePoint.RightUpBack),   //RightBack
            new MarchingCubeEdge(MarchingCubePoint.RightUpBack,MarchingCubePoint.LeftUpBack),     //TopBack  
            new MarchingCubeEdge(MarchingCubePoint.LeftUpBack, MarchingCubePoint.LeftDownBack),    //LeftBack
            new MarchingCubeEdge(MarchingCubePoint.LeftDownFore, MarchingCubePoint.LeftDownBack),  //LeftDown
            new MarchingCubeEdge(MarchingCubePoint.RightDownFore, MarchingCubePoint.RightDownBack),//RightBottom
            new MarchingCubeEdge(MarchingCubePoint.RightUpFore, MarchingCubePoint.RightUpBack),    //RightTop
            new MarchingCubeEdge(MarchingCubePoint.LeftUpFore, MarchingCubePoint.LeftUpBack)       //LeftUp 
        };

        static MarchingCubes()
        {
            InitBlockShapes();
        }
        internal static byte GetBlockMap(byte shape, byte terrain, byte left, byte right, byte down, byte up, byte fore, byte back)
        {
            byte result = 0x3f;
            if (terrain == (MarchingCubes.FullLeft | MarchingCubes.FullRight))
                return 0;

            return (byte)(result & ~(
                (TerrainBlockShapes.CoversLeft(shape, left, terrain) ? MarchingCubes.ShapeMaskLeft : 0) |
                (TerrainBlockShapes.CoversRight(shape, right, terrain) ? MarchingCubes.ShapeMaskRight : 0) |
                (TerrainBlockShapes.CoversDown(shape, down, terrain) ? MarchingCubes.ShapeMaskDown : 0) |
                (TerrainBlockShapes.CoversUp(shape, up, terrain) ? MarchingCubes.ShapeMaskUp : 0) |
                (TerrainBlockShapes.CoversFore(shape, fore, terrain) ? MarchingCubes.ShapeMaskFore : 0) |
                (TerrainBlockShapes.CoversBack(shape, back, terrain) ? MarchingCubes.ShapeMaskBack : 0)
            ));
        }

        internal static Vector3 GetVertexPos(byte edge, byte aVal, byte bVal)
        {
            Vector3 pointa = PointPositions[EdgePoints[edge].PointA].Vector;
            Vector3 pointb = PointPositions[EdgePoints[edge].PointB].Vector;


            if (aVal == 0)
                return Vector3.Lerp(pointb, pointa, 1f / 6f + ((float)bVal) / 3f);
            else if (bVal == 0)
                return Vector3.Lerp(pointa, pointb, 1f / 6f + ((float)aVal) / 3f);
            else
                throw new Exception("One Of The Edge Values Must Be 0, Probably Magic Table Anomaly.");
        }

        public static void TriangleTextureCoords(
           Vector3 a, Vector3 b, Vector3 c,
           Vector3 normal,
           Vector3 posVector,
           out TriplanarTextureMappingAxis axis, out Vector2 aCoords, out Vector2 bCoords, out Vector2 cCoords)
        {


            bool YXB = Math.Abs(normal.Y) >= Math.Abs(normal.X);
            bool YZB = Math.Abs(normal.Y) >= Math.Abs(normal.Z);
            bool ZXB = Math.Abs(normal.Z) >= Math.Abs(normal.X);
            bool ZYB = Math.Abs(normal.Z) >= Math.Abs(normal.X);
            bool XZB = Math.Abs(normal.X) >= Math.Abs(normal.Z);
            bool XYB = Math.Abs(normal.X) >= Math.Abs(normal.Y);


            if (YXB && YZB)
            {
                axis = TriplanarTextureMappingAxis.Y;
                if (normal.Y >= 0)
                {
                    aCoords = new Vector2(1 - (a.X - posVector.X), 1 - (a.Z - posVector.Z));
                    bCoords = new Vector2(1 - (b.X - posVector.X), 1 - (b.Z - posVector.Z));
                    cCoords = new Vector2(1 - (c.X - posVector.X), 1 - (c.Z - posVector.Z));
                }
                else
                {
                    aCoords = new Vector2(1 - (a.X - posVector.X), (a.Z - posVector.Z));
                    bCoords = new Vector2(1 - (b.X - posVector.X), (b.Z - posVector.Z));
                    cCoords = new Vector2(1 - (c.X - posVector.X), (c.Z - posVector.Z));
                }
            }
            else if (XYB && XZB)
            {
                axis = TriplanarTextureMappingAxis.X;
                if (normal.X >= 0)
                {
                    aCoords = new Vector2(1 - (a.Z - posVector.Z), 1 - (a.Y - posVector.Y));
                    bCoords = new Vector2(1 - (b.Z - posVector.Z), 1 - (b.Y - posVector.Y));
                    cCoords = new Vector2(1 - (c.Z - posVector.Z), 1 - (c.Y - posVector.Y));
                }
                else
                {
                    aCoords = new Vector2((a.Z - posVector.Z), 1 - (a.Y - posVector.Y));
                    bCoords = new Vector2((b.Z - posVector.Z), 1 - (b.Y - posVector.Y));
                    cCoords = new Vector2((c.Z - posVector.Z), 1 - (c.Y - posVector.Y));
                }
            }
            else
            {
                axis = TriplanarTextureMappingAxis.Z;
                if (normal.Z >= 0)
                {
                    aCoords = new Vector2((a.X - posVector.X), 1 - (a.Y - posVector.Y));
                    bCoords = new Vector2((b.X - posVector.X), 1 - (b.Y - posVector.Y));
                    cCoords = new Vector2((c.X - posVector.X), 1 - (c.Y - posVector.Y));
                }
                else
                {
                    aCoords = new Vector2(1 - (a.X - posVector.X), 1 - (a.Y - posVector.Y));
                    bCoords = new Vector2(1 - (b.X - posVector.X), 1 - (b.Y - posVector.Y));
                    cCoords = new Vector2(1 - (c.X - posVector.X), 1 - (c.Y - posVector.Y));
                }
            }
        }

        #region Intencive Hacking 
        internal static byte[][] megatable = {
            new byte[] { },
            new byte[] {0, 8, 3},
            new byte[] {0, 1, 9},
            new byte[] {1, 8, 3, 9, 8, 1},
            new byte[] {1, 2, 10},
            new byte[] {0, 8, 3, 1, 2, 10},
            new byte[] {9, 2, 10, 0, 2, 9},
            new byte[] {2, 8, 3, 2, 10, 8, 10, 9, 8},
            new byte[] {3, 11, 2},
            new byte[] {0, 11, 2, 8, 11, 0},
            new byte[] {1, 9, 0, 2, 3, 11},
            new byte[] {1, 11, 2, 1, 9, 11, 9, 8, 11},
            new byte[] {3, 10, 1, 11, 10, 3},
            new byte[] {0, 10, 1, 0, 8, 10, 8, 11, 10},
            new byte[] {3, 9, 0, 3, 11, 9, 11, 10, 9},
            new byte[] {9, 8, 10, 10, 8, 11},
            new byte[] {4, 7, 8},
            new byte[] {4, 3, 0, 7, 3, 4},
            new byte[] {0, 1, 9, 8, 4, 7},
            new byte[] {4, 1, 9, 4, 7, 1, 7, 3, 1},
            new byte[] {1, 2, 10, 8, 4, 7},
            new byte[] {3, 4, 7, 3, 0, 4, 1, 2, 10},
            new byte[] {9, 2, 10, 9, 0, 2, 8, 4, 7},
            new byte[] {2, 10, 9, 2, 9, 7, 2, 7, 3, 7, 9, 4},
            new byte[] {8, 4, 7, 3, 11, 2},
            new byte[] {11, 4, 7, 11, 2, 4, 2, 0, 4},
            new byte[] {9, 0, 1, 8, 4, 7, 2, 3, 11},
            new byte[] {4, 7, 11, 9, 4, 11, 9, 11, 2, 9, 2, 1},
            new byte[] {3, 10, 1, 3, 11, 10, 7, 8, 4},
            new byte[] {1, 11, 10, 1, 4, 11, 1, 0, 4, 7, 11, 4},
            new byte[] {4, 7, 8, 9, 0, 11, 9, 11, 10, 11, 0, 3},
            new byte[] {4, 7, 11, 4, 11, 9, 9, 11, 10},
            new byte[] {9, 5, 4},
            new byte[] {9, 5, 4, 0, 8, 3},
            new byte[] {0, 5, 4, 1, 5, 0},
            new byte[] {8, 5, 4, 8, 3, 5, 3, 1, 5},
            new byte[] {1, 2, 10, 9, 5, 4},
            new byte[] {3, 0, 8, 1, 2, 10, 4, 9, 5},
            new byte[] {5, 2, 10, 5, 4, 2, 4, 0, 2},
            new byte[] {2, 10, 5, 3, 2, 5, 3, 5, 4, 3, 4, 8},
            new byte[] {9, 5, 4, 2, 3, 11},
            new byte[] {0, 11, 2, 0, 8, 11, 4, 9, 5},
            new byte[] {0, 5, 4, 0, 1, 5, 2, 3, 11},
            new byte[] {2, 1, 5, 2, 5, 8, 2, 8, 11, 4, 8, 5},
            new byte[] {10, 3, 11, 10, 1, 3, 9, 5, 4},
            new byte[] {4, 9, 5, 0, 8, 1, 8, 10, 1, 8, 11, 10},
            new byte[] {5, 4, 0, 5, 0, 11, 5, 11, 10, 11, 0, 3},
            new byte[] {5, 4, 8, 5, 8, 10, 10, 8, 11},
            new byte[] {9, 7, 8, 5, 7, 9},
            new byte[] {9, 3, 0, 9, 5, 3, 5, 7, 3},
            new byte[] {0, 7, 8, 0, 1, 7, 1, 5, 7},
            new byte[] {1, 5, 3, 3, 5, 7},
            new byte[] {9, 7, 8, 9, 5, 7, 10, 1, 2},
            new byte[] {10, 1, 2, 9, 5, 0, 5, 3, 0, 5, 7, 3},
            new byte[] {8, 0, 2, 8, 2, 5, 8, 5, 7, 10, 5, 2},
            new byte[] {2, 10, 5, 2, 5, 3, 3, 5, 7},
            new byte[] {7, 9, 5, 7, 8, 9, 3, 11, 2},
            new byte[] {9, 5, 7, 9, 7, 2, 9, 2, 0, 2, 7, 11},
            new byte[] {2, 3, 11, 0, 1, 8, 1, 7, 8, 1, 5, 7},
            new byte[] {11, 2, 1, 11, 1, 7, 7, 1, 5},
            new byte[] {9, 5, 8, 8, 5, 7, 10, 1, 3, 10, 3, 11},
            new byte[] {5, 7, 0, 5, 0, 9, 7, 11, 0, 1, 0, 10, 11, 10, 0},
            new byte[] {11, 10, 0, 11, 0, 3, 10, 5, 0, 8, 0, 7, 5, 7, 0},
            new byte[] {11, 10, 5, 7, 11, 5},
            new byte[] {10, 6, 5},
            new byte[] {0, 8, 3, 5, 10, 6},
            new byte[] {9, 0, 1, 5, 10, 6},
            new byte[] {1, 8, 3, 1, 9, 8, 5, 10, 6},
            new byte[] {1, 6, 5, 2, 6, 1},
            new byte[] {1, 6, 5, 1, 2, 6, 3, 0, 8},
            new byte[] {9, 6, 5, 9, 0, 6, 0, 2, 6},
            new byte[] {5, 9, 8, 5, 8, 2, 5, 2, 6, 3, 2, 8},
            new byte[] {2, 3, 11, 10, 6, 5},
            new byte[] {11, 0, 8, 11, 2, 0, 10, 6, 5},
            new byte[] {0, 1, 9, 2, 3, 11, 5, 10, 6},
            new byte[] {5, 10, 6, 1, 9, 2, 9, 11, 2, 9, 8, 11},
            new byte[] {6, 3, 11, 6, 5, 3, 5, 1, 3},
            new byte[] {0, 8, 11, 0, 11, 5, 0, 5, 1, 5, 11, 6},
            new byte[] {3, 11, 6, 0, 3, 6, 0, 6, 5, 0, 5, 9},
            new byte[] {6, 5, 9, 6, 9, 11, 11, 9, 8},
            new byte[] {5, 10, 6, 4, 7, 8},
            new byte[] {4, 3, 0, 4, 7, 3, 6, 5, 10},
            new byte[] {1, 9, 0, 5, 10, 6, 8, 4, 7},
            new byte[] {10, 6, 5, 1, 9, 7, 1, 7, 3, 7, 9, 4},
            new byte[] {6, 1, 2, 6, 5, 1, 4, 7, 8},
            new byte[] {1, 2, 5, 5, 2, 6, 3, 0, 4, 3, 4, 7},
            new byte[] {8, 4, 7, 9, 0, 5, 0, 6, 5, 0, 2, 6},
            new byte[] {7, 3, 9, 7, 9, 4, 3, 2, 9, 5, 9, 6, 2, 6, 9},
            new byte[] {3, 11, 2, 7, 8, 4, 10, 6, 5},
            new byte[] {5, 10, 6, 4, 7, 2, 4, 2, 0, 2, 7, 11},
            new byte[] {0, 1, 9, 4, 7, 8, 2, 3, 11, 5, 10, 6},
            new byte[] {9, 2, 1, 9, 11, 2, 9, 4, 11, 7, 11, 4, 5, 10, 6},
            new byte[] {8, 4, 7, 3, 11, 5, 3, 5, 1, 5, 11, 6},
            new byte[] {5, 1, 11, 5, 11, 6, 1, 0, 11, 7, 11, 4, 0, 4, 11},
            new byte[] {0, 5, 9, 0, 6, 5, 0, 3, 6, 11, 6, 3, 8, 4, 7},
            new byte[] {6, 5, 9, 6, 9, 11, 4, 7, 9, 7, 11, 9},
            new byte[] {10, 4, 9, 6, 4, 10},
            new byte[] {4, 10, 6, 4, 9, 10, 0, 8, 3},
            new byte[] {10, 0, 1, 10, 6, 0, 6, 4, 0},
            new byte[] {8, 3, 1, 8, 1, 6, 8, 6, 4, 6, 1, 10},
            new byte[] {1, 4, 9, 1, 2, 4, 2, 6, 4},
            new byte[] {3, 0, 8, 1, 2, 9, 2, 4, 9, 2, 6, 4},
            new byte[] {0, 2, 4, 4, 2, 6},
            new byte[] {8, 3, 2, 8, 2, 4, 4, 2, 6},
            new byte[] {10, 4, 9, 10, 6, 4, 11, 2, 3},
            new byte[] {0, 8, 2, 2, 8, 11, 4, 9, 10, 4, 10, 6},
            new byte[] {3, 11, 2, 0, 1, 6, 0, 6, 4, 6, 1, 10},
            new byte[] {6, 4, 1, 6, 1, 10, 4, 8, 1, 2, 1, 11, 8, 11, 1},
            new byte[] {9, 6, 4, 9, 3, 6, 9, 1, 3, 11, 6, 3},
            new byte[] {8, 11, 1, 8, 1, 0, 11, 6, 1, 9, 1, 4, 6, 4, 1},
            new byte[] {3, 11, 6, 3, 6, 0, 0, 6, 4},
            new byte[] {6, 4, 8, 11, 6, 8},
            new byte[] {7, 10, 6, 7, 8, 10, 8, 9, 10},
            new byte[] {0, 7, 3, 0, 10, 7, 0, 9, 10, 6, 7, 10},
            new byte[] {10, 6, 7, 1, 10, 7, 1, 7, 8, 1, 8, 0},
            new byte[] {10, 6, 7, 10, 7, 1, 1, 7, 3},
            new byte[] {1, 2, 6, 1, 6, 8, 1, 8, 9, 8, 6, 7},
            new byte[] {2, 6, 9, 2, 9, 1, 6, 7, 9, 0, 9, 3, 7, 3, 9},
            new byte[] {7, 8, 0, 7, 0, 6, 6, 0, 2},
            new byte[] {7, 3, 2, 6, 7, 2},
            new byte[] {2, 3, 11, 10, 6, 8, 10, 8, 9, 8, 6, 7},
            new byte[] {2, 0, 7, 2, 7, 11, 0, 9, 7, 6, 7, 10, 9, 10, 7},
            new byte[] {1, 8, 0, 1, 7, 8, 1, 10, 7, 6, 7, 10, 2, 3, 11},
            new byte[] {11, 2, 1, 11, 1, 7, 10, 6, 1, 6, 7, 1},
            new byte[] {8, 9, 6, 8, 6, 7, 9, 1, 6, 11, 6, 3, 1, 3, 6},
            new byte[] {0, 9, 1, 11, 6, 7},
            new byte[] {7, 8, 0, 7, 0, 6, 3, 11, 0, 11, 6, 0},
            new byte[] {7, 11, 6},
            new byte[] {7, 6, 11},
            new byte[] {3, 0, 8, 11, 7, 6},
            new byte[] {0, 1, 9, 11, 7, 6},
            new byte[] {8, 1, 9, 8, 3, 1, 11, 7, 6},
            new byte[] {10, 1, 2, 6, 11, 7},
            new byte[] {1, 2, 10, 3, 0, 8, 6, 11, 7},
            new byte[] {2, 9, 0, 2, 10, 9, 6, 11, 7},
            new byte[] {6, 11, 7, 2, 10, 3, 10, 8, 3, 10, 9, 8},
            new byte[] {7, 2, 3, 6, 2, 7},
            new byte[] {7, 0, 8, 7, 6, 0, 6, 2, 0},
            new byte[] {2, 7, 6, 2, 3, 7, 0, 1, 9},
            new byte[] {1, 6, 2, 1, 8, 6, 1, 9, 8, 8, 7, 6},
            new byte[] {10, 7, 6, 10, 1, 7, 1, 3, 7},
            new byte[] {10, 7, 6, 1, 7, 10, 1, 8, 7, 1, 0, 8},
            new byte[] {0, 3, 7, 0, 7, 10, 0, 10, 9, 6, 10, 7},
            new byte[] {7, 6, 10, 7, 10, 8, 8, 10, 9},
            new byte[] {6, 8, 4, 11, 8, 6},
            new byte[] {3, 6, 11, 3, 0, 6, 0, 4, 6},
            new byte[] {8, 6, 11, 8, 4, 6, 9, 0, 1},
            new byte[] {9, 4, 6, 9, 6, 3, 9, 3, 1, 11, 3, 6},
            new byte[] {6, 8, 4, 6, 11, 8, 2, 10, 1},
            new byte[] {1, 2, 10, 3, 0, 11, 0, 6, 11, 0, 4, 6},
            new byte[] {4, 11, 8, 4, 6, 11, 0, 2, 9, 2, 10, 9},
            new byte[] {10, 9, 3, 10, 3, 2, 9, 4, 3, 11, 3, 6, 4, 6, 3},
            new byte[] {8, 2, 3, 8, 4, 2, 4, 6, 2},
            new byte[] {0, 4, 2, 4, 6, 2},
            new byte[] {1, 9, 0, 2, 3, 4, 2, 4, 6, 4, 3, 8},
            new byte[] {1, 9, 4, 1, 4, 2, 2, 4, 6},
            new byte[] {8, 1, 3, 8, 6, 1, 8, 4, 6, 6, 10, 1},
            new byte[] {10, 1, 0, 10, 0, 6, 6, 0, 4},
            new byte[] {4, 6, 3, 4, 3, 8, 6, 10, 3, 0, 3, 9, 10, 9, 3},
            new byte[] {10, 9, 4, 6, 10, 4},
            new byte[] {4, 9, 5, 7, 6, 11},
            new byte[] {0, 8, 3, 4, 9, 5, 11, 7, 6},
            new byte[] {5, 0, 1, 5, 4, 0, 7, 6, 11},
            new byte[] {11, 7, 6, 8, 3, 4, 3, 5, 4, 3, 1, 5},
            new byte[] {9, 5, 4, 10, 1, 2, 7, 6, 11},
            new byte[] {6, 11, 7, 1, 2, 10, 0, 8, 3, 4, 9, 5},
            new byte[] {7, 6, 11, 5, 4, 10, 4, 2, 10, 4, 0, 2},
            new byte[] {3, 4, 8, 3, 5, 4, 3, 2, 5, 10, 5, 2, 11, 7, 6},
            new byte[] {7, 2, 3, 7, 6, 2, 5, 4, 9},
            new byte[] {9, 5, 4, 0, 8, 6, 0, 6, 2, 6, 8, 7},
            new byte[] {3, 6, 2, 3, 7, 6, 1, 5, 0, 5, 4, 0},
            new byte[] {6, 2, 8, 6, 8, 7, 2, 1, 8, 4, 8, 5, 1, 5, 8},
            new byte[] {9, 5, 4, 10, 1, 6, 1, 7, 6, 1, 3, 7},
            new byte[] {1, 6, 10, 1, 7, 6, 1, 0, 7, 8, 7, 0, 9, 5, 4},
            new byte[] {4, 0, 10, 4, 10, 5, 0, 3, 10, 6, 10, 7, 3, 7, 10},
            new byte[] {7, 6, 10, 7, 10, 8, 5, 4, 10, 4, 8, 10},
            new byte[] {6, 9, 5, 6, 11, 9, 11, 8, 9},
            new byte[] {3, 6, 11, 0, 6, 3, 0, 5, 6, 0, 9, 5},
            new byte[] {0, 11, 8, 0, 5, 11, 0, 1, 5, 5, 6, 11},
            new byte[] {6, 11, 3, 6, 3, 5, 5, 3, 1},
            new byte[] {1, 2, 10, 9, 5, 11, 9, 11, 8, 11, 5, 6},
            new byte[] {0, 11, 3, 0, 6, 11, 0, 9, 6, 5, 6, 9, 1, 2, 10},
            new byte[] {11, 8, 5, 11, 5, 6, 8, 0, 5, 10, 5, 2, 0, 2, 5},
            new byte[] {6, 11, 3, 6, 3, 5, 2, 10, 3, 10, 5, 3},
            new byte[] {5, 8, 9, 5, 2, 8, 5, 6, 2, 3, 8, 2},
            new byte[] {9, 5, 6, 9, 6, 0, 0, 6, 2},
            new byte[] {1, 5, 8, 1, 8, 0, 5, 6, 8, 3, 8, 2, 6, 2, 8},
            new byte[] {1, 5, 6, 2, 1, 6},
            new byte[] {1, 3, 6, 1, 6, 10, 3, 8, 6, 5, 6, 9, 8, 9, 6},
            new byte[] {10, 1, 0, 10, 0, 6, 9, 5, 0, 5, 6, 0},
            new byte[] {0, 3, 8, 5, 6, 10},
            new byte[] {10, 5, 6},
            new byte[] {11, 5, 10, 7, 5, 11},
            new byte[] {11, 5, 10, 11, 7, 5, 8, 3, 0},
            new byte[] {5, 11, 7, 5, 10, 11, 1, 9, 0},
            new byte[] {10, 7, 5, 10, 11, 7, 9, 8, 1, 8, 3, 1},
            new byte[] {11, 1, 2, 11, 7, 1, 7, 5, 1},
            new byte[] {0, 8, 3, 1, 2, 7, 1, 7, 5, 7, 2, 11},
            new byte[] {9, 7, 5, 9, 2, 7, 9, 0, 2, 2, 11, 7},
            new byte[] {7, 5, 2, 7, 2, 11, 5, 9, 2, 3, 2, 8, 9, 8, 2},
            new byte[] {2, 5, 10, 2, 3, 5, 3, 7, 5},
            new byte[] {8, 2, 0, 8, 5, 2, 8, 7, 5, 10, 2, 5},
            new byte[] {9, 0, 1, 5, 10, 3, 5, 3, 7, 3, 10, 2},
            new byte[] {9, 8, 2, 9, 2, 1, 8, 7, 2, 10, 2, 5, 7, 5, 2},
            new byte[] {1, 3, 5, 3, 7, 5},
            new byte[] {0, 8, 7, 0, 7, 1, 1, 7, 5},
            new byte[] {9, 0, 3, 9, 3, 5, 5, 3, 7},
            new byte[] {9, 8, 7, 5, 9, 7},
            new byte[] {5, 8, 4, 5, 10, 8, 10, 11, 8},
            new byte[] {5, 0, 4, 5, 11, 0, 5, 10, 11, 11, 3, 0},
            new byte[] {0, 1, 9, 8, 4, 10, 8, 10, 11, 10, 4, 5},
            new byte[] {10, 11, 4, 10, 4, 5, 11, 3, 4, 9, 4, 1, 3, 1, 4},
            new byte[] {2, 5, 1, 2, 8, 5, 2, 11, 8, 4, 5, 8},
            new byte[] {0, 4, 11, 0, 11, 3, 4, 5, 11, 2, 11, 1, 5, 1, 11},
            new byte[] {0, 2, 5, 0, 5, 9, 2, 11, 5, 4, 5, 8, 11, 8, 5},
            new byte[] {9, 4, 5, 2, 11, 3},
            new byte[] {2, 5, 10, 3, 5, 2, 3, 4, 5, 3, 8, 4},
            new byte[] {5, 10, 2, 5, 2, 4, 4, 2, 0},
            new byte[] {3, 10, 2, 3, 5, 10, 3, 8, 5, 4, 5, 8, 0, 1, 9},
            new byte[] {5, 10, 2, 5, 2, 4, 1, 9, 2, 9, 4, 2},
            new byte[] {8, 4, 5, 8, 5, 3, 3, 5, 1},
            new byte[] {0, 4, 5, 1, 0, 5},
            new byte[] {8, 4, 5, 8, 5, 3, 9, 0, 5, 0, 3, 5},
            new byte[] {9, 4, 5},
            new byte[] {4, 11, 7, 4, 9, 11, 9, 10, 11},
            new byte[] {0, 8, 3, 4, 9, 7, 9, 11, 7, 9, 10, 11},
            new byte[] {1, 10, 11, 1, 11, 4, 1, 4, 0, 7, 4, 11},
            new byte[] {3, 1, 4, 3, 4, 8, 1, 10, 4, 7, 4, 11, 10, 11, 4},
            new byte[] {4, 11, 7, 9, 11, 4, 9, 2, 11, 9, 1, 2},
            new byte[] {9, 7, 4, 9, 11, 7, 9, 1, 11, 2, 11, 1, 0, 8, 3},
            new byte[] {11, 7, 4, 11, 4, 2, 2, 4, 0},
            new byte[] {11, 7, 4, 11, 4, 2, 8, 3, 4, 3, 2, 4},
            new byte[] {2, 9, 10, 2, 7, 9, 2, 3, 7, 7, 4, 9},
            new byte[] {9, 10, 7, 9, 7, 4, 10, 2, 7, 8, 7, 0, 2, 0, 7},
            new byte[] {3, 7, 10, 3, 10, 2, 7, 4, 10, 1, 10, 0, 4, 0, 10},
            new byte[] {1, 10, 2, 8, 7, 4},
            new byte[] {4, 9, 1, 4, 1, 7, 7, 1, 3},
            new byte[] {4, 9, 1, 4, 1, 7, 0, 8, 1, 8, 7, 1},
            new byte[] {4, 0, 3, 7, 4, 3},
            new byte[] {4, 8, 7},
            new byte[] {9, 10, 8, 10, 11, 8},
            new byte[] {3, 0, 9, 3, 9, 11, 11, 9, 10},
            new byte[] {0, 1, 10, 0, 10, 8, 8, 10, 11},
            new byte[] {3, 1, 10, 11, 3, 10},
            new byte[] {1, 2, 11, 1, 11, 9, 9, 11, 8},
            new byte[] {3, 0, 9, 3, 9, 11, 1, 2, 9, 2, 11, 9},
            new byte[] {0, 2, 11, 8, 0, 11},
            new byte[] {3, 2, 11},
            new byte[] {2, 3, 8, 2, 8, 10, 10, 8, 9},
            new byte[] {9, 10, 2, 0, 9, 2},
            new byte[] {2, 3, 8, 2, 8, 10, 0, 1, 8, 1, 10, 8},
            new byte[] {1, 10, 2},
            new byte[] {1, 3, 8, 9, 1, 8},
            new byte[] {0, 9, 1},
            new byte[] {0, 3, 8},
            new byte[] { }
        };
        #endregion
    }
}
