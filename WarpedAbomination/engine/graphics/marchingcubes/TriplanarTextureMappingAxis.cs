﻿using System;
namespace WarpedAbominationEngine
{
    [Flags]
    public enum TriplanarTextureMappingAxis : byte
    {
        None = 0,
        X = 1,
        Y = 2,
        Z = 4
    }
}
