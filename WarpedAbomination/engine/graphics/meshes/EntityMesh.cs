﻿using System;
using Microsoft.Xna.Framework;
using WarpedAbominationEngine.Graphics;

namespace WarpedAbominationEngine
{
    public abstract class EntityMesh : PortalLinkable
    {
        public EntityMesh()
        {
            LinkType = PortalLinkType.Mesh;
        }

        public abstract void Draw(AbstractRenderer renderer);
    }
}
