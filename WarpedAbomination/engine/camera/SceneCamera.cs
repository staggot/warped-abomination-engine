﻿using System;
using Microsoft.Xna.Framework;

namespace WarpedAbominationEngine
{
    public class SceneCamera
    {

        protected const float AngleEpsilon = 0.0001f;

        protected readonly Scene Scene;
        protected readonly CameraPVS pvs;
        protected float fov = MathHelper.PI * 90.0f / 180.0f, fogEnd = 10, aspect = 1;
        protected float aspectRatio = 1;
        protected float nearClip = 0.0001f, farClip = 1000f;

        public float Yaw, Pitch, Roll;
        public Vector3 Position;
        public Vector4 FogColor = Color.Black.ToVector4();
        public Sector Sector;
        public Vector3 Forward => YawPitchRoll.Forward;
        public Vector3 Backward => YawPitchRoll.Backward;
        public Vector3 Left => YawPitchRoll.Left;
        public Vector3 Right => YawPitchRoll.Right;
        public Vector3 Up => YawPitchRoll.Up;
        public Vector3 Down => YawPitchRoll.Down;

        public Matrix YawPitchRoll { get; private set; }
        public Matrix Projection { get; private set; }

        public SceneCamera(Scene scene)
        {
            this.Scene = scene;
            pvs = new CameraPVS();
        }

        public float FOV
        {
            get { return fov; }
            set
            {
                float newFov = value;
                if (fov <= 0)
                    fov = AngleEpsilon;
                if (fov >= MathHelper.PI)
                    fov = MathHelper.PI - AngleEpsilon;
                if(newFov != fov)
                {
                    fov = newFov;
                    UpdateProjectionMatrix();
                }
            }
        }

        public float FarClip
        {
            get { return farClip; }
            set
            {
                if (farClip == value)
                    return;
                farClip = value;
                UpdateProjectionMatrix();
            }
        }

        public float NearClip
        {
            get { return nearClip; }
            set
            {
                if (nearClip == value)
                    return;
                nearClip = value;
                UpdateProjectionMatrix();
            }
        }

        public Matrix View
        {
            get
            {
                return Matrix.CreateLookAt(Position, Position + Forward, Up);
            }
        }

        public float AspectRatio
        {
            get { return aspect; }
            set
            {
                if (aspect == value)
                    return;
                aspect = value;
                UpdateProjectionMatrix();
            }
        }

        public virtual void Update()
        {
            Portal portalledPortal = Sector.Portals.Portalize(Position, Vector2.Zero);
            if (portalledPortal != null) {
                Position = portalledPortal.TransformPositionTo(Position);
                Sector = portalledPortal.OutSector;
                Yaw = portalledPortal.TransformYawTo(Yaw);
                Pitch = portalledPortal.TransformPitchTo(Pitch);
                Roll = portalledPortal.TransformPitchTo(Roll);
            }
            Yaw %= MathHelper.TwoPI;
            if (Pitch < -MathHelper.PIOver2 + AngleEpsilon)
                Pitch = -MathHelper.PIOver2 + AngleEpsilon;
            if (Pitch > MathHelper.PIOver2 - AngleEpsilon)
                Pitch = MathHelper.PIOver2 - AngleEpsilon;
            if (Roll < -MathHelper.PIOver2 + AngleEpsilon)
                Roll = -MathHelper.PIOver2 + AngleEpsilon;
            if (Roll > MathHelper.PIOver2 - AngleEpsilon)
                Roll = MathHelper.PIOver2 - AngleEpsilon;
            YawPitchRoll = Matrix.CreateFromYawPitchRoll(Yaw, Pitch, Roll);
            pvs.Fill(Sector, new WarpCamera() {
                CameraPosition = Position,
                CameraUp = Up,
                CameraDirection = Forward,
                CameraSpriteFacing = Right,
                Distance = farClip
            });
        }

        public CameraPVS PVS {
            get { return pvs; }
        }

        void UpdateProjectionMatrix()
        {
            Projection = Matrix.CreatePerspectiveFieldOfView(fov, aspect, 0.0001f, fogEnd > 1f ? fogEnd : 1f);
        }


        public void ClearPVS()
        {
            PVS.Clear();
        }
    }
}
