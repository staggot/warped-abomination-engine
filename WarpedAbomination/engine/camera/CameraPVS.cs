﻿using System;
namespace WarpedAbominationEngine
{
    public class CameraPVS : PVSBase
    {
        internal CameraPVS() :
            base()
        {
        }

        internal CameraPVS(int maxBranches, int maxSectorStackSize, int maxDeep) :
            base(maxBranches, maxSectorStackSize, maxDeep)
        {
        }

        protected override bool PassWarp(WarpCamera camera, Portal exitPortal)
        {
            throw new NotImplementedException();
        }

        protected override bool PassSector(WarpCamera camera, SectorView sector)
        {
            throw new NotImplementedException();
        }
    }
}
