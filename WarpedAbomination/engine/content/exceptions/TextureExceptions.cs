﻿using System;
namespace WarpedAbominationEngine.Content
{
    public class TextureException : Exception
    {
        public TextureException(string message): base(message)
        {
        }

        public TextureException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }

    public class MismatchedTextureResolution : TextureException
    {
        public MismatchedTextureResolution(string textureName, string textureType, Point2 expectedResolution, Point2 gotResolution) :
            base(string.Format("Mismatched resolution on: texture name: \"{1}\":\"{0}\"[{2}x{3}], expected [{4}x{5}]",
                textureType, textureName, gotResolution.X, gotResolution.Y, expectedResolution.X, expectedResolution.Y))
        {

        }
    }

    public class TextureIdsDepleted : TextureException
    {
        public MegaTexture Megatexture { get; private set; }
        readonly int countAllocated; 

        public TextureIdsDepleted(MegaTexture megatexture, int allocated) : base(string.Format(
            "Megatexrure '{0}' ids depleted, total allocated {1}", megatexture.Name, allocated))
        {
            Megatexture = megatexture;
            countAllocated = allocated;
        }
    }

    public class CantAllocateMegatextureFragment : TextureException
    {
        readonly MegaTexture megatexture;
        Point2 target;

        public CantAllocateMegatextureFragment(MegaTexture megatexture, Point2 target) : base(string.Format(
            "Cannot allocate fragment [{0}x{1}] on megatexture '{0}'", megatexture.Name, target.X, target.Y))
        {
            this.megatexture = megatexture;
            this.target = target;
        }
    }
}
