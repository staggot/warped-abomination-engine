﻿using System;
using System.IO;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using WarpedAbominationEngine.Configuration;
using Microsoft.Xna.Framework;

namespace WarpedAbominationEngine.Content
{
    public static class AbominationContent
    {
        public readonly static MegaTexture[] Textures = new MegaTexture[AbominationSystem.TotalTextureTypes];
        public static ShaderCollection Shaders { get; private set; }
        readonly static HashSet<EngineTextureTarget> loadedTextures = new HashSet<EngineTextureTarget>();

        const int RenderStatesCount = 256;
        public readonly static DepthStencilState[] OpaqueDS = ((Func<DepthStencilState[]>)(() => {
            DepthStencilState[] result = new DepthStencilState[RenderStatesCount];
            for (int i = 0; i < RenderStatesCount; i++) {
                result[i] = new DepthStencilState() {
                    StencilEnable = true,
                    StencilFunction = CompareFunction.Equal,
                    ReferenceStencil = i,
                    StencilMask = 0xff,
                    StencilPass = StencilOperation.Keep,
                    DepthBufferWriteEnable = true
                };
            }
            return result;
        }))();

        public readonly static DepthStencilState[] TransparentDS = ((Func<DepthStencilState[]>)(() => {
            DepthStencilState[] result = new DepthStencilState[RenderStatesCount];
            for (int i = 0; i < RenderStatesCount; i++) {
                result[i] = new DepthStencilState() {
                    StencilEnable = true,
                    StencilFunction = CompareFunction.Equal,
                    ReferenceStencil = i,
                    StencilMask = 0xff,
                    StencilPass = StencilOperation.Keep,
                    DepthBufferWriteEnable = false
                };
            }
            return result;
        }))();

        public readonly static DepthStencilState[] PortalOverlaysDS = ((Func<DepthStencilState[]>)(() => {
            DepthStencilState[] result = new DepthStencilState[RenderStatesCount];
            for (int i = 0; i < RenderStatesCount; i++) {
                result[i] = new DepthStencilState() {
                    DepthBufferFunction = CompareFunction.LessEqual,
                    DepthBufferWriteEnable = true,
                    StencilEnable = true,
                    StencilFunction = CompareFunction.Equal,
                    StencilPass = StencilOperation.Keep,
                    ReferenceStencil = i,
                    StencilMask = 0xff,
                };
            }
            return result;
        }))();

        public readonly static DepthStencilState PortalStencilDS = new DepthStencilState() {
            StencilEnable = true,
            StencilFunction = CompareFunction.Always,
            StencilPass = StencilOperation.Increment,
            DepthBufferFunction = CompareFunction.Always,
            DepthBufferWriteEnable = false,
            StencilWriteMask = 0xff
        };

        static AbominationContent()
        {
        }


        static void Init()
        {

        }

        static MegaTexture CreateMegatexture(EngineTextureTarget target)
        {
            ConfigSection section = AbominationSystem.GetConfigSection(target);
            int resolution = (int)(section[AbominationSystem.ResolutionParameter] ?? 2048);
            MegaTextureBorder border = ((string)(
                section[AbominationSystem.BorderParameter] ??
                MegaTextureBorder.None.GetName())).ParseTextureBorder();
            MegaTextureFlags flags = (MegaTextureFlags)(
                (((bool)(section[AbominationSystem.NormalEnabledParameter] ?? false)) ?
                MegaTextureFlags.NormalEnabled : MegaTextureFlags.None) |
                (((bool)(section[AbominationSystem.MetaEnabledParameter] ?? false)) ?
                MegaTextureFlags.MetaEnabled : MegaTextureFlags.None)
            );
            bool mipmap = (bool)(section[AbominationSystem.MipmapEnabledParameter] ?? false);
            return new MegaTexture(target.GetName(), resolution, mipmap, flags, border);
        }


        static void Load(EngineTextureTarget target)
        {
            if (loadedTextures.Contains(target))
                return;
            int targetIndex = (int)target;
            if (Textures[targetIndex] != null)
                Textures[targetIndex].Dispose();
            else
                Textures[targetIndex] = CreateMegatexture(target);
            Textures[(int)target].Load(new ResourceSubStorage(
                AbominationSystem.TextureEngineStorage,
                target.GetName()
                ));
            if (AbominationSystem.TextureUserStorage != null)
                Textures[(int)target].Load(new ResourceSubStorage(
                    AbominationSystem.TextureUserStorage,
                    target.GetName()
                    ));
            loadedTextures.Add(target);
        }
    }
}
