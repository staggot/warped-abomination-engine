﻿using System;
using System.Collections.Generic;
using System.IO;

namespace WarpedAbominationEngine.Content
{
    public class FSResourceStorage : ResourceStorage
    {
        public DirectoryInfo FSRoot { get; private set; }

        public FSResourceStorage(DirectoryInfo directory)
        {
            FSRoot = directory;
            if (!FSRoot.Exists)
                FSRoot.Create();
        }

        protected string GetFSPath(string path)
        {
            return Path.Combine(FSRoot.FullName, path);
        }

        public FSResourceStorage(string path):this(new DirectoryInfo(path))
        {
        }

        public override void CreateDirectory(string path)
        {
            Directory.CreateDirectory(GetFSPath(path));
        }

        public override bool IsDirectoryExists(string path)
        {
            return Directory.Exists(GetFSPath(path));
        }

        public override bool IsResourceExists(string path)
        {
            return File.Exists(GetFSPath(path));
        }

        public override string[] ListResources(string path)
        {
            return Directory.GetFiles(GetFSPath(path));
        }

        public override Stream Read(string path)
        {
            return new FileStream(GetFSPath(path), FileMode.Open, FileAccess.Read);
        }

        public override Stream Write(string path)
        {
            return new FileStream(GetFSPath(path), FileMode.Create, FileAccess.Write);
        }
    }
}
