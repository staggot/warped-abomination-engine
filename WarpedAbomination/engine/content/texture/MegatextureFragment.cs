﻿using System;
using Microsoft.Xna.Framework;

namespace WarpedAbominationEngine.Content
{
    public struct MegatextureFragment
    {
        static readonly byte[] edgeMapping = new byte[64];

        readonly Vector2[] edges;

        public MegatextureFragment(Vector4 position, TextureMode mode)
        {
            edges = new Vector2[4];
            edges[edgeMapping[(byte)mode | (TexureCoordinateIndex.LeftTop << 4)]]
            = new Vector2(position.X, position.Y);
            edges[edgeMapping[(byte)mode | (TexureCoordinateIndex.LeftBottom << 4)]]
            = new Vector2(position.X, position.Y + position.W);
            edges[edgeMapping[(byte)mode | (TexureCoordinateIndex.RightTop << 4)]]
            = new Vector2(position.X + position.Z, position.Y);
            edges[edgeMapping[(byte)mode | (TexureCoordinateIndex.RightBottom << 4)]]
            = new Vector2(position.X + position.Z, position.Y + position.W);
        }

        public MegatextureFragment(MegatextureFragment old, TextureMode mode)
        {
            edges = new Vector2[4];
            ModEdges(old.edges, edges, mode);
        }

        public MegatextureFragment(Vector2 lt, Vector2 rt, Vector2 rb, Vector2 lb)
        {
            edges = new Vector2[4];
            this.LT = lt;
            this.RT = rt;
            this.RB = rb;
            this.LB = lb;
        }


        public MegatextureFragment Mod(TextureMode mode)
        {
            return new MegatextureFragment(this, mode);
        }

        public MegatextureFragment Subtexture(Vector4 subTex)
        {
            return new MegatextureFragment(
                GetCoord(subTex.X, subTex.Y),
                GetCoord(subTex.X + subTex.Z, subTex.Y),
                GetCoord(subTex.X + subTex.Z, subTex.Y + subTex.W),
                GetCoord(subTex.X, subTex.Y + subTex.W)
            );
        }

        public Vector2 GetCoord(float x, float y)
        {
            Vector2 top = Vector2.Lerp(
                edges[TexureCoordinateIndex.LeftTop],
                edges[TexureCoordinateIndex.RightTop],
                x
            );
            Vector2 bottom = Vector2.Lerp(
                edges[TexureCoordinateIndex.LeftBottom],
                edges[TexureCoordinateIndex.RightBottom],
                x
            );
            return Vector2.Lerp(top, bottom, y);
        }

        public Vector2 GetCoord(Vector2 uv)
        {
            return GetCoord(uv.X, uv.Y);
        }

        public Vector2 LT
        {
            get { return edges[TexureCoordinateIndex.LeftTop]; }
            set { edges[TexureCoordinateIndex.LeftTop] = value; }
        }

        public Vector2 LB
        {
            get { return edges[TexureCoordinateIndex.LeftBottom]; }
            set { edges[TexureCoordinateIndex.LeftBottom] = value; }
        }

        public Vector2 RT
        {
            get { return edges[TexureCoordinateIndex.RightTop]; }
            set { edges[TexureCoordinateIndex.RightTop] = value; }
        }

        public Vector2 RB
        {
            get { return edges[TexureCoordinateIndex.RightBottom]; }
            set { edges[TexureCoordinateIndex.RightBottom] = value; }
        }

        static void ModEdges(Vector2[] oldE, Vector2[] newE, TextureMode mode)
        {
            newE[edgeMapping[(byte)mode | (TexureCoordinateIndex.LeftTop << 4)]] = oldE[TexureCoordinateIndex.LeftTop];
            newE[edgeMapping[(byte)mode | (TexureCoordinateIndex.LeftBottom << 4)]] = oldE[TexureCoordinateIndex.LeftBottom];
            newE[edgeMapping[(byte)mode | (TexureCoordinateIndex.RightTop << 4)]] = oldE[TexureCoordinateIndex.RightTop];
            newE[edgeMapping[(byte)mode | (TexureCoordinateIndex.RightBottom << 4)]] = oldE[TexureCoordinateIndex.RightBottom];
        }


        static MegatextureFragment()
        {
            edgeMapping[(byte)(TextureMode.Default | TextureMode.Default) | (TexureCoordinateIndex.LeftTop << 4)] = TexureCoordinateIndex.LeftTop;
            edgeMapping[(byte)(TextureMode.Default | TextureMode.Default) | (TexureCoordinateIndex.RightTop << 4)] = TexureCoordinateIndex.RightTop;
            edgeMapping[(byte)(TextureMode.Default | TextureMode.Default) | (TexureCoordinateIndex.LeftBottom << 4)] = TexureCoordinateIndex.LeftBottom;
            edgeMapping[(byte)(TextureMode.Default | TextureMode.Default) | (TexureCoordinateIndex.RightBottom << 4)] = TexureCoordinateIndex.RightBottom;

            edgeMapping[(byte)(TextureMode.Default | TextureMode.ReflectHorisontal) | (TexureCoordinateIndex.LeftTop << 4)] = TexureCoordinateIndex.RightTop;
            edgeMapping[(byte)(TextureMode.Default | TextureMode.ReflectHorisontal) | (TexureCoordinateIndex.RightTop << 4)] = TexureCoordinateIndex.LeftTop;
            edgeMapping[(byte)(TextureMode.Default | TextureMode.ReflectHorisontal) | (TexureCoordinateIndex.LeftBottom << 4)] = TexureCoordinateIndex.RightBottom;
            edgeMapping[(byte)(TextureMode.Default | TextureMode.ReflectHorisontal) | (TexureCoordinateIndex.RightBottom << 4)] = TexureCoordinateIndex.LeftBottom;

            edgeMapping[(byte)(TextureMode.Default | TextureMode.ReflectVertical) | (TexureCoordinateIndex.LeftTop << 4)] = TexureCoordinateIndex.LeftBottom;
            edgeMapping[(byte)(TextureMode.Default | TextureMode.ReflectVertical) | (TexureCoordinateIndex.RightTop << 4)] = TexureCoordinateIndex.RightBottom;
            edgeMapping[(byte)(TextureMode.Default | TextureMode.ReflectVertical) | (TexureCoordinateIndex.LeftBottom << 4)] = TexureCoordinateIndex.LeftTop;
            edgeMapping[(byte)(TextureMode.Default | TextureMode.ReflectVertical) | (TexureCoordinateIndex.RightBottom << 4)] = TexureCoordinateIndex.RightTop;

            edgeMapping[(byte)(TextureMode.Default | TextureMode.ReflectAll) | (TexureCoordinateIndex.LeftTop << 4)] = TexureCoordinateIndex.RightBottom;
            edgeMapping[(byte)(TextureMode.Default | TextureMode.ReflectAll) | (TexureCoordinateIndex.RightTop << 4)] = TexureCoordinateIndex.LeftBottom;
            edgeMapping[(byte)(TextureMode.Default | TextureMode.ReflectAll) | (TexureCoordinateIndex.LeftBottom << 4)] = TexureCoordinateIndex.RightTop;
            edgeMapping[(byte)(TextureMode.Default | TextureMode.ReflectAll) | (TexureCoordinateIndex.RightBottom << 4)] = TexureCoordinateIndex.LeftTop;

            edgeMapping[(byte)(TextureMode.RotateLeft | TextureMode.Default) | (TexureCoordinateIndex.LeftTop << 4)] = TexureCoordinateIndex.LeftBottom;
            edgeMapping[(byte)(TextureMode.RotateLeft | TextureMode.Default) | (TexureCoordinateIndex.RightTop << 4)] = TexureCoordinateIndex.LeftTop;
            edgeMapping[(byte)(TextureMode.RotateLeft | TextureMode.Default) | (TexureCoordinateIndex.LeftBottom << 4)] = TexureCoordinateIndex.RightBottom;
            edgeMapping[(byte)(TextureMode.RotateLeft | TextureMode.Default) | (TexureCoordinateIndex.RightBottom << 4)] = TexureCoordinateIndex.RightTop;

            edgeMapping[(byte)(TextureMode.RotateRight | TextureMode.Default) | (TexureCoordinateIndex.LeftTop << 4)] = TexureCoordinateIndex.RightTop;
            edgeMapping[(byte)(TextureMode.RotateRight | TextureMode.Default) | (TexureCoordinateIndex.RightTop << 4)] = TexureCoordinateIndex.RightBottom;
            edgeMapping[(byte)(TextureMode.RotateRight | TextureMode.Default) | (TexureCoordinateIndex.LeftBottom << 4)] = TexureCoordinateIndex.LeftTop;
            edgeMapping[(byte)(TextureMode.RotateRight | TextureMode.Default) | (TexureCoordinateIndex.RightBottom << 4)] = TexureCoordinateIndex.LeftBottom;

            edgeMapping[(byte)(TextureMode.Flip | TextureMode.Default) | (TexureCoordinateIndex.LeftTop << 4)] = TexureCoordinateIndex.RightBottom;
            edgeMapping[(byte)(TextureMode.Flip | TextureMode.Default) | (TexureCoordinateIndex.RightTop << 4)] = TexureCoordinateIndex.LeftBottom;
            edgeMapping[(byte)(TextureMode.Flip | TextureMode.Default) | (TexureCoordinateIndex.LeftBottom << 4)] = TexureCoordinateIndex.RightTop;
            edgeMapping[(byte)(TextureMode.Flip | TextureMode.Default) | (TexureCoordinateIndex.RightBottom << 4)] = TexureCoordinateIndex.LeftTop;

            edgeMapping[(byte)(TextureMode.RotateLeft | TextureMode.ReflectHorisontal) | (TexureCoordinateIndex.LeftTop << 4)] =
                edgeMapping[
                    (byte)TextureMode.ReflectHorisontal |
                    edgeMapping[(byte)TextureMode.RotateLeft | (TexureCoordinateIndex.LeftTop << 4)]];
            edgeMapping[(byte)(TextureMode.RotateLeft | TextureMode.ReflectHorisontal) | (TexureCoordinateIndex.RightTop << 4)] =
                edgeMapping[
                    (byte)TextureMode.ReflectHorisontal |
                    edgeMapping[(byte)TextureMode.RotateLeft | (TexureCoordinateIndex.RightTop << 4)]];
            edgeMapping[(byte)(TextureMode.RotateLeft | TextureMode.ReflectHorisontal) | (TexureCoordinateIndex.LeftBottom << 4)] =
                edgeMapping[
                    (byte)TextureMode.ReflectHorisontal |
                    edgeMapping[(byte)TextureMode.RotateLeft | (TexureCoordinateIndex.LeftBottom << 4)]];
            edgeMapping[(byte)(TextureMode.RotateLeft | TextureMode.ReflectHorisontal) | (TexureCoordinateIndex.RightBottom << 4)] =
                edgeMapping[
                    (byte)TextureMode.ReflectHorisontal |
                    edgeMapping[(byte)TextureMode.RotateLeft | (TexureCoordinateIndex.RightBottom << 4)]];

            edgeMapping[(byte)(TextureMode.RotateLeft | TextureMode.ReflectVertical) | (TexureCoordinateIndex.LeftTop << 4)] =
                edgeMapping[
                    (byte)TextureMode.ReflectVertical |
                    edgeMapping[(byte)TextureMode.RotateLeft | (TexureCoordinateIndex.LeftTop << 4)]];
            edgeMapping[(byte)(TextureMode.RotateLeft | TextureMode.ReflectVertical) | (TexureCoordinateIndex.RightTop << 4)] =
                edgeMapping[
                    (byte)TextureMode.ReflectVertical |
                    edgeMapping[(byte)TextureMode.RotateLeft | (TexureCoordinateIndex.RightTop << 4)]];
            edgeMapping[(byte)(TextureMode.RotateLeft | TextureMode.ReflectVertical) | (TexureCoordinateIndex.LeftBottom << 4)] =
                edgeMapping[
                    (byte)TextureMode.ReflectVertical |
                    edgeMapping[(byte)TextureMode.RotateLeft | (TexureCoordinateIndex.LeftBottom << 4)]];
            edgeMapping[(byte)(TextureMode.RotateLeft | TextureMode.ReflectVertical) | (TexureCoordinateIndex.RightBottom << 4)] =
                edgeMapping[
                    (byte)TextureMode.ReflectVertical |
                    edgeMapping[(byte)TextureMode.RotateLeft | (TexureCoordinateIndex.RightBottom << 4)]];

            edgeMapping[(byte)(TextureMode.RotateLeft | TextureMode.ReflectAll) | (TexureCoordinateIndex.LeftTop << 4)] =
                edgeMapping[
                    (byte)TextureMode.ReflectAll |
                    edgeMapping[(byte)TextureMode.RotateLeft | (TexureCoordinateIndex.LeftTop << 4)]];
            edgeMapping[(byte)(TextureMode.RotateLeft | TextureMode.ReflectAll) | (TexureCoordinateIndex.RightTop << 4)] =
                edgeMapping[
                    (byte)TextureMode.ReflectAll |
                    edgeMapping[(byte)TextureMode.RotateLeft | (TexureCoordinateIndex.RightTop << 4)]];
            edgeMapping[(byte)(TextureMode.RotateLeft | TextureMode.ReflectAll) | (TexureCoordinateIndex.LeftBottom << 4)] =
                edgeMapping[
                    (byte)TextureMode.ReflectAll |
                    edgeMapping[(byte)TextureMode.RotateLeft | (TexureCoordinateIndex.LeftBottom << 4)]];
            edgeMapping[(byte)(TextureMode.RotateLeft | TextureMode.ReflectAll) | (TexureCoordinateIndex.RightBottom << 4)] =
                edgeMapping[
                    (byte)TextureMode.ReflectAll |
                    edgeMapping[(byte)TextureMode.RotateLeft | (TexureCoordinateIndex.RightBottom << 4)]];
            edgeMapping[(byte)(TextureMode.RotateRight | TextureMode.ReflectHorisontal) | (TexureCoordinateIndex.LeftTop << 4)] =
                edgeMapping[
                    (byte)TextureMode.ReflectHorisontal |
                    edgeMapping[(byte)TextureMode.RotateRight | (TexureCoordinateIndex.LeftTop << 4)]];
            edgeMapping[(byte)(TextureMode.RotateRight | TextureMode.ReflectHorisontal) | (TexureCoordinateIndex.RightTop << 4)] =
                edgeMapping[
                    (byte)TextureMode.ReflectHorisontal |
                    edgeMapping[(byte)TextureMode.RotateRight | (TexureCoordinateIndex.RightTop << 4)]];
            edgeMapping[(byte)(TextureMode.RotateRight | TextureMode.ReflectHorisontal) | (TexureCoordinateIndex.LeftBottom << 4)] =
                edgeMapping[
                    (byte)TextureMode.ReflectHorisontal |
                    edgeMapping[(byte)TextureMode.RotateRight | (TexureCoordinateIndex.LeftBottom << 4)]];
            edgeMapping[(byte)(TextureMode.RotateRight | TextureMode.ReflectHorisontal) | (TexureCoordinateIndex.RightBottom << 4)] =
                edgeMapping[
                    (byte)TextureMode.ReflectHorisontal |
                    edgeMapping[(byte)TextureMode.RotateRight | (TexureCoordinateIndex.RightBottom << 4)]];
            edgeMapping[(byte)(TextureMode.RotateRight | TextureMode.ReflectVertical) | (TexureCoordinateIndex.LeftTop << 4)] =
                edgeMapping[
                    (byte)TextureMode.ReflectVertical |
                    edgeMapping[(byte)TextureMode.RotateRight | (TexureCoordinateIndex.LeftTop << 4)]];
            edgeMapping[(byte)(TextureMode.RotateRight | TextureMode.ReflectVertical) | (TexureCoordinateIndex.RightTop << 4)] =
                edgeMapping[
                    (byte)TextureMode.ReflectVertical |
                    edgeMapping[(byte)TextureMode.RotateRight | (TexureCoordinateIndex.RightTop << 4)]];
            edgeMapping[(byte)(TextureMode.RotateRight | TextureMode.ReflectVertical) | (TexureCoordinateIndex.LeftBottom << 4)] =
                edgeMapping[
                    (byte)TextureMode.ReflectVertical |
                    edgeMapping[(byte)TextureMode.RotateRight | (TexureCoordinateIndex.LeftBottom << 4)]];
            edgeMapping[(byte)(TextureMode.RotateRight | TextureMode.ReflectVertical) | (TexureCoordinateIndex.RightBottom << 4)] =
                edgeMapping[
                    (byte)TextureMode.ReflectVertical |
                    edgeMapping[(byte)TextureMode.RotateRight | (TexureCoordinateIndex.RightBottom << 4)]];
            edgeMapping[(byte)(TextureMode.RotateRight | TextureMode.ReflectAll) | (TexureCoordinateIndex.LeftTop << 4)] =
                edgeMapping[
                    (byte)TextureMode.ReflectAll |
                    edgeMapping[(byte)TextureMode.RotateRight | (TexureCoordinateIndex.LeftTop << 4)]];
            edgeMapping[(byte)(TextureMode.RotateRight | TextureMode.ReflectAll) | (TexureCoordinateIndex.RightTop << 4)] =
                edgeMapping[
                    (byte)TextureMode.ReflectAll |
                    edgeMapping[(byte)TextureMode.RotateRight | (TexureCoordinateIndex.RightTop << 4)]];
            edgeMapping[(byte)(TextureMode.RotateRight | TextureMode.ReflectAll) | (TexureCoordinateIndex.LeftBottom << 4)] =
                edgeMapping[
                    (byte)TextureMode.ReflectAll |
                    edgeMapping[(byte)TextureMode.RotateRight | (TexureCoordinateIndex.LeftBottom << 4)]];
            edgeMapping[(byte)(TextureMode.RotateRight | TextureMode.ReflectAll) | (TexureCoordinateIndex.RightBottom << 4)] =
                edgeMapping[
                    (byte)TextureMode.ReflectAll |
                    edgeMapping[(byte)TextureMode.RotateRight | (TexureCoordinateIndex.RightBottom << 4)]];
            edgeMapping[(byte)(TextureMode.Flip | TextureMode.ReflectHorisontal) | (TexureCoordinateIndex.LeftTop << 4)] =
                edgeMapping[
                    (byte)TextureMode.ReflectHorisontal |
                    edgeMapping[(byte)TextureMode.Flip | (TexureCoordinateIndex.LeftTop << 4)]];
            edgeMapping[(byte)(TextureMode.Flip | TextureMode.ReflectHorisontal) | (TexureCoordinateIndex.RightTop << 4)] =
                edgeMapping[
                    (byte)TextureMode.ReflectHorisontal |
                    edgeMapping[(byte)TextureMode.Flip | (TexureCoordinateIndex.RightTop << 4)]];
            edgeMapping[(byte)(TextureMode.Flip | TextureMode.ReflectHorisontal) | (TexureCoordinateIndex.LeftBottom << 4)] =
                edgeMapping[
                    (byte)TextureMode.ReflectHorisontal |
                    edgeMapping[(byte)TextureMode.Flip | (TexureCoordinateIndex.LeftBottom << 4)]];
            edgeMapping[(byte)(TextureMode.Flip | TextureMode.ReflectHorisontal) | (TexureCoordinateIndex.RightBottom << 4)] =
                edgeMapping[
                    (byte)TextureMode.ReflectHorisontal |
                    edgeMapping[(byte)TextureMode.Flip | (TexureCoordinateIndex.RightBottom << 4)]];
            edgeMapping[(byte)(TextureMode.Flip | TextureMode.ReflectVertical) | (TexureCoordinateIndex.LeftTop << 4)] =
                edgeMapping[
                    (byte)TextureMode.ReflectVertical |
                    edgeMapping[(byte)TextureMode.Flip | (TexureCoordinateIndex.LeftTop << 4)]];
            edgeMapping[(byte)(TextureMode.Flip | TextureMode.ReflectVertical) | (TexureCoordinateIndex.RightTop << 4)] =
                edgeMapping[
                    (byte)TextureMode.ReflectVertical |
                    edgeMapping[(byte)TextureMode.Flip | (TexureCoordinateIndex.RightTop << 4)]];
            edgeMapping[(byte)(TextureMode.Flip | TextureMode.ReflectVertical) | (TexureCoordinateIndex.LeftBottom << 4)] =
                edgeMapping[
                    (byte)TextureMode.ReflectVertical |
                    edgeMapping[(byte)TextureMode.Flip | (TexureCoordinateIndex.LeftBottom << 4)]];
            edgeMapping[(byte)(TextureMode.Flip | TextureMode.ReflectVertical) | (TexureCoordinateIndex.RightBottom << 4)] =
                edgeMapping[
                    (byte)TextureMode.ReflectVertical |
                    edgeMapping[(byte)TextureMode.Flip | (TexureCoordinateIndex.RightBottom << 4)]];
            edgeMapping[(byte)(TextureMode.Flip | TextureMode.ReflectAll) | (TexureCoordinateIndex.LeftTop << 4)] =
                edgeMapping[
                    (byte)TextureMode.ReflectAll |
                    edgeMapping[(byte)TextureMode.Flip | (TexureCoordinateIndex.LeftTop << 4)]];
            edgeMapping[(byte)(TextureMode.Flip | TextureMode.ReflectAll) | (TexureCoordinateIndex.RightTop << 4)] =
                edgeMapping[
                    (byte)TextureMode.ReflectAll |
                    edgeMapping[(byte)TextureMode.Flip | (TexureCoordinateIndex.RightTop << 4)]];
            edgeMapping[(byte)(TextureMode.Flip | TextureMode.ReflectAll) | (TexureCoordinateIndex.LeftBottom << 4)] =
                edgeMapping[
                    (byte)TextureMode.ReflectAll |
                    edgeMapping[(byte)TextureMode.Flip | (TexureCoordinateIndex.LeftBottom << 4)]];
            edgeMapping[(byte)(TextureMode.Flip | TextureMode.ReflectAll) | (TexureCoordinateIndex.RightBottom << 4)] =
                edgeMapping[
                    (byte)TextureMode.ReflectAll |
                    edgeMapping[(byte)TextureMode.Flip | (TexureCoordinateIndex.RightBottom << 4)]];
        }
    }
}
