﻿using System;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace WarpedAbominationEngine.Content
{
    public class MegatexturePreloadData
    {
        public readonly string Name;
        public readonly MegaTexture MegaTexture;
        public ResourceLink Diffuse, Normal, Meta;

        public Point2 Resolution { get; private set; }

        public Color[] DiffuseMap, NormalMap, MetaMap;
        public Color DiffuseAvarage, NormalAvarage, MetaAvarage;

        public MegatexturePreloadData(MegaTexture megatexture, string name)
        {
            MegaTexture = megatexture;
            Name = name;
            Diffuse = null;
            Normal = null;
            Meta = null;
        }

        public MegatexturePreloadData(MegaTexture megatexture, string name, ResourceLink diffuse, ResourceLink normal, ResourceLink meta)
        {
            MegaTexture = megatexture;
            Name = name;
            Diffuse = diffuse;
            Normal = normal;
            Meta = meta;
        }

        public void Load()
        {
            if (Diffuse != null) {
                using (Stream stream = Diffuse.Read()) {
                    DiffuseMap = GetTextureData(Diffuse.Read(), out Point2 diffuseResolution, out DiffuseAvarage);
                    Resolution = diffuseResolution;
                }
            }
            if (Normal != null && MegaTexture.NormalEnabled) {
                using (Stream stream = Normal.Read()) {
                    NormalMap = GetTextureData(stream, out Point2 normalResolution, out NormalAvarage);
                    if (DiffuseMap != null && normalResolution != Resolution)
                        throw new MismatchedTextureResolution(Name, "normal", Resolution, normalResolution);
                    if (DiffuseMap == null)
                        Resolution = normalResolution;
                }
            }
            if (Meta != null && MegaTexture.MetaEnabled) {
                using (Stream stream = Meta.Read()) {
                    MetaMap = GetTextureData(stream, out Point2 metaResolution, out MetaAvarage);
                    if (DiffuseMap != null || NormalMap != null && metaResolution != Resolution)
                        throw new MismatchedTextureResolution(Name, "meta", Resolution, metaResolution);
                    if (DiffuseMap == null && NormalMap == null)
                        Resolution = metaResolution;
                }
            }
            if (DiffuseMap == null)
            {
                DiffuseMap = new Color[Resolution.X * Resolution.Y];
                for (int i = 0; i < DiffuseMap.Length; i++)
                    DiffuseMap[i] = MegaTexture.DefaultDiffuseColor;
                DiffuseAvarage = MegaTexture.DefaultDiffuseColor;
            }

            if (NormalMap == null && MegaTexture.NormalEnabled)
            {
                NormalMap = new Color[Resolution.X * Resolution.Y];
                for (int i = 0; i < NormalMap.Length; i++)
                    NormalMap[i] = MegaTexture.DefaultNormalColor;
                NormalAvarage = MegaTexture.DefaultNormalColor;
            }

            if (MetaMap == null && MegaTexture.MetaEnabled)
            {
                MetaMap = new Color[Resolution.X * Resolution.Y];
                for (int i = 0; i < MetaMap.Length; i++)
                    MetaMap[i] = MegaTexture.DefaultMetaColor;
                MetaAvarage = MegaTexture.DefaultMetaColor;
            }
        }

        private unsafe static Color[] GetTextureData(Stream fs, out Point2 size, out Color avarageColor)
        {
            Texture2D tmp = null;
            try {
                tmp = Texture2D.FromStream(XnaSystem.GraphicsDevice, fs);
                size = new Point2(tmp.Width, tmp.Height);
                Color[] result = new Color[size.X * size.Y];
                tmp.GetData<Color>(result);
                long r = 0, g = 0, b = 0, a = 0;
                for (int i = 0; i < result.Length; i++) {
                    r += result[i].R;
                    g += result[i].G;
                    b += result[i].B;
                    a += result[i].A;
                }
                avarageColor = new Color(
                    (int)(r / result.LongLength),
                    (int)(g / result.LongLength),
                    (int)(b / result.LongLength),
                    (int)(a / result.LongLength)
                );
                return result;
            } finally {
                if (tmp != null)
                    tmp.Dispose();
            }
        }
    }
}
