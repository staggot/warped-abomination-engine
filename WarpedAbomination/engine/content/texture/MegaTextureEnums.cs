﻿using System;
using System.Collections.Generic;


namespace WarpedAbominationEngine.Content
{
    public partial class MegaTexture
    {
        const byte EdgeExtract = 0xC0;
    }

    [Flags]
    public enum MegatextureFilter : byte
    {
        Nearest = 0,
        Linear = 1,
    }

    public enum MegaTextureBorder : byte
    {
        None = 0,
        Clamp = 1,
        Wrap = 2,
        Transparent = 3
    }

    public static class MegatextureExtentions
    {
        static readonly string[] borderNames = new string[] {
            "none",
            "clamp",
            "wrap",
            "transparent"
        };

        static readonly Dictionary<string, MegaTextureBorder> borderMap = new Dictionary<string, MegaTextureBorder> {
            { "none", MegaTextureBorder.None },
            { "clamp", MegaTextureBorder.Clamp },
            { "wrap", MegaTextureBorder.Wrap },
            { "transparent", MegaTextureBorder.Transparent }
        };

        public static bool IsExtendedBorder(this MegaTextureBorder border)
        {
            return border != MegaTextureBorder.None;
        }

        public static string GetName(this MegaTextureBorder border)
        {
            return borderNames[(int)border];
        }

        public static MegaTextureBorder ParseTextureBorder(this string text)
        {
            string lower = text.ToLower();
            if (borderMap.ContainsKey(lower))
                return borderMap[lower];
            return MegaTextureBorder.None;
        }

    }

    [Flags]
    public enum MegaTextureFlags : byte
    {
        None = 0,
        NormalEnabled = 1,
        MetaEnabled = 2
    }

    public struct TexureCoordinateIndex
    {
        public const byte
            LeftTop = 0,
            LeftBottom = 2,
            RightTop = 1,
            RightBottom = 3;
    }

    [Flags]
    public enum TextureMode : byte
    {
        Default = 0,
        RotateLeft = 1,
        RotateRight = 2,
        Flip = 3,
        ReflectHorisontal = 4,
        ReflectVertical = 8,
        ReflectAll = 12
    }

    public enum TextureFileType : byte
    {
        Diffuse,
        Normal,
        Meta
    }
}
