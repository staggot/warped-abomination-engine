﻿#pragma warning disable RECS0061 //For type endings

using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.IO;

namespace WarpedAbominationEngine.Content
{
    public partial class MegaTexture : IDisposable
    {
        public const int MaxMipmap = 4;

        public static readonly string NormalEnding = "_n.png";
        public static readonly string MetaEnding = "_m.png";
        public static readonly string DiffuseEnding = ".png";
        public static readonly string DefaultName = "_default";

        const int MaxTextures = 2048;
        const ushort EmptyId = 0x7fff;
        readonly MegatextureInfo[] texturesArray = new MegatextureInfo[MaxTextures];
        readonly Dictionary<string, ushort> textureMap = new Dictionary<string, ushort>();
        readonly MegaTextureFlags flags;
        readonly MegaTextureBorder border;
        readonly int mipmapLevels;
        readonly int allocationUnitSize = 8;
        readonly bool mipmapEnabled;
        bool[,] allocation;
        ushort lastId = 0;

        public readonly string Name;
        public Color DefaultMetaColor;
        public Color DefaultDiffuseColor;
        public Color DefaultNormalColor;

        public int BorderSize {
            get {
                if (border.IsExtendedBorder())
                    return 1 << MaxMipmap;
                else
                    return 0;
            }
        }

        public bool NormalEnabled {
            get { return (flags & MegaTextureFlags.NormalEnabled) != 0; }
        }

        public bool MetaEnabled {
            get { return (flags & MegaTextureFlags.MetaEnabled) != 0; }
        }

        public MegaTexture(
            string name,
            int resolution,
            bool mipmapEnabled,
            MegaTextureFlags flags,
            MegaTextureBorder border)
        {
            Name = name;
            this.Resolution = resolution;
            this.flags = flags;
            this.border = border;
            this.mipmapEnabled = mipmapEnabled;
            this.mipmapLevels = mipmapEnabled ? CalculateMipmapLevels() : 1;
            DefaultDiffuseColor = Color.White;
            DefaultNormalColor = new Color(128, 128, 255);
            DefaultMetaColor = Color.Black;
            Clear();
        }

        int CalculateMipmapLevels()
        {
            int levels = 1;
            while ((Resolution >> levels) > 1) {
                levels++;
            }
            return levels;
        }

        public Texture2D Diffuse { get; private set; }

        public Texture2D Normal { get; private set; }

        public Texture2D Meta { get; private set; }

        public MegatextureFragment this[string name, TextureMode mode] {
            get { return GetEdges(name, mode); }
        }

        public MegatextureFragment this[string name] {
            get { return GetEdges(name, TextureMode.Default); }
        }

        public MegatextureFragment this[ushort data] {
            get { return GetEdges(data); }
        }

        public void GetNameMode(ushort data, out string name, out TextureMode mode)
        {
            mode = (TextureMode)((data & 0x7800) >> 11);
            ushort index = (ushort)(data & 0x07ff);
            if (texturesArray[index].Id == EmptyId)
                name = DefaultName;
            else
                name = texturesArray[index].Name;
        }

        public ushort GetData(ushort id, TextureMode mode = TextureMode.Default)
        {
            return (ushort)((id & 0x07ff) | ((ushort)mode << 11));
        }

        public ushort GetData(string name, TextureMode mode = TextureMode.Default)
        {
            if (!textureMap.ContainsKey(name))
                name = DefaultName;
            return (ushort)(textureMap[name] | ((ushort)mode << 11));
        }

        public MegatextureFragment GetEdges(string name, TextureMode mode)
        {
            return GetEdges(GetData(name, mode));
        }

        public MegatextureFragment GetEdges(ushort data)
        {
            ushort index = (ushort)(data & 0x07ff);
            if (texturesArray[index].Id == EmptyId)
                index = textureMap[DefaultName];
            TextureMode mode = (TextureMode)((data & 0x7800) >> 11);
            return texturesArray[index].GetEdges(mode);
        }

        public Vector4 GetDefaultEdges(ushort data)
        {
            ushort index = (ushort)(data & 0x07ff);
            if (texturesArray[index].Id == EmptyId)
                index = textureMap[DefaultName];
            return texturesArray[index].TexCoords;
        }

        public Vector4 GetDefaultEdges(string name)
        {
            return GetDefaultEdges(GetData(name, TextureMode.Default));
        }

        private void Clear()
        {
            Dispose();

            Diffuse = new Texture2D(XnaSystem.GraphicsDevice, Resolution, Resolution, mipmapEnabled, SurfaceFormat.Color);
            if (NormalEnabled)
                Normal = new Texture2D(XnaSystem.GraphicsDevice, Resolution, Resolution, mipmapEnabled, SurfaceFormat.Color);
            if (MetaEnabled)
                Meta = new Texture2D(XnaSystem.GraphicsDevice, Resolution, Resolution, mipmapEnabled, SurfaceFormat.Color);
        }

        IEnumerable<MegatexturePreloadData> ListDirectory(ResourceStorage storage)
        {
            string[] files = storage.ListResources("");
            Dictionary<string, MegatexturePreloadData> resultMap = new Dictionary<string, MegatexturePreloadData>();
            foreach (string file in files) {
                string name;
                TextureFileType type;
                if (file.ToLower().EndsWith(NormalEnding)) {
                    name = file.Substring(0, file.Length - NormalEnding.Length);
                    type = TextureFileType.Normal;
                } else if (file.ToLower().EndsWith(MetaEnding)) {
                    name = file.Substring(0, file.Length - MetaEnding.Length);
                    type = TextureFileType.Meta;
                } else if (file.ToLower().EndsWith(DiffuseEnding)) {
                    name = file.Substring(0, file.Length - DiffuseEnding.Length);
                    type = TextureFileType.Diffuse;
                } else
                    continue;
                if (!resultMap.ContainsKey(name))
                    resultMap[name] = new MegatexturePreloadData(this, name);
                switch (type) {
                    case TextureFileType.Diffuse:
                        resultMap[name].Diffuse = new ResourceLink(storage, file);
                        break;
                    case TextureFileType.Meta:
                        resultMap[name].Meta = new ResourceLink(storage, file);
                        break;
                    case TextureFileType.Normal:
                        resultMap[name].Normal = new ResourceLink(storage, file);
                        break;
                }
            }
            return resultMap.Values;
        }

        public int Resolution { get; }

        Color[][] GetTextureData()
        {
            Color[][] result = new Color[mipmapLevels][];
            for (int i = 0; i < mipmapLevels; i++) {
                int layerResolution = Resolution >> i;
                result[i] = new Color[layerResolution * layerResolution];
            }
            return result;
        }

        Color[][] GetTextureData(Texture2D texture)
        {
            Color[][] result = GetTextureData();
            FillDataWithTexture(result, texture);
            return result;
        }

        void FillDataWithTexture(Color[][] data, Texture2D texture)
        {
            for (int i = 0; i < mipmapLevels; i++)
                texture.GetData<Color>(i, null, data[i], 0, data[i].Length);
        }

        void FillTextureWithData(Color[][] data, Texture2D texture)
        {
            for (int i = 0; i < mipmapLevels; i++)
                texture.SetData<Color>(i, null, data[i], 0, data[i].Length);
        }

        public void Load(ResourceStorage storage)
        {
            Color[][] diffuse = GetTextureData(Diffuse);
            Color[][] normal = null, meta = null;

            if (NormalEnabled)
                normal = GetTextureData(Normal);
            if (MetaEnabled)
                meta = GetTextureData(Meta);
            bool updated = false;
            foreach (MegatexturePreloadData preloadData in ListDirectory(storage)) {
                preloadData.Load();
                AddTexture(diffuse, normal, meta, preloadData);
                updated = true;
            }
            if (!updated)
                return;
            ExpandMipmap(diffuse);
            FillTextureWithData(diffuse, Diffuse);
            if (NormalEnabled) {
                ExpandMipmap(normal);
                FillTextureWithData(normal, Normal);
            }
            if (MetaEnabled) {
                ExpandMipmap(meta);
                FillTextureWithData(meta, Meta);
            }
        }

        private ushort AllocateId()
        {
            ushort result = lastId;
            if (result >= MaxTextures)
                throw new TextureIdsDepleted(this, Resolution);
            lastId++;
            return result;
        }

        private void ExpandMipmap(Color[][] colorData)
        {
            for (int i = 1; i < mipmapLevels; i++) {
                int layerResolution = Resolution >> i;
                for (int t = 0; t < layerResolution * layerResolution; t++) {
                    int x = t % layerResolution;
                    int y = t / layerResolution;
                    Color c00 = colorData[i - 1][(x * 2 + 0) * layerResolution * 2 + (y * 2 + 0)];
                    Color c01 = colorData[i - 1][(x * 2 + 1) * layerResolution * 2 + (y * 2 + 0)];
                    Color c10 = colorData[i - 1][(x * 2 + 0) * layerResolution * 2 + (y * 2 + 1)];
                    Color c11 = colorData[i - 1][(x * 2 + 1) * layerResolution * 2 + (y * 2 + 1)];
                    colorData[i][t] = new Color(
                        (c00.R + c01.R + c10.R + c11.R) / 4,
                        (c00.G + c01.G + c10.G + c11.G) / 4,
                        (c00.B + c01.B + c10.B + c11.B) / 4,
                        (c00.A + c01.A + c10.A + c11.A) / 4
                        );
                }
            }
        }

        private void AddTexture(
            Color[][] targetDiffuse,
            Color[][] targetNormal,
            Color[][] targetMeta,
            MegatexturePreloadData data)
        {
            Point2 allocationResolution = data.Resolution + new Point2(BorderSize * 2, BorderSize * 2);
            ushort id = AllocateId();
            Point2 allocationStart = AllocatePosition(allocationResolution);
            Point4 pixelRect = new Point4(
                allocationStart.X + BorderSize,
                allocationStart.Y + BorderSize,
                data.Resolution.X,
                data.Resolution.Y);

            Copy(targetDiffuse[0], data.DiffuseMap, new Point2(pixelRect.X, pixelRect.Y), data.Resolution);
            if (NormalEnabled)
                Copy(targetNormal[0], data.NormalMap, new Point2(pixelRect.X, pixelRect.Y), data.Resolution);
            if (MetaEnabled)
                Copy(targetMeta[0], data.MetaMap, new Point2(pixelRect.X, pixelRect.Y), data.Resolution);
            texturesArray[id] = new MegatextureInfo() {

                PixelRectangle = pixelRect,
                TexCoords = new Vector4(
                     (float)pixelRect.X / (float)Resolution,
                     (float)pixelRect.Y / (float)Resolution,
                     (float)pixelRect.Z / (float)Resolution,
                     (float)pixelRect.W / (float)Resolution),
                Id = id,
                Color = data.DiffuseAvarage,
                Name = data.Name
            };
            textureMap[data.Name] = id;
        }

        void Copy(Color[] target, Color[] source, Point2 targetStart, Point2 sourceResolution)
        {
            Point2 start = new Point2(-BorderSize, -BorderSize);
            Point2 end = sourceResolution + new Point2(BorderSize * 2, BorderSize * 2);

            for (int y = start.Y; y < end.Y; y++)
                for (int x = start.X; x < end.X; x++) {
                    int _x = x;
                    int _y = y;
                    Color color = Color.TransparentBlack;
                    switch (border) {
                        case MegaTextureBorder.Clamp:
                            if (_x < 0)
                                _x = 0;
                            if (_y < 0)
                                _y = 0;
                            if (_x >= sourceResolution.X)
                                _x = sourceResolution.X - 1;
                            if (_y >= sourceResolution.Y)
                                _y = sourceResolution.Y - 1;
                            color = source[_y * sourceResolution.X + _x];
                            break;
                        case MegaTextureBorder.None:
                            color = source[_y * sourceResolution.X + _x];
                            break;
                        case MegaTextureBorder.Transparent:
                            if (_x < 0 || _y < 0 || _x >= sourceResolution.X || _y >= sourceResolution.Y)
                                color = Color.TransparentBlack;
                            else
                                color = source[_y * sourceResolution.X + _x];
                            break;
                        case MegaTextureBorder.Wrap:
                            if (_x < 0)
                                _x += sourceResolution.X;
                            if (_y < 0)
                                _y += sourceResolution.Y;
                            if (_x >= sourceResolution.X)
                                _x -= sourceResolution.X;
                            if (_y >= sourceResolution.Y)
                                _y -= sourceResolution.Y;
                            color = source[_y * sourceResolution.X + _x];
                            break;
                    }
                    target[(targetStart.Y + _y) * Resolution + targetStart.X + _x] = color;
                }
        }

        private Point2 AllocatePosition(Point2 size)
        {
            int aW = size.X / allocationUnitSize;
            if (size.X % allocationUnitSize != 0)
                aW += 1;

            int aH = size.Y / allocationUnitSize;
            if (size.Y % allocationUnitSize != 0)
                aH += 1;

            for (int y = 0; y < Resolution / allocationUnitSize - aH; y++)
                for (int x = 0; x < Resolution / allocationUnitSize - aW; x++) {
                    bool good = true;
                    for (int _y = 0; _y < aH && good; _y++)
                        for (int _x = 0; _x < aW && good; _x++) {
                            if (allocation[x + _x, y + _y])
                                good = false;
                        }
                    if (good) {
                        for (int _y = 0; _y < aH; _y++)
                            for (int _x = 0; _x < aW; _x++)
                                allocation[x + _x, y + _y] = true;
                        return new Point2(x * allocationUnitSize, y * allocationUnitSize);
                    }
                }
            throw new CantAllocateMegatextureFragment(this, size);
        }

        public void Dispose()
        {
            allocation = new bool[Resolution / allocationUnitSize, Resolution / allocationUnitSize];
            if (Meta != null)
                Meta.Dispose();
            if (Normal != null)
                Normal.Dispose();
            if (Diffuse != null)
                Diffuse.Dispose();
            for (int i = 0; i < texturesArray.Length; i++)
                texturesArray[i] = new MegatextureInfo() {
                    Id = EmptyId
                };
            textureMap.Clear();
        }
    }
}
