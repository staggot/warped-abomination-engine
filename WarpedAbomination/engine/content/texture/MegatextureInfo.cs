﻿using System;
using Microsoft.Xna.Framework;

namespace WarpedAbominationEngine.Content
{
    public struct MegatextureInfo
    {
        public ushort Id;
        public string Name;
        public Point4 PixelRectangle;
        public Vector4 TexCoords;
        public Color Color;

        public MegatextureFragment GetEdges(TextureMode mode)
        {
            return new MegatextureFragment(TexCoords, mode);
        }
    }
}
