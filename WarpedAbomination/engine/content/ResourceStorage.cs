﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace WarpedAbominationEngine.Content
{
    public abstract class ResourceStorage
    {
        public abstract Stream Read(string path);
        public abstract Stream Write(string path);
        public abstract string[] ListResources(string path);
        public abstract bool IsResourceExists(string path);
        public abstract bool IsDirectoryExists(string path);
        public abstract void CreateDirectory(string path);


        public Stream Create(string path)
        {
            string[] splittedPath = SplitPath(path);
            string currentPath = "";
            for (int i = 0; i < splittedPath.Length; i++) {
                string name = splittedPath[i];
                currentPath = CombinePath(currentPath, name);
                if (i == splittedPath.Length - 1)
                    return Write(path);
                if (!IsDirectoryExists(currentPath))
                    CreateDirectory(currentPath);
            }
            return null;
        }

        public void EnsureDirectoryExists(string path)
        {
            string currentPath = "";
            foreach (string name in SplitPath(path)) {
                currentPath = CombinePath(currentPath, name);
                if (!IsDirectoryExists(currentPath))
                    CreateDirectory(currentPath);
            }
        }

        public static string[] SplitPath(string path)
        {
            List<string> result = new List<string>();
            foreach (string line in path.Split
                (new char[] {
                    Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar
                }, StringSplitOptions.RemoveEmptyEntries)) {
                result.Add(line);
            }
            return result.ToArray();
        }

        public static string CombinePath(params string[] path)
        {
            StringBuilder result = new StringBuilder();
            if (path.Length == 0)
                return "";
            if (path.Length == 1)
                return path[0];
            int count = 0;
            for (int i = 0; i < path.Length; i++) {
                foreach (string name in SplitPath(path[i])) {
                    if (count != 0)
                        result.Append(Path.DirectorySeparatorChar);
                    result.Append(name);
                    count++;
                }
            }
            return result.ToString();
        }
    }

    public class ResourceSubStorage : ResourceStorage
    {
        readonly ResourceStorage storage;
        public string Root { get; private set; }

        public ResourceSubStorage(ResourceStorage storage, string path)
        {
            storage.EnsureDirectoryExists(path);
            this.storage = storage;
            Root = path;
        }

        public override void CreateDirectory(string path)
        {
            storage.CreateDirectory(CombinePath(Root, path));
        }

        public override bool IsDirectoryExists(string path)
        {
            return storage.IsDirectoryExists(CombinePath(Root, path));
        }

        public override bool IsResourceExists(string path)
        {
            return storage.IsResourceExists(CombinePath(Root, path));
        }

        public override string[] ListResources(string path)
        {
            return storage.ListResources(CombinePath(Root, path));
        }

        public override Stream Read(string path)
        {
            return Read(CombinePath(Root, path));
        }

        public override Stream Write(string path)
        {
            return Write(CombinePath(Root, path));
        }
    }

    public class ResourceLink
    {
        public readonly ResourceStorage Storage;
        public readonly string Path;

        public ResourceLink(ResourceStorage storage, string path)
        {
            Storage = storage;
            Path = path;
        }

        public Stream Read()
        {
            return Storage.Read(Path);
        }

        public Stream Write()
        {
            return Storage.Write(Path);
        }

        public Stream Create()
        {
            return Storage.Create(Path);
        }
    }
}
