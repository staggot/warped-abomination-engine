﻿using System;
namespace WarpedAbominationEngine.Content
{
    public enum EngineTextureTarget : byte
    {
        UI = 0,
        Terrain = 1,
        Portal = 2,
        Sprite = 3,
        Mesh = 4,
        Decal = 5
    }

    public static class EngineTextureTypeExtention 
    {
        readonly static string[] nameMap =
        {
            "ui",
            "terrain",
            "portal",
            "sprite",
            "mesh",
            "decal"
        };

        public static string GetName(this EngineTextureTarget textureTarget)
        {
            return nameMap[(int)textureTarget];
        }

        public static bool IsTile(this EngineTextureTarget textureTarget)
        {
            return textureTarget == EngineTextureTarget.Terrain ||
                textureTarget == EngineTextureTarget.Mesh ||
                textureTarget == EngineTextureTarget.Portal ||
                textureTarget == EngineTextureTarget.Decal;
        } 
    }
}
