﻿using System;
using System.IO;
using System.IO.IsolatedStorage;
using System.Collections.Generic;
using WarpedAbominationEngine.Configuration;
using Microsoft.Xna.Framework;

namespace WarpedAbominationEngine.Content
{
    public static partial class AbominationSystem
    {
        public static string ApplicationName = "abomination";

        public const string PhysicsSection = "physics";

        public const string ResolutionParameter = "resolution";
        public const string MipmapEnabledParameter = "mipmap_enabled";
        public const string MetaEnabledParameter = "meta_enabled";
        public const string NormalEnabledParameter = "normal_enabled";
        public const string BorderParameter = "border";

        public const string TexturesDirectoryName = "data/megatex";
        public const string ShadersDirectoryName = "data/fx";
        public const string MeshDirectoryName = "data/mesh";
        public const string SoundDirectoryName = "data/sound";
        public const string MusicDirectoryName = "data/music";
        public const string LevelsDirectoryName = "data/levels";
        public const string ConfigFileName = "conf.ini";
        public const string PhysicsTableFilename = "collision.bin";

        public const int TotalTextureTypes = 6;

        public const float PhysicsEpsilon = 0.002f;
        public const float GraphicsEpsilon = 0.001f;

        public const int RegionThings = 4096 * 4;
        public const int RegionBodies = 4096;

        public const int SectorExpirationTicks = 600;
        public const int RegionExpirationTicks = 600;

        public static ResourceStorage EngineStorage { get; private set; }
        public static ResourceStorage UserStorage { get; private set; }

        public readonly static ResourceStorage TextureEngineStorage, TextureUserStorage;
        public readonly static ResourceStorage LevelEngineStorage, LevelUserStorage;
        public readonly static ResourceStorage MeshEngineStorage, MeshUserStorage;
        public readonly static ResourceStorage ShaderEngineStorage, ShaderUserStorage;
        public readonly static ResourceStorage SoundEngineStorage, SoundUserStorage;
        public readonly static ResourceStorage MusicEngineStorage, MusicUserStorage;

        public static EngineConfig Configuration = new EngineConfig();

        internal static ConfigSection GetConfigSection(EngineTextureTarget target)
        {
            string name = string.Format("textures.{0}", target.GetName());
            return Configuration[name] ?? Configuration.CreateSection(name);
        }

        internal static Stream OpenPhysicsTable()
        {
            return EngineStorage.Read(PhysicsTableFilename);
        }

        static AbominationSystem()
        {
            InitPlatform();

            TextureEngineStorage = new ResourceSubStorage(EngineStorage, TexturesDirectoryName);
            LevelEngineStorage = new ResourceSubStorage(EngineStorage, LevelsDirectoryName);
            ShaderEngineStorage = new ResourceSubStorage(EngineStorage, ShadersDirectoryName);
            MeshEngineStorage = new ResourceSubStorage(EngineStorage, MeshDirectoryName);
            SoundEngineStorage = new ResourceSubStorage(EngineStorage, SoundDirectoryName);
            MusicEngineStorage = new ResourceSubStorage(EngineStorage, MusicDirectoryName);

            if(UserStorage != null) {
                TextureUserStorage = new ResourceSubStorage(UserStorage, TexturesDirectoryName);
                LevelUserStorage = new ResourceSubStorage(UserStorage, LevelsDirectoryName);
                ShaderUserStorage = new ResourceSubStorage(UserStorage, ShadersDirectoryName);
                MeshUserStorage = new ResourceSubStorage(UserStorage, MeshDirectoryName);
                SoundUserStorage = new ResourceSubStorage(UserStorage, SoundDirectoryName);
                MusicUserStorage = new ResourceSubStorage(UserStorage, MusicDirectoryName);
            }

            #region init_configuration

            for (int index = 0; index < TotalTextureTypes; index++) {
                EngineTextureTarget textureTarget = (EngineTextureTarget)index;
                ConfigSection textureSection = GetConfigSection(textureTarget);
                bool isUi = textureTarget == EngineTextureTarget.UI;
                bool isTile = textureTarget.IsTile();

                textureSection.AddVariable(ResolutionParameter, WarpTypes.UShort, isUi ? 2048 : 4096);
                textureSection.AddVariable(NormalEnabledParameter, WarpTypes.Bool, isUi ? false : true);
                textureSection.AddVariable(MetaEnabledParameter, WarpTypes.Bool, isUi ? false : true);
                textureSection.AddVariable(BorderParameter, WarpTypes.String,
                    isTile ? MegaTextureBorder.Wrap.GetName() : MegaTextureBorder.Transparent.GetName());

            }

            #endregion
        }
    }
}
