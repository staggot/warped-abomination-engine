﻿using System;
namespace WarpedAbominationEngine
{
    public struct BranchSectorView
    {
        public WarpCamera Camera;
        public SectorView Sector;
    }
}
