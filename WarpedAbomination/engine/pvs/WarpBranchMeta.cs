﻿using System;
namespace WarpedAbominationEngine
{
    public struct WarpBranchMeta
    {
        internal WarpBranch ParentWarp;
        internal Portal Portal;
        internal SectorView RootSector;
        internal WarpCamera CameraView;
        internal int Deep;
        internal byte Flags;
        internal int Stencil;
    }
}
