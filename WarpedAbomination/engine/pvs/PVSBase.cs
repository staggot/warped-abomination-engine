﻿using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace WarpedAbominationEngine
{
    public abstract class PVSBase
    {
        const int DefaultMaxBranches = 255;
        const int DefaultMaxSectorStackSize = 512;
        internal const int DefaultMaxDeep = 16;
        internal WarpBranch[] branches;

        readonly WarpBranchMeta[] branchStack;
        readonly SectorView[] sectorStack;

        protected int maxBranches;
        protected int maxDeep;

        protected int branchIndex = 0;

        protected int branchStackStart = 0;
        protected int branchStackEnd = 0;

        protected int sectorStackStart = 0;
        protected int sectorStackEnd = 0;

        protected BranchFrontToBackEnumerable branchFrontToBack;
        protected BranchBackToFrontEnumerable branchBackToFront;

        protected SectorFrontToBackEnumarable sectorFrontToBack;
        protected SectorBackToFrontEnumarable sectorBackToFront;

        public BranchBackToFrontEnumerable ChunkBackToFront {
            get { return branchBackToFront; }
        }

        public BranchFrontToBackEnumerable ChunkFrontToBack {
            get { return branchFrontToBack; }
        }

        public SectorFrontToBackEnumarable SectorBackToFront {
            get { return sectorFrontToBack; }
        }

        public SectorBackToFrontEnumarable SectorFrontToBack {
            get { return sectorBackToFront; }
        }

        protected abstract bool PassWarp(WarpCamera camera, Portal portal);
        protected abstract bool PassSector(WarpCamera camera, SectorView sector);

        internal PVSBase() : this(DefaultMaxBranches, DefaultMaxSectorStackSize, DefaultMaxDeep)
        {

        }

        internal PVSBase(int maxBranches, int maxSectorStackSize, int maxDeep)
        {
            this.maxBranches = maxBranches;
            this.maxDeep = maxDeep;
            this.branchBackToFront = new BranchBackToFrontEnumerable(this);
            this.branchFrontToBack = new BranchFrontToBackEnumerable(this);
            this.sectorBackToFront = new SectorBackToFrontEnumarable(this);
            this.sectorFrontToBack = new SectorFrontToBackEnumarable(this);
            branches = new WarpBranch[maxBranches + 1];
            for (int i = 0; i <= maxBranches; i++)
                branches[i] = new WarpBranch(this, maxDeep);
            branchStack = new WarpBranchMeta[maxBranches * 4];
            sectorStack = new SectorView[maxSectorStackSize];
        }

        void PushWarp(WarpBranchMeta packet)
        {
            branchStack[branchStackEnd++] = packet;
        }

        internal int ChunkCount {
            get { return branchIndex; }
        }

        WarpBranchMeta PopWarp()
        {
            WarpBranchMeta result = branchStack[branchStackStart];
            branchStack[branchStackStart].RootSector.Sector = null;
            branchStack[branchStackStart].ParentWarp = null;
            branchStack[branchStackStart].Portal = null;
            branchStackStart++;
            return result;
        }

        int WarpStackSize {
            get { return branchStackEnd - branchStackStart; }
        }

        void PushSector(SectorView sectorView)
        {
            sectorStack[sectorStackEnd] = sectorView;
            sectorStackEnd++;
        }


        SectorView PopSector()
        {
            SectorView result = sectorStack[sectorStackStart];
            sectorStack[sectorStackStart].Sector = null;
            sectorStackStart++;
            return result;
        }

        int SectorStackSize {
            get { return sectorStackEnd - sectorStackStart; }
        }

        internal void Fill(Sector rootSector, WarpCamera rootCamera)
        {
            Clear();
            PushWarp(new WarpBranchMeta() {
                Portal = null,
                Deep = 0,
                Flags = 0,
                ParentWarp = null,
                Stencil = 0,
                RootSector = new SectorView() {
                    WorldPosition = Vector3.Zero,
                    Sector = rootSector,
                    Deep = 0
                }
            });

            while (WarpStackSize > 0) {
                WarpBranchMeta chunkMeta = PopWarp();
                if (branchIndex > maxBranches)
                    continue;
                WarpBranch headBranch = branches[branchIndex++];
                headBranch.PortalFlags = chunkMeta.Flags;
                headBranch.parentBranch = chunkMeta.ParentWarp;
                headBranch.StartDeep = chunkMeta.Deep;
                headBranch.EntrancePortal = chunkMeta.Portal;
                headBranch.RootSector = chunkMeta.RootSector;
                headBranch.Stencil = chunkMeta.Stencil;
                FillBranch(headBranch);
            }
        }

        public void FillBranch(WarpBranch branch)
        {
            if (!PassSector(branch.BranchCamera, branch.RootSector))
                return;
            branch.AddSector(branch.RootSector);
            PushSector(branch.RootSector);
            SectorView rootSector;
            while (SectorStackSize > 0) {
                rootSector = PopSector();
                if (rootSector.Deep > maxDeep)
                    return;
                int deep = rootSector.Deep + 1;

                foreach (Portal portal in rootSector.Sector.Portals) {
                    if (!PVSMath.PortalCheck(-rootSector.WorldPosition, portal, branch.BranchCamera.Distance, branch.PortalFlags, out byte newFlags))
                        continue;
                    SectorView nextSector = new SectorView() {
                        Sector = portal.OutSector,
                        WorldPosition = portal.TransformPositionFrom(rootSector.WorldPosition),
                        Deep = deep,
                    };
                    if (portal.Natural) {

                        if (!PassSector(branch.BranchCamera, nextSector))
                            continue;
                        if (branch.AddSector(nextSector))
                            PushSector(nextSector);
                    } else {
                        WarpCamera camera = branch.BranchCamera;
                        camera.CameraPosition -= rootSector.WorldPosition;
                        rootSector.WorldPosition = Vector3.Zero;
                        camera = camera.Portal(portal);
                        if (!PassWarp(camera, portal.OutPortal))
                            continue;
                        WarpBranchMeta packet = new WarpBranchMeta() {
                            Deep = deep,
                            Flags = newFlags,
                            ParentWarp = branch,
                            Stencil = branch.Stencil + 1,
                            Portal = portal.OutPortal,
                            RootSector = nextSector,
                            CameraView = camera
                        };
                        PushWarp(packet);
                    }
                }
            }
        }

        internal void Clear()
        {
            branchIndex = 0;
            branchStackEnd = 0;
            branchStackStart = 0;
            sectorStackEnd = 0;
            sectorStackStart = 0;
            for (int i = 0; i < branches.Length; i++)
                branches[i].Clear();
        }
    }
}
