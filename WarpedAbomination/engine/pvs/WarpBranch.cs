﻿using System;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace WarpedAbominationEngine
{

    public class WarpBranch
    {
        readonly PVSBase pvs;
        internal PVSectorBucket[] Buckets;
        readonly Dictionary<Sector, Point2> branchSectorIndex;
        internal WarpCamera BranchCamera;
        internal int StartDeep;
        readonly int maxDeep;
        internal SectorView RootSector;
        internal WarpBranch parentBranch;
        internal byte PortalFlags;
        internal Portal EntrancePortal;
        internal int Stencil;
        internal int Sectors = 0;

        protected ChunkSectorFrontToBackEnumerable chunkSectorFrontToBack;
        protected ChunkSectorBackToFrontEnumerable chunkSectorBackToFront;

        public ChunkSectorBackToFrontEnumerable SectorBackToFront
        {
            get { return chunkSectorBackToFront; }
        }

        public ChunkSectorFrontToBackEnumerable SectorFrontToBack
        {
            get { return chunkSectorFrontToBack; }
        }

        Point2 Indexer;

        internal WarpBranch(PVSBase pvs, int maxDeep)
        {
            this.pvs = pvs;
            this.maxDeep = maxDeep;
            Buckets = new PVSectorBucket[maxDeep];
            for (int i = 0; i < Buckets.Length; i++)
                Buckets[i] = new PVSectorBucket(4)
                {
                    Sector = new SectorView[1],
                    Count = 0
                };
            branchSectorIndex = new Dictionary<Sector, Point2>();
            StartDeep = 0;
            chunkSectorBackToFront = new ChunkSectorBackToFrontEnumerable(this);
            chunkSectorFrontToBack = new ChunkSectorFrontToBackEnumerable(this);
        }

        internal bool IsEmpty
        {
            get
            {
                return Indexer.X == 0 && Indexer.Y >= Buckets[Indexer.X].Count || Sectors == 0;
            }
        }

        internal bool AddSector(SectorView sectorView)
        {
            if (sectorView.Deep >= maxDeep)
                return false;
            int deep = sectorView.Deep - StartDeep;
            if (branchSectorIndex.ContainsKey(sectorView.Sector))
            {
                Point2 oldPointer = branchSectorIndex[sectorView.Sector];
                if (oldPointer.X > deep)
                    Buckets[oldPointer.X].RemoveAt(oldPointer.Y);
                else
                    return false;
            }

            int bucketIndex = Buckets[deep].Add(sectorView);
            branchSectorIndex[sectorView.Sector] = new Point2(deep, bucketIndex);
            Sectors++;
            return true;
        }

        internal void Clear()
        {
            for (int i = 0; i < Buckets.Length; i++)
                Buckets[i].Clear();
            branchSectorIndex.Clear();
            RootSector.Sector = null;
            parentBranch = null;
            EntrancePortal = null;
            Sectors = 0;
        }
    }
}
