﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace WarpedAbominationEngine 
{
    public class BranchBackToFrontEnumerable : IEnumerable<WarpBranch>
    {
        PVSBase pVs;

        public BranchBackToFrontEnumerable(PVSBase pvs)
        {
            this.pVs = pvs;
        }

        public IEnumerator<WarpBranch> GetEnumerator()
        {
            return new BranchEnumerator(this.pVs, true);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    public class BranchFrontToBackEnumerable : IEnumerable<WarpBranch>
    {
        PVSBase pVs;

        public BranchFrontToBackEnumerable(PVSBase pvs)
        {
            this.pVs = pvs;
        }

        public IEnumerator<WarpBranch> GetEnumerator()
        {
            return new BranchEnumerator(this.pVs, false);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
