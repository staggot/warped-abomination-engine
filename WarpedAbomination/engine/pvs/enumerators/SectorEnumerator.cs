﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace WarpedAbominationEngine
{
    public class SectorEnumerator : IEnumerator<SectorView>
    {
        PVSBase pVS;
        bool backward;
        BranchEnumerator branchEnumerator;
        BranchSectorEnumerator sectorEnumerator = null;

        public SectorEnumerator(PVSBase pvs, bool backward = true)
        {
            this.pVS = pvs;
            this.backward = backward;
            Reset();
        }

        public SectorView Current => sectorEnumerator.Current;

        object IEnumerator.Current => Current;

        public void Dispose()
        {
            Reset();
        }

        public bool MoveNext()
        {
            while (true)
            {
                if (sectorEnumerator == null)
                {
                    if (branchEnumerator.MoveNext())
                        sectorEnumerator = new BranchSectorEnumerator(branchEnumerator.Current, backward);
                    else
                        return false;
                }
                if (sectorEnumerator.MoveNext())
                    return true;
                else
                    sectorEnumerator = null;
            }
        }

        public void Reset()
        {
            sectorEnumerator = null;
            branchEnumerator = new BranchEnumerator(pVS, backward);
        }
    }
}
