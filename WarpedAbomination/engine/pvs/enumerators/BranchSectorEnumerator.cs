﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace WarpedAbominationEngine
{
    public class BranchSectorEnumerator : IEnumerator<SectorView>
    {
        WarpBranch branch;
        Point2 indexer;
        bool backward;

        public BranchSectorEnumerator(WarpBranch chunk, bool backward = true)
        {
            this.branch = chunk;
            this.backward = backward;
            Reset();
        }

        public SectorView Current => branch.Buckets[indexer.X].Sector[indexer.Y];

        object IEnumerator.Current => Current;

        public void Dispose()
        {
            Reset();
        }

        public bool MoveNext()
        {
            if (
                (
                    indexer.X == 0 && backward ||
                    !backward && indexer.X == branch.Buckets.Length - 1
                ) && indexer.Y >= branch.Buckets[indexer.X].Count || branch.Sectors == 0
            )
                return false;
            while (branch.Buckets[indexer.X].Count <= indexer.Y + 1)
            {
                if(backward)
                    indexer = new Point2(indexer.X - 1, -1);
                else
                    indexer = new Point2(indexer.X + 1, -1);
                if (indexer.X < 0)
                    return false;
            }
            indexer.Y++;
            return true;
        }

        public void Reset()
        {
            if (backward)
            {
                indexer = new Point2(branch.Buckets.Length - 1, -1);
            }
            else
            {
                indexer = new Point2(0, -1);
            }
        }
    }

}
