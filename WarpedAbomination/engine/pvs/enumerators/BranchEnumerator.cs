﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace WarpedAbominationEngine
{
    public class BranchEnumerator : IEnumerator<WarpBranch>
    {
        PVSBase pVS;
        int index = -1;
        bool backward;
        public BranchEnumerator(PVSBase pvs, bool backward = true)
        {
            this.pVS = pvs;
            this.index = 0;
            this.backward = backward;
            Reset();
        }

        public WarpBranch Current
        {
            get { return pVS.branches[index]; }
        }

        object IEnumerator.Current => Current;

        public void Dispose()
        {
        }

        public bool MoveNext()
        {
            if (backward)
            {
                if (index > 0)
                {
                    index--;
                    return true;
                }
            }
            else
            {
                if(index < pVS.ChunkCount - 1)
                {
                    index++;
                    return true;
                }
            }
            return false;
        }

        public void Reset()
        {
            if (backward)
                index = pVS.ChunkCount;
            else
                index = -1;
        }
    }
}
