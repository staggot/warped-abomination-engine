﻿using System;
using System.Collections;
using System.Collections.Generic;
namespace WarpedAbominationEngine
{
    public class SectorFrontToBackEnumarable : IEnumerable<SectorView>
    {
        PVSBase pvs;

        public SectorFrontToBackEnumarable(PVSBase pvs)
        {
            this.pvs = pvs;
        }

        public IEnumerator<SectorView> GetEnumerator()
        {
            return new SectorEnumerator(this.pvs, false);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    public class SectorBackToFrontEnumarable : IEnumerable<SectorView>
    {
        PVSBase pvs;

        public SectorBackToFrontEnumarable(PVSBase pvs)
        {
            this.pvs = pvs;
        }

        public IEnumerator<SectorView> GetEnumerator()
        {
            return new SectorEnumerator(this.pvs, true);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    public class ChunkSectorBackToFrontEnumerable : IEnumerable<SectorView>
    {
        WarpBranch chunk;

        public ChunkSectorBackToFrontEnumerable(WarpBranch chunk)
        {
            this.chunk = chunk;
        }

        public IEnumerator<SectorView> GetEnumerator()
        {
            return new BranchSectorEnumerator(this.chunk, true);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    public class ChunkSectorFrontToBackEnumerable : IEnumerable<SectorView>
    {
        WarpBranch chunk;

        public ChunkSectorFrontToBackEnumerable(WarpBranch chunk)
        {
            this.chunk = chunk;
        }

        public IEnumerator<SectorView> GetEnumerator()
        {
            return new BranchSectorEnumerator(this.chunk, false);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
