﻿using System;
using Microsoft.Xna.Framework;

namespace WarpedAbominationEngine
{
    public struct SectorView
    {
        internal int Deep;
        internal Sector Sector;
        internal Vector3 WorldPosition;

        internal Matrix WorldMatrix => Matrix.CreateWorld(WorldPosition, Vector3.Forward, Vector3.Up);

        public override int GetHashCode()
        {
            return Sector.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (!(obj is SectorView))
                return false;
            SectorView b = (SectorView)obj;
            return b.Sector == Sector;
        }

        public static bool operator ==(SectorView a, SectorView b)
        {
            return a.Sector == b.Sector;
        }

        public static bool operator !=(SectorView a, SectorView b)
        {
            return a.Sector != b.Sector;
        }

        public void Clear()
        {
            Deep = 0;
            Sector = null;
            WorldPosition = Vector3.Zero;
        }
    }
}
