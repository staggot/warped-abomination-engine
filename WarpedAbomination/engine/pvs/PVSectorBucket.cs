﻿using System;

namespace WarpedAbominationEngine
{
    struct PVSectorBucket
    {
        const int TicksToReallocate = 3000;

        internal SectorView[] Sector;
        internal int Count;
        int capacity;
        int reallocateCountDown;

        internal PVSectorBucket(int initialCapacity = 4)
        {
            reallocateCountDown = TicksToReallocate;
            capacity = initialCapacity;
            Count = 0;
            Sector = new SectorView[capacity];
        }

        internal SectorView this[int i]
        {
            get { return Sector[i]; }
            set
            {
                while(capacity <= i)
                    Reallocate(capacity * 2);
                Sector[i] = value;
            }
        }

        internal void RemoveAt(int i)
        {
            if (Count > 1)
                Sector[i] = Sector[Count - 1];
            else
                Sector[i].Clear();
            Count--;
        }

        internal int Add(SectorView view)
        {
            int index = Count;
            Count++;
            this[Count] = view;
            return index;
        }

        void Reallocate(int newCapacity)
        {
            SectorView[] newArray = new SectorView[newCapacity];
            for (int i = 0; i < Sector.Length; i++)
                newArray[i] = Sector[i];
            Sector = newArray;
            capacity = newCapacity;
        }

        internal void Clear()
        {
            if (Count * 2 < capacity && capacity > 4)
                reallocateCountDown--;
            else
                reallocateCountDown = TicksToReallocate;
            for (int i = 0; i < Count; i++)
                Sector[i].Clear();
            Count = 0;
            if (reallocateCountDown <= 0)
            {
                Reallocate(capacity / 2);
                reallocateCountDown = TicksToReallocate;
            }
        }
    }
}
