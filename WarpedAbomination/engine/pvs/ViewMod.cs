﻿using System;
namespace WarpedAbominationEngine
{
    [Flags]
    public enum ViewModFlag : byte
    {
        None = 0,
        Mirror = 1,
    }
    
    public struct ViewMod
    {
        public ViewModFlag Flags;

        public static ViewMod operator+(ViewMod a, ViewMod b)
        {
            return new ViewMod()
            {
                Flags = a.Flags | b.Flags
            };
        }
    }
}
