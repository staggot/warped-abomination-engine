﻿using System;
using Microsoft.Xna.Framework;

namespace WarpedAbominationEngine
{
    public struct WarpCamera
    {
        internal Vector3 CameraPosition;
        internal Vector3 CameraDirection;
        internal Vector3 CameraUp;
        internal Vector3 CameraSpriteFacing;
        internal ViewMod Modifier;
        internal float Distance;

        internal WarpCamera Portal(Portal portal)
        {
            return new WarpCamera()
            {
                CameraPosition = portal.TransformPositionTo(this.CameraPosition),
                CameraDirection = portal.TransformDirectionTo(this.CameraDirection),
                CameraUp = portal.TransformDirectionTo(this.CameraUp),
                CameraSpriteFacing = portal.TransformDirectionTo(this.CameraSpriteFacing),
                Modifier = this.Modifier + portal.ViewMod,
                Distance = Distance
            };
        }

        internal Matrix ViewMatrix {
            get { return Matrix.CreateLookAt(CameraPosition, CameraPosition + CameraDirection, CameraUp); }
        }
    }
}
