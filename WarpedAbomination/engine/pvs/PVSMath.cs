﻿using System;
using Microsoft.Xna.Framework;

namespace WarpedAbominationEngine
{
    public static class PVSMath
    {
        const int LeftFlag = 0x8, RightFlag = 0x4, ForeFlag = 0x2, BackFlag = 0x1, BottomFlag = 0x10, TopFlag = 0x20;
        public const byte BlockingFlags = 0;

        static byte ModBlockingFlags(byte flags, Portal portal)
        {
            byte newFlags = flags;
            switch (portal.DirectionToExit)
            {
                case Direction.Fore:
                    newFlags = flags;
                    break;
                case Direction.Back:
                    newFlags = (byte)(
                        ((flags & RightFlag) << 1) |
                        ((flags & LeftFlag) >> 1) |
                        ((flags & BackFlag) << 1) |
                        ((flags & ForeFlag) >> 1) |
                        ((flags & BottomFlag)) |
                        ((flags & TopFlag))
                    );
                    break;
                case Direction.Left:
                    newFlags = (byte)(
                        ((flags & ForeFlag) << 2) |
                        ((flags & BackFlag) << 2) |
                        ((flags & RightFlag) >> 1) |
                        ((flags & LeftFlag) >> 3) |
                        ((flags & BottomFlag)) |
                        ((flags & TopFlag))
                    );
                    break;
                case Direction.Right:
                    newFlags = (byte)(
                        ((flags & BackFlag) << 3) |
                        ((flags & ForeFlag) << 1) |
                        ((flags & LeftFlag) >> 2) |
                        ((flags & RightFlag) >> 2) |
                        ((flags & BottomFlag)) |
                        ((flags & TopFlag))
                    );
                    break;
                default:
                    throw new Exception("Unsupported Portal");
            }
            switch (portal.OutPortal.Side)
            {
                case SectorSide.Left:
                    newFlags |= LeftFlag;
                    break;
                case SectorSide.Right:
                    newFlags |= RightFlag;
                    break;
                case SectorSide.Fore:
                    newFlags |= ForeFlag;
                    break;
                case SectorSide.Back:
                    newFlags |= BackFlag;
                    break;
                case SectorSide.Bottom:
                    newFlags |= BottomFlag;
                    break;
                case SectorSide.Top:
                    newFlags |= TopFlag;
                    break;

            }
            return newFlags;
        }

        internal static bool PortalCheck(Vector3 objectPosition, Portal portal, float objectRadius, byte blockingFlags, out byte newBlockingFlags)
        {
            newBlockingFlags = blockingFlags;
            switch (portal.Side)
            {
                case SectorSide.Left:
                    if ((blockingFlags & LeftFlag) > 0)
                        return false;
                    if (objectPosition.X < 0)
                        return false;
                    break;
                case SectorSide.Right:
                    if ((blockingFlags & RightFlag) > 0)
                        return false;
                    if (objectPosition.X > portal.Sector.Size.X)
                        return false;
                    break;
                case SectorSide.Fore:
                    if ((blockingFlags & ForeFlag) > 0)
                        return false;
                    if (objectPosition.Z < 0)
                        return false;
                    break;
                case SectorSide.Back:
                    if ((blockingFlags & BackFlag) > 0)
                        return false;
                    if (objectPosition.Z > portal.Sector.Size.Z)
                        return false;
                    break;
                case SectorSide.Bottom:
                    if ((blockingFlags & BottomFlag) > 0)
                        return false;
                    if (objectPosition.Y < 0)
                        return false;
                    break;
                case SectorSide.Top:
                    if ((blockingFlags & TopFlag) > 0)
                        return false;
                    if (objectPosition.Y > portal.Sector.Size.Y)
                        return false;
                    break;
            }
            float distSq = (objectPosition - portal.PortalCenter).LengthSquared();
            if ((distSq - portal.Size.X * portal.Size.X / 4f - portal.Size.Y * portal.Size.Y / 4f) > objectRadius * objectRadius)
                return false;
            newBlockingFlags = ModBlockingFlags(blockingFlags, portal);
            return true;
        }
    }
}
