﻿using System;
using System.Collections.Generic;

namespace WarpedAbominationEngine.SceneCore
{
    public class SceneGraphicsResources : IDisposable
    {
        readonly Scene scene;
        readonly HashSet<Sector> loadedSectors = new HashSet<Sector>();
        readonly Dictionary<SectorID, SectorGraphics> graphics = new Dictionary<SectorID, SectorGraphics>();
        volatile bool isDisposing;

        internal SceneGraphicsResources(Scene scene)
        {
            this.scene = scene;
        }

        public SectorGraphics this[SectorID id]
        {
            get { return graphics[id]; }
        }

        internal void ReloadSector(Sector sector)
        {
            if (!isDisposing)
                throw new RapidException("Graphics Resources Disposing");
            if (!loadedSectors.Contains(sector))
                throw new RapidException(string.Format("Sector realoading: {0} is not loaded", sector.FullID));
            if(!sector.Region.IsLoaded)
                throw new RapidException("Sector reloading: {0} region is not loaded");
            graphics[sector.FullID].RecreateGraphics();
        }

        internal void LoadSector(Sector sector)
        {
            if (!isDisposing)
                throw new RapidException("Graphics Resources Disposing");
            if (graphics.ContainsKey(sector.FullID) || loadedSectors.Contains(sector))
                throw new RapidException(string.Format("Sector loading: {0} is already in graphics map", sector.FullID));
            if (!sector.Region.IsLoaded)
                throw new RapidException("Sector loading: {0} region is not loaded");
            graphics.Add(sector.FullID, new SectorGraphics(sector));
            loadedSectors.Add(sector);
            sector.Tick();
        }

        internal void UnloadSector(Sector sector)
        {
            if (!isDisposing)
                throw new RapidException("Graphics Resources Disposing");
            if (!loadedSectors.Contains(sector) || !graphics.ContainsKey(sector.FullID))
                throw new RapidException(string.Format("Sector unloading: {0} is not loaded", sector.FullID));
            loadedSectors.Remove(sector);
            graphics[sector.FullID].Dispose();
            graphics.Remove(sector.FullID);
        }

        public void Dispose()
        {
            isDisposing = false;
            foreach (var kv in graphics)
                kv.Value.Dispose();
            loadedSectors.Clear();
            graphics.Clear();
        }
    }
}
