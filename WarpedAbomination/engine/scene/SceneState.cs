﻿using System;
using System.Collections.Generic;
using WarpedAbominationEngine.Serialization;

namespace WarpedAbominationEngine
{ 
    public class SceneState
    {
        readonly Scene scene;

        readonly Dictionary<string, object> variables = new Dictionary<string, object>();

        public SceneState(Scene scene)
        {
            this.scene = scene;
        }

        public object this[string key]
        {
            get
            {
                return variables[key];
            }
            set
            {
                if (AbominationSerialization.DefineObjectType(value) == null)
                    throw new Exception("Uncompatible type: {0}" + value.GetType().FullName);
                variables[key] = value;
            }
        }
    }
}
