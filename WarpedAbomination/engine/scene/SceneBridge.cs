﻿using System;
using System.Collections.Generic;

namespace WarpedAbominationEngine.SceneCore
{
    public class SceneBridge
    {
        readonly Scene scene;

        readonly HashSet<PortalPair> linkedPortals = new HashSet<PortalPair>();
        readonly Dictionary<Point2, HashSet<PortalPair>> regionPortals = new Dictionary<Point2, HashSet<PortalPair>>();
        readonly HashSet<PortalPair> portals = new HashSet<PortalPair>();

        public SceneBridge(Scene scene)
        {
            this.scene = scene;
        }

        SceneStorage Storage => scene.Storage;

        //This whould be called after region is loaded or generated
        internal void FinishLinkingRegion(Region region)
        {
            if (!regionPortals.ContainsKey(region.ID))
                //Creating portal_region collections
                regionPortals.Add(region.ID, new HashSet<PortalPair>());
            else
                //Or Linking existing portals
                foreach (PortalPair pair in regionPortals[region.ID])
                    if (pair.IsReadyToLink)
                        Link(pair);
            region.Tick(); //Prevent From Premature unloading
        }

        internal void UnlinkRegion(Region region)
        {
            if (regionPortals.ContainsKey(region.ID))
            {
                foreach (PortalPair pair in regionPortals[region.ID])
                {
                    if (pair.IsLinked)
                        Unlink(pair);
                }
            }
        }

        internal bool RequestConnectedRegionsLoad(Sector sector)
        {
            bool result = false;
            if (regionPortals.ContainsKey(sector.Region.ID))
            {
                foreach (PortalPair pair in regionPortals[sector.Region.ID])
                {
                    if (pair.Sector1 == sector.FullID || pair.Sector2 == sector.FullID)
                    {
                        result |= Storage.RequestLoadRegion(pair.Sector1.RegionID) != null;
                        result |= Storage.RequestLoadRegion(pair.Sector2.RegionID) != null;
                    }
                }
            }
            return result;
        }

        internal void RegionRemoved(Region region)
        {
            if (regionPortals.ContainsKey(region.ID))
            {
                HashSet<PortalPair> remove = new HashSet<PortalPair>();
                foreach (PortalPair pair in regionPortals[region.ID])
                    remove.Add(pair);
                foreach (PortalPair pair in remove)
                {
                    portals.Remove(pair);
                    //Removing Pair From portal_region storage
                    if (regionPortals[pair.Sector1.RegionID].Contains(pair))
                        regionPortals[pair.Sector1.RegionID].Remove(pair);
                    if (regionPortals[pair.Sector2.RegionID].Contains(pair))
                        regionPortals[pair.Sector2.RegionID].Remove(pair);
                }
                regionPortals.Remove(region.ID);
            }
        }

        internal void RegionUnloaded(Region region)
        {
            if (regionPortals.ContainsKey(region.ID))
            {
                HashSet<PortalPair> remove = new HashSet<PortalPair>();
                foreach (PortalPair pair in regionPortals[region.ID])
                {
                    if (!Storage.ContainsKey(pair.Sector1.RegionID) && !Storage.ContainsKey(pair.Sector2.RegionID))
                    {
                        if (pair.IsLinked)
                            throw new Exception(string.Format(
                                "Portal pair: {0} is still linked while region {1} is removing",
                                pair,
                                region.ID
                                ));
                        remove.Add(pair);
                    }
                }
                foreach (PortalPair pair in remove)
                {
                    portals.Remove(pair);
                    //Removing Pair From portal_region storage
                    regionPortals[pair.Sector1.RegionID].Remove(pair);
                    if (regionPortals[pair.Sector2.RegionID].Contains(pair))
                        regionPortals[pair.Sector2.RegionID].Remove(pair);

                    //Removing Empty portal_region collections
                    if (regionPortals[pair.Sector1.RegionID].Count == 0)
                        regionPortals.Remove(pair.Sector1.RegionID);
                    if (regionPortals.ContainsKey(pair.Sector2.RegionID))
                        if (regionPortals[pair.Sector2.RegionID].Count == 0)
                            regionPortals.Remove(pair.Sector2.RegionID);
                }
            }
        }

        //Can be called before region is fully loaded
        internal void AddPortal(PortalPair portal)
        {
            if (portals.Contains(portal))
                return;
            portals.Add(portal);
            //Checking if collections existing
            if (!regionPortals.ContainsKey(portal.Sector1.RegionID))
                regionPortals[portal.Sector1.RegionID] = new HashSet<PortalPair>();
            if (!regionPortals.ContainsKey(portal.Sector2.RegionID))
                regionPortals[portal.Sector2.RegionID] = new HashSet<PortalPair>();

            regionPortals[portal.Sector1.RegionID].Add(portal);
            regionPortals[portal.Sector2.RegionID].Add(portal);
            portal.Scene = scene;
            //Linking if regions loaded
            if (portal.IsReadyToLink && !portal.IsLinked)
                Link(portal);
        }

        //Only for editing
        internal bool RemovePortal(PortalPair portal)
        {
            //First unlinking portals from sectors
            if (portal.IsLinked)
                Unlink(portal);

            //Can remove only if regions are Ok
            if (!portal.IsReadyToLink)
                return false;

            portals.Remove(portal);
            //Removing Pair From portal_region storage
            regionPortals[portal.Sector1.RegionID].Remove(portal);
            if (regionPortals[portal.Sector2.RegionID].Contains(portal))
                regionPortals[portal.Sector2.RegionID].Remove(portal);

            //Removing Empty portal_region collections
            if (regionPortals[portal.Sector1.RegionID].Count == 0)
                regionPortals.Remove(portal.Sector1.RegionID);
            if (regionPortals.ContainsKey(portal.Sector2.RegionID))
                if (regionPortals[portal.Sector2.RegionID].Count == 0)
                    regionPortals.Remove(portal.Sector2.RegionID);
            return true;
        }

        internal void Link(PortalPair pair)
        {
            if (pair.IsLinked)
                throw new Exception("Portal Already linked");
            linkedPortals.Add(pair);
            switch (pair.Type)
            {
                case PortalType.Euclid:
                    pair.Portal1 = new EuclidPortal();
                    pair.Portal2 = new EuclidPortal();
                    pair.SetDefaults();
                    pair.Portal1.Solid = false;
                    pair.Portal2.Solid = false;
                    break;
                case PortalType.EuclidWall:
                    pair.Portal1 = new EuclidPortal();
                    pair.Portal2 = new EuclidPortal();
                    pair.SetDefaults();
                    pair.Portal1.Solid = true;
                    pair.Portal2.Solid = true;
                    break;
                case PortalType.Mirror:
                    pair.Portal1 = new MirrorPortal();
                    pair.SetDefaults();
                    pair.Portal1.Solid = true;
                    break;
                case PortalType.Warp:
                    pair.Portal1 = new WarpPortal();
                    pair.Portal2 = new WarpPortal();
                    pair.SetDefaults();
                    pair.Portal1.Solid = false;
                    pair.Portal2.Solid = false;
                    break;
                case PortalType.WarpWall:
                    pair.Portal1 = new WarpPortal();
                    pair.Portal2 = new WarpPortal();
                    pair.SetDefaults();
                    pair.Portal1.Solid = true;
                    pair.Portal2.Solid = true;
                    break;
            }
            if (pair.Portal1 != null)
                pair.Portal1.Sector.Portals.Add(pair.Portal1);
            if (pair.Portal2 != null)
                pair.Portal2.Sector.Portals.Add(pair.Portal2);

        }

        internal void Unlink(PortalPair pair)
        {
            if (!pair.IsLinked)
                throw new Exception("Portal Already unlinked");
            pair.Portal1.Sector.Portals.Remove(pair.Portal1);
            if (pair.Portal2 != null)
                pair.Portal2.Sector.Portals.Remove(pair.Portal2);
            pair.Portal1 = null;
            pair.Portal2 = null;
            linkedPortals.Remove(pair);
        }
    }
}
