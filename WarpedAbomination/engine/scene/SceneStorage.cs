﻿using System;
using System.Collections.Generic;

namespace WarpedAbominationEngine.SceneCore
{
	public class SceneStorage
	{
        const int InitialSectors = 256;
        const string LoadRegionJobName = "load-region";
        const string UnloadRegionJobName = "unload-region";

        readonly Dictionary<Point2, Region> regions = new Dictionary<Point2, Region>();
        readonly Stack<int> freeIds = new Stack<int>();
        readonly Scene scene;

        Sector[] sectors = new Sector[InitialSectors];
        int allocatedSectorsCount = 0;

        internal SceneStorage(Scene scene)
		{
            this.scene = scene;
		}

        SceneBridge Bridge => scene.Bridge;
        Cron Cron => scene.Cron;

        public Region this[Point2 position]
        {
            get { if (regions.ContainsKey(position)) return regions[position]; return null; }
            internal set
            {
                if (regions.ContainsKey(position))
                    throw new Exception("Can't reassign region");
                regions[position] = value;
            }
        }

        public Sector this[SectorID sector]
        {
            get
            {
                if (regions.ContainsKey(sector.RegionID))
                    return regions[sector.RegionID][sector.ID];
                return null;
            }
        }

        public bool ContainsKey(Point2 position)
        {
            return regions.ContainsKey(position);
        }

        internal Region NewRegion(string name, Point2 position)
        {
            Region region = new Region(scene, position) {
                Name = name,
                State = RegionState.Loaded
            };
            AddRegion(region);
            return region;
        }

        //Only for editing
        internal void RemoveSector(Sector sector)
        {
            foreach (Portal portal in sector.Portals)
                Bridge.RemovePortal(portal.Pair);
            if (sectors[sector.StorageId] != sector)
                throw new InconsistentSceneDataException("Can't remove sector. inconsistent StorageID");
            sectors[sector.StorageId] = null;
            allocatedSectorsCount--;
        }

        //Can be called before region is fully loaded
        internal int AddSector(Sector sector)
        {
            //if region even exists
            if (!regions.ContainsKey(sector.Region.ID) || (regions[sector.Region.ID].State != RegionState.Loaded))
                throw new Exception("Can't Watch of unloaded region");

            //need more sectors
            if (allocatedSectorsCount >= sectors.Length)
                ReallocateSectors(sectors.Length * 2);

            //generating sector id and applying
            for (int i = 0; i < sectors.Length; i++)
            {
                if (sectors[i] == null)
                {
                    sectors[i] = sector;
                    sector.StorageId = i;
                    allocatedSectorsCount++;
                    return i;
                }
            }
            throw new Exception("Can't allocate sector watch ID");
        }


        internal void AddRegion(Region region)
        {
            regions.Add(region.ID, region);
            RegionLoaded(region);
        }

        //This whould be called after regions is loaded or generated
        internal void RegionLoaded(Region region)
        {
            Bridge.FinishLinkingRegion(region);
        }

        internal void ForgetRegion(Region region)
        {
            regions.Remove(region.ID);
            Bridge.RegionRemoved(region);
        }

        //Call this after saving region, lock it's updates before saving
        internal void RegionUnloaded(Region region)
        {
            //Removing from loaded regions
            regions.Remove(region.ID);
            //Unlinking or Removing Portals
            Bridge.RegionUnloaded(region);
            //Removing Sectors from storage
            foreach (KeyValuePair<int, Sector> kv in region.sectors)
            {
                Sector sector = kv.Value;
                sectors[sector.StorageId] = null;
                allocatedSectorsCount--;
            }
            region.Dispose();
        }

        internal Region ForceSetRegion(Point2 position)
        {
            if (!scene.IsAlive)
                return null;
            Region region = ForceGetRegion(position);
            if (region != null)
                return region;
            return NewRegion(position.ToString(), position);
        }

        internal Region ForceGetRegion(Point2 position)
        {
            if (!scene.IsAlive)
                return null;
            CronJobInstance job = RequestLoadRegion(position);
            if (job != null)
                job.Wait();
            return this[position];
        }

        private void ReallocateSectors(int count)
        {
            Sector[] newSectors = new Sector[count];
            int index = 0;
            for (int i = 0; i < sectors.Length; i++)
            {
                Sector sector = sectors[i];
                if (sector == null)
                    continue;
                newSectors[index] = sector;
                sector.StorageId = index;
                index++;
            }
            sectors = newSectors;
        }

        internal bool CallLoadRegion(Region region)
        {
            if (region == null)
                throw new Exception("Null region to load");
            bool result = false;
            if (scene.RegionCreator != null) {
                try {
                    result = scene.RegionCreator(scene, region);
                } catch (Exception ex) {
                    scene.Logger.Error(
                        string.Format("Exception while loading region {0}",
                        region.ID),
                        ex);
                }
            }
            return result;
        }

        internal void CallUnloadRegion(Region region)
        {
            if (region == null)
                throw new Exception("Null Region To Unload");
            try {
                scene.RegionDisposer?.Invoke(scene, region);
            } catch (Exception ex) {
                scene.Logger.Error(
                        string.Format("Exception while unloading region {0}",
                        region.ID),
                        ex);
            }
        }

        internal CronJobInstance RequestLoadRegion(Point2 position)
        {
            if (!scene.IsAlive)
                return null;
            Region region = null;
            if (ContainsKey(position)) {
                region = this[position];
                if (region.State != RegionState.Unloaded)
                    return null;
            } else {
                region = new Region(scene, position);
            }
            lock (region.ResourceLock) {
                region.State = RegionState.RequestedLoad;
                this[position] = region;
                return Cron.Enqueue(LoadRegionJobName, new Action<Region>(RegionProcessor), region);
            }
        }

        void RegionProcessor(Region region)
        {
            lock (region.ResourceLock) {
                switch (region.State) {
                    case RegionState.RequestedLoad:
                        region.State = RegionState.Loading;
                        if (CallLoadRegion(region)) {
                            RegionLoaded(region);
                            region.State = RegionState.Loaded;
                        }
                        else {
                            ForgetRegion(region);
                        }
                        break;
                    case RegionState.RequestedUnload:
                        region.State = RegionState.Unloading;
                        CallUnloadRegion(region);
                        RegionUnloaded(region);
                        region.State = RegionState.Unloaded;
                        break;
                }
            }
        }

        internal CronJobInstance RequestUnloadRegion(Point2 position)
        {
            if (!scene.IsAlive)
                return null;
            Region region = this[position];
            if (region == null)
                return null;
            if (!region.State.IsReadyToUnload())
                return null;
            lock (region.ResourceLock) {
                Bridge.UnlinkRegion(region);
                region.State = RegionState.RequestedUnload;
                return Cron.Enqueue(UnloadRegionJobName, new Action<Region>(RegionProcessor), region);
            }
        }
    }
}
