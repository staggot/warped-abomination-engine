﻿using System;

namespace WarpedAbominationEngine.SceneCore
{
    public class InconsistentSceneDataException : Exception
    {
        public InconsistentSceneDataException(string message) : base(message)
        {
        }
    }
}
