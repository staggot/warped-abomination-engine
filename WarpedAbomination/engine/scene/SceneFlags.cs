﻿using System;
namespace WarpedAbominationEngine
{
    [Flags]
    public enum SceneFlags
    {
        None = 0,
        GraphicsEnabled = 1,
        RenderingEnabled = 2
    }
}
