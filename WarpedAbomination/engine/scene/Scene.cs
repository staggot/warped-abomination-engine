﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using qsldotnet.lib;
using WarpedAbominationEngine.SceneCore;

namespace WarpedAbominationEngine
{
	public class Scene : IDisposable, IPausable
	{
        public readonly SceneStorage Storage;
        public readonly SceneBridge Bridge;
        public readonly SceneState State;
        public readonly SceneFlags Flags;
        public readonly Logger Logger;
        public readonly SceneScripting Scripting;
        public readonly WarpedStateMachine StateMachine;
        public readonly Cron Cron;

        public long MaxRegionAge = 1800;

        public RegionChecher RegionChecher;
        public RegionMaterializer RegionCreator;
        public RegionDisposer RegionDisposer;
        public Vector3 Gravitation = new Vector3(0, -9.8f, 0);

        internal event Action<Portal> OnPortalPlaced;
        internal event Action<Portal> OnPortalRemoved;
        internal event Action<SectorID> OnSectorTerrainChanged;
        internal event Action<SectorID> OnSectorRemoved;
        internal event Action<Point2> RegionUnloadRequested;
        internal event Action<Point2> RegionUnloaded;
        internal event Action<Point2> RegionLoadRequested;
        internal event Action<Point2> RegionLoaded;

        volatile bool isPaused;
        volatile bool isAlive = true;

        public bool IsPaused
        {
            get { return isPaused; }
        }

        public bool IsAlive => isAlive;

        public long Tick { get; private set; }

        public Scene(SceneFlags flags)
		{
            Tick = 0;
            isPaused = false;
            Flags = flags;
            Storage = new SceneStorage(this);
            Cron = new Cron("scene") {
                Logger = Logger
            };
            Bridge = new SceneBridge(this);
            Scripting = new SceneScripting(this);
            StateMachine = new WarpedStateMachine(this, SceneScripting.Engine);
            Logger = new Logger();
		}

        public object this[string name]
        {
            get { return State[name]; }
            set { State[name] = value; }
        }

        public Region this[Point2 position]
        {
            get { return Storage[position]; }
        }

        public Sector this[SectorID sector]
        {
            get
            {
                return Storage[sector];
            }
        }

        public Sector[] CreateSector(Point3 position, Point3 size, SectorSideMask sideFlags = SectorSideMask.ALL)
        {
            List<Bounds3> allocation = new List<Bounds3>();
            Stack<Bounds3> stack = new Stack<Bounds3>();
            stack.Push(new Bounds3(position, size));

            while (stack.Count > 0) {
                Bounds3 current = stack.Pop();
                if (SceneMath.ChopSector(current.Position, current.Size, out Point3 posA, out Point3 posB, out Point3 sizeA, out Point3 sizeB)) {
                    stack.Push(new Bounds3(posA, sizeA));
                    stack.Push(new Bounds3(posB, sizeB));
                } else
                    allocation.Add(current);
            }

            Sector[] result = new Sector[allocation.Count];

            for (int i = 0; i < allocation.Count; i++) {
                Bounds3 bounds = allocation[i];
                result[i] = Storage.ForceSetRegion(
                    SceneMath.GetRegionID(bounds.Position)
                ).CreateSector(bounds.Position, bounds.Size, sideFlags);
            }
            return result;
        }

        public void Dispose()
        {
            if (!isAlive)
                return;
            isAlive = false;
            Cron.Dispose();
            StateMachine.Dispose();
            Scripting.Dispose();
        }

        public void Pause()
        {
            if (isPaused)
                return;
            StateMachine.Pause();
            isPaused = true;
        }

        public void Resume()
        {
            if (!isPaused)
                return;
            StateMachine.Resume();
            isPaused = false;
        }

        public void Update(float time)
        {
            Tick += 1;
            StateMachine.Update(time);
        }

        internal void CallPortalPlaced(Portal portal)
        {
            OnPortalPlaced?.Invoke(portal);
        }

        internal void CallPortalRemoved(Portal portal)
        {
            OnPortalRemoved?.Invoke(portal);
        }

        internal void CallSectorTerrainChanged(SectorID id)
        {
            OnSectorTerrainChanged?.Invoke(id);
        }

        internal void CallSectorRemoved(SectorID id)
        {
            OnSectorRemoved?.Invoke(id);
        }
    }
}
