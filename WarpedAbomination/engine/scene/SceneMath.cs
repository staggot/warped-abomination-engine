﻿using System;
namespace WarpedAbominationEngine
{
    public static class SceneMath
    {
        public static bool ChopSector(Point3 position, Point3 size, out Point3 positionA, out Point3 positionB, out Point3 sizeA, out Point3 sizeB)
        {
            Point2 lRegion = GetRegionID(position);
            Point2 rRegion = GetRegionID(position + new Point3(size.X - 1, 0, 0));
            if (lRegion != rRegion)
            {
                positionA = position;
                sizeA = new Point3(rRegion.X * Region.RegionSize - position.X, size.Y, size.Z);
                positionB = positionA + new Point3(sizeA.X, 0, 0);
                sizeB = new Point3(size.X - sizeA.X, size.Y, size.Z);
                return true;
            }
            Point2 fRegion = GetRegionID(position);
            Point2 bRegion = GetRegionID(position + new Point3(0, 0, size.Z - 1));
            if (fRegion != bRegion)
            {
                positionA = position;
                sizeA = new Point3(size.X, size.Y, rRegion.Y * Region.RegionSize - position.Z);
                positionB = positionA + new Point3(0, 0, sizeA.Z);
                sizeB = new Point3(size.X, size.Y, size.Z - sizeA.Z);
                return true;
            }
            positionA = position;
            positionB = new Point3(-1, -1, -1);
            sizeA = size;
            sizeB = new Point3(-1, -1, -1);
            return false;
        }

        public static Point2 GetRegionID(Point3 position)
        {
            return new Point2(position.X / Region.RegionSize, position.Z / Region.RegionSize);
        }
    }
}
