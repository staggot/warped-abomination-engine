﻿using System;
using Microsoft.Xna.Framework;
using WarpedAbominationEngine.Content;

namespace WarpedAbominationEngine.Physics
{
    public class MatterPoint : Shape
    {
        public override Vector2 Size => Vector2.Zero;

        public override AABB GetAABB(Vector3 position, Vector3 delta)
        {
            return new AABB(
                new Vector3(
                    position.X + (delta.X < 0 ? delta.X : 0),
                    position.Y + (delta.Y < 0 ? delta.Y : 0),
                    position.Z + (delta.Z < 0 ? delta.Z : 0)
                ),
                new Vector3(
                    position.X + (delta.X > 0 ? delta.X : 0),
                    position.Y + (delta.Y > 0 ? delta.Y : 0),
                    position.Z + (delta.Z > 0 ? delta.Z : 0)
                )
            );
        }

        public override AABB GetAABB(Vector3 position)
        {
            return new AABB(
                new Vector3(
                    position.X,
                    position.Y,
                    position.Z
                ),
                new Vector3(
                    position.X,
                    position.Y,
                    position.Z
                )
            );
        }

        public override bool RayCast(Vector3 position, Vector3 rayOrigin, Vector3 rayDirection, out float distance, out Vector3 normal)
        {
            normal = Vector3.Zero;
            distance = float.PositiveInfinity;
            return false;
        }

        public override bool SpecificCollide(Vector3 position, Sphere shape, Vector3 shapePosition, out float distance, out Vector3 normal)
        {
            normal = MathHelper.Normalize(position - shapePosition, out distance);
            distance -= shape.Radius;
            return true;
        }

        public override bool SpecificCollide(Vector3 position, Capsule shape, Vector3 shapePosition, out float distance, out Vector3 normal)
        {
            float diffY = position.Y - shapePosition.Y;
            if (diffY > shape.Height / 2)
            {
                Vector3 capsuleCenter = new Vector3(shapePosition.X, shapePosition.Y + shape.Height / 2, shapePosition.Z);
                normal =
                    MathHelper.Normalize(position - capsuleCenter, out distance);
                distance -= shape.Radius;
            }
            if (diffY < -shape.Height / 2)
            {
                Vector3 capsuleCenter = new Vector3(shapePosition.X, shapePosition.Y - shape.Height / 2, shapePosition.Z);
                normal =
                    MathHelper.Normalize(position - capsuleCenter, out distance);
                distance -= shape.Radius;
            }
            normal = MathHelper.Normalize(new Vector3(position.X - shapePosition.X, 0, position.Z - shapePosition.Z), out distance);
            distance -= shape.Radius;
            return true;
        }

        public override bool SpecificCollide(Vector3 position, TriangleCollider shape, Vector3 shapePosition, out float distance, out Vector3 normal)
        {
            Vector3 pos = shapePosition - position;
            float planeDistance = shape.PlaneDistance(pos);
            distance = float.PositiveInfinity;
            normal = Vector3.Zero;

            if (planeDistance < 0)
                return false;

            float abDist = shape.DistAB(pos);
            float bcDist = shape.DistBC(pos);
            float caDist = shape.DistCA(pos);

            bool isIsland = abDist >= 0 && bcDist >= 0 && caDist >= 0;

            if (isIsland)
            {
                normal = shape.Normal;
                distance = planeDistance;
                if (distance < -AbominationSystem.PhysicsEpsilon * 4)
                    return false;
                return true;
            }
            return false;
        }

        public override bool SpecificCollide(Vector3 position, SquareCollider shape, Vector3 shapePosition, out float distance, out Vector3 normal)
        {
            Vector3 pos = shapePosition - position;
            float planeDistance = shape.PlaneDistance(pos);
            distance = float.PositiveInfinity;
            normal = Vector3.Zero;

            if (planeDistance < 0)
                return false;

            float abDist = shape.DistAB(pos);
            float bcDist = shape.DistBC(pos);
            float cdDist = shape.DistCD(pos);
            float daDist = shape.DistDA(pos);

            bool isIsland = abDist >= 0 && bcDist >= 0 && cdDist >= 0 && daDist >= 0;

            if (isIsland)
            {
                normal = shape.Normal;
                distance = planeDistance;
                if (distance < -AbominationSystem.PhysicsEpsilon * 4)
                    return false;
                return true;
            }
            return false;
        }

        public override bool SpecificCollide(Vector3 position, MatterPoint shape, Vector3 shapePosition, out float distance, out Vector3 normal)
        {
            normal = MathHelper.Normalize(position - shapePosition, out distance);
            return true;
        }
    }
}
