﻿using System;
using Microsoft.Xna.Framework;

namespace WarpedAbominationEngine.Physics
{
    public class Capsule : Shape
    {
        public float Height, Radius;

        public override Vector2 Size
        {
            get
            {
                return new Vector2(Radius, Height);
            }
        }

        public override AABB GetAABB(Vector3 position, Vector3 delta)
        {
            throw new NotImplementedException();
        }

        public override AABB GetAABB(Vector3 position)
        {
            throw new NotImplementedException();
        }

        public override bool RayCast(Vector3 position, Vector3 rayOrigin, Vector3 rayDirection, out float distance, out Vector3 normal)
        {
            throw new NotImplementedException();
        }

        public override bool SpecificCollide(Vector3 position, Sphere shape, Vector3 shapePosition, out float distance, out Vector3 normal)
        {
            throw new NotImplementedException();
        }

        public override bool SpecificCollide(Vector3 position, Capsule shape, Vector3 shapePosition, out float distance, out Vector3 normal)
        {
            throw new NotImplementedException();
        }

        public override bool SpecificCollide(Vector3 position, TriangleCollider shape, Vector3 shapePosition, out float distance, out Vector3 normal)
        {
            throw new NotImplementedException();
        }

        public override bool SpecificCollide(Vector3 position, SquareCollider shape, Vector3 shapePosition, out float distance, out Vector3 normal)
        {
            throw new NotImplementedException();
        }

        public override bool SpecificCollide(Vector3 position, MatterPoint shape, Vector3 shapePosition, out float distance, out Vector3 normal)
        {
            throw new NotImplementedException();
        }
    }
}
