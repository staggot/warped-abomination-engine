﻿using System;
using Microsoft.Xna.Framework;

namespace WarpedAbominationEngine.Physics
{
    public struct AABB
    {
        public Vector3 Min, Max;

        public AABB(Vector3 min, Vector3 max)
        {
            this.Min = min;
            this.Max = max;
        }

        public Point3 MaxInt
        {
            get { return Point3.Round(Max); }
        }

        public Point3 MinInt
        {
            get { return Point3.Round(Min); }
        }
    }
}
