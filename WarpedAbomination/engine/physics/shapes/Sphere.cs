﻿using System;
using Microsoft.Xna.Framework;

namespace WarpedAbominationEngine.Physics
{
    public class Sphere : Shape
    {
        public float Radius;

        public override Vector2 Size
        {
            get
            {
                return new Vector2(Radius * 2, Radius * 2);
            }
        }

        public override AABB GetAABB(Vector3 position)
        {

            return new AABB(
                new Vector3(
                    position.X - Radius,
                    position.Y - Radius,
                    position.Z - Radius
                ),
                new Vector3(
                    position.X + Radius,
                    position.Y + Radius,
                    position.Z + Radius
                )
            );
        }

        public override AABB GetAABB(Vector3 position, Vector3 delta)
        {

            return new AABB(
                new Vector3(
                    position.X - Radius + (delta.X < 0 ? delta.X : 0),
                    position.Y - Radius + (delta.Y < 0 ? delta.Y : 0),
                    position.Z - Radius + (delta.Z < 0 ? delta.Z : 0)
                ),
                new Vector3(
                    position.X + Radius + (delta.X > 0 ? delta.X : 0),
                    position.Y + Radius + (delta.Y > 0 ? delta.Y : 0),
                    position.Z + Radius + (delta.Z > 0 ? delta.Z : 0)
                )
            );
        }

        public override bool RayCast(Vector3 position, Vector3 rayOrigin, Vector3 rayDirection, out float distance, out Vector3 normal)
        {
            Vector3 s0_r0 = rayOrigin - position;
            float a = 1;
            float b = 2 * Vector3.Dot(rayDirection, s0_r0);
            float c = Vector3.Dot(s0_r0, s0_r0) - (Radius * Radius);
            if (b * b - 4 * a * c < 0)
            {
                normal = Vector3.Zero;
                distance = float.PositiveInfinity;
                return false;
            }
            distance = (-b - MathHelper.Sqrt((b * b) - 4 * a * c)) / (2 * a);
            normal = Vector3.Normalize((rayOrigin + rayDirection * distance) - position);
            return true;
        }

        public override bool SpecificCollide(Vector3 position, Sphere shape, Vector3 shapePosition, out float distance, out Vector3 normal)
        {
            normal = MathHelper.Normalize(position - shapePosition, out distance);
            distance -= Radius + shape.Radius;
            return true;
        }

        public override bool SpecificCollide(Vector3 position, Capsule shape, Vector3 shapePosition, out float distance, out Vector3 normal)
        {
            float diffY = position.Y - shapePosition.Y;
            if (diffY > shape.Height / 2)
            {
                Vector3 capsuleCenter = new Vector3(shapePosition.X, shapePosition.Y + shape.Height / 2, shapePosition.Z);
                normal =
                    MathHelper.Normalize(position - capsuleCenter, out distance);
                distance -= Radius + shape.Radius;
            }
            if (diffY < -shape.Height / 2)
            {
                Vector3 capsuleCenter = new Vector3(shapePosition.X, shapePosition.Y - shape.Height / 2, shapePosition.Z);
                normal =
                    MathHelper.Normalize(position - capsuleCenter, out distance);
                distance -= Radius + shape.Radius;
            }
            normal = MathHelper.Normalize(new Vector3(position.X - shapePosition.X, 0, position.Z - shapePosition.Z), out distance);
            distance -= shape.Radius + Radius;
            return true;
        }

        public override bool SpecificCollide(Vector3 position, TriangleCollider shape, Vector3 shapePosition, out float distance, out Vector3 normal)
        {
            Vector3 pos = shapePosition - position;
            float planeDistance = shape.PlaneDistance(pos);
            distance = float.PositiveInfinity;
            normal = Vector3.Zero;

            if (planeDistance < 0)
                return false;

            float abDist = shape.DistAB(pos);
            float bcDist = shape.DistBC(pos);
            float caDist = shape.DistCA(pos);

            bool isIsland = abDist >= 0 && bcDist >= 0 && caDist >= 0;

            if (isIsland)
            {
                normal = shape.Normal;
                distance = planeDistance - Radius;
                if (distance < -Radius)
                    return false;
                return true;
            }

            if (abDist > 0 && bcDist > 0 && caDist <= 0)
            {
                normal = Vector3.Normalize(caDist * shape.NormalCA + planeDistance * shape.Normal);
                distance = MathHelper.PlaneDistance(normal, shape.C, pos) - Radius;
                if (distance < -Radius)
                    return false;
                return true;
            }
            if (abDist > 0 && bcDist <= 0 && caDist > 0)
            {
                normal = Vector3.Normalize(bcDist * shape.NormalBC + planeDistance * shape.Normal);
                distance = MathHelper.PlaneDistance(normal, shape.B, pos) - Radius;
                if (distance < -Radius)
                    return false;
                return true;
            }
            if (abDist <= 0 && bcDist > 0 && caDist > 0)
            {
                normal = Vector3.Normalize(abDist * shape.NormalAB + planeDistance * shape.Normal);
                distance = MathHelper.PlaneDistance(normal, shape.A, pos) - Radius;
                if (distance < -Radius)
                    return false;
                return true;
            }
            if (abDist <= 0 && bcDist <= 0)
            {
                normal = MathHelper.Normalize(pos - shape.B, out distance);
                distance -= Radius;
                if (distance < -Radius)
                    return false;
                return true;
            }
            if (abDist <= 0 && caDist <= 0)
            {
                normal = MathHelper.Normalize(pos - shape.A, out distance);
                distance -= Radius;
                if (distance < -Radius)
                    return false;
                return true;
            }
            if (bcDist <= 0 && caDist <= 0)
            {
                normal = MathHelper.Normalize(pos - shape.C, out distance);
                distance -= Radius;
                if (distance < -Radius)
                    return false;
                return true;
            }
            return false;
        }

        public override bool SpecificCollide(Vector3 position, SquareCollider shape, Vector3 shapePosition, out float distance, out Vector3 normal)
        {
            Vector3 pos = shapePosition - position;
            float planeDistance = shape.PlaneDistance(pos);
            distance = float.PositiveInfinity;
            normal = Vector3.Zero;

            if (planeDistance < 0)
                return false;

            float abDist = shape.DistAB(pos);
            float bcDist = shape.DistBC(pos);
            float cdDist = shape.DistCD(pos);
            float daDist = shape.DistDA(pos);

            bool isIsland = abDist >= 0 && bcDist >= 0 && cdDist >= 0 && daDist >= 0;

            if (isIsland)
            {
                normal = shape.Normal;
                distance = planeDistance - Radius;
                if (distance < -Radius)
                    return false;
                return true;
            }

            if (abDist > 0 && cdDist > 0)
            {
                if (bcDist <= 0)
                {
                    normal = Vector3.Normalize(bcDist * shape.NormalBC + planeDistance * shape.Normal);
                    distance = MathHelper.PlaneDistance(normal, shape.B, pos) - Radius;
                    if (distance < -Radius)
                        return false;
                    return true;
                }
                if (daDist <= 0)
                {
                    normal = Vector3.Normalize(daDist * shape.NormalDA + planeDistance * shape.Normal);
                    distance = MathHelper.PlaneDistance(normal, shape.D, pos) - Radius;
                    if (distance < -Radius)
                        return false;
                    return true;
                }
            }

            if (bcDist > 0 && daDist > 0)
            {
                if (abDist <= 0)
                {
                    normal = Vector3.Normalize(abDist * shape.NormalAB + planeDistance * shape.Normal);
                    distance = MathHelper.PlaneDistance(normal, shape.A, pos) - Radius;
                    if (distance < -Radius)
                        return false;
                    return true;
                }
                if (cdDist <= 0)
                {
                    normal = Vector3.Normalize(cdDist * shape.NormalCD + planeDistance * shape.Normal);
                    distance = MathHelper.PlaneDistance(normal, shape.C, pos) - Radius;
                    if (distance < -Radius)
                        return false;
                    return true;
                }
            }

            if (abDist <= 0 && bcDist <= 0)
            {
                normal = MathHelper.Normalize(pos - shape.B, out distance);
                distance -= Radius;
                if (distance < -Radius)
                    return false;
                return true;
            }

            if (bcDist <= 0 && cdDist <= 0)
            {
                normal = MathHelper.Normalize(pos - shape.C, out distance);
                distance -= Radius;
                if (distance < -Radius)
                    return false;
                return true;
            }

            if (cdDist <= 0 && daDist <= 0)
            {
                normal = MathHelper.Normalize(pos - shape.D, out distance);
                distance -= Radius;
                if (distance < -Radius)
                    return false;
                return true;
            }

            if (daDist <= 0 && abDist <= 0)
            {
                normal = MathHelper.Normalize(pos - shape.A, out distance);
                distance -= Radius;
                if (distance < -Radius)
                    return false;
                return true;
            }
            return false;
        }

        public override bool SpecificCollide(Vector3 position, MatterPoint shape, Vector3 shapePosition, out float distance, out Vector3 normal)
        {
            normal = MathHelper.Normalize(position - shapePosition, out distance);
            distance -= Radius;
            return true;
        }
    }
}
