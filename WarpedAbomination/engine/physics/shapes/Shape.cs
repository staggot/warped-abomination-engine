﻿using System;
using Microsoft.Xna.Framework;

namespace WarpedAbominationEngine.Physics
{
    public abstract class Shape
    {
        public bool Collide(Vector3 position, Shape shape, Vector3 shapePosition, out float distance, out Vector3 normal)
        {
            if (shape is Capsule)
                return SpecificCollide(position, shape as Capsule, shapePosition, out distance, out normal);
            if (shape is Sphere)
                return SpecificCollide(position, shape as Sphere, shapePosition, out distance, out normal);
            throw new NotImplementedException(string.Format("{0} -> {1}",this.GetType().Name, shape.GetType().Name));
        }

        public abstract Vector2 Size { get; }
        public abstract bool RayCast(Vector3 position, Vector3 rayOrigin, Vector3 rayDirection, out float distance, out Vector3 normal);
        public abstract AABB GetAABB(Vector3 position, Vector3 delta);
        public abstract AABB GetAABB(Vector3 position);

        public abstract bool SpecificCollide(Vector3 position, Sphere shape, Vector3 shapePosition, out float distance, out Vector3 normal);
        public abstract bool SpecificCollide(Vector3 position, Capsule shape, Vector3 shapePosition, out float distance, out Vector3 normal);
        public abstract bool SpecificCollide(Vector3 position, TriangleCollider shape, Vector3 shapePosition, out float distance, out Vector3 normal);
        public abstract bool SpecificCollide(Vector3 position, SquareCollider shape, Vector3 shapePosition, out float distance, out Vector3 normal);
        public abstract bool SpecificCollide(Vector3 position, MatterPoint shape, Vector3 shapePosition, out float distance, out Vector3 normal);
    }
}
