﻿using System;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using WarpedAbominationEngine.Content;

namespace WarpedAbominationEngine.Physics
{
    public class CollisionData
    {
        public HashSet<CollisionSource> Sources;
        float linearDelta;
        internal CollisionSourceType CollidesWith;

        public CollisionData()
        {
            Sources = new HashSet<CollisionSource>();
        }

        public void Reset()
        {
            Sources.Clear();
        }

        internal void FillData(Sector sector, Vector3 position, Shape shape, Vector3 delta)
        {
            linearDelta = Math.Abs(delta.X) + Math.Abs(delta.Y) + Math.Abs(delta.Z);
            AABB aabb = shape.GetAABB(position, delta);
            FillData(sector, position, shape, null, delta);
            Portal[] portals = new Portal[6];
            sector.Portals.GetSurroundingPortals(position, shape.Size + new Vector2(linearDelta, linearDelta), portals);
            for (int i = 0; i < portals.Length; i++) {
                if (portals[i] == null)
                    continue;
                FillData(sector, position, shape, portals[i].OutPortal, delta);
            }
        }

        private void FillData(Sector sector, Vector3 position, Shape shape, Portal portalOut, Vector3 delta)
        {
            if (portalOut != null) {
                sector = portalOut.Sector;
                position = portalOut.TransformPositionFrom(position);
                delta = portalOut.TransformDirectionFrom(delta);
            }
            AABB aabb = shape.GetAABB(position, delta);
            Point3 min = aabb.MinInt;
            Point3 max = aabb.MaxInt;

            if (portalOut != null) {
                switch (portalOut.Side) {
                    case SectorSide.Left:
                        min.X = Math.Max(min.X, 0);
                        break;
                    case SectorSide.Right:
                        max.X = Math.Min(max.X, sector.Size.X - 1);
                        break;
                    case SectorSide.Fore:
                        min.Z = Math.Max(min.Z, 0);
                        break;
                    case SectorSide.Back:
                        max.Z = Math.Min(max.Z, sector.Size.Z - 1);
                        break;
                    case SectorSide.Bottom:
                        min.Y = Math.Max(min.Y, 0);
                        break;
                    case SectorSide.Top:
                        max.Y = Math.Min(max.Y, sector.Size.Y - 1);
                        break;
                }
            }

            if ((CollidesWith & (CollisionSourceType.Block | CollisionSourceType.Terrain)) > 0) {
                for (int y = min.Y; y <= max.Y; y++)
                    for (int z = min.Z; z <= max.Z; z++)
                        for (int x = min.X; x <= max.X; x++) {
                            Point3 nodePosition = new Point3(x, y, z);
                            Point3 fromDelta = sector.HowFarOutside(nodePosition);
                            Point3 from = nodePosition + fromDelta;
                            if (fromDelta == Point3.Zero) {
                                AppendNode(shape, position, sector, nodePosition, portalOut, null);
                                continue;
                            }
                            if (nodePosition.X < 0) {
                                Portal pportal = sector.Portals.Get(SectorSide.Left, new Point2(from.Z, from.Y));
                                if (pportal != null)
                                    AppendNode(
                                    shape,
                                    position,
                                    pportal.OutSector,
                                    pportal.TransformNodePositionTo(nodePosition),
                                    portalOut,
                                    pportal
                                );
                            }
                            if (nodePosition.X >= sector.Size.X) {
                                Portal pportal = sector.Portals.Get(SectorSide.Right, new Point2(from.Z, from.Y));
                                if (pportal != null)
                                    AppendNode(
                                    shape,
                                    position,
                                    pportal.OutSector,
                                    pportal.TransformNodePositionTo(nodePosition),
                                    portalOut,
                                    pportal
                                );
                            }
                            if (nodePosition.Z < 0) {
                                Portal pportal = sector.Portals.Get(SectorSide.Fore, new Point2(from.X, from.Y));
                                if (pportal != null)
                                    AppendNode(
                                    shape,
                                    position,
                                    pportal.OutSector,
                                    pportal.TransformNodePositionTo(nodePosition),
                                    portalOut,
                                    pportal
                                );
                            }
                            if (nodePosition.Z >= sector.Size.Z) {
                                Portal pportal = sector.Portals.Get(SectorSide.Back, new Point2(from.X, from.Y));
                                if (pportal != null)
                                    AppendNode(
                                    shape,
                                    position,
                                    pportal.OutSector,
                                    pportal.TransformNodePositionTo(nodePosition),
                                    portalOut,
                                    pportal
                                );
                            }
                            if (nodePosition.Y < 0) {
                                Portal pportal = sector.Portals.Get(SectorSide.Bottom, new Point2(from.X, from.Z));
                                if (pportal != null)
                                    AppendNode(
                                    shape,
                                    position,
                                    pportal.OutSector,
                                    pportal.TransformNodePositionTo(nodePosition),
                                    portalOut,
                                    pportal
                                );
                            }
                            if (nodePosition.Y >= sector.Size.Y) {
                                Portal pportal = sector.Portals.Get(SectorSide.Top, new Point2(from.X, from.Z));
                                if (pportal != null)
                                    AppendNode(
                                    shape,
                                    position,
                                    pportal.OutSector,
                                    pportal.TransformNodePositionTo(nodePosition),
                                    portalOut,
                                    pportal
                                );
                            }
                        }
            }
        }

        private void AppendNode(Shape shape, Vector3 shapePosition, Sector sector, Point3 node, Portal portalTo, Portal portalFrom)
        {
            if (portalFrom != null)
                shapePosition = portalFrom.TransformPositionTo(shapePosition);
            if (sector.HowFarOutside(node) != Point3.Zero)
                return;
            NodeShape nodeShape = sector.Terrain.GetNodeShape(node);
            if (!nodeShape.IsEmptyTerrain) {
                int i = 0;
                short tIndex;
                while ((tIndex = CollisionTable.TriangleIndex[nodeShape.TerrainShapeData, i++]) >= 0) {
                    if (shape.SpecificCollide(node.Vector, CollisionTable.TriangleTable[tIndex], shapePosition, out float distance, out Vector3 normal)) {

                        if (distance > linearDelta + AbominationSystem.PhysicsEpsilon * 4)
                            continue;

                        if (portalFrom != null)
                            normal = portalFrom.TransformDirectionFrom(normal);
                        if (portalTo != null)
                            normal = portalTo.TransformDirectionTo(normal);

                        AddSource(new CollisionSource() {
                            Sector = sector,
                            SectorNode = node,
                            Type = CollisionSourceType.Terrain,
                            Distance = distance,
                            Body = null,
                            Portal = null,
                            StaticMesh = null,
                            NodeIndex = i,
                            Normal = normal
                        });
                    }
                }
            }
            if (!nodeShape.IsEmptyBlock) {
                int i = 0;
                short sIndex;
                int map = nodeShape.BlockShapeMap << 8;
                while ((sIndex = CollisionTable.SquareIndex[nodeShape.BlockShape, i++]) >= 0) {
                    if ((sIndex & map) == 0)
                        continue;
                    if (shape.SpecificCollide(node.Vector, CollisionTable.SquareTable[sIndex & 0xff], shapePosition, out float distance, out Vector3 normal)) {
                        if (distance > linearDelta + AbominationSystem.PhysicsEpsilon * 4)
                            continue;

                        if (portalFrom != null)
                            normal = portalFrom.TransformDirectionFrom(normal);
                        if (portalTo != null)
                            normal = portalTo.TransformDirectionTo(normal);

                        AddSource(new CollisionSource() {
                            Sector = sector,
                            SectorNode = node,
                            Type = CollisionSourceType.Block,
                            Distance = distance,
                            Body = null,
                            Portal = null,
                            StaticMesh = null,
                            NodeIndex = i,
                            Normal = normal
                        });
                    }
                }
            }
        }

        private void AddSource(CollisionSource source)
        {
            if (Sources.Contains(source))
                return;
            Sources.Add(source);

        }

        internal void FillBodyData(PhysicsBody body, Vector3 delta)
        {
            linearDelta = Math.Abs(delta.X) + Math.Abs(delta.Y) + Math.Abs(delta.Z);
            if (body.Shape == null)
                return;
            if (body.Link != null) {
                FillData(body.Sector, body.Position, body.Shape, null, delta);
            }
            for (int i = 0; i < body.ChildLinks.Length; i++) {
                FillData(body.Sector, body.Position, body.Shape, body.ChildLinks[i].Portal, delta);
            }
        }

        internal Vector3 CalculateDelta(Vector3 delta)
        {
            Vector3 newDelta = delta;
            foreach (CollisionSource source in Sources) {
                float normalledDelta = newDelta.X * (-source.Normal.X) + newDelta.Y * (-source.Normal.Y) + newDelta.Z * (-source.Normal.Z);
                float difference = source.Distance - normalledDelta;
                if (difference <= AbominationSystem.PhysicsEpsilon * 2) {
                    newDelta -= source.Normal * (difference - AbominationSystem.PhysicsEpsilon);
                }
            }
            return newDelta;
        }
    }
}
