﻿
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using WarpedAbominationEngine.Content;

namespace WarpedAbominationEngine.Physics
{
    public static partial class CollisionTable
    {
        public const int MaxTrisPerTerrainNode = 6;
        public const int MaxSquaresPerTerrainNode = 16;
        internal static TriangleCollider[] TriangleTable;
        internal static SquareCollider[] SquareTable;
        internal readonly static short[,] TriangleIndex = new short[65536, MaxTrisPerTerrainNode];
        internal readonly static short[,] SquareIndex = new short[256, MaxSquaresPerTerrainNode];

        internal static void Prepare()
        {
            if (AbominationSystem.EngineStorage.IsResourceExists(AbominationSystem.PhysicsTableFilename)) {
                using(Stream steam = AbominationSystem.EngineStorage.Read(AbominationSystem.PhysicsTableFilename)) {
                    Read(steam);
                }
            } else {
                BakeTriangles();
                BakeSquares();
                using (Stream steam = AbominationSystem.EngineStorage.Read(AbominationSystem.PhysicsTableFilename)) {
                    Write(steam);
                }
            }
        }

        static TriangleCollider ReadTriangle(BinaryReader reader)
        {
            Vector3 A, B, C, Normal, NormalAB, NormalBC, NormalCA;

            A.X = reader.ReadSingle();
            A.Y = reader.ReadSingle();
            A.Z = reader.ReadSingle();

            B.X = reader.ReadSingle();
            B.Y = reader.ReadSingle();
            B.Z = reader.ReadSingle();

            C.X = reader.ReadSingle();
            C.Y = reader.ReadSingle();
            C.Z = reader.ReadSingle();

            Normal.X = reader.ReadSingle();
            Normal.Y = reader.ReadSingle();
            Normal.Z = reader.ReadSingle();

            NormalAB.X = reader.ReadSingle();
            NormalAB.Y = reader.ReadSingle();
            NormalAB.Z = reader.ReadSingle();

            NormalBC.X = reader.ReadSingle();
            NormalBC.Y = reader.ReadSingle();
            NormalBC.Z = reader.ReadSingle();

            NormalCA.X = reader.ReadSingle();
            NormalCA.Y = reader.ReadSingle();
            NormalCA.Z = reader.ReadSingle();

            return new TriangleCollider(A, B, C, Normal, NormalAB, NormalBC, NormalCA);
        }

        static SquareCollider ReadSquare(BinaryReader reader)
        {
            Vector3 A, B, C, D, Normal, NormalAB, NormalBC, NormalCD, NormalDA;

            A.X = reader.ReadSingle();
            A.Y = reader.ReadSingle();
            A.Z = reader.ReadSingle();

            B.X = reader.ReadSingle();
            B.Y = reader.ReadSingle();
            B.Z = reader.ReadSingle();

            C.X = reader.ReadSingle();
            C.Y = reader.ReadSingle();
            C.Z = reader.ReadSingle();

            D.X = reader.ReadSingle();
            D.Y = reader.ReadSingle();
            D.Z = reader.ReadSingle();

            Normal.X = reader.ReadSingle();
            Normal.Y = reader.ReadSingle();
            Normal.Z = reader.ReadSingle();

            NormalAB.X = reader.ReadSingle();
            NormalAB.Y = reader.ReadSingle();
            NormalAB.Z = reader.ReadSingle();

            NormalBC.X = reader.ReadSingle();
            NormalBC.Y = reader.ReadSingle();
            NormalBC.Z = reader.ReadSingle();

            NormalCD.X = reader.ReadSingle();
            NormalCD.Y = reader.ReadSingle();
            NormalCD.Z = reader.ReadSingle();

            NormalDA.X = reader.ReadSingle();
            NormalDA.Y = reader.ReadSingle();
            NormalDA.Z = reader.ReadSingle();

            return new SquareCollider(A, B, C, D, Normal, NormalAB, NormalBC, NormalCD, NormalDA);
        }

        static void WriteTriangle(BinaryWriter writer, TriangleCollider triangle)
        {
            writer.Write((float)triangle.A.X);
            writer.Write((float)triangle.A.Y);
            writer.Write((float)triangle.A.Z);

            writer.Write((float)triangle.B.X);
            writer.Write((float)triangle.B.Y);
            writer.Write((float)triangle.B.Z);

            writer.Write((float)triangle.C.X);
            writer.Write((float)triangle.C.Y);
            writer.Write((float)triangle.C.Z);

            writer.Write((float)triangle.Normal.X);
            writer.Write((float)triangle.Normal.Y);
            writer.Write((float)triangle.Normal.Z);

            writer.Write((float)triangle.NormalAB.X);
            writer.Write((float)triangle.NormalAB.Y);
            writer.Write((float)triangle.NormalAB.Z);

            writer.Write((float)triangle.NormalBC.X);
            writer.Write((float)triangle.NormalBC.Y);
            writer.Write((float)triangle.NormalBC.Z);

            writer.Write((float)triangle.NormalCA.X);
            writer.Write((float)triangle.NormalCA.Y);
            writer.Write((float)triangle.NormalCA.Z);
        }

        static void WriteSquare(BinaryWriter writer, SquareCollider square)
        {
            writer.Write((float)square.A.X);
            writer.Write((float)square.A.Y);
            writer.Write((float)square.A.Z);

            writer.Write((float)square.B.X);
            writer.Write((float)square.B.Y);
            writer.Write((float)square.B.Z);

            writer.Write((float)square.C.X);
            writer.Write((float)square.C.Y);
            writer.Write((float)square.C.Z);

            writer.Write((float)square.D.X);
            writer.Write((float)square.D.Y);
            writer.Write((float)square.D.Z);

            writer.Write((float)square.Normal.X);
            writer.Write((float)square.Normal.Y);
            writer.Write((float)square.Normal.Z);

            writer.Write((float)square.NormalAB.X);
            writer.Write((float)square.NormalAB.Y);
            writer.Write((float)square.NormalAB.Z);

            writer.Write((float)square.NormalBC.X);
            writer.Write((float)square.NormalBC.Y);
            writer.Write((float)square.NormalBC.Z);

            writer.Write((float)square.NormalCD.X);
            writer.Write((float)square.NormalCD.Y);
            writer.Write((float)square.NormalCD.Z);

            writer.Write((float)square.NormalDA.X);
            writer.Write((float)square.NormalDA.Y);
            writer.Write((float)square.NormalDA.Z);
        }

        static void Write(Stream stream)
        {
            using (BinaryWriter writer = new BinaryWriter(stream)) {
                writer.Write((int)TriangleTable.Length);
                for (int i = 0; i < TriangleTable.Length; i++)
                    WriteTriangle(writer, TriangleTable[i]);
                for (int i = 0; i < 65536; i++)
                    for (int j = 0; j < MaxTrisPerTerrainNode; j++)
                        writer.Write((short)TriangleIndex[i, j]);

                writer.Write((int)SquareTable.Length);
                for (int i = 0; i < SquareTable.Length; i++)
                    WriteSquare(writer, SquareTable[i]);
                for (int i = 0; i < 256; i++)
                    for (int j = 0; j < MaxSquaresPerTerrainNode; j++)
                        writer.Write((short)SquareIndex[i, j]);
            }
        }

        static void Read(Stream stream)
        {
            using (BinaryReader reader = new BinaryReader(stream)) {
                int tris = reader.ReadInt32();
                TriangleTable = new TriangleCollider[tris];
                for (int i = 0; i < tris; i++)
                    TriangleTable[i] = ReadTriangle(reader);

                for (int i = 0; i < 65536; i++)
                    for (int j = 0; j < MaxTrisPerTerrainNode; j++)
                        TriangleIndex[i, j] = reader.ReadInt16();
                int squares = reader.ReadInt32();
                SquareTable = new SquareCollider[squares];
                for (int i = 0; i < squares; i++)
                    SquareTable[i] = ReadSquare(reader);
                for (int i = 0; i < 256; i++)
                    for (int j = 0; j < MaxSquaresPerTerrainNode; j++)
                        SquareIndex[i, j] = reader.ReadInt16();

            }
        }

        static short AppendSquare(Dictionary<SquareCollider, short> map, List<SquareCollider> squareTable, SquareCollider collider)
        {
            if (!map.ContainsKey(collider))
            {
                map[collider] = (short)squareTable.Count;
                squareTable.Add(collider);
            }
            return map[collider];
        }

        static void BakeTriangles()
        {
            for (int i = 0; i < 65536; i++)
            {
                for (int j = 0; j < MaxTrisPerTerrainNode; j++)
                    TriangleIndex[i, j] = -1;
            }
            Dictionary<TriangleCollider, short> tris = new Dictionary<TriangleCollider, short>();
            List<TriangleCollider> triangleTable = new List<TriangleCollider>();
            short counter = 0;

            for (int s = 0; s < ushort.MaxValue; s++)
            {
                ushort or = (ushort)(((s & 0xAAAA) >> 1) | (s & 0x5555));
                byte index = (byte)(
                        ((or & 0x4000) >> 7) |
                        ((or & 0x1000) >> 6) |
                        ((or & 0x0400) >> 5) |
                        ((or & 0x0100) >> 4) |
                        ((or & 0x0040) >> 3) |
                        ((or & 0x0010) >> 2) |
                        ((or & 0x0004) >> 1) |
                        (or & 0x0001)
                    );
                byte[] cubePoints = new byte[8];
                for (int j = 0; j < MarchingCubes.PointPositions.Length; j++)
                    cubePoints[j] = (byte)((s >> (j * 2)) & 0x03);
                byte[] current = MarchingCubes.megatable[index];

                for (int i = 0; i < current.Length; i += 3)
                {
                    Vector3 a = MarchingCubes.GetVertexPos(current[i], cubePoints[MarchingCubes.EdgePoints[current[i]].PointA], cubePoints[MarchingCubes.EdgePoints[current[i]].PointB]);
                    Vector3 b = MarchingCubes.GetVertexPos(current[i + 1], cubePoints[MarchingCubes.EdgePoints[current[i + 1]].PointA], cubePoints[MarchingCubes.EdgePoints[current[i + 1]].PointB]);
                    Vector3 c = MarchingCubes.GetVertexPos(current[i + 2], cubePoints[MarchingCubes.EdgePoints[current[i + 2]].PointA], cubePoints[MarchingCubes.EdgePoints[current[i + 2]].PointB]);
                    TriangleCollider collider = new TriangleCollider(a, b, c);
                    if (!tris.ContainsKey(collider))
                    {
                        tris[collider] = counter;
                        triangleTable.Add(collider);
                        TriangleIndex[s, i / 3] = counter;
                        counter++;
                    }
                    else
                    {
                        TriangleIndex[s, i / 3] = tris[collider];
                    }
                }
            }
            TriangleTable = triangleTable.ToArray();
        }

        static Vector3 Round(Vector3 number)
        {
            return new Vector3(Round(number.X), Round(number.Y), Round(number.Z));
        }

        static float Round(float number)
        {
            return (float)Math.Round((double)number, 3);
        }
    }
}
