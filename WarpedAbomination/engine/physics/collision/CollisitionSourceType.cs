﻿using System;

namespace WarpedAbominationEngine.Physics
{
    [Flags]
    public enum CollisionSourceType : byte
    {
        None = 0,
        Terrain = 1,
        Block = 2,
        Static = 4,
        Body = 8,
        PortalWall = 16,
        Sky = 32
    };
}
