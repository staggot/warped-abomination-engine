﻿using System;
using Microsoft.Xna.Framework;
using WarpedAbominationEngine.Content;

namespace WarpedAbominationEngine.Physics
{
    public struct CollisionSource
    {
        public float Distance;
        public Vector3 Normal;
        public CollisionSourceType Type;

        internal PhysicsBody Body;
        internal Sector Sector;
        internal Point3 SectorNode;
        internal int NodeIndex;
        internal StaticObject StaticMesh;
        internal Portal  Portal;

        public bool Touch 
        {
            get { return Distance <= AbominationSystem.PhysicsEpsilon * 4; }
        }

        #pragma warning disable RECS0025
        public override int GetHashCode()
        {
            int result = (int)(Type) << 12;
            if (Body != null)
                result ^= Body.GetHashCode();
            if (StaticMesh != null)
                result ^= StaticMesh.GetHashCode();
            if (Sector != null)
                result ^= Sector.GetHashCode();
            if (Portal != null)
                result ^= Portal.GetHashCode();
            return result  ^ SectorNode.GetHashCode() ^ (NodeIndex << 14);
        }
        #pragma warning restore RECS0025

        public override bool Equals(object obj)
        {
            if (!(obj is CollisionSource))
                return false;
            CollisionSource b = (CollisionSource)obj;
            return
                b.Type == Type &&
                 b.Sector == Sector &&
                 b.SectorNode == SectorNode &&
                 b.Body == Body &&
                 b.Portal == Portal &&
                 b.StaticMesh == StaticMesh &&
                 b.NodeIndex == NodeIndex;
        }

        public static bool operator ==(CollisionSource a, CollisionSource b)
        {
            return
                b.Type == a.Type &&
                 b.Sector == a.Sector &&
                 b.SectorNode == a.SectorNode &&
                 b.Body == a.Body &&
                 b.Portal == a.Portal &&
                 b.StaticMesh == a.StaticMesh &&
                 b.NodeIndex == a.NodeIndex;
        }

        public static bool operator !=(CollisionSource a, CollisionSource b)
        {
            return
                b.Type != a.Type ||
                 b.Sector != a.Sector ||
                 b.SectorNode != a.SectorNode ||
                 b.Body != a.Body ||
                 b.Portal != a.Portal ||
                 b.StaticMesh != a.StaticMesh ||
                 b.NodeIndex != a.NodeIndex;
        }
    }
}
