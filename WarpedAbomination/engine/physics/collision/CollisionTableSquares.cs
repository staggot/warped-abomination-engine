﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace WarpedAbominationEngine.Physics
{
    public static partial class CollisionTable
    {

        #pragma warning disable CS0675
        static void BakeSquares()
        {
            Dictionary<SquareCollider, short> squares = new Dictionary<SquareCollider, short>();
            List<SquareCollider> squareTable = new List<SquareCollider>();

            for (int i = 0; i < 256; i++)
            {
                for (int j = 0; j < MaxSquaresPerTerrainNode; j++)
                    SquareIndex[i, j] = -1;
            }

            for (int shape = 0; shape < 256; shape++)
            {
                int squareIndex = 0;
                for (int side = 0; side < MarchingCubes.blockShapePoints[shape].Length; side++)
                {
                    for (int square = 0; square < MarchingCubes.blockShapePoints[shape][side].Length; square += 4)
                    {
                        SquareIndex[shape, squareIndex] =

                            (short)(AppendSquare(squares, squareTable, new SquareCollider(
                            MarchingCubes.blockShapePoints[shape][side][square + 0],
                            MarchingCubes.blockShapePoints[shape][side][square + 1],
                            MarchingCubes.blockShapePoints[shape][side][square + 2],
                            MarchingCubes.blockShapePoints[shape][side][square + 3]
                        )) | ((int)(1 << side) << 8));

                        squareIndex += 1;
                    }
                }
            }
            SquareTable = squareTable.ToArray();
        }
        #pragma warning restore CS0675
    }
}
