﻿using System;
using Microsoft.Xna.Framework;

namespace WarpedAbominationEngine.Physics
{
    public class PhysicsBody : PortalLinkable
    {
        public Vector3 Speed;
        public float GravityFactor;

        Vector3 delta;
        Vector3 fixedDelta;

        Shape shape;
        public float GravitationFactor = 1;
        public float Mass = 1;
        public float Resitution = 0.5f;

        public float TargetSpeed = 0;
        public float MoveForce = 1;
        public Vector3 MoveDirection;
        public bool HorisontalMove = true;

        internal CollisionData CollisionData;

        public override Vector2 Size
        {
            get { return shape == null ? Vector2.Zero : shape.Size; }
            set { }
    }

        public PhysicsBody()
        {
            LinkType = PortalLinkType.PhysicsBody;
            CollisionData = new CollisionData {
                CollidesWith =
                CollisionSourceType.Block |
                CollisionSourceType.PortalWall |
                CollisionSourceType.Static |
                CollisionSourceType.Terrain
            };
            IsChildLinksEnabled = shape != null;
        }

        public Shape Shape
        {
            get { return shape; }
            set
            {
                shape = value;
                IsChildLinksEnabled = shape != null;
            }
        }

        public bool ThingCollision {
            get {
                return (CollisionData.CollidesWith & CollisionSourceType.Body) > 0;
            }
            set {
                if (value) {
                    CollisionData.CollidesWith |= CollisionSourceType.Body;
                } else {
                    CollisionData.CollidesWith &= ~CollisionSourceType.Body;
                }
            }
        }

        void ApplyMovement(float time)
        {
            Vector3 currentMoveSpeed = Speed;
            if (HorisontalMove)
                currentMoveSpeed.Y = 0;
            Vector3 moveVector = MoveDirection * MoveForce;
            Vector3 targetSpeedVector = TargetSpeed * MoveDirection;
            Vector3 speedDiff = targetSpeedVector - Speed;
            Vector3 speedDiffNormal = MathHelper.Normalize(speedDiff, out float speedDiffFactor);
            float needTime = speedDiffFactor / MoveForce;
            Vector3 speedAdder = Vector3.Zero;
            if (needTime > time)
            {
                speedAdder = speedDiffNormal * MoveForce * time;
            }
            else
            {
                speedAdder = speedDiff;
            }
            Speed += speedAdder;
        }

        internal void CalculateSpeed(float time)
        {
            Vector3 naturalForce = Scene.Gravitation * GravitationFactor;
            Vector3 realSpeedNormal = MathHelper.Normalize(Speed, out _);
            ApplyMovement(time);
            Speed += naturalForce * time;
        }

        internal void DetectCollision(float time)
        {
            CollisionData.FillBodyData(this, delta);
            for (int i = 0; i < ChildLinks.Length; i++)
                fixedDelta = CollisionData.CalculateDelta(delta);
            float deltaMatch = Vector3.Dot(delta, fixedDelta);
        }

        protected override void Teleport(Portal portal)
        {
        }
    }
}
