﻿using System;
using Microsoft.Xna.Framework;
using WarpedAbominationEngine.Content;

namespace WarpedAbominationEngine.Physics
{
    public struct TriangleCollider
    {
        public Vector3 A, B, C;
        public readonly Vector3 Normal;
        public readonly Vector3 NormalAB, NormalBC, NormalCA;

        public TriangleCollider(Vector3 a, Vector3 b, Vector3 c)
        {
            A = a;
            B = b;
            C = c;
           
            Normal = MathHelper.GetTriangleNormal(A, B, C);
            NormalAB = Vector3.Normalize(Vector3.Cross(B - A, Normal));
            NormalBC = Vector3.Normalize(Vector3.Cross(C - B, Normal));
            NormalCA = Vector3.Normalize(Vector3.Cross(A - C, Normal));
        }
        public TriangleCollider(
            Vector3 a,
            Vector3 b,
            Vector3 c,
            Vector3 normal,
            Vector3 normalAB,
            Vector3 normalBC,
            Vector3 normalCA)
        {
            this.A = a;
            this.B = b;
            this.C = c;
            this.Normal = normal;
            this.NormalAB = normalAB;
            this.NormalBC = normalBC;
            this.NormalCA = normalCA;
        }

        public float PlaneDistance(Vector3 point)
        {
            return MathHelper.PlaneDistance(Normal, A, point);
        }

        public float DistAB(Vector3 point)
        {
            return MathHelper.PlaneDistance(NormalAB, A, point);
        }

        public float DistBC(Vector3 point)
        {
            return MathHelper.PlaneDistance(NormalBC, B, point);
        }

        public float DistCA(Vector3 point)
        {
            return MathHelper.PlaneDistance(NormalCA, C, point);
        }

        public override int GetHashCode()
        {
            return
                A.GetHashCode() +
                B.GetHashCode() +
                C.GetHashCode();
        }


        public override bool Equals(object obj)
        {
            if (!(obj is TriangleCollider))
                return false;
            return this == (TriangleCollider)obj;
        }

        static float Rounded(float a)
        {
            return (float)Math.Round((double)a, 2);
        }

        static Vector3 Rounded(Vector3 a)
        {
            return new Vector3((float)Math.Round((double)a.X, 2),
                               (float)Math.Round((double)a.Y, 2),
                               (float)Math.Round((double)a.Z, 2));
        }

        public static bool operator ==(TriangleCollider a, TriangleCollider b)
        {
            return
                (a.A == b.A && a.B == b.B && a.C == b.C) ||
                (a.A == b.B && a.B == b.C && a.C == b.A) ||
                (a.A == b.C && a.B == b.A && a.C == b.B);
        }

        public static bool operator !=(TriangleCollider a, TriangleCollider b)
        {
            return !(a == b);
        }

        public bool RayCast(Vector3 position, Vector3 rayOrigin, Vector3 rayDirection, out float distance, out Vector3 normal)
        {
            
            normal = Vector3.Zero;
            distance = float.PositiveInfinity;
            Vector3 rayPos = rayOrigin - position;

            float d = Vector3.Dot(-rayDirection, Normal);
            if (d < -AbominationSystem.PhysicsEpsilon)
                return false;
            float t = MathHelper.PlaneDistance(Normal, A, rayPos) / d;

            if (t < -AbominationSystem.PhysicsEpsilon)
                return false;
            Vector3 P = rayPos + t * rayDirection;

            if (DistAB(P) < -AbominationSystem.PhysicsEpsilon || DistBC(P) < -AbominationSystem.PhysicsEpsilon || DistCA(P) < -AbominationSystem.PhysicsEpsilon)
                return false;
            normal = Normal;
            distance = t;
            return true;
        }

        public static TriangleCollider Lerp(TriangleCollider a, TriangleCollider b, float t)
        {
            return new TriangleCollider(
                Vector3.Lerp(a.A, b.A, t),
                Vector3.Lerp(a.B, b.B, t),
                Vector3.Lerp(a.C, b.C, t),
                Vector3.Lerp(a.Normal, b.Normal, t),
                Vector3.Lerp(a.NormalAB, b.NormalAB, t),
                Vector3.Lerp(a.NormalBC, b.NormalBC, t),
                Vector3.Lerp(a.NormalCA, b.NormalCA, t));
        }
    }
}
