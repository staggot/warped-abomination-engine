﻿using System;
using Microsoft.Xna.Framework;
using WarpedAbominationEngine.Content;

namespace WarpedAbominationEngine.Physics
{
    public struct SquareCollider
    {
        public Vector3 A, B, C, D;
        public readonly Vector3 Normal;
        public readonly Vector3 NormalAB, NormalBC, NormalCD, NormalDA;

        public SquareCollider(Vector3 a, Vector3 b, Vector3 c, Vector3 d)
        {
            A = a;
            B = b;
            C = c;
            D = d;
            Normal = MathHelper.GetTriangleNormal(a, b, c);
            NormalAB = Vector3.Normalize(Vector3.Cross(B - A, Normal));
            NormalBC = Vector3.Normalize(Vector3.Cross(C - B, Normal));
            NormalCD = Vector3.Normalize(Vector3.Cross(D - C, Normal));
            NormalDA = Vector3.Normalize(Vector3.Cross(A - D, Normal));
        }
        public SquareCollider(
            Vector3 a,
            Vector3 b,
            Vector3 c,
            Vector3 d,
            Vector3 normal,
            Vector3 normalAB,
            Vector3 normalBC,
            Vector3 normalCD,
            Vector3 normalDA)
        {
            this.A = a;
            this.B = b;
            this.C = c;
            this.D = d;
            this.Normal = normal;
            this.NormalAB = normalAB;
            this.NormalBC = normalBC;
            this.NormalCD = normalCD;
            this.NormalDA = normalDA;
        }

        public float PlaneDistance(Vector3 point)
        {
            return MathHelper.PlaneDistance(Normal, A, point);
        }

        public float DistAB(Vector3 point)
        {
            return MathHelper.PlaneDistance(NormalAB, A, point);
        }

        public float DistBC(Vector3 point)
        {
            return MathHelper.PlaneDistance(NormalBC, B, point);
        }

        public float DistCD(Vector3 point)
        {
            return MathHelper.PlaneDistance(NormalCD, C, point);
        }

        public float DistDA(Vector3 point)
        {
            return MathHelper.PlaneDistance(NormalDA, D, point);
        }

        public override int GetHashCode()
        {
            return
                A.GetHashCode() +
                B.GetHashCode() +
                C.GetHashCode() +
                 D.GetHashCode();
        }


        public override bool Equals(object obj)
        {
            if (!(obj is SquareCollider))
                return false;
            return this == (SquareCollider)obj;
        }

        static float Rounded(float a)
        {
            return (float)Math.Round((double)a, 2);
        }

        static Vector3 Rounded(Vector3 a)
        {
            return new Vector3((float)Math.Round((double)a.X, 2),
                               (float)Math.Round((double)a.Y, 2),
                               (float)Math.Round((double)a.Z, 2));
        }

        public static bool operator ==(SquareCollider a, SquareCollider b)
        {
            return
                (a.A == b.A && a.B == b.B && a.C == b.C && a.D == b.D) ||
                (a.A == b.B && a.B == b.C && a.C == b.D && a.D == b.A) ||
                (a.A == b.C && a.B == b.D && a.C == b.A && a.D == b.B) ||
                (a.A == b.D && a.B == b.A && a.C == b.B && a.D == b.C)
                ;
        }

        public static bool operator !=(SquareCollider a, SquareCollider b)
        {
            return !(a == b);
        }

        public bool RayCast(Vector3 position, Vector3 rayOrigin, Vector3 rayDirection, out float distance, out Vector3 normal)
        {

            normal = Vector3.Zero;
            distance = float.PositiveInfinity;
            Vector3 rayPos = rayOrigin - position;

            float d = Vector3.Dot(-rayDirection, Normal);
            if (d < -AbominationSystem.PhysicsEpsilon)
                return false;
            float t = MathHelper.PlaneDistance(Normal, A, rayPos) / d;

            if (t < -AbominationSystem.PhysicsEpsilon)
                return false;
            Vector3 P = rayPos + t * rayDirection;

            if (DistAB(P) < -AbominationSystem.PhysicsEpsilon ||
                DistBC(P) < -AbominationSystem.PhysicsEpsilon ||
                DistCD(P) < -AbominationSystem.PhysicsEpsilon ||
                DistDA(P) < -AbominationSystem.PhysicsEpsilon)
                return false;
            normal = Normal;
            distance = t;
            return true;
        }

        public static SquareCollider Lerp(SquareCollider a, SquareCollider b, float t)
        {
            return new SquareCollider(
                Vector3.Lerp(a.A, b.A, t),
                Vector3.Lerp(a.B, b.B, t),
                Vector3.Lerp(a.C, b.C, t),
                Vector3.Lerp(a.D, b.D, t),
                Vector3.Lerp(a.Normal, b.Normal, t),
                Vector3.Lerp(a.NormalAB, b.NormalAB, t),
                Vector3.Lerp(a.NormalBC, b.NormalBC, t),
                Vector3.Lerp(a.NormalCD, b.NormalCD, t),
                Vector3.Lerp(a.NormalDA, b.NormalDA, t));
        }
    }
}
