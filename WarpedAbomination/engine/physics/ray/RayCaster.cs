﻿using System;
using Microsoft.Xna.Framework;

#pragma warning disable RECS0018
namespace WarpedAbominationEngine.Physics
{
    public abstract class RayCaster
    {
        public readonly Vector3 OriginPosition;
        public readonly Vector3 OriginDirection;
        public readonly Sector OriginSector;
        public readonly Point3 OriginNode;
        public readonly float MaxDistance;
        public readonly int MaxPortals;
        protected CollisionSourceType IntersectsWith;
        int portalPassed;
        public CollisionSourceType HitWith;
        public Sector HitSector;
        public Vector3 HitPosition;
        public Vector3 HitNormal;
        public float Distance;



        internal RayCaster(Sector originSector, Vector3 originPosition, Point3 originNode, Vector3 originDirection, float maxDistance, int maxPortals)
        {
            OriginNode = originNode;
            portalPassed = 0;
            this.MaxDistance = maxDistance;
            this.MaxPortals = maxPortals;
            this.OriginSector = originSector;
            this.OriginPosition = originPosition;
            this.OriginDirection = Vector3.Normalize(originDirection);

        }

        public void Cast()
        {

            Vector3 currentPosition = OriginPosition;
            Point3 currentNode = OriginNode;
            Vector3 startPosition = OriginPosition;
            Sector currentSector = OriginSector;
            if (currentNode.Z >= currentSector.Size.Z)
                currentNode.Z = currentSector.Size.Z - 1;
            if (currentNode.Y >= currentSector.Size.Y)
                currentNode.Y = currentSector.Size.Y - 1;
            if (currentNode.X >= currentSector.Size.X)
                currentNode.X = currentSector.Size.X - 1;
            Vector3 currentDirection = OriginDirection;
            float currentLength = 0;
            while (true) {
                SectorSide portalHitSide;
                Point2 portalHitPosition;
                Vector3 portalHitAbsolutePosition;

                Point3 step = new Point3();
                if (currentDirection.X == 0)
                    step.X = 0;
                else if (currentDirection.X > 0)
                    step.X = 1;
                else if (currentDirection.X < 0)
                    step.X = -1;

                if (currentDirection.Y == 0)
                    step.Y = 0;
                else if (currentDirection.Y > 0)
                    step.Y = 1;
                else if (currentDirection.Y < 0)
                    step.Y = -1;

                if (currentDirection.Z == 0)
                    step.Z = 0;
                else if (currentDirection.Z > 0)
                    step.Z = 1;
                else if (currentDirection.Z < 0)
                    step.Z = -1;

                Vector3 delta = new Vector3(1f / currentDirection.X, 1f / currentDirection.Y, 1f / currentDirection.Z);

                while (true) {

                    Vector3 dists = new Vector3(float.PositiveInfinity, float.PositiveInfinity, float.PositiveInfinity);
                    Vector3 distLength = new Vector3(float.PositiveInfinity, float.PositiveInfinity, float.PositiveInfinity);
                    if (step.X > 0) {
                        dists.X = currentNode.X + 1f - currentPosition.X;
                        distLength.X = dists.X / currentDirection.X;
                    } else if (step.X < 0) {
                        dists.X = currentPosition.X - currentNode.X;
                        distLength.X = dists.X / -currentDirection.X;
                    }
                    if (step.Y > 0) {
                        dists.Y = currentNode.Y + 1f - currentPosition.Y;
                        distLength.Y = dists.Y / currentDirection.Y;
                    } else if (step.Y < 0) {
                        dists.Y = currentPosition.Y - currentNode.Y;
                        distLength.Y = dists.Y / -currentDirection.Y;
                    }
                    if (step.Z > 0) {
                        dists.Z = currentNode.Z + 1f - currentPosition.Z;
                        distLength.Z = dists.Z / currentDirection.Z;
                    } else if (step.Z < 0) {
                        dists.Z = currentPosition.Z - currentNode.Z;
                        distLength.Z = dists.Z / -currentDirection.Z;
                    }

                    float stepLength = 0;
                    Point3 stepNode = Point3.Zero;

                    if (distLength.X <= distLength.Y && distLength.X <= distLength.Z) {
                        stepNode.X = step.X;
                        stepLength = distLength.X;
                    } else if (distLength.Y <= distLength.X && distLength.Y <= distLength.Z) {
                        stepNode.Y = step.Y;
                        stepLength = distLength.Y;
                    } else if (distLength.Z <= distLength.X && distLength.Z <= distLength.Y) {
                        stepNode.Z = step.Z;
                        stepLength = distLength.Z;
                    }

                    int i = 0;
                    short tIndex = 0;
                    NodeShape gNode = currentSector.Terrain.GetNodeShape(currentNode);
                    PassNode(currentSector, currentNode, currentDirection);
                    while (
                            (IntersectsWith & CollisionSourceType.Terrain) > 0 &&
                            (tIndex = CollisionTable.TriangleIndex[gNode.TerrainShapeData, i++]) >= 0
                        ) {

                        if (CollisionTable.TriangleTable[tIndex].RayCast(
                            currentNode.Vector,
                            currentPosition,
                            currentDirection,
                            out float distance,
                            out Vector3 normal)) {
                            if (currentLength + distance > MaxDistance)
                                continue;
                            Distance = currentLength + distance;
                            HitPosition = currentPosition + distance * currentDirection;
                            HitNormal = normal;
                            HitSector = currentSector;
                            HitWith = CollisionSourceType.Terrain;
                            return;

                        }
                    }
                    if (currentLength + stepLength >= MaxDistance) {
                        Distance = MaxDistance;
                        HitNormal = -currentDirection;
                        HitPosition = (stepLength - (MaxDistance - (currentLength + stepLength))) * currentDirection;
                        HitSector = currentSector;
                        HitWith = CollisionSourceType.None;
                        return;
                    }
                    currentLength += stepLength;
                    currentPosition += stepLength * currentDirection;
                    currentNode += stepNode;

                    if (currentNode.X < 0 && step.X < 0) {
                        portalHitSide = SectorSide.Left;
                        portalHitPosition = new Point2(currentNode.Z, currentNode.Y);
                        portalHitAbsolutePosition = currentPosition;
                        break;
                    }

                    if (currentNode.Y < 0 && step.Y < 0) {
                        portalHitSide = SectorSide.Bottom;
                        portalHitPosition = new Point2(currentNode.X, currentNode.Z);
                        portalHitAbsolutePosition = currentPosition;
                        break;
                    }
                    if (currentNode.Z < 0 && step.Z < 0) {
                        portalHitSide = SectorSide.Fore;
                        portalHitPosition = new Point2(currentNode.X, currentNode.Y);
                        portalHitAbsolutePosition = currentPosition;
                        break;
                    }
                    if (currentNode.X >= currentSector.Size.X && step.X > 0) {
                        portalHitSide = SectorSide.Right;
                        portalHitPosition = new Point2(currentNode.Z, currentNode.Y);
                        portalHitAbsolutePosition = currentPosition;
                        break;
                    }
                    if (currentNode.Y >= currentSector.Size.Y && step.Y > 0) {
                        portalHitSide = SectorSide.Top;
                        portalHitPosition = new Point2(currentNode.X, currentNode.Z);
                        portalHitAbsolutePosition = currentPosition;
                        break;
                    }
                    if (currentNode.Z >= currentSector.Size.Z && step.Z > 0) {
                        portalHitSide = SectorSide.Back;
                        portalHitPosition = new Point2(currentNode.X, currentNode.Y);
                        portalHitAbsolutePosition = currentPosition;
                        break;
                    }

                }

                Portal portal = currentSector.Portals.Get(
                    portalHitSide,
                    portalHitPosition
                );


                if (portal != null) {
                    if ((IntersectsWith & CollisionSourceType.PortalWall) > 0 && portal.Solid) {
                        HitWith = CollisionSourceType.PortalWall;
                        Distance = (portalHitAbsolutePosition - startPosition).Length();
                        HitSector = currentSector;
                        HitNormal = portal.Side.GetNormal();
                        HitPosition = portalHitAbsolutePosition;
                        return;
                    } else {
                        if (portalPassed >= MaxPortals) {
                            HitWith = CollisionSourceType.None;
                            Distance = (portalHitAbsolutePosition - startPosition).Length();
                            HitSector = currentSector;
                            HitNormal = portal.Side.GetNormal();
                            HitPosition = portalHitAbsolutePosition;
                            return;
                        }
                        portalPassed++;
                        PassPortal(portal, currentPosition);
                        currentNode = portal.TransformNodePositionTo(currentNode);
                        currentPosition = portal.TransformPositionTo(currentPosition);
                        currentDirection = portal.TransformDirectionTo(currentDirection);
                        currentSector = portal.OutSector;
                        startPosition = portal.TransformPositionTo(startPosition);
                        continue;
                    }

                }
                Distance = float.PositiveInfinity;
                HitWith = CollisionSourceType.Sky;
                HitNormal = portalHitSide.GetNormal(); HitPosition = portalHitAbsolutePosition;
                HitSector = currentSector;
                HitPosition = portalHitAbsolutePosition;
                return;
            }
        }

        protected abstract void PassNode(Sector sector, Point3 nodePosition, Vector3 direction);
        protected abstract void PassPortal(Portal portal, Vector3 position);

        public static void CastToWall(Sector sector, Vector3 position, Vector3 direction, out SectorSide hitSide, out Vector2 sidePosition, out Vector3 absolutePosition)
        {
            Vector3 pos;
            sidePosition = new Vector2(0, 0);
            hitSide = SectorSide.Left;
            absolutePosition = new Vector3(0, 0, 0);

            if (direction.X < 0) {
                pos = position + direction * (position.X / -direction.X);

                if (pos.Z >= 0 && pos.Z <= sector.Size.Z && pos.Y >= 0 && pos.Y <= sector.Size.Y) {
                    sidePosition = new Vector2(pos.Z, pos.Y);
                    hitSide = SectorSide.Left;
                    absolutePosition = new Vector3(0, pos.Y, pos.Z);
                    return;
                }
            } else if (direction.X > 0) {
                pos = position + direction * ((sector.Size.X - position.X) / direction.X);
                if (pos.Z >= 0 && pos.Z <= sector.Size.Z && pos.Y >= 0 && pos.Y <= sector.Size.Y) {
                    sidePosition = new Vector2(pos.Z, pos.Y);
                    hitSide = SectorSide.Right;
                    absolutePosition = new Vector3(sector.Size.X, pos.Y, pos.Z);
                    return;
                }
            }
            if (direction.Y < 0) {
                pos = position + direction * (position.Y / -direction.Y);
                if (pos.X >= 0 && pos.X <= sector.Size.X && pos.Z >= 0 && pos.Z <= sector.Size.Z) {
                    sidePosition = new Vector2(pos.X, pos.Z);
                    hitSide = SectorSide.Bottom;
                    absolutePosition = new Vector3(pos.X, 0, pos.Z);
                    return;
                }
            } else if (direction.Y > 0) {
                pos = position + direction * ((sector.Size.Y - position.Y / direction.Y));
                if (pos.X >= 0 && pos.Z >= 0 && pos.X <= sector.Size.X && pos.Z <= sector.Size.Z) {
                    sidePosition = new Vector2(pos.X, pos.Z);
                    hitSide = SectorSide.Top;
                    absolutePosition = new Vector3(pos.X, sector.Size.Y, pos.Z);
                    return;
                }
            }

            if (direction.Z < 0) {
                pos = position + direction * (position.Z / -direction.Z);

                if (pos.X >= 0 && pos.X <= sector.Size.X && pos.Y >= 0 && pos.Y <= sector.Size.Y) {
                    sidePosition = new Vector2(pos.X, pos.Y);
                    hitSide = SectorSide.Fore;
                    absolutePosition = new Vector3(pos.X, pos.Y, 0);
                    return;
                }
            } else if (direction.Z > 0) {
                pos = position + direction * ((sector.Size.Z - position.Z) / direction.Z);
                if (pos.X >= 0 && pos.X <= sector.Size.X && pos.Y >= 0 && pos.Y <= sector.Size.Y) {
                    sidePosition = new Vector2(pos.X, pos.Y);
                    hitSide = SectorSide.Back;
                    absolutePosition = new Vector3(pos.X, pos.Y, sector.Size.Z);
                    return;
                }
            }
            return;
        }

    }
}
