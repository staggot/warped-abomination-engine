﻿using System;
namespace WarpedAbominationEngine
{
    public class WarpedException : Exception
    {
        public WarpedException(string message) : base(message)
        {
        }
    }
}
