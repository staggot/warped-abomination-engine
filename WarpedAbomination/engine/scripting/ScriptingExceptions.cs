﻿using System;
namespace WarpedAbominationEngine
{
    public class StageTimeoutExceeded : Exception
    {
        public readonly WarpState State;

        public StageTimeoutExceeded(WarpState state) : base(
            string.Format("State machine stuck on state: {0}", state == null ? "null" : state.ToString()))
        {
            State = state;
        }
    }
}
