﻿using System;
using System.Collections.Generic;
using System.Collections;
using qsldotnet;
using qsldotnet.lib;
using Microsoft.Xna.Framework;

namespace WarpedAbominationEngine.SceneCore
{
    public class SceneScripting : IDisposable
    {
        static readonly Type[] allowedTypes = {
            typeof(Scene),
            typeof(SceneStorage),

            typeof(Portal),
            typeof(WarpPortal),
            typeof(MirrorPortal),
            typeof(EuclidPortal),
            typeof(PortalPair),
            typeof(SectorSide),
            typeof(Sector),
            typeof(SectorID),
            typeof(Region),
            typeof(WarpState),
            typeof(Dictionary<string, object>),
            typeof(IDictionary<string, object>),
            typeof(IDictionary),
            typeof(MathHelper),
            typeof(Vector2),
            typeof(Vector3),
            typeof(Vector4),
            typeof(Color),
            typeof(Point2),
            typeof(Point3),
            typeof(Point4),

        };
        public static readonly QEngine Engine;

        static SceneScripting()
        {
            Engine = new QEngine(
                    QSecurityFlags.ForbidAssembly |
                    QSecurityFlags.ForbidHttp |
                    QSecurityFlags.ForbidIO |
                    QSecurityFlags.ForbidLogging |
                    QSecurityFlags.ForbidNet |
                    QSecurityFlags.ForbidSystem |
                    QSecurityFlags.ForbidTheading |
                    QSecurityFlags.StrictTypes
                );
            foreach(Type t in allowedTypes)
                Engine.AllowType(t);
        }

        readonly Scene scene;
        public QContext SceneContext { get; private set; }

        public SceneScripting(Scene scene)
        {
            this.scene = scene;
            SceneContext = Engine.CreateContext();
            SceneContext["scene"] = scene;
        }

        public void Dispose()
        {
        }
    }
}
