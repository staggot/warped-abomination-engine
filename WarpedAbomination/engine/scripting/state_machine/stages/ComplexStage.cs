﻿using System;
using System.Collections.Generic;

namespace WarpedAbominationEngine
{
    public class ComplexStage : WarpedStage
    {
        readonly string[] Stages;

        public ComplexStage(string name, WarpedStateMachine machine, params string[] stages) : base(name, machine)
        {
            Machine = machine;
            Stages = stages;
        }

        internal override WarpState Handle(WarpState state, float time)
        {
            if (state.StagesQueue == null)
            {
                state.StagesQueue = new Queue<string>();
                for (int i = 0; i < Stages.Length; i++)
                    state.StagesQueue.Enqueue(Stages[i]);
            }
            WarpState newSubState = null;
            
            if(state.SubState != null)
                newSubState = Machine.Stages[state.SubState.Stage].Handle(state.SubState, time);

            if (newSubState == null && state.StagesQueue.Count > 0)
            {
                string nextStage = state.StagesQueue.Dequeue();
                newSubState = new WarpState(nextStage, state.Parameters, state.Actor);
            }

            if(newSubState != null)
            {
                state.SubState = newSubState;
                return state;
            }
            else
            {
                return null;
            }
        }
    }
}
