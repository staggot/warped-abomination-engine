﻿using System;
namespace WarpedAbominationEngine
{
    public class SleepStage : WarpedStage
    {
        const string SleepTimeParameter = "sleepTime";

        public SleepStage(string name, WarpedStateMachine machine): base(name, machine)
        {
        }

        internal override WarpState Handle(WarpState state, float time)
        {
            if (state == null || state.Parameters == null || !state.Parameters.ContainsKey(SleepTimeParameter))
                return null;
            float sleepTime = Convert.ToSingle(state.Parameters[SleepTimeParameter]);
            sleepTime -= time;
            if (sleepTime <= 0)
            {
                state.Parameters.Remove(SleepTimeParameter);
                return null;
            }
            state.Parameters[SleepTimeParameter] = sleepTime;
            return state;
        }
    }
}
