﻿using System;
namespace WarpedAbominationEngine
{
    public abstract class WarpedStage
    {
        public readonly string Name;
        public WarpedStateMachine Machine;

        internal WarpedStage(string name, WarpedStateMachine machine)
        {
            Name = name;
            Machine = machine;
        }
        internal abstract WarpState Handle(WarpState state, float time);
    }
}
