﻿using System;
using System.Collections.Generic;
using qsldotnet;

namespace WarpedAbominationEngine
{
    public class ScriptedStage : WarpedStage
    {
        const string StateArg = "state";
        const string StageArg = "stage";
        const string TimeArg = "time";

        public string Source { get; private set; }
        readonly QEngine engine;
        readonly QFunction handleFunction;

        public ScriptedStage(string name, WarpedStateMachine machine, string scriptSource, bool compile = true): base(name, machine)
        {
            engine = machine.ScriptEngine;
            Source = scriptSource;
            if (compile)
                handleFunction = engine.Compile(scriptSource);
        }

        public ScriptedStage(string name, WarpedStateMachine machine, QFunction handler) : base (name, machine)
        {
            Source = null;
            handleFunction = handler;
        }

        internal override WarpState Handle(WarpState state, float time)
        {
            Dictionary<string, object> arguments = new Dictionary<string, object>();
            foreach (KeyValuePair<string, object> kv in state.Parameters)
                arguments[kv.Key] = kv.Value;
            arguments[StateArg] = state;
            arguments[StageArg] = state.Stage;
            arguments[TimeArg] = time;
            object result = null;

            if(handleFunction != null)
                result = handleFunction(new FunctionArguments(arguments));
            else if(Source != null)
                result = engine.Eval(Source, arguments);
            if (result is WarpState)
                return result as WarpState;
            else if (result is string)
                return new WarpState(result as string, state);
            else
                return null;
        }
    }
}