﻿using System;
using qsldotnet;
using System.Collections.Generic;

namespace WarpedAbominationEngine
{
    public class UserStage : WarpedStage
    {
        const string StateArg = "state";
        const string StageArg = "stage";
        const string TimeArg = "time";
        const string SourceParameter = "script";

        QEngine engine;

        public UserStage(string name, WarpedStateMachine machine, QEngine engine) : base(name, machine)
        {
            this.engine = engine;
        }

        internal override WarpState Handle(WarpState state, float time)
        {
            Dictionary<string, object> arguments = new Dictionary<string, object>();
            foreach (var kv in state.Parameters)
                arguments[kv.Key] = kv.Value;
            string source = (string)state.Parameters[SourceParameter];
            arguments[StateArg] = state;
            arguments[StageArg] = state.Stage;
            arguments[TimeArg] = time;
            object result = engine.Eval(source, arguments);
            if (result is WarpState)
                return result as WarpState;
            else if (result is string)
                return new WarpState(result as string, state);
            else
                return null;
        }
    }
}
