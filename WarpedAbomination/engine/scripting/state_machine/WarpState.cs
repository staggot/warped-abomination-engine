﻿using System;
using System.Text;
using System.Collections.Generic;

namespace WarpedAbominationEngine
{
    public class WarpState
    {
        public string Stage;
        internal WarpState SubState;
        internal Queue<string> StagesQueue;
        public IDictionary<string, object> Parameters;
        public ISceneActor Actor;

        public WarpState(string stageName, IDictionary<string, object> parameters = null, ISceneActor target = null, WarpState subState = null)
        {
            SubState = subState;
            Stage = stageName;
            Parameters = parameters;
            if (Parameters == null)
                Parameters = new Dictionary<string, object>();
            Actor = target;
        }


        public WarpState(string stageName, WarpState oldState)
        {
            Stage = stageName;
            SubState = oldState.SubState;
            Parameters = oldState.Parameters;
            Actor = oldState.Actor;
            if (Parameters == null)
                Parameters = new Dictionary<string, object>();
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(string.Format("Stage:  {0}", Stage ?? "null"));
            builder.AppendLine(string.Format("Target: {0}", Actor == null ? "null": Actor.ToString()));
            builder.AppendLine("Parameters:");
            foreach(KeyValuePair<string, object> kv in Parameters)
            {
                builder.AppendLine(string.Format("{0}: {1}",
                    kv.Key, kv.Value ?? "null"));
            }
            if (StagesQueue != null)
            {
                builder.AppendLine("Queue: ");
                foreach(string stage in StagesQueue)
                    builder.Append(stage ?? "null");
            }

            if (SubState != null)
            {
                builder.AppendLine("Sub State: ");
                builder.AppendLine("    " + SubState.ToString().Replace("\n", "\n    "));
                foreach (string stage in StagesQueue)
                    builder.Append(stage ?? "null");
            }
            return builder.ToString();
        }

        public WarpState Backup()
        {
            Dictionary<string, object> clonedParameters = new Dictionary<string, object>();
            foreach (KeyValuePair<string, object> kv in Parameters)
                clonedParameters.Add(kv.Key, kv.Value);

            Queue<string> newQueue = null;
            if (StagesQueue != null)
                foreach (string stage in StagesQueue)
                    newQueue.Enqueue(stage);
            return new WarpState(Stage, clonedParameters, Actor, SubState == null ? null : SubState.Backup())
            {
                StagesQueue = newQueue
            };

        }
    }
}
