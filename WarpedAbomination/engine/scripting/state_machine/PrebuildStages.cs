﻿using System;
namespace WarpedAbominationEngine
{
    static class PrebuildStages
    {
        const string SleepStageName = "sleep";

        internal static void Init(WarpedStateMachine machine)
        {
            machine.AddStage(SleepStageName, new SleepStage(SleepStageName, machine));
        }
    }
}
