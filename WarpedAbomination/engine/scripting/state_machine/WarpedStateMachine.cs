﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Collections.Concurrent;
using qsldotnet;
using WarpedAbominationEngine.SceneCore;

namespace WarpedAbominationEngine
{
    public class WarpedStateMachine : IDisposable, IWarpSaveState, IPausable
    {
        const float MaxProcessingTime = 10f;

        readonly Scene scene;
        public readonly QEngine ScriptEngine;
        internal readonly Dictionary<string, WarpedStage> Stages = new Dictionary<string, WarpedStage>();
        readonly ConcurrentQueue<WarpState> smQueue = new ConcurrentQueue<WarpState>();
        readonly Semaphore semaphore;
        readonly Thread smThread;
        volatile float lastTime;
        volatile bool isPaused = false;
        volatile bool isAlive;
        volatile bool isWorking = false;
        WarpState currentState = null;


        public bool IsAlive {
            get { return isAlive; }
        }

        public bool IsPaused {
            get { return isPaused; }
        }

        public WarpedStateMachine(Scene scene, QEngine scriptEngine)
        {
            this.scene = scene;
            this.ScriptEngine = scriptEngine;
            this.semaphore = new Semaphore(0, 1);
            isAlive = true;
            PrebuildStages.Init(this);
            smThread = new Thread(new ThreadStart(ThreadMethod));
            smThread.Start();
        }

        void ThreadMethod()
        {
            while (isAlive)
            {
                semaphore.WaitOne();
                isWorking = true;
                if (!isPaused)
                    UpdateStates(lastTime);
                lastTime = 0f;
                isWorking = false;
            }
        }

        public void AddStage(string name, string scriptHandler)
        {
            Stages[name] = new ScriptedStage(name, this, scriptHandler, true);
        }

        public void AddStage(string name, QFunction qHandler)
        {
            Stages[name] = new ScriptedStage(name, this, qHandler);
        }

        public void AddStage(string name, WarpedStage stage)
        {
            Stages[name] = stage;
        }

        void UpdateStates(float time)
        {
            for (int i = smQueue.Count; i > 0 && isAlive; i--)
            {
                WarpState state;
                while (!smQueue.TryDequeue(out state))
                {
                    if (smQueue.Count == 0)
                        return;
                    Thread.Yield();
                }
                WarpState backup = state.Backup();
                try
                {
                    ProcessState(state, time);
                }
                catch (Exception ex)
                {
                    scene.Logger.Error(string.Format(
                        "State machine error while processing state: {0}",
                        state == null ? "null" : state.ToString()
                        ), ex);
                }
            }
        }

        internal void Update(float time)
        {
            if (lastTime >= MaxProcessingTime)
                throw new StageTimeoutExceeded(currentState);
            lastTime += time;
            if (!isWorking)
                semaphore.Release();
        }

        void ProcessState(WarpState state, float time)
        {
            WarpState newState;
            currentState = state;
            if (Stages.ContainsKey(state.Stage)) {
                WarpedStage stage = Stages[state.Stage];
                newState = stage.Handle(state, time);
            } else {
                throw new StageTimeoutExceeded(currentState);
            }
            if (state.Actor != null && !state.Actor.IsAlive)
                return;
            if (newState != null && newState.Stage != null) {
                if (newState.Parameters == null)
                    newState.Parameters = new Dictionary<string, object>();
                smQueue.Enqueue(newState);
            }
        }

        public void Pause()
        {
            if (isPaused)
                return;
        }

        public void Resume()
        {
            if (!isPaused)
                return;
        }

        public void Dispose()
        {
            isAlive = false;
        }

        public void Save(WarpSaveStream stream)
        {
            throw new NotImplementedException();
        }

        public void Load(WarpLoadStream stream)
        {
            throw new NotImplementedException();
        }
    }
}
