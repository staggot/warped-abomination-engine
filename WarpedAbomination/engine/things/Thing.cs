﻿using System;
using Microsoft.Xna.Framework;
using WarpedAbominationEngine.Physics;

namespace WarpedAbominationEngine
{
    public class Thing: PortalLinkable
    {
        public ushort ID;
        public EntityMesh Mesh;
        public PhysicsBody Body;
        public float Yaw, Pitch, Roll;

        public Region Region
        {
            get { return Sector.Region; }
        }

        public override Vector2 Size
        {
            get { return Mesh != null ? Mesh.Size : Body != null ? Body.Size : Vector2.Zero; }
            set { }
        }

        public Thing()
        { 
            IsChildLinksEnabled = false;
            LinkType = PortalLinkType.Thing;
        }

        public override void Update(float time)
        {
            if (Body!=null)
                
            base.Update(time);
        }

        protected override void Teleport(Portal portal)
        {
            Yaw = portal.TransformYawTo(Yaw);
            Pitch = portal.TransformPitchTo(Pitch);
            Roll = portal.TransformRollTo(Roll);
        }
    }
}
