﻿using System;
namespace WarpedAbominationEngine
{
    public interface IPausable
    {
        bool IsPaused { get; }
        void Pause();
        void Resume();
    }
}
