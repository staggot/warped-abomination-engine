﻿using System;
namespace WarpedAbominationEngine
{
    public interface ISceneActor
    {
        bool IsAlive { get; }
    }
}
