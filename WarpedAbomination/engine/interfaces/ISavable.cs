﻿using System;
using System.IO;

namespace WarpedAbominationEngine
{
    public interface IWarpSaveState
    {
        void Save(WarpSaveStream stream);
        void Load(WarpLoadStream stream);
    }
}
