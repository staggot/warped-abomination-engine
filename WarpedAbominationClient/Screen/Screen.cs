﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace WarpedAbominationEngine
{
    public abstract class Screen
    {
        protected int screenWidth, screenHeight;

        ~Screen()
        {
            Dispose();
        }

        public Vector2 ScreenSize
        {
            get { return new Vector2(screenWidth, screenHeight); }    
        }

        public virtual void SetScreenSize(int width, int height)
        {
            this.screenWidth = width;
            this.screenHeight = height;
        }

        public virtual void Dispose()
        {
        }

        public abstract void Render();
        public abstract void Draw(RenderTarget2D renderTarget);

        public virtual void Update(float time)
        {
            
        }

        public abstract void ApplyControls(float time);
    }
}
