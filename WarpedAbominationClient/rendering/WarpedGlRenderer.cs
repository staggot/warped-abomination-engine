﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using WarpedAbominationEngine.Content;


namespace WarpedAbominationEngine.Graphics
{
    public class SceneGLRenderer : IDisposable
    {
        static Matrix DefaultWorld = Matrix.CreateWorld(Vector3.Zero, Vector3.Forward, Vector3.Up);

        public readonly Scene Scene;
        public readonly RenderCamera Camera;
        public const string SceneShaderName = "main";
        

        readonly GraphicsDevice Device;
        readonly SceneGraphicsResources GraphicsResources;
       
        RenderTarget2D colorRenderTarget;
        readonly SceneShader Shader;
        readonly SpriteQueue spriteQueue;

        public int ScreenWidth { get; private set; }
        public int ScreenHeight { get; private set; }


        public int Tick { get; private set; }

        public bool IsAlive {
            get { return true; }
        }

        private Cron Cron => Scene.Cron;


        public SceneGLRenderer(GraphicsDevice device, Scene scene)
        {
            Scene = scene;
            Device = device;
            GraphicsResources = new SceneGraphicsResources(scene, this);
            spriteQueue = new SpriteQueue();
            Shader = new SceneShader(AbominationContent.Shaders, SceneShaderName);
        }

        public void SetResolution(int width, int height)
        {
            if (ScreenWidth == width && ScreenHeight == height)
                return;
            ScreenWidth = width;
            ScreenHeight = height;

            Camera.AspectRatio = (float)width / (float)height;

            RenderTarget2D oldColorRenderTarget = colorRenderTarget;
            RenderTarget2D newColorRenderTarget = new RenderTarget2D(
                Device,
                ScreenWidth,
                ScreenHeight,
                false,
                SurfaceFormat.Color,
                DepthFormat.Depth24Stencil8,
                0,
                RenderTargetUsage.PreserveContents
            );

            this.colorRenderTarget = newColorRenderTarget;
            if (oldColorRenderTarget != null)
                oldColorRenderTarget.Dispose();
        }


        void DisposeRenderTarget()
        {
            if (colorRenderTarget != null)
                colorRenderTarget.Dispose();
        }

        void PrepareChunk(WarpBranch chunk)
        {
            Shader.WorldMatrix = DefaultWorld;
            while (chunk.Stencil != 0) {
                Shader.ApplyWarp(chunk.BranchCamera);
                WarpVertex[] warpVerices = GraphicsResources[chunk.EntrancePortal];
                if (warpVerices != null)
                    Device.DrawUserPrimitives<WarpVertex>(
                                PrimitiveType.TriangleList,
                                warpVerices,
                                SectorGraphics.WarpStencilIndex,
                                SectorGraphics.WarpStencilMeshSize / 3);
                chunk = chunk.parentBranch;
            }
        }

        void EndChunk(WarpBranch chunk)
        {
            Shader.WorldMatrix = DefaultWorld;
            if (chunk.Stencil == 0)
                return;
            Shader.ApplyWarpPortalOverlay(chunk.BranchCamera, chunk.Stencil);
            WarpVertex[] warpVerices = GraphicsResources[chunk.EntrancePortal];
            if (warpVerices != null)
                Device.DrawUserPrimitives<WarpVertex>(
                            PrimitiveType.TriangleList,
                            warpVerices,
                            SectorGraphics.WarpOverlayIndex,
                            SectorGraphics.WarpStencilMeshSize / 3);
            Device.Clear(ClearOptions.Stencil, Color.White, 0, 0);
        }

        public void Render()
        {
            
            Camera.Update();
            Shader.Begin(Camera);

            Device.SetRenderTarget(colorRenderTarget);
            Device.Clear(ClearOptions.Target | ClearOptions.Stencil | ClearOptions.DepthBuffer, Vector4.Zero, 1, 0);

            foreach (WarpBranch branch in Camera.PVS.ChunkBackToFront) {
                if (branch.Sectors <= 0)
                    break;
                PrepareChunk(branch);

                foreach (SectorView sectorView in branch.SectorBackToFront) {
                    SectorGraphics sg = GraphicsResources[sectorView.Sector.FullID];
                    if (sg == null)
                        continue;
                    sg.Tick();
                    
                    if (sg.TerrainVertexBuffer.Length > 0) {
                        Shader.ApplyTerrain(branch.Stencil);
                        XnaSystem.GraphicsDevice.DrawUserPrimitives<TerrainVertex>(
                            PrimitiveType.TriangleList,
                            sg.TerrainVertexBuffer,
                            0,
                            sg.TerrainVertexBuffer.Length / 3
                        );
                    }

                    if (sg.PortalVertexBuffer.Length > 0) {
                        XnaSystem.GraphicsDevice.DrawUserPrimitives<PortalVertex>(
                            PrimitiveType.TriangleList,
                            sg.PortalVertexBuffer,
                            0,
                            sg.PortalVertexBuffer.Length / 3
                        );
                    }

                    PortalLink link = sectorView.Sector.Portals[PortalLinkType.Mesh];
                    while (link != null) {
                        //if (link.Thing.Mesh != null)
                        //link.Thing.Mesh.Draw(this, pvSec, link);
                        link = link.Next;
                    }
                    
                    spriteQueue.MakeSpriteVertices();
                    if (spriteQueue.SpriteVerticesCount > 0) {
                        Shader.ApplySprite(branch.Stencil);
                        XnaSystem.GraphicsDevice.DrawUserPrimitives<SpriteVertex>(
                            PrimitiveType.TriangleList,
                            spriteQueue.SpriteVertices,
                            0,
                            spriteQueue.SpriteVerticesCount / 3
                        );
                    }
                }
                EndChunk(branch);
            }

            Camera.PVS.Clear();
            Tick += 1;
        }

        public void Draw(RenderTarget2D renderTarget)
        {
            Point2 targetSize;
            XnaSystem.GraphicsDevice.SetRenderTarget(renderTarget);
            if (renderTarget == null)
                targetSize = new Point2(XnaSystem.GraphicsManager.PreferredBackBufferWidth, XnaSystem.GraphicsManager.PreferredBackBufferHeight);
            else
                targetSize = new Point2(renderTarget.Width, renderTarget.Height);
            XnaSystem.SpriteBatch.Begin();
            XnaSystem.SpriteBatch.Draw(this.colorRenderTarget, new Rectangle(0, 0, targetSize.X, targetSize.Y), Color.White);
            XnaSystem.SpriteBatch.End();
        }

        public void Dispose()
        {
            GraphicsResources.Dispose();
            DisposeRenderTarget();
        }
    }
}
