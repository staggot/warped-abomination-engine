﻿using System;
namespace WarpedAbominationEngine
{
    public class GraphicsResourcesException : Exception
    {
        public GraphicsResourcesException(string message) : base(
            string.Format("Graphics Resources: {0}", message))
        {
        }
    }

}
