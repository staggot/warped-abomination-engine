﻿using System;
using Microsoft.Xna.Framework;

namespace WarpedAbominationEngine
{
    public class RenderCamera : SceneCamera
    {
        public Matrix ProjectionMatrix { get; protected set; }
        public Matrix ViewMatrix { get; protected set; }

        public RenderCamera(Scene scene):base(scene)
        {

        }
    }
}
