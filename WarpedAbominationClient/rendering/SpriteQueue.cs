﻿using System;
using Microsoft.Xna.Framework;
using WarpedAbominationEngine.Content;

namespace WarpedAbominationEngine.Graphics
{
    public class SpriteQueue
    {
        const int SpriteBufferSize = 4096;
        const int SpriteVertexBufferSize = 4096 * 6;

        readonly Sprite[] SpritebufferA;
        readonly Sprite[] SpritebufferB;

        Sprite[] currentSpriteBuffer;

        private int spriteIndex = 0;

        internal int SpriteVerticesCount;
        internal readonly SpriteVertex[] SpriteVertices;

        public SpriteQueue()
        {
            SpritebufferA = new Sprite[SpriteBufferSize];
            SpritebufferB = new Sprite[SpriteBufferSize];
            SpriteVertices = new SpriteVertex[SpriteVertexBufferSize];
        }

        public bool StartSprite(Vector3 position, Vector2 scale, SectorView view, WarpCamera camera)
        {
            float distance = MathHelper.PlaneDistance(camera.CameraDirection, camera.CameraPosition, position + view.WorldPosition);
            if (distance < -16f)
                return false;
            spriteIndex++;
            SpritebufferA[spriteIndex].Begin(
                position,
                scale,
                camera.CameraSpriteFacing,
                distance
            );
            return true;
        }

        public void AppendSprite(MegatextureFragment texture, Vector2 delta, Vector2 size, Vector4 color)
        {
            SpritebufferA[spriteIndex].AddPart(
                texture,
                delta,
                size,
                color
            );
        }

        public void EndSprite()
        {
            spriteIndex++;
        }

        internal void SortSprites()
        {
            currentSpriteBuffer = MergeSort(SpritebufferA, SpritebufferB, spriteIndex);
        }

        internal void MakeSpriteVertices()
        {
            int spriteVerticesIndex = 0;
            for (int i = 0; i < spriteIndex; i++)
                spriteVerticesIndex += currentSpriteBuffer[i].MakeFacingVertices(SpriteVertices, spriteVerticesIndex);
            SpriteVerticesCount = spriteVerticesIndex;
        }

        static void Merge(Sprite[] a, Sprite[] tmp, int left, int middle, int right)
        {
            int i, j, k;

            i = left;
            j = middle;
            k = right;

            while (i < middle || j < right) {
                if (i < middle && j < right) {
                    if (a[i].Distance > a[j].Distance)
                        tmp[k++] = a[i++];
                    else
                        tmp[k++] = a[j++];
                } else if (i == middle)
                    tmp[k++] = a[j++];
                else if (j == right)
                    tmp[k++] = a[i++];
            }
        }


        static Sprite[] MergeSort(Sprite[] spriteBufferA, Sprite[] spriteBufferB, int length)
        {
            int width;
            bool swap = false;
            Sprite[] result = null;
            for (width = 1; width < length; width = 2 * width) {
                int i;
                for (i = 0; i < length; i = i + 2 * width) {
                    int left, middle, right;

                    left = i;
                    middle = Math.Min(i + width, length);
                    right = Math.Min(i + 2 * width, length);

                    Sprite[] a, tmp;
                    if (swap) {
                        a = spriteBufferB;
                        tmp = spriteBufferA;
                    } else {
                        a = spriteBufferA;
                        tmp = spriteBufferB;
                    }
                    Merge(a, tmp, left, middle, right);
                    result = tmp;
                }
                swap = !swap;
            }
            return result;
        }
    }
}
