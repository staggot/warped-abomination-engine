﻿using System;
using System.Collections.Generic;

namespace WarpedAbominationEngine.Graphics
{
    public class SceneGraphicsResources
    {
        public const string LoadSectorGraphicsJobName = "graphics_processor";

        public readonly Scene Scene;
        public readonly SceneGLRenderer Renderer;
        readonly Dictionary<SectorID, SectorGraphics> graphics = new Dictionary<SectorID, SectorGraphics>();
        readonly object sectorProcessorLock = new object();
        volatile bool isDisposing = false;

        public SceneGraphicsResources(Scene scene, SceneGLRenderer renderer)
        {
            Scene = scene;
            Renderer = renderer;
            Scene.OnPortalPlaced += PortalChanged;
            Scene.OnPortalRemoved += PortalChanged;
        }

        Cron Cron => Scene.Cron;

        public SectorGraphics this[SectorID id] {
            get {
                if (graphics.ContainsKey(id))
                    return graphics[id];
                return null;
            }
        }

        public SectorGraphics RequestSector(SectorID id)
        {
            if (graphics.ContainsKey(id))
                return graphics[id];
            return null;
        }

        public WarpVertex[] this[Portal portal] {
            get {

                return this[portal.Sector.FullID]?.PortalStencils[portal];
            }
        }

        void PortalChanged(Portal portal)
        {
            RequestReloadSector(portal.Sector.FullID);
        }

        internal CronJobInstance RequestLoadSector(SectorID id)
        {
            if (isDisposing)
                return null;
            SectorGraphics sectorGraphics = null;
            if (graphics.ContainsKey(id)) {

                sectorGraphics = graphics[id];
                if (sectorGraphics.State != SectorGraphicsState.Unloaded)
                    return null;
            } else {
                sectorGraphics = new SectorGraphics(Renderer, Scene[id]);
                graphics[id] = sectorGraphics;
            }
            sectorGraphics.State = SectorGraphicsState.RequestedLoad;
            return Cron.Enqueue(LoadSectorGraphicsJobName, new Action<SectorGraphics>(SectorProcessor), sectorGraphics);
        }

        internal CronJobInstance RequestReloadSector(SectorID id)
        {
            if (isDisposing)
                return null;
            SectorGraphics sectorGraphics = null;
            if (graphics.ContainsKey(id)) {

                sectorGraphics = graphics[id];
                if (!sectorGraphics.State.IsCanReload())
                    return null;
            } else
                return null;
            sectorGraphics.State = SectorGraphicsState.RequestedReload;
            return Cron.Enqueue(LoadSectorGraphicsJobName, new Action<SectorGraphics>(SectorProcessor), sectorGraphics);
        }

        internal CronJobInstance RequestUnloadSector(SectorID id)
        {
            if (isDisposing)
                return null;
            SectorGraphics sectorGraphics = null;
            if (graphics.ContainsKey(id)) {

                sectorGraphics = graphics[id];
                if (!sectorGraphics.State.IsCanReload())
                    return null;
            } else
                return null;
            sectorGraphics.State = SectorGraphicsState.RequestedReload;
            return Cron.Enqueue(LoadSectorGraphicsJobName, new Action<SectorGraphics>(SectorProcessor), sectorGraphics);
        }

        CronJobInstance EnqueueSectorGraphicsJob(SectorGraphics sectorGraphics)
        {
            return Cron.Enqueue(LoadSectorGraphicsJobName, new Action<SectorGraphics>(SectorProcessor), sectorGraphics);
        }

        void SectorProcessor(SectorGraphics sectorGraphics)
        {
            lock (sectorProcessorLock) {
                if (isDisposing)
                    return;
                switch (sectorGraphics.State) {
                    case SectorGraphicsState.Unloaded:
                    case SectorGraphicsState.Loaded:
                    case SectorGraphicsState.Reloading:
                    case SectorGraphicsState.Loading:
                        break;
                    case SectorGraphicsState.RequestedLoad:
                        sectorGraphics.State = SectorGraphicsState.Loading;
                        Scene.Logger.Info("Loading sector: " + sectorGraphics.Sector.FullID);
                        sectorGraphics.RecreateGraphics();
                        if (sectorGraphics.State == SectorGraphicsState.Loading) {
                            sectorGraphics.State = SectorGraphicsState.Loaded;
                            Scene.Logger.Info("Loaded sector: " + sectorGraphics.Sector.FullID);
                            Scene.Bridge.RequestConnectedRegionsLoad(sectorGraphics.Sector);
                        } else {
                            Scene.Logger.Warning(string.Format(
                                "Loaded sector: {0}, changing plans: {1}",
                                sectorGraphics.Sector.FullID,
                                sectorGraphics.State));
                            EnqueueSectorGraphicsJob(sectorGraphics);
                        }
                        break;
                    case SectorGraphicsState.RequestedReload:
                        sectorGraphics.State = SectorGraphicsState.Reloading;
                        Scene.Logger.Info("Reloading sector: " + sectorGraphics.Sector.FullID);
                        sectorGraphics.RecreateGraphics();
                        if (sectorGraphics.State == SectorGraphicsState.Reloading) {
                            sectorGraphics.State = SectorGraphicsState.Loaded;
                            Scene.Logger.Info("Reloaded sector: " + sectorGraphics.Sector.FullID);
                        } else {
                            Scene.Logger.Warning(string.Format(
                                "Reloaded sector: {0}, changing plans: {1}",
                                sectorGraphics.Sector.FullID,
                                sectorGraphics.State));
                            EnqueueSectorGraphicsJob(sectorGraphics);
                        }
                        break;
                    case SectorGraphicsState.RequestedUnload:
                        sectorGraphics.State = SectorGraphicsState.Unloading;
                        Scene.Logger.Info(string.Format(
                                "Unloading sector: {0}",
                                sectorGraphics.Sector.FullID));
                        sectorGraphics.Dispose();
                        graphics.Remove(sectorGraphics.Sector.FullID);
                        break;
                }
            }
        }

        public void Dispose()
        {
            lock (sectorProcessorLock) {
                isDisposing = true;
                Scene.OnPortalPlaced -= PortalChanged;
                Scene.OnPortalRemoved -= PortalChanged;
                foreach (KeyValuePair<SectorID, SectorGraphics> kv in graphics)
                    kv.Value.Dispose();
                graphics.Clear();
            }
        }
    }
}
