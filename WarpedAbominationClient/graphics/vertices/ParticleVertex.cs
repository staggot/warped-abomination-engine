﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace WarpedAbominationEngine.Graphics
{
    public struct ParticleVertex : IVertexType
    {
        Vector4 vertexPosition;
        Vector2 vertexParameter;
        Vector4 vertexColor;

        public readonly static VertexDeclaration VertexDeclaration = new VertexDeclaration
            (
            new VertexElement(0, VertexElementFormat.Vector3, VertexElementUsage.Position, 0),
            new VertexElement(16, VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 0),
            new VertexElement(24, VertexElementFormat.Vector4, VertexElementUsage.Color, 0)
            );

        VertexDeclaration IVertexType.VertexDeclaration
        {
            get { return VertexDeclaration; }
        }

        public ParticleVertex(Vector3 position, float ignoreLight, Vector3 color)
        {
            vertexPosition = new Vector4(position, 0);
            vertexParameter = new Vector2(ignoreLight, 0);
            vertexColor = new Vector4(color, 1);

        }

        public Vector4 Position
        {
            get { return vertexPosition; }
            set { vertexPosition = value; }
        }

        public Vector2 Parameter
        {
            get { return vertexParameter; }
            set { vertexParameter = value; }
        }

        public Vector4 Color
        {
            get { return vertexColor; }
            set { vertexColor = value; }
        }
    }
}
