﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace WarpedAbominationEngine.Graphics
{
    public struct TerrainVertex : IVertexType
    {
        Vector4 vertexPosition;
        Vector4 vertexColor;
        Vector2 vertexTexture;
        Vector3 vertexNormal;

        public readonly static VertexDeclaration VertexDeclaration = new VertexDeclaration
            (
                new VertexElement(0, VertexElementFormat.Vector4, VertexElementUsage.Position, 0),
                new VertexElement(16, VertexElementFormat.Vector4, VertexElementUsage.Color, 0),
                new VertexElement(32, VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 0),
                new VertexElement(40, VertexElementFormat.Vector3, VertexElementUsage.Normal, 0)
            );

        VertexDeclaration IVertexType.VertexDeclaration
        {
            get { return VertexDeclaration; }
        }

        public TerrainVertex(Vector3 position, Vector4 color, Vector2 texCoord, Vector3 normal)
        {
            vertexPosition = new Vector4(position, 1);
            vertexTexture = texCoord;
            vertexColor = color;
            vertexNormal = normal;
        }

        public Vector3 Position
        {
            get { return new Vector3(vertexPosition.X, vertexPosition.Y, vertexPosition.Z); }
            set { vertexPosition.X = value.X; vertexPosition.Y = value.Y; vertexPosition.Z = value.Z;}
        }

        public Vector4 Color
        {
            get { return vertexColor; }
            set { vertexColor = value; }
        }

        public Vector2 Texture
        {
            get { return vertexTexture; }
            set { vertexTexture = value; }
        }

        public Vector3 Normal
        {
            get { return vertexNormal; }
            set { vertexNormal = value; }
        }
    }
}
