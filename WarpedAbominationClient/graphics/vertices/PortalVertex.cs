﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using WarpedAbominationEngine.Content;

namespace WarpedAbominationEngine.Graphics
{
    public struct PortalVertex : IVertexType
    {
        Vector4 vertexPosition;
        Vector4 vertexColor;
        Vector2 vertexTexture;
        Vector2 vertexTextureTL;
        Vector2 vertexTextureTR;
        Vector2 vertexTextureBL;
        Vector2 vertexTextureBR;
        Vector2 vertexWobble;
        Vector3 vertexNormal;


        public readonly static VertexDeclaration VertexDeclaration = new VertexDeclaration
            (
                new VertexElement(0, VertexElementFormat.Vector4, VertexElementUsage.Position, 0),
                new VertexElement(16, VertexElementFormat.Vector4, VertexElementUsage.Color, 0),
                new VertexElement(32, VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 0),
                new VertexElement(40, VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 1),
                new VertexElement(48, VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 2),
                new VertexElement(56, VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 3),
                new VertexElement(64, VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 4),
                new VertexElement(72, VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 5),
                new VertexElement(80, VertexElementFormat.Vector3, VertexElementUsage.Normal, 0)
            );

        VertexDeclaration IVertexType.VertexDeclaration
        {
            get { return VertexDeclaration; }
        }

        public PortalVertex(Vector3 position, Vector4 color, Vector2 texCoord, MegatextureFragment texture, Vector2 wobble,Vector3 normal)
        {
            vertexPosition = new Vector4(position, 0);
            vertexTexture = texCoord;
            vertexColor = color;
            vertexTextureTL = texture.LT;
            vertexTextureTR = texture.RT;
            vertexTextureBL = texture.LB;
            vertexTextureBR = texture.RB;
            vertexWobble = wobble;
            vertexNormal = normal;
        }

        public Vector3 Position
        {
            get { return new Vector3(vertexPosition.X, vertexPosition.Y, vertexPosition.Z); }
            set { vertexPosition.X = value.X; vertexPosition.Y = value.Y; vertexPosition.Z = value.Z; }
        }

        public Vector4 Color
        {
            get { return vertexColor; }
            set { vertexColor = value; }
        }

        public Vector3 Normal
        {
            get { return vertexNormal; }
            set { vertexNormal = value; }
        }
    }
}
