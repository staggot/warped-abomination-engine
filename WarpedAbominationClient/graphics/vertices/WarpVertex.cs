﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace WarpedAbominationEngine.Graphics
{
    public struct WarpVertex : IVertexType
    {
        Vector4 vertexPosition;

        public readonly static VertexDeclaration VertexDeclaration = new VertexDeclaration
            (
                new VertexElement(0, VertexElementFormat.Vector4, VertexElementUsage.Position, 0)
            );

        VertexDeclaration IVertexType.VertexDeclaration
        {
            get { return VertexDeclaration; }
        }

        public WarpVertex(Vector3 position)
        {
            this.vertexPosition = new Vector4(position, 1);
        }

        public Vector4 Position
        {
            get { return vertexPosition; }
            set { vertexPosition = value; }
        }
    }
}
