﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using WarpedAbominationEngine.Content;

namespace WarpedAbominationEngine.Graphics
{
    public struct Sprite
    {
        public const int MaxSpriteParts = 16;

        public int Count;
        public float Distance;

        public Vector3 Position;
        public Vector2 Scale;
        public Vector3 Normal;
        public readonly Vector2[] Deltas;
        public readonly MegatextureFragment[] SpriteTextures;
        public readonly Vector2[] SpriteSizes;
        public readonly Vector4[] Colors;

        public Sprite(int maxSpriteParts = MaxSpriteParts)
        {
            Count = 0;
            Distance = 0;
            Scale = Vector2.One;
            Normal = Vector3.Zero;
            Position = Vector3.Zero;
            Deltas = new Vector2[maxSpriteParts];
            SpriteTextures = new MegatextureFragment[maxSpriteParts];
            SpriteSizes = new Vector2[maxSpriteParts];
            Colors = new Vector4[maxSpriteParts];
        }

        public void Begin(Vector3 position, Vector2 scale, Vector3 facingDirection, float distance)
        {
            this.Position = position;
            this.Distance = distance;
            this.Scale = scale;
            this.Normal = facingDirection;
        }

        public void AddPart(MegatextureFragment texture, Vector2 positionDelta, Vector2 size, Vector4 color)
        {
            if (Count >= MaxSpriteParts)
                return;
            SpriteTextures[Count] = texture;
            Deltas[Count] = positionDelta;
            SpriteSizes[Count] = size;
            Colors[Count] = color;
            Count++;
        }

        public int MakeFacingVertices(SpriteVertex[] vertexBuffer, int index)
        {
            int vertices = 0;
            for (int i = 0; i < Count; i++)
            {
                if (vertexBuffer.Length - vertices - index < 6)
                    return vertices;
                Vector2 size = Scale * SpriteSizes[i];
                Vector3 position = Position + new Vector3(
                    Deltas[i].X * Scale.X * Normal.Z,
                    Deltas[i].Y * Scale.Y,
                    Deltas[i].X * Scale.X * Normal.X);
                Vector3 bottomLeft = new Vector3(position.X - size.X / 2 * Normal.Z, position.Y - size.Y / 2, position.Z + size.X / 2 * Normal.X);
                Vector3 topRight = new Vector3(position.X + size.X / 2 * Normal.Z, position.Y + size.Y / 2, position.Z - size.X / 2 * Normal.X);
                Vector3 topLeft = new Vector3(position.X - size.X / 2 * Normal.Z, position.Y + size.Y / 2, position.Z + size.X / 2 * Normal.X);
                Vector3 bottomRight = new Vector3(position.X + size.X / 2 * Normal.Z, position.Y - size.Y / 2, position.Z - size.X / 2 * Normal.X);

                vertexBuffer[index + 0] = new SpriteVertex(
                    bottomLeft,
                    Colors[i],
                    SpriteTextures[i].LB,
                    Normal
                );
                vertexBuffer[index + 1] = new SpriteVertex(
                    topLeft,
                    Colors[i],
                    SpriteTextures[i].LT,
                    Normal
                );
                vertexBuffer[index + 2] = new SpriteVertex(
                    topRight,
                    Colors[i],
                    SpriteTextures[i].RT,
                    Normal
                );
                vertexBuffer[index + 3] = new SpriteVertex(
                    bottomRight,
                    Colors[i],
                    SpriteTextures[i].RB,
                    Normal
                );
                vertexBuffer[index + 4] = new SpriteVertex(
                    bottomLeft,
                    Colors[i],
                    SpriteTextures[i].LB,
                    Normal
                );
                vertexBuffer[index + 5] = new SpriteVertex(
                    topRight,
                    Colors[i],
                    SpriteTextures[i].RT,
                    Normal
                );
                vertices += 6;
            }
            return vertices;
        }
    }
}
