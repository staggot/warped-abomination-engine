﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using WarpedAbominationEngine.Content;

namespace WarpedAbominationEngine.Graphics
{
    public class SectorMesh : MarchingCubes
    {
        protected Sector sector;

        public override Point3 Size
        {
            get { return sector.Size; }
        }


        internal List<TerrainVertex> triangleList;
        internal List<PortalVertex> portalVertexList;

        void GeneratePortalMesh()
        {
            portalVertexList = new List<PortalVertex>();
            foreach(Portal portal in sector.Portals) {
                if (portal.Textured == PortalTextureType.None)
                    continue;
                Vector2 tlTex, trTex, blTex, brTex;
                MegatextureFragment tex = AbominationContent.Textures[(int)EngineTextureTarget.Portal].GetEdges(portal.TextureData);

                portal.GetVertexPositions(out Vector3 tlPos, out Vector3 trPos, out Vector3 brPos, out Vector3 blPos, out Vector3 normal);
                tlPos += normal * AbominationSystem.GraphicsEpsilon;
                trPos += normal * AbominationSystem.GraphicsEpsilon;
                blPos += normal * AbominationSystem.GraphicsEpsilon;
                brPos += normal * AbominationSystem.GraphicsEpsilon;

                tlTex = new Vector2(0, 0);
                trTex = new Vector2(0, 1);
                blTex = new Vector2(1, 0);
                brTex = new Vector2(1, 1);
                switch (portal.Textured)
                {
                    case PortalTextureType.TileHorisontal:
                        trTex.X = portal.Size.X;
                        brTex.X = portal.Size.X;
                        break;
                    case PortalTextureType.TileVertival:
                        brTex.Y = portal.Size.Y;
                        blTex.Y = portal.Size.Y;
                        break;
                    case PortalTextureType.Tile:
                        trTex.X = portal.Size.X;
                        brTex.X = portal.Size.X;
                        brTex.Y = portal.Size.Y;
                        blTex.Y = portal.Size.Y;
                        break;
                }

                PortalVertex bl = new PortalVertex(
                    blPos,
                    sector.GetLightVector(blPos),
                    blTex,
                    tex,
                    Vector2.Zero,
                    normal
                );
                PortalVertex tl = new PortalVertex(
                    tlPos,
                    sector.GetLightVector(blPos),
                    tlTex,
                    tex,
                    Vector2.Zero,
                    normal
                );
                PortalVertex tr = new PortalVertex(
                    trPos,
                    sector.GetLightVector(blPos),
                    trTex,
                    tex,
                    Vector2.Zero,
                    normal
                );
                PortalVertex br = new PortalVertex(
                    brPos,
                    sector.GetLightVector(blPos),
                    brTex,
                    tex,
                    Vector2.Zero,
                    normal
                );
                portalVertexList.Add(bl);
                portalVertexList.Add(tl);
                portalVertexList.Add(tr);
                portalVertexList.Add(br);
                portalVertexList.Add(bl);
                portalVertexList.Add(tr);
            }
        }

        public SectorMesh(Sector sector)
        {
            this.sector = sector;
            triangleList = new List<TerrainVertex>();
            March();

            GeneratePortalMesh();
        }

        protected override void SampleNode(Point3 position, out NodeTexture node, out NodeShape gNode)
        {
            int index = position.X + position.Z * (sector.Size.X) + position.Y * (sector.Size.X) * (sector.Size.Z);
            sector.Terrain.GetNode(position, out gNode, out node);
        }


        protected override void AddTriangle(Vector3 a, Vector3 b, Vector3 c, Vector3 normal, Point3 nodePosition, NodeTexture node, byte edgeA, byte edgeB, byte edgeC)
        {

            Vector3 posVector = nodePosition.Vector;

            TriangleTextureCoords(a, b, c, normal, posVector, out TriplanarTextureMappingAxis axis, out Vector2 aCoords, out Vector2 bCoords, out Vector2 cCoords);

            switch (axis)
            {
                case TriplanarTextureMappingAxis.X:
                    aCoords = (edgeA == 255 ? node.XBlock : node.XTerrain).GetCoord(aCoords);
                    bCoords = (edgeB == 255 ? node.XBlock : node.XTerrain).GetCoord(bCoords);
                    cCoords = (edgeC == 255 ? node.XBlock : node.XTerrain).GetCoord(cCoords);
                    break;
                case TriplanarTextureMappingAxis.Z:
                    aCoords = (edgeA == 255 ? node.ZBlock : node.ZTerrain).GetCoord(aCoords);
                    bCoords = (edgeB == 255 ? node.ZBlock : node.ZTerrain).GetCoord(bCoords);
                    cCoords = (edgeC == 255 ? node.ZBlock : node.ZTerrain).GetCoord(cCoords);
                    break;
                case TriplanarTextureMappingAxis.Y:
                    aCoords = (edgeA == 255 ? node.YBlock : node.YTerrain).GetCoord(aCoords);
                    bCoords = (edgeB == 255 ? node.YBlock : node.YTerrain).GetCoord(bCoords);
                    cCoords = (edgeC == 255 ? node.YBlock : node.YTerrain).GetCoord(cCoords);
                    break;
            }

            Vector4 light = new Vector4(1, 1, 1, 1);

            triangleList.Add(new TerrainVertex(
                a, light, aCoords, normal
            ));
            triangleList.Add(new TerrainVertex(
                b, light, bCoords, normal
            ));
            triangleList.Add(new TerrainVertex(
                c, light, cCoords, normal
            ));
        }
    }


}
