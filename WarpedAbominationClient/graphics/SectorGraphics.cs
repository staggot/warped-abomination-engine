﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using WarpedAbominationEngine.Content;

namespace WarpedAbominationEngine.Graphics
{
    public class SectorGraphics : IDisposable
    {
        readonly Sector sector;
        readonly SceneGLRenderer renderer;
        public TerrainVertex[] TerrainVertexBuffer;
        public PortalVertex[] PortalVertexBuffer;

        public const int WarpStencilIndex = 0;
        public const int WarpOverlayIndex = 6;
        public const int WarpStencilMeshSize = 6;

        public volatile SectorGraphicsState State;

        public readonly Dictionary<Portal, WarpVertex[]> PortalStencils = new Dictionary<Portal, WarpVertex[]>();

        long tick = 0;

        public Sector Sector { get { return sector; } }

        internal SectorGraphics(SceneGLRenderer renderer, Sector sector)
        {
            this.sector = sector;
            this.renderer = renderer;
            State = SectorGraphicsState.Unloaded;
        }

        void CreateGraphics()
        {
            SectorMesh mesh = new SectorMesh(sector);
            TerrainVertexBuffer = mesh.triangleList.ToArray();
            PortalVertexBuffer = mesh.portalVertexList.ToArray();
            foreach(Portal portal in sector.Portals) {
                if (!portal.Natural)
                    CreateWarpVertices(portal);
            }
        }

        void CreateWarpVertices(Portal portal)
        {
            portal.GetVertexPositions(out Vector3 tlPos, out Vector3 trPos, out Vector3 brPos, out Vector3 blPos, out Vector3 normal);
            WarpVertex[] stencilVertices = new WarpVertex[WarpStencilMeshSize * 2];
            stencilVertices[WarpStencilIndex + 5] = new WarpVertex(blPos);
            stencilVertices[WarpStencilIndex + 4] = new WarpVertex(tlPos);
            stencilVertices[WarpStencilIndex + 3] = new WarpVertex(trPos);
            stencilVertices[WarpStencilIndex + 2] = new WarpVertex(brPos);
            stencilVertices[WarpStencilIndex + 1] = new WarpVertex(blPos);
            stencilVertices[WarpStencilIndex + 0] = new WarpVertex(trPos);

            stencilVertices[WarpStencilMeshSize + 5] = new WarpVertex(blPos + normal * 0.1f);
            stencilVertices[WarpStencilMeshSize + 4] = new WarpVertex(tlPos + normal * 0.1f);
            stencilVertices[WarpStencilMeshSize + 3] = new WarpVertex(trPos + normal * 0.1f);
            stencilVertices[WarpStencilMeshSize + 2] = new WarpVertex(brPos + normal * 0.1f);
            stencilVertices[WarpStencilMeshSize + 1] = new WarpVertex(blPos + normal * 0.1f);
            stencilVertices[WarpStencilMeshSize + 0] = new WarpVertex(trPos + normal * 0.1f);
        }

        internal void RecreateGraphics()
        {
            CreateGraphics();
        }

        public void Dispose()
        {
            PortalVertexBuffer = null;
            PortalVertexBuffer = null;
        }

        internal bool IsExpired {
            get { return renderer.Tick - tick > AbominationSystem.SectorExpirationTicks; }
        }

        internal void Tick()
        {
            tick = renderer.Tick;
        }
    }
}
