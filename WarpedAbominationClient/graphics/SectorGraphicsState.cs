﻿using System;
namespace WarpedAbominationEngine.Graphics
{
    public enum SectorGraphicsState : byte
    {
        Unloaded = 0,
        RequestedLoad,
        Loading,
        Loaded,
        RequestedReload,
        Reloading,
        RequestedUnload,
        Unloading
    }

    public static class SectorStateMethods
    {
        public static bool IsLoaded(this SectorGraphicsState state)
        {
            return (
                state == SectorGraphicsState.Loaded ||
                state == SectorGraphicsState.RequestedReload ||
                state == SectorGraphicsState.Reloading
            );
        }

        public static bool IsReadyToUnload(this SectorGraphicsState state)
        {
            return (
                state != SectorGraphicsState.Unloaded &&
                state != SectorGraphicsState.RequestedUnload &&
                state != SectorGraphicsState.Unloading
            );
        }

        public static bool IsNeedLoad(this SectorGraphicsState state)
        {
            return (
                state == SectorGraphicsState.Unloaded
            );
        }

        public static bool IsCanReload(this SectorGraphicsState state)
        {
            return (
                state == SectorGraphicsState.Loaded ||
                state == SectorGraphicsState.Loading ||
                state == SectorGraphicsState.Reloading
            );
        }
    }
}
