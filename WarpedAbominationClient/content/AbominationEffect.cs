﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using WarpedAbominationEngine.Content;


namespace WarpedAbominationEngine.Graphics
{
    public class AbominationEffect : Effect, IDisposable
    {
        public const string WorldParameterName = "world";
        public const string ViewParameterName = "view";
        public const string ProjectionParameterName = "projection";
        public const string TimeParameterName = "time";
        public const string ResolutionParameterName = "resolution";

        protected EffectParameter
        worldParam, viewParam, projParam,
        timeParam, resolutionParam;

        public AbominationEffect(ShaderCollection collection, string effectName) :
            this(collection.Device, collection.LoadCode(effectName))
        {
            collection.AddLoaded(this);
        }

        AbominationEffect(GraphicsDevice device, byte[] byteCode) : base(new Effect(device, byteCode))
        {
            worldParam = Parameters[WorldParameterName];
            viewParam = Parameters[ViewParameterName];
            projParam = Parameters[ProjectionParameterName];
            timeParam = Parameters[TimeParameterName];
            resolutionParam = Parameters[ResolutionParameterName];
        }

        public Matrix ProjectionMatrix
        {
            get
            {
                if (projParam != null)
                    return projParam.GetValueMatrix();
                return Matrix.Identity;
            }
            set
            {
                if (projParam == null)
                    return;
                projParam.SetValue(value);
            }
        }

        public Matrix ViewMatrix
        {
            get
            {
                if (viewParam != null)
                    return viewParam.GetValueMatrix();
                return Matrix.Identity;
            }
            set
            {
                if (viewParam == null)
                    return;
                viewParam.SetValue(value);
            }
        }

        public Matrix WorldMatrix
        {
            get
            {
                if (worldParam != null)
                    return worldParam.GetValueMatrix();
                return Matrix.Identity;
            }
            set
            {
                if (worldParam == null)
                    return;
                worldParam.SetValue(value);
            }
        }

        public float Time
        {
            get
            {
                if (timeParam != null)
                    return timeParam.GetValueSingle();
                return float.NaN;
            }
            set
            {
                if (timeParam == null)
                    return;
                timeParam.SetValue(value);
            }
        }

        public Vector2 Resolution
        {
            get
            {
                if (resolutionParam != null)
                    return worldParam.GetValueVector2();
                return new Vector2(float.NaN);
            }
            set
            {
                if (resolutionParam == null)
                    return;
                resolutionParam.SetValue(value);
            }
        }

        ~AbominationEffect()
        {
            Dispose();
        }
    }
}
