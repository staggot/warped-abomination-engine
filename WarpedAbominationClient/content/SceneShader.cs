﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using WarpedAbominationEngine.Content;

namespace WarpedAbominationEngine.Graphics
{
    [Flags]
    enum RenderStatesMap: byte
    {
        Terrain = 1,
        Mesh = 2,
        Decal = 4,
        Particle = 8,
        Sprite = 16,
        Portal = 32
    }

    public class SceneShader : AbominationEffect
    {
        private BlendState
            TerrainBlendState,
            MeshBlendState,
            SpriteBlendState,
            DecalBlendState,
            PortalBlendState,
            ParticleBlendState;
        private readonly BlendState StencilPortalBlendState = new BlendState() {
            AlphaBlendFunction = BlendFunction.Add,
            ColorBlendFunction = BlendFunction.Add,
            AlphaDestinationBlend = Blend.One,
            ColorDestinationBlend = Blend.One,
            AlphaSourceBlend = Blend.Zero,
            ColorSourceBlend = Blend.Zero,
            ColorWriteChannels = ColorWriteChannels.None,
            ColorWriteChannels1 = ColorWriteChannels.None,
            ColorWriteChannels2 = ColorWriteChannels.None,
            ColorWriteChannels3 = ColorWriteChannels.None
        };

        private RasterizerState
            TerrainRasterizerState,
            MeshRasterizerState,
            SpriteRasterizerState,
            DecalRasterizerState,
            PortalRasterizerState,
            ParticleRasterizerState;
        private readonly RasterizerState StencilPortalRasteriserState = new RasterizerState() {
            CullMode = CullMode.None,
            DepthBias = 0,
            DepthClipEnable = false,
            FillMode = FillMode.Solid,
            MultiSampleAntiAlias = false,
            ScissorTestEnable = false,
            SlopeScaleDepthBias = 0
        };

        private readonly EffectParameter
            diffuseTextureParameter,
            normalTextureParameter,
            metaTextureParameter,
            fogStartParameter,
            fogEndParameter,
            fogColorParameter,
            cameraPositionParameter,
            cameraDirectionParameter,
            sectorSizeParameter;


        private SamplerState DiffuseSamplerState, NormalSamplerState, MetaSamplerState;

        private bool culling = true;
        private bool diffuseFilter = false, normalFilter = false, metaFilter = false;
        private byte wireframe = 0;

        public SceneShader(ShaderCollection collection, string effectName = "main") : base(collection, effectName)
        {
            diffuseTextureParameter = Parameters["DiffuseTexture"];
            normalTextureParameter =Parameters["NormalTexture"];
            metaTextureParameter = Parameters["MetaTexture"];
            fogStartParameter = Parameters["FogStart"];
            fogEndParameter = Parameters["FogEnd"];
            fogColorParameter = Parameters["FogColor"];
            cameraPositionParameter = Parameters["CameraPosition"];
            cameraDirectionParameter = Parameters["CameraDirections"];
            sectorSizeParameter = Parameters["SectorSize"];
            LoadBlendState();
            LoadSamplerState();
            LoadRasteriserState();
        }

        #region Properties
        public byte Wireframe {
            get {
                return wireframe;
            }
            set {
                if (wireframe == value)
                    return;
                wireframe = value;
                LoadRasteriserState();
            }
        }

        public bool Culling {
            get {
                return culling;
            }
            set {
                if (value == culling) return;
                culling = value;
                LoadRasteriserState();
            }
        }

        public bool TerrainWireframe {
            get {
                return ((RenderStatesMap)wireframe & RenderStatesMap.Terrain) > 0;
            }
            set {
                Wireframe =
                    (byte)((wireframe & ~(byte)RenderStatesMap.Terrain) |
                    (value ? (byte)RenderStatesMap.Terrain : 0));
            }
        }

        public bool MeshWireframe {
            get {
                return ((RenderStatesMap)wireframe & RenderStatesMap.Mesh) > 0;
            }
            set {
                Wireframe =
                    (byte)((wireframe & ~(byte)RenderStatesMap.Mesh) |
                    (value ? (byte)RenderStatesMap.Mesh : 0));
            }
        }

        public bool SpriteWireframe {
            get {
                return ((RenderStatesMap)wireframe & RenderStatesMap.Sprite) > 0;
            }
            set {
                Wireframe =
                    (byte)((wireframe & ~(byte)RenderStatesMap.Sprite) |
                    (value ? (byte)RenderStatesMap.Sprite : 0));
            }
        }

        public bool DecalWireframe {
            get {
                return ((RenderStatesMap)wireframe & RenderStatesMap.Decal) > 0;
            }
            set {
                Wireframe =
                    (byte)((wireframe & ~(byte)RenderStatesMap.Decal) |
                    (value ? (byte)RenderStatesMap.Decal : 0));
            }
        }

        public bool PortalWireframe {
            get {
                return ((RenderStatesMap)wireframe & RenderStatesMap.Portal) > 0;
            }
            set {
                Wireframe =
                    (byte)((wireframe & ~(byte)RenderStatesMap.Portal) |
                    (value ? (byte)RenderStatesMap.Portal : 0));
            }
        }

        public bool ParticleWireframe {
            get {
                return ((RenderStatesMap)wireframe & RenderStatesMap.Particle) > 0;
            }
            set {
                Wireframe =
                    (byte)((wireframe & ~(byte)RenderStatesMap.Particle) |
                    (value ? (byte)RenderStatesMap.Particle : 0));
            }
        }

        public Vector3 CameraPosition {
            get { if (cameraPositionParameter == null) return Vector3.Zero; return cameraPositionParameter.GetValueVector3(); }
            set { if (cameraPositionParameter != null) cameraPositionParameter.SetValue(value); }
        }

        public Vector3 CameraDirection {
            get { if (cameraDirectionParameter == null) return Vector3.Zero; return cameraDirectionParameter.GetValueVector3(); }
            set { if (cameraDirectionParameter != null) cameraDirectionParameter.SetValue(value); }
        }

        public Vector4 FogColor {
            get { if (fogColorParameter == null) return Vector4.Zero; return fogColorParameter.GetValueVector4(); }
            set { if (fogColorParameter != null) fogColorParameter.SetValue(value); }
        }

        public float FogStart {
            get { if (fogStartParameter == null) return 0; return fogStartParameter.GetValueSingle(); }
            set { if (fogStartParameter != null) fogStartParameter.SetValue(value); }
        }

        public float FogEnd {
            get { if (fogEndParameter == null) return 0; return fogEndParameter.GetValueSingle(); }
            set { if (fogEndParameter != null) fogEndParameter.SetValue(value); }
        }

        public Vector3 SectorSize {
            get { if (sectorSizeParameter == null) return Vector3.Zero; return sectorSizeParameter.GetValueVector3(); }
            set { if (sectorSizeParameter != null) sectorSizeParameter.SetValue(value); }
        }

        public Texture2D Diffuse {
            get { if (diffuseTextureParameter == null) return null; return diffuseTextureParameter.GetValueTexture2D(); }
            set { if (diffuseTextureParameter != null) diffuseTextureParameter.SetValue(value); }
        }

        public Texture2D Normal {
            get { if (normalTextureParameter == null) return null; return normalTextureParameter.GetValueTexture2D(); }
            set { if (normalTextureParameter != null) normalTextureParameter.SetValue(value); }
        }

        public Texture2D Meta {
            get { if (metaTextureParameter == null) return null; return metaTextureParameter.GetValueTexture2D(); }
            set { if (metaTextureParameter != null) metaTextureParameter.SetValue(value); }
        }
        #endregion

        public void Begin(RenderCamera camera)
        {
            ApplySamplerStates();
            ProjectionMatrix = camera.ProjectionMatrix;
            Time = Time;
            FogEnd = camera.FarClip;
            FogStart = camera.NearClip;
            FogColor = camera.FogColor;
        }

        void ApplySamplerStates()
        {
            GraphicsDevice.SamplerStates[0] = DiffuseSamplerState;
            GraphicsDevice.SamplerStates[1] = NormalSamplerState;
            GraphicsDevice.SamplerStates[2] = MetaSamplerState;
        }

        internal void ApplyWarp(WarpCamera camera)
        {
            GraphicsDevice.BlendState = StencilPortalBlendState;
            GraphicsDevice.RasterizerState = StencilPortalRasteriserState;
            GraphicsDevice.DepthStencilState = AbominationContent.PortalStencilDS;
            ApplyCamera(camera);
            CurrentTechnique.Passes["Warp"].Apply();
        }

        internal void ApplyWarpPortalOverlay(WarpCamera camera, int stencil)
        {
            GraphicsDevice.BlendState = StencilPortalBlendState;
            GraphicsDevice.RasterizerState = StencilPortalRasteriserState;
            GraphicsDevice.DepthStencilState = AbominationContent.PortalOverlaysDS[stencil];
            ApplyCamera(camera);
            CurrentTechnique.Passes["Warp"].Apply();
        }


        internal void ApplyCamera(WarpCamera camera)
        {
            ViewMatrix = camera.ViewMatrix;
            CameraPosition = camera.CameraPosition;
            CameraDirection = camera.CameraDirection;
        }


        internal void ApplyTerrain(int stencil)
        {
            Diffuse = AbominationContent.Textures[(int)EngineTextureTarget.Terrain].Diffuse;
            Normal = AbominationContent.Textures[(int)EngineTextureTarget.Terrain].Normal;
            Meta = AbominationContent.Textures[(int)EngineTextureTarget.Terrain].Meta;
            GraphicsDevice.BlendState = TerrainBlendState;
            GraphicsDevice.DepthStencilState = AbominationContent.OpaqueDS[stencil];
            GraphicsDevice.RasterizerState = TerrainRasterizerState;
            CurrentTechnique.Passes["Terrain"].Apply();
        }

        internal void ApplyPortal(int stencil)
        {
            Diffuse = AbominationContent.Textures[(int)EngineTextureTarget.Portal].Diffuse;
            Normal = AbominationContent.Textures[(int)EngineTextureTarget.Portal].Normal;
            Meta = AbominationContent.Textures[(int)EngineTextureTarget.Portal].Meta;
            GraphicsDevice.BlendState = PortalBlendState;
            GraphicsDevice.DepthStencilState = AbominationContent.TransparentDS[stencil];
            GraphicsDevice.RasterizerState = PortalRasterizerState;
            CurrentTechnique.Passes["Portal"].Apply();
        }

        internal void ApplySprite(int stencil)
        {
            Diffuse = AbominationContent.Textures[(int)EngineTextureTarget.Sprite].Diffuse;
            Normal = AbominationContent.Textures[(int)EngineTextureTarget.Sprite].Normal;
            Meta = AbominationContent.Textures[(int)EngineTextureTarget.Sprite].Meta;
            GraphicsDevice.BlendState = SpriteBlendState;
            GraphicsDevice.DepthStencilState = AbominationContent.TransparentDS[stencil];
            GraphicsDevice.RasterizerState = SpriteRasterizerState;
            CurrentTechnique.Passes["Sprite"].Apply();
        }

        internal void ApplyMesh(int stencil)
        {
            Diffuse = AbominationContent.Textures[(int)EngineTextureTarget.Mesh].Diffuse;
            Normal = AbominationContent.Textures[(int)EngineTextureTarget.Mesh].Normal;
            Meta = AbominationContent.Textures[(int)EngineTextureTarget.Mesh].Meta;
            GraphicsDevice.BlendState = MeshBlendState;
            GraphicsDevice.DepthStencilState = AbominationContent.OpaqueDS[stencil];
            GraphicsDevice.RasterizerState = MeshRasterizerState;
            CurrentTechnique.Passes["Mesh"].Apply();
        }

        internal void ApplyParticle(int stencil)
        {
            GraphicsDevice.BlendState = ParticleBlendState;
            GraphicsDevice.DepthStencilState = AbominationContent.OpaqueDS[stencil];
            GraphicsDevice.RasterizerState = ParticleRasterizerState;
            CurrentTechnique.Passes["Particle"].Apply();
        }

        internal void ApplyDecal(int stencil)
        {
            Diffuse = AbominationContent.Textures[(int)EngineTextureTarget.Decal].Diffuse;
            Normal = AbominationContent.Textures[(int)EngineTextureTarget.Decal].Normal;
            Meta = AbominationContent.Textures[(int)EngineTextureTarget.Decal].Meta;
            GraphicsDevice.BlendState = DecalBlendState;
            GraphicsDevice.DepthStencilState = AbominationContent.TransparentDS[stencil];
            GraphicsDevice.RasterizerState = DecalRasterizerState;
            CurrentTechnique.Passes["Decal"].Apply();
        }


        public bool DiffuseFilter {
            get {
                return diffuseFilter;
            }
            set {
                if (diffuseFilter == value)
                    return;
                diffuseFilter = value;
                LoadSamplerState();
            }
        }

        public bool NormalFilter {
            get {
                return normalFilter;
            }
            set {
                if (normalFilter == value)
                    return;
                normalFilter = value;
                LoadSamplerState();
            }
        }

        public bool MetaFilter {
            get {
                return metaFilter;
            }
            set {
                if (metaFilter == value)
                    return;
                metaFilter = value;
                LoadSamplerState();
            }
        }

        void LoadBlendState()
        {
            TerrainBlendState = BlendState.Opaque;
            MeshBlendState = BlendState.Opaque;
            ParticleBlendState = BlendState.Opaque;
            DecalBlendState = BlendState.NonPremultiplied;
            SpriteBlendState = BlendState.NonPremultiplied;
            PortalBlendState = BlendState.NonPremultiplied;
        }

        void LoadRasteriserState()
        {
            RasterizerState old;

            old = TerrainRasterizerState;
            TerrainRasterizerState = new RasterizerState() {
                CullMode = culling ? CullMode.CullCounterClockwiseFace : CullMode.None,
                DepthBias = 0,
                DepthClipEnable = true,
                FillMode = TerrainWireframe ? FillMode.WireFrame : FillMode.Solid,
                MultiSampleAntiAlias = false,
                ScissorTestEnable = false,
                SlopeScaleDepthBias = 0
            };
            if (old != null)
                old.Dispose();

            old = MeshRasterizerState;
            MeshRasterizerState = new RasterizerState() {
                CullMode = culling ? CullMode.CullCounterClockwiseFace : CullMode.None,
                DepthBias = 0,
                DepthClipEnable = true,
                FillMode = MeshWireframe ? FillMode.WireFrame : FillMode.Solid,
                MultiSampleAntiAlias = false,
                ScissorTestEnable = false,
                SlopeScaleDepthBias = 0
            };
            if (old != null)
                old.Dispose();

            old = PortalRasterizerState;
            PortalRasterizerState = new RasterizerState() {
                CullMode = culling ? CullMode.CullCounterClockwiseFace : CullMode.None,
                DepthBias = 0,
                DepthClipEnable = true,
                FillMode = PortalWireframe ? FillMode.WireFrame : FillMode.Solid,
                MultiSampleAntiAlias = false,
                ScissorTestEnable = false,
                SlopeScaleDepthBias = 0
            };
            if (old != null)
                old.Dispose();

            old = SpriteRasterizerState;
            SpriteRasterizerState = new RasterizerState() {
                CullMode = culling ? CullMode.CullCounterClockwiseFace : CullMode.None,
                DepthBias = 0,
                DepthClipEnable = true,
                FillMode = SpriteWireframe ? FillMode.WireFrame : FillMode.Solid,
                MultiSampleAntiAlias = false,
                ScissorTestEnable = false,
                SlopeScaleDepthBias = 0
            };
            if (old != null)
                old.Dispose();

            old = DecalRasterizerState;
            DecalRasterizerState = new RasterizerState() {
                CullMode = culling ? CullMode.CullCounterClockwiseFace : CullMode.None,
                DepthBias = 0,
                DepthClipEnable = true,
                FillMode = DecalWireframe ? FillMode.WireFrame : FillMode.Solid,
                MultiSampleAntiAlias = false,
                ScissorTestEnable = false,
                SlopeScaleDepthBias = 0
            };
            if (old != null)
                old.Dispose();

            old = ParticleRasterizerState;
            ParticleRasterizerState = new RasterizerState() {
                CullMode = culling ? CullMode.CullCounterClockwiseFace : CullMode.None,
                DepthBias = 0,
                DepthClipEnable = true,
                FillMode = ParticleWireframe ? FillMode.WireFrame : FillMode.Solid,
                MultiSampleAntiAlias = false,
                ScissorTestEnable = false,
                SlopeScaleDepthBias = 0
            };
            if (old != null)
                old.Dispose();
        }


        void LoadSamplerState()
        {
            SamplerState old;
            old = DiffuseSamplerState;
            DiffuseSamplerState = new SamplerState() {

                AddressU = TextureAddressMode.Clamp,
                AddressV = TextureAddressMode.Clamp,
                AddressW = TextureAddressMode.Clamp,
                ComparisonFunction = CompareFunction.Never,
                Filter = diffuseFilter ? TextureFilter.Anisotropic : TextureFilter.Point,
                FilterMode = TextureFilterMode.Default,
                MaxAnisotropy = 16,
                MaxMipLevel = MegaTexture.MaxMipmap,
                MipMapLevelOfDetailBias = 0
            };
            if (old != null)
                old.Dispose();

            old = NormalSamplerState;
            NormalSamplerState = new SamplerState() {
                AddressU = TextureAddressMode.Clamp,
                AddressV = TextureAddressMode.Clamp,
                AddressW = TextureAddressMode.Clamp,
                ComparisonFunction = CompareFunction.Never,
                Filter = normalFilter ? TextureFilter.Anisotropic : TextureFilter.Point,
                FilterMode = TextureFilterMode.Default,
                MaxAnisotropy = 16,
                MaxMipLevel = MegaTexture.MaxMipmap,
                MipMapLevelOfDetailBias = 0
            };
            if (old != null)
                old.Dispose();

            old = MetaSamplerState;
            MetaSamplerState = new SamplerState() {
                AddressU = TextureAddressMode.Clamp,
                AddressV = TextureAddressMode.Clamp,
                AddressW = TextureAddressMode.Clamp,
                ComparisonFunction = CompareFunction.Equal,
                Filter = metaFilter ? TextureFilter.Anisotropic : TextureFilter.Point,
                FilterMode = TextureFilterMode.Default,
                MaxAnisotropy = 16,
                MaxMipLevel = MegaTexture.MaxMipmap,
                MipMapLevelOfDetailBias = 0
            };
            if (old != null)
                old.Dispose();


        }

        ~SceneShader()
        {
            Dispose();
        }
    }
}
