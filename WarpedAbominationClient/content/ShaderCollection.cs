﻿using System;
using System.IO;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;

namespace WarpedAbominationEngine.Content
{
    public class ShaderCollection : IDisposable
    {
        const string compiledShaderExtention = ".fx";

        public readonly GraphicsDevice Device;
        readonly Dictionary<string, ResourceLink> shadersHash = new Dictionary<string, ResourceLink>();
        readonly HashSet<Effect> loadedShaders = new HashSet<Effect>();


        public ShaderCollection(GraphicsDevice graphicsDevice, params ResourceStorage[] storages)
        {
            Device = graphicsDevice;
            foreach (ResourceStorage storage in storages)
                foreach (string name in storage.ListResources("")) {
                    string lower = name.ToLower();
                    if (lower.EndsWith(compiledShaderExtention))
                        shadersHash[lower.Substring(0, lower.Length - compiledShaderExtention.Length)] =
                            new ResourceLink(storage, name);
                }
                
        }

        public void Dispose()
        {
            foreach(Effect shader in loadedShaders)
            {
                if (!shader.IsDisposed)
                    shader.Dispose();
                loadedShaders.Clear();
            }
        }

        internal void AddLoaded(Effect effect)
        {
            loadedShaders.Add(effect);
        }

        public byte[] LoadCode(string name)
        {
            name = name.ToLower();
            if (shadersHash.ContainsKey(name)) {
                ResourceLink link = shadersHash[name];
                using (Stream st = link.Read()) {
                    byte[] code = new byte[st.Length];
                    st.Read(code, 0, code.Length);
                    return code;
                }
            }
            return null;
        }
    }
}
